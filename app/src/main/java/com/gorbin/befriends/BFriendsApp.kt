package com.gorbin.befriends

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.gorbin.befriends.data.api.apiModule
import com.gorbin.befriends.data.repo.repositoryModule
import com.gorbin.befriends.domain.localization.localizationModule
import com.gorbin.befriends.domain.usecases.impl.useCasesModule
import com.gorbin.befriends.domain.websocket.webSocketModule
import com.gorbin.befriends.fcm.fcmModule
import com.gorbin.befriends.presentation.base.activityModule
import com.gorbin.befriends.presentation.chat.chatModule
import com.gorbin.befriends.presentation.chooseCity.chooseCityModule
import com.gorbin.befriends.presentation.chooseCountry.chooseCountryModule
import com.gorbin.befriends.presentation.chooseSpecialization.chooseSpecializationModule
import com.gorbin.befriends.presentation.cicerone.ciceroneModule
import com.gorbin.befriends.presentation.editProfile.editProfileModule
import com.gorbin.befriends.presentation.enterPassword.enterPasswordModule
import com.gorbin.befriends.presentation.executorProfile.executorProfileModule
import com.gorbin.befriends.presentation.gallery.galleryModule
import com.gorbin.befriends.presentation.myRequests.myRequestsModule
import com.gorbin.befriends.presentation.passwordRecovery.passwordRecoveryModule
import com.gorbin.befriends.presentation.personalInfo.personalInfoModule
import com.gorbin.befriends.presentation.phoneCallError.phoneCallErrorModule
import com.gorbin.befriends.presentation.priceList.priceListModule
import com.gorbin.befriends.presentation.profile.aboutModule
import com.gorbin.befriends.presentation.profile.consultModule
import com.gorbin.befriends.presentation.profile.profileModule
import com.gorbin.befriends.presentation.profile.profileSpecializationModule
import com.gorbin.befriends.presentation.requestsToMe.requestsToMeModule
import com.gorbin.befriends.presentation.review.reviewModule
import com.gorbin.befriends.presentation.search.searchModule
import com.gorbin.befriends.presentation.settings.settingsModule
import com.gorbin.befriends.presentation.signIn.signInModule
import com.gorbin.befriends.presentation.signUp.signUpModule
import com.gorbin.befriends.presentation.smsCode.smsCodeModule
import com.gorbin.befriends.presentation.splash.splashModule
import com.gorbin.befriends.presentation.welcome.welcomeModule
import com.gorbin.befriends.utils.GlobalValues
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*

class BFriendsApp : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
        RxPaparazzo.register(this)
                .withFileProviderPath("beFriends")
    }

    override fun attachBaseContext(base: Context) {
        val preferences = base.getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val langTag = preferences
                .getString(GlobalValues.LANGUAGE_KEY, Locale.getDefault().toLanguageTag())
                ?: Locale.getDefault().toLanguageTag()
        val newLocale = Locale.forLanguageTag(langTag.replace("\"", ""))
        val res = base.resources
        val config = Configuration(res.configuration)
        Locale.setDefault(newLocale)
        config.setLocale(newLocale)
        super.attachBaseContext(base.createConfigurationContext(config))
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@BFriendsApp)
            modules(
                    listOf(
                            localizationModule,
                            activityModule,
                            ciceroneModule,
                            repositoryModule,
                            apiModule,
                            useCasesModule,
                            splashModule,
                            signUpModule,
                            signInModule,
                            welcomeModule,
                            chooseCountryModule,
                            personalInfoModule,
                            chooseCityModule,
                            passwordRecoveryModule,
                            phoneCallErrorModule,
                            enterPasswordModule,
                            smsCodeModule,
                            chooseSpecializationModule,
                            profileModule,
                            myRequestsModule,
                            executorProfileModule,
                            searchModule,
                            requestsToMeModule,
                            settingsModule,
                            reviewModule,
                            webSocketModule,
                            chatModule,
                            fcmModule,
                            priceListModule,
                            aboutModule,
                            profileSpecializationModule,
                            consultModule,
                            galleryModule,
                            editProfileModule
                    )
            )
        }
    }
}