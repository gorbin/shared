package com.gorbin.befriends.data.api

import android.content.Context

abstract class AbstractService(protected var context: Context)