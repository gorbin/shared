package com.gorbin.befriends.data.api

import android.content.Context
import com.gorbin.befriends.BuildConfig
import com.gorbin.befriends.data.api.interceptors.HeaderInterceptor
import com.gorbin.befriends.data.gson.GsonManager
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.Single
import io.reactivex.SingleTransformer
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class ApiDataService(context: Context, gsonManager: GsonManager, val localRepository: IKeyValueRepository<String>) :
        AbstractService(context) {
    companion object {
        private const val TIMEOUT = 60
    }

    private val client = initClient()
    protected val authAdapter = initAdapter(BuildConfig.API_AUTH_PATH, client, gsonManager)
    protected val mobileAdapter = initAdapter(BuildConfig.API_MOBILE_PATH, client, gsonManager)

    private fun initAdapter(baseUrl: String, client: OkHttpClient, gsonManager: GsonManager): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gsonManager.gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
    }

    private fun initClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .also(this::initHeadersInterception)
                .also(this::initAuthentication)
                .also(this::initLogs)
                .followRedirects(false)
                .readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
                .build()
    }

    protected fun initAuthentication(builder: OkHttpClient.Builder) {

    }

    protected open fun initHeadersInterception(builder: OkHttpClient.Builder) {
        builder.addInterceptor(HeaderInterceptor(localRepository, context))
    }

    private fun initLogs(builder: OkHttpClient.Builder) {
        if (!BuildConfig.DEBUG) {
            return
        }

        builder.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
    }

    private fun <T> checkTokenCorrectness(): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream.onErrorResumeNext {
                if (it is HttpException) {
                    //if (it.apiError.errorKind == ErrorKind.INVALID_SESSION) {
                    if (it.message.equals("HTTP 401 Unauthorized") || it.code() == 401) {
                        return@onErrorResumeNext updateSession().flatMap { upstream }
                    }
                }

                return@onErrorResumeNext Single.error(it)
            }
        }
    }

    protected fun <T : Any> Single<T>.requestHandling(): Single<T> = this
            .compose(checkTokenCorrectness())
            .flatMap { response -> Single.just(response) }
            .compose(SingleRequestTransformer())

    abstract fun updateSession(): Single<*>
}