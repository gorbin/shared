package com.gorbin.befriends.data.api

import com.gorbin.befriends.data.gson.GsonManager
import org.koin.dsl.module


val apiModule = module {
    single { GsonManager() }
    single<IApiService> { ApiService(context = get(), gsonManager = get(), localRepository = get()) }
}