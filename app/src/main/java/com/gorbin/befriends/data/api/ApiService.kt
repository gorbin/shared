package com.gorbin.befriends.data.api

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.data.gson.GsonManager
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getMimeType
import com.punicapp.core.utils.executors.ExecutorProvider
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

class ApiService(context: Context, gsonManager: GsonManager, localRepository: IKeyValueRepository<String>) : ApiDataService(context, gsonManager, localRepository), IApiService {
    private val authService: IAuthService = authAdapter.create(IAuthService::class.java)
    private val mobileService: IMobileService = mobileAdapter.create(IMobileService::class.java)

    override fun isAuthorized(): Single<Boolean> {
        return localRepository
                .get<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java)
                .map {
                    it.isPresent
                            && it.get().tokens != null
                            && it.get().tokens?.refreshToken != null
                            && it.get().tokens?.accessToken != null
                }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun updateSession(): Single<SessionData> {
        return localRepository
                .get<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java)
                .map { it.get() }
                .flatMap { session ->
                    session.tokens?.refreshToken?.let { refreshToken ->
                        authService.refreshToken(refreshToken).map {
                            saveSession(it)
                        }
                    } ?: Single.just(session)
                }
    }

    private fun saveSession(session: SessionData): SessionData {
        return localRepository.instantSave(GlobalValues.SESSION_DATA, session)
    }

    private fun savePinData(pinData: PinData): Single<PinData> {
        return localRepository
                .save(GlobalValues.PIN_DATA, pinData)
    }

    override fun login(data: LoginData, token: String): Single<Boolean> {
        return authService.login(data).requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .map {
                    return@map saveSession(it)
                }.flatMap { sessionData ->
                    localRepository.get<String>(GlobalValues.DEVICE_UUID, String::class.java).flatMap {
                        if (it.isPresent) {
                            Single.just(it.get() to sessionData)
                        } else {
                            localRepository.save(GlobalValues.DEVICE_UUID, UUID.randomUUID().toString())
                                    .map { it to sessionData }
                        }
                    }
                }
                .flatMap {
                    val uniqueID = it.first
                    mobileService.setPushToken(it.second.tokens?.accessToken, PushTokenRequest(token, uniqueID))
                }
                .flatMap { mobileService.profile() }
                .map {
                    return@map it.isRegistered()
                }
                .compose(SingleRequestTransformer())
    }

    override fun checkOtp(data: CodePhoneData): Single<SessionData> {
        return authService.checkOtp(data)
                .flatMap {
                    saveSession(it)
                    return@flatMap Single.just(it)
                }.compose(SingleRequestTransformer())
    }

    override fun resendOtp(data: PhoneData): Single<SignUpTokens> {
        return authService.resendOtp(data)
                .flatMap {
                    saveSession(SessionData(Tokens(it.accessToken, it.refreshToken)))
                    return@flatMap Single.just(it)
                }.compose(SingleRequestTransformer())
    }

    override fun requestCall(data: PhoneData): Single<PinData> {
        return authService.requestCall(data)
                .flatMap {
                    savePinData(it)
                            .flatMap { Single.just(it) }
                }.compose(SingleRequestTransformer())
    }

    override fun requestCallCheck(data: CodePhoneData): Single<RestoreTokenData> {
        val pinData = localRepository.instantGet<PinData>(GlobalValues.PIN_DATA, PinData::class.java)
        data.pinAttempt = pinData.get().pinAttempt
        return authService.requestCallCheck(data).compose(SingleRequestTransformer())
    }

    override fun requestSetPassword(data: NewPasswordData): Single<SessionData> {
        return authService.requestSetPassword(data)
                .flatMap {
                    saveSession(it)
                    return@flatMap Single.just(it)
                }.compose(SingleRequestTransformer())
    }

    override fun register(data: RegisterRequestData): Single<SignUpTokens> {
        return authService.register(data)
                .flatMap {
                    saveSession(SessionData(Tokens(it.accessToken, it.refreshToken)))
                    return@flatMap Single.just(it)
                }.compose(SingleRequestTransformer())
    }

    override fun getLanguages(): Single<List<Language>> {
        return mobileService.getLanguages().compose(SingleRequestTransformer())
    }

    override fun getCities(): Single<List<City>> {
        return mobileService.getCities().compose(SingleRequestTransformer()).onErrorResumeNext { Single.just(emptyList()) }
    }

    override fun getCountries(nameCountry: String): Single<List<Country>> {
        return mobileService.getCountries(nameCountry).compose(SingleRequestTransformer()).onErrorResumeNext { Single.just(emptyList()) }
    }

    override fun getSpecializations(): Single<List<Specialization>> {
        return mobileService.getSpecializations().compose(SingleRequestTransformer())
    }

    override fun getSpecializationAreas(): Single<List<SpecializationArea>> {
        return mobileService.getSpecializationAreas().compose(SingleRequestTransformer())
    }

    override fun getAreaSpecializations(areaId: String): Single<List<Specialization>> {
        return mobileService.getAreaSpecializations(areaId).compose(SingleRequestTransformer())
    }

    override fun getCityByCountry(id: String, nameCity: String): Single<List<City>> {
        return mobileService.getCityByCountry(id, nameCity).compose(SingleRequestTransformer())
    }

    override fun profile(): Single<User> {
        return mobileService.profile().requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .compose(SingleRequestTransformer())
    }

    override fun getGallery(): Single<List<GalleryPhoto>> {
        return mobileService.getGallery().requestHandling().compose(SingleRequestTransformer())
    }

    override fun uploadUserPhoto(keyPhoto: String?, isAvatar: Boolean, photo: File?): Single<PhotoResponse> {
        if (photo == null) {
            return Single.error(Throwable("ERROR"))
        }
        val photoPart = MultipartBody.Part.createFormData(
                "image",
                photo.name,
                RequestBody.create(photo.getMimeType()?.let { MediaType.parse(it) }, photo)
        )
        return mobileService.uploadUserPhoto(keyPhoto, isAvatar, photoPart).requestHandling().compose(SingleRequestTransformer())
    }

    override fun registerProfile(photo: File?, personalData: PersonalDataBody): Single<UserShort> {
        return if (photo != null) {
            uploadUserPhoto(null, true, photo).flatMap {
                personalData.setPhoto(it)
                mobileService.updateUserInfo(personalData).compose(SingleRequestTransformer())
            }
        } else {
            mobileService.updateUserInfo(personalData).compose(SingleRequestTransformer())
        }
    }

    override fun getMyRequests(pageSize: Int, offset: Int): Single<List<MyRequest>> {
        return mobileService.getMyRequests(pageSize, offset).requestHandling().compose(SingleRequestTransformer())
    }

    override fun createRequest(body: RequestCreationBody): Single<MyRequest> {
        return mobileService.createRequest(body).requestHandling().compose(SingleRequestTransformer())
    }

    override fun getRequestsToMe(pageSize: Int, offset: Int): Single<List<MyRequest>> {
        return mobileService.getRequestsToMe(pageSize, offset).requestHandling().compose(SingleRequestTransformer())
    }

    override fun getProfile(userId: String): Single<User> {
        return mobileService.getProfile(userId).requestHandling().compose(SingleRequestTransformer())
    }

    override fun search(pageSize: Int, offset: Int, sort: String, sortDirection: String, searchForm: SearchForm): Single<List<List<UserExecutor>>> {
        return mobileService.search(pageSize, offset, sort, sortDirection, searchForm).compose(SingleRequestTransformer())
    }

    override fun sendReview(userUuid: String, review: ReviewBody): Single<Boolean> {
        return mobileService.sendReview(userUuid, review).requestHandling().compose(SingleRequestTransformer())
    }

    override fun getUserRating(userUuid: String): Single<UserRating> {
        return mobileService.getUserRating(userUuid).compose(SingleRequestTransformer())
    }

    override fun getReviews(userUuid: String, sortBy: String, sortOrder: String, specializations: List<String>?): Single<List<Review>> {
        return mobileService.getReviews(userUuid, sortBy, sortOrder, specializations).compose(SingleRequestTransformer())
    }

    override fun getServicePrices(userUuid: String): Single<List<ServicePrice>> {
        return mobileService.getServicePrices(userUuid).compose(SingleRequestTransformer())
    }

    override fun saveServicePrices(data: PriceList): Single<BooleanResult> {
        return mobileService.saveServicePrices(data).compose(SingleRequestTransformer())
    }

    override fun updateAbout(about: String): Single<User> {
        val personalData = PersonalDataBody(about = about)
        return mobileService.updatePutUserInfo(personalData).flatMap { mobileService.profile() }.requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .compose(SingleRequestTransformer())
    }

    override fun updateProfile(data: PersonalDataBody): Single<User> {
        return mobileService.updatePutUserInfo(data).flatMap { mobileService.profile() }.requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .compose(SingleRequestTransformer())
    }

    override fun updateCost(cost: String): Single<User> {
        val costData = CostDataBody(cost = cost)
        return mobileService.updateCost(costData).flatMap { mobileService.profile() }.requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .compose(SingleRequestTransformer())
    }

    override fun updateActivity(activity: Boolean): Single<User> {
        return mobileService.updateActivity(ActivityBody(activity))
                .flatMap { mobileService.profile() }.requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .compose(SingleRequestTransformer())
    }

    override fun getRequestInfo(uuidRequest: String): Single<MyRequest> {
        return mobileService.getRequestInfo(uuidRequest).requestHandling().compose(SingleRequestTransformer())
    }

    override fun setProfilePhoto(photoKey: String): Single<BooleanResult> {
        return mobileService.setProfilePhoto(PhotoKeyBody(photoKey)).requestHandling().compose(SingleRequestTransformer())
    }

    override fun removePhoto(photoKey: String): Single<BooleanResult> {
        return mobileService.removePhoto(PhotoKeyBody(photoKey)).compose(SingleRequestTransformer())
    }

    override fun registerComplaint(uuidUser: String, comment: String, type: ComplaintType): Single<Boolean> {
        return mobileService.registerComplaint(ComplaintBody(uuidUser, comment, type)).requestHandling().compose(SingleRequestTransformer())
    }

    override fun updatePhoneNumber(phoneNumber: String): Single<OtpInfo> {
        return mobileService.updatePhoneNumber(PhoneNumberBody(phoneNumber)).requestHandling().compose(SingleRequestTransformer())
    }

    override fun getUserSpecializations(): Single<UserSpecializations> {
        return mobileService.getUserSpecializations().requestHandling().compose(SingleRequestTransformer())
    }

    override fun updateSpecializations(body: SpecializationsBody): Single<BooleanResult> {
        return mobileService.updateUserSpecializations(body).requestHandling().compose(SingleRequestTransformer())
    }

    override fun exit(deviceId: String): Single<Boolean> {
        return mobileService.exit(DeviceIdBody(deviceId)).compose(SingleRequestTransformer())
    }

    override fun newPassword(data: NewPasswordData, token: String): Single<Boolean> {
        return authService.requestSetPassword(data).requestHandling()
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .map {
                    return@map saveSession(it)
                }.flatMap { sessionData ->
                    localRepository.get<String>(GlobalValues.DEVICE_UUID, String::class.java).flatMap {
                        if (it.isPresent) {
                            Single.just(it.get() to sessionData)
                        } else {
                            localRepository.save(GlobalValues.DEVICE_UUID, UUID.randomUUID().toString())
                                    .map { it to sessionData }
                        }
                    }
                }
                .flatMap {
                    val uniqueID = it.first
                    mobileService.setPushToken(it.second.tokens?.accessToken, PushTokenRequest(token, uniqueID))
                }
                .flatMap { mobileService.profile() }
                .map {
                    return@map it.isRegistered()
                }.requestHandling()
                .compose(SingleRequestTransformer())
    }

    override fun getCityByLocation(latLng: LatLng): Single<City> {
        return mobileService.getCityByLocation(latLng.latitude.toString(), latLng.longitude.toString())
                .compose(SingleRequestTransformer()).onErrorResumeNext(Single.just(City()))
    }

    override fun deleteRequest(uuidRequest: String): Single<BooleanResult> {
        return mobileService.deleteRequest(uuidRequest).compose(SingleRequestTransformer())
    }

    override fun uploadFile(file: File): Single<FileResponse> {
        val filePart = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(file.getMimeType()?.let { MediaType.parse(it) }, file)
        )
        return mobileService.uploadFile(filePart).requestHandling()
                .compose(SingleRequestTransformer())
    }
}