package com.gorbin.befriends.data.api

import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single
import java.io.File

interface IApiService {
    fun isAuthorized(): Single<Boolean>
    fun login(data: LoginData, token: String): Single<Boolean>
    fun checkOtp(data: CodePhoneData): Single<SessionData>
    fun resendOtp(data: PhoneData): Single<SignUpTokens>
    fun requestCall(data: PhoneData): Single<PinData>
    fun requestCallCheck(data: CodePhoneData): Single<RestoreTokenData>
    fun requestSetPassword(data: NewPasswordData): Single<SessionData>
    fun register(data: RegisterRequestData): Single<SignUpTokens>
    fun getLanguages(): Single<List<Language>>
    fun getCities(): Single<List<City>>
    fun getCountries(nameCountry: String = ""): Single<List<Country>>
    fun getSpecializations(): Single<List<Specialization>>
    fun getSpecializationAreas(): Single<List<SpecializationArea>>
    fun getAreaSpecializations(areaId: String): Single<List<Specialization>>
    fun getCityByCountry(id: String, nameCity: String): Single<List<City>>
    fun profile(): Single<User>
    fun getGallery(): Single<List<GalleryPhoto>>
    fun uploadUserPhoto(keyPhoto: String?, isAvatar: Boolean = true, photo: File?): Single<PhotoResponse>
    fun registerProfile(photo: File?, personalData: PersonalDataBody): Single<UserShort>
    fun getMyRequests(pageSize: Int, offset: Int): Single<List<MyRequest>>
    fun createRequest(body: RequestCreationBody): Single<MyRequest>
    fun getRequestsToMe(pageSize: Int, offset: Int): Single<List<MyRequest>>
    fun getProfile(userId: String): Single<User>
    fun search(pageSize: Int, offset: Int, sort: String, sortDirection: String, searchForm: SearchForm): Single<List<List<UserExecutor>>>
    fun sendReview(userUuid: String, review: ReviewBody): Single<Boolean>
    fun getUserRating(userUuid: String): Single<UserRating>
    fun getReviews(userUuid: String, sortBy: String, sortOrder: String, specializations: List<String>?): Single<List<Review>>
    fun getServicePrices(userUuid: String): Single<List<ServicePrice>>
    fun saveServicePrices(data: PriceList): Single<BooleanResult>
    fun updateAbout(about: String): Single<User>
    fun updateProfile(data: PersonalDataBody): Single<User>
    fun updateCost(cost: String): Single<User>
    fun updateActivity(activity: Boolean): Single<User>
    fun getRequestInfo(uuidRequest: String): Single<MyRequest>
    fun setProfilePhoto(photoKey: String): Single<BooleanResult>
    fun removePhoto(photoKey: String): Single<BooleanResult>
    fun registerComplaint(uuidUser: String, comment: String, type: ComplaintType): Single<Boolean>
    fun updatePhoneNumber(phoneNumber: String): Single<OtpInfo>
    fun getUserSpecializations(): Single<UserSpecializations>
    fun updateSpecializations(body: SpecializationsBody): Single<BooleanResult>
    fun exit(deviceId: String): Single<Boolean>
    fun newPassword(data: NewPasswordData, token: String): Single<Boolean>
    fun getCityByLocation(latLng: LatLng): Single<City>
    fun deleteRequest(uuidRequest: String): Single<BooleanResult>
    fun uploadFile(file: File): Single<FileResponse>
}