package com.gorbin.befriends.data.api

import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single
import retrofit2.http.*


interface IAuthService {
    companion object {
        private const val API_PREFIX = "/api/v1"
    }

    @POST("$API_PREFIX/auth/login")
    fun login(@Body data: LoginData): Single<SessionData>

    @PUT("$API_PREFIX/auth/otp")
    fun checkOtp(@Body data: CodePhoneData): Single<SessionData>

    @PUT("$API_PREFIX/auth/otp/resend")
    fun resendOtp(@Body data: PhoneData): Single<SignUpTokens>

    @PUT("$API_PREFIX/auth/reset")
    fun requestCall(@Body data: PhoneData): Single<PinData>

    @PUT("$API_PREFIX/auth/reset/otp")
    fun requestCallCheck(@Body data: CodePhoneData): Single<RestoreTokenData>

    @PUT("$API_PREFIX/auth/reset/new")
    fun requestSetPassword(@Body data: NewPasswordData): Single<SessionData>

    @POST("$API_PREFIX/auth/registration")
    fun register(@Body data: RegisterRequestData): Single<SignUpTokens>

    @POST("$API_PREFIX/auth/refresh")
    fun refreshToken(@Header("X-Refresh-Token") data: String): Single<SessionData>
}