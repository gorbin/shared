package com.gorbin.befriends.data.api

import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*


interface IMobileService {
    companion object {
        private const val API_PREFIX = "/api/v1"
    }

    @GET("$API_PREFIX/languages")
    fun getLanguages(): Single<List<Language>>

    @GET("$API_PREFIX/cities")
    fun getCities(): Single<List<City>>

    @GET("$API_PREFIX/countries/{nameCountry}")
    fun getCountries(@Path("nameCountry") nameCountry: String): Single<List<Country>>

    @GET("$API_PREFIX/specializations")
    fun getSpecializations(): Single<List<Specialization>>

    @GET("$API_PREFIX/areas")
    fun getSpecializationAreas(): Single<List<SpecializationArea>>

    @GET("$API_PREFIX/areas/{areaId}/specializations")
    fun getAreaSpecializations(@Path("areaId") areaId: String): Single<List<Specialization>>

    @GET("$API_PREFIX/cities/{nameCity}")
    fun getCityByCountry(@Header("countryID") areaId: String, @Path("nameCity") nameCity: String): Single<List<City>>

    @GET("$API_PREFIX/user/info")
    fun profile(): Single<User>

    @GET("$API_PREFIX/user/photo")
    fun getGallery(): Single<List<GalleryPhoto>>

    @POST("$API_PREFIX/user/photo")
    @Multipart
    fun uploadUserPhoto(@Header("X-KeyPhoto") keyPhoto: String?, @Header("X-IsAvatar") isAvatar: Boolean = true, @Part imageFile: MultipartBody.Part): Single<PhotoResponse>

    @POST("$API_PREFIX/file")
    @Multipart
    fun uploadFile(@Part file: MultipartBody.Part): Single<FileResponse>

    @POST("$API_PREFIX/user/info")
    fun updateUserInfo(@Body body: PersonalDataBody): Single<UserShort>

    @GET("$API_PREFIX/request")
    fun getMyRequests(@Header("X-Limit") pageSize: Int, @Header("X-Offset") offset: Int): Single<List<MyRequest>>

    @POST("$API_PREFIX/request")
    fun createRequest(@Body body: RequestCreationBody): Single<MyRequest>

    @GET("$API_PREFIX/request/executor")
    fun getRequestsToMe(@Header("X-Limit") pageSize: Int, @Header("X-Offset") offset: Int): Single<List<MyRequest>>

    @GET("$API_PREFIX/user/profile/{userId}")
    fun getProfile(@Path("userId") userId: String): Single<User>

    @POST("$API_PREFIX/executor")
    fun search(@Header("X-Limit") pageSize: Int, @Header("X-Offset") offset: Int, @Header("X-Sort") sort: String, @Header("X-Direction") sortDirection: String, @Body searchForm: SearchForm): Single<List<List<UserExecutor>>>

    @POST("$API_PREFIX/comments/{uuid}")
    fun sendReview(@Path("uuid") userId: String, @Body body: ReviewBody): Single<Boolean>

    @POST("$API_PREFIX/user/push")
    fun setPushToken(@Header("X-Access-Token") accessToken: String?, @Body body: PushTokenRequest): Single<Unit>

    @GET("$API_PREFIX/comments/rating/{uuid}")
    fun getUserRating(@Path("uuid") userId: String): Single<UserRating>

    @GET("$API_PREFIX/comments/list/{uuid}/{orderBy}/{sortOrder}")
    fun getReviews(@Path("uuid") userId: String, @Path("orderBy") orderBy: String, @Path("sortOrder") sortOrder: String, @Query("services[]") specializations: List<String>?): Single<List<Review>>

    @GET("$API_PREFIX/user/price/{uuid}")
    fun getServicePrices(@Path("uuid") userId: String): Single<List<ServicePrice>>

    @POST("$API_PREFIX/user/price")
    fun saveServicePrices(@Body body: PriceList): Single<BooleanResult>

    @PUT("$API_PREFIX/user/info")
    fun updatePutUserInfo(@Body body: PersonalDataBody): Single<Unit>

    @PUT("$API_PREFIX/user/cost")
    fun updateCost(@Body body: CostDataBody): Single<Unit>

    @PUT("$API_PREFIX/user/activity")
    fun updateActivity(@Body body: ActivityBody): Single<Unit>

    @GET("$API_PREFIX/request/push/info/{uuid}")
    fun getRequestInfo(@Path("uuid") uuidRequest: String): Single<MyRequest>

    @PUT("$API_PREFIX/user/photo/main")
    fun setProfilePhoto(@Body body: PhotoKeyBody): Single<BooleanResult>

    @HTTP(method = "DELETE", path = "$API_PREFIX/user/photo", hasBody = true)
    fun removePhoto(@Body body: PhotoKeyBody): Single<BooleanResult>

    @POST("$API_PREFIX/complaint")
    fun registerComplaint(@Body body: ComplaintBody): Single<Boolean>

    @PUT("$API_PREFIX/user/phone")
    fun updatePhoneNumber(@Body body: PhoneNumberBody): Single<OtpInfo>

    @GET("$API_PREFIX/user/specializations")
    fun getUserSpecializations(): Single<UserSpecializations>

    @POST("$API_PREFIX/user/specializations")
    fun updateUserSpecializations(@Body body: SpecializationsBody): Single<BooleanResult>

    @POST("$API_PREFIX/user/push/logout")
    fun exit(@Body body: DeviceIdBody): Single<Boolean>

    @GET("$API_PREFIX/city/geo")
    fun getCityByLocation(@Header("X-Lat") lat: String, @Header("X-Lng") lng: String): Single<City>

    @DELETE("$API_PREFIX/request/{uuid}")
    fun deleteRequest(@Path("uuid") uuidRequest: String): Single<BooleanResult>
}