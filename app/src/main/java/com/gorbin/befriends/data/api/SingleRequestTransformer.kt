package com.gorbin.befriends.data.api

import com.punicapp.core.utils.executors.ExecutorProvider
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SingleRequestTransformer<T> : SingleTransformer<T, T> {
    override fun apply(upstream: Single<T>): SingleSource<T> = upstream
            .subscribeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
            .observeOn(AndroidSchedulers.mainThread())
}