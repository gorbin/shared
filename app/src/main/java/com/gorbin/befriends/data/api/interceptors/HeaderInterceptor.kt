package com.gorbin.befriends.data.api.interceptors

import android.content.Context
import androidx.core.os.ConfigurationCompat
import com.gorbin.befriends.BuildConfig
import com.gorbin.befriends.domain.entities.SessionData
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.rxrepocore.IKeyValueRepository
import okhttp3.Interceptor
import okhttp3.Request.Builder
import okhttp3.Response
import java.io.IOException
import java.util.*

class HeaderInterceptor(private val localRepo: IKeyValueRepository<String>?, private val context: Context) : Interceptor {
    companion object {
        const val AUTH_TOKEN = "X-Access-Token"
        const val X_LOCALE = "X-Locale"
        const val X_TIMEZONE = "X-Timezone"
        const val DEVICE_TYPE = "DeviceType"
        const val DEVICE_TYPE_VALUE = "Android"
        const val VERSION = "Version"
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val request: Builder
        request = original.newBuilder()
                .method(original.method(), original.body())
                .removeHeader("Content-Length")

        if (localRepo != null) {
            val session =
                    localRepo.instantGet<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java).orNull()
            session?.tokens?.accessToken?.let {
                request.addHeader(AUTH_TOKEN, it)
            }
        }
        request.addHeader(DEVICE_TYPE, DEVICE_TYPE_VALUE)
//        var currentLocale = ConfigurationCompat.getLocales(context.resources.configuration)[0]
        //todo set normal locale
//        currentLocale = Locale("RU")
//        request.addHeader(X_LOCALE, currentLocale.toLanguageTag())
        val currentLocale = ConfigurationCompat.getLocales(context.resources.configuration)[0]
        request.addHeader(X_LOCALE, currentLocale.language.toUpperCase(Locale.ENGLISH))
        if (!original.url().url().toString().contains("v1/executor")) {
            val cal: Calendar = Calendar.getInstance()
            val tz: TimeZone = cal.timeZone
            request.addHeader(X_TIMEZONE, tz.rawOffset.toString())
        }
        val regex = "\\d+.\\d+.\\d+".toRegex()
        regex.find(BuildConfig.VERSION_NAME)?.value?.let {
            request.addHeader(VERSION, it)
        }

        return chain.proceed(request.build())
    }
}