package com.gorbin.befriends.data.api.model

import com.google.gson.annotations.SerializedName


class ApiError(
        @SerializedName("code")
        val code: Int,
        @SerializedName("description")
        val description: String
) {
    val message get() = description.takeIf { it.isNotEmpty() } ?: "Unknown error"
}