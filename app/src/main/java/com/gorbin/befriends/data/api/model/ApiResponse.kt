package com.gorbin.befriends.data.api.model

import com.google.gson.annotations.SerializedName

open class ApiResponse<out T>(
    @SerializedName("data")
    val data: T?,
    @SerializedName("result")
    val result: ApiResult
) {
    fun isOk(): Boolean {
        return result.status == ApiResult.OK_STATUS && data != null
    }
}

class ApiResult(
    @SerializedName("status")
    val status: String,
    @SerializedName("error")
    val error: ApiError?
) {
    companion object {
        const val OK_STATUS = "ok"
    }
}