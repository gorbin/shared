package com.gorbin.befriends.data.api.model

import com.google.gson.annotations.SerializedName

data class ErrorList(
        @SerializedName("errorList")
        val errorList: List<SpecificError>
)