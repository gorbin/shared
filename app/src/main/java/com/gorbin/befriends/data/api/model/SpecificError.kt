package com.gorbin.befriends.data.api.model

import com.google.gson.annotations.SerializedName

data class SpecificError(
        @SerializedName("errorCode")
        val code: Long,
        @SerializedName("errorDescription")
        val message: String
)