package com.gorbin.befriends.data.gson

import com.google.gson.Gson
import com.google.gson.GsonBuilder

class GsonManager {
    val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

    fun serialize(obj: Any): String {
        return gson.toJson(obj)
    }

    fun <T> deserialize(repr: String, clazz: Class<T>): T {
        return gson.fromJson(repr, clazz)
    }
}
