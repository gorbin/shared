package com.gorbin.befriends.data.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ServerDateAtZDeserializer : JsonDeserializer<Date> {
    companion object {
        val timeWithMillisDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
        val dateFormat = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.ENGLISH)
        } else {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.ENGLISH)
        }
    }

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Date? = if (json?.asString.isNullOrBlank()) null else {
        try {
            dateFormat.parse(json?.asString!!)
        } catch (e: ParseException) {
            timeWithMillisDateFormat.parse(json?.asString!!)
        }
    }
}