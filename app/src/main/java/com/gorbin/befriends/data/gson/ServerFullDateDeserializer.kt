package com.gorbin.befriends.data.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class ServerFullDateDeserializer : JsonDeserializer<Date> {
    companion object {
        val dateTimeSecondsFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
    }

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Date? = if (json?.asString.isNullOrBlank()) null else {
        dateTimeSecondsFormat.parse(json?.asString!!)
    }
}