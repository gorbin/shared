package com.gorbin.befriends.data.repo

import com.punicapp.rxrepocore.IKeyValueRepository
import com.punicapp.rxrepocore.IRepository
import com.punicapp.rxrepoinmemory.InMemoryRepository
import com.punicapp.rxrepolocal.LocalRepository
import org.koin.dsl.module
import java.util.*

val repositoryModule = module {
    single<IKeyValueRepository<String>> { LocalRepository(get()) }
    single<IRepository<Locale>> { InMemoryRepository() }
}