package com.gorbin.befriends.domain.entities

import java.io.Serializable

data class ActivityBody(
        val activity: Boolean?
) : Serializable