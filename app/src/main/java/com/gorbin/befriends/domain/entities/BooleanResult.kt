package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class BooleanResult (
        @SerializedName("message")
        val result: Boolean
)