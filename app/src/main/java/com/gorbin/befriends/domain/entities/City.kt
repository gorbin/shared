package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import com.punicapp.mvvm.adapters.IIdentity
import java.io.Serializable

data class City(
        @SerializedName("city_id")
        val id: Long? = null,
        @SerializedName("nameCity")
        val name: String? = null,
        @SerializedName("region")
        val region: String? = null,
        @SerializedName("nameCountry")
        val country: String? = null
) : Serializable, IIdentity {
        override fun getId(): String {
                return id.toString()
        }
}