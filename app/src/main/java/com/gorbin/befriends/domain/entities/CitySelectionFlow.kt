package com.gorbin.befriends.domain.entities

enum class CitySelectionFlow {
    PERSONAL_INFO,
    SEARCH,
    PROFILE_EDITING
}