package com.gorbin.befriends.domain.entities

data class CodePhoneData(
        var pin: String,
        var pinAttempt: String?
)