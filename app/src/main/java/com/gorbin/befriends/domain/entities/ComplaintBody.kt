package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class ComplaintBody(
        @SerializedName("uuidUser")
        val uuidUser: String,
        @SerializedName("comment")
        val comment: String,
        @SerializedName("type")
        val type: ComplaintType
)