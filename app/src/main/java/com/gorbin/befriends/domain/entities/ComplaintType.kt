package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

enum class ComplaintType {
    @SerializedName("scam")
    SCAM,
    @SerializedName("spam")
    SPAM,
    @SerializedName("insult")
    INSULT,
    @SerializedName("copy")
    COPY,
    @SerializedName("swindler")
    SWINDLER,
    @SerializedName("other")
    OTHER
}