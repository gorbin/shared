package com.gorbin.befriends.domain.entities

import java.io.Serializable

data class CostDataBody(
        val cost: String? = null,
        val currency: String? = "USD"
) : Serializable