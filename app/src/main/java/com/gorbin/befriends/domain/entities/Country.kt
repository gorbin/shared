package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import com.punicapp.mvvm.adapters.IIdentity
import java.io.Serializable

data class Country(
        @SerializedName("country_id")
        val id: Long?,
        @SerializedName("nameCountry")
        val name: String?,
        @SerializedName("tel_code")
        val region: String?,
        @SerializedName("country_flag")
        val country: String?,
        @SerializedName("nativeName")
        val nativeName: String?,
        @SerializedName("currenciesSymbol")
        val currenciesSymbol: String?,
        @SerializedName("country_code")
        val country_code: String?,
        @SerializedName("availableForReg")
        val isAvailable: Boolean?
) : Serializable, IIdentity {
    override fun getId(): String {
        return id.toString()
    }
}