package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

enum class Currency(val sign: String) {
    @SerializedName("USD")
    USD("\$"),
    @SerializedName("EUR")
    EUR("€"),
    @SerializedName("RUB")
    RUB("₽")
}