package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class DeviceIdBody(
    @SerializedName("idDevice")
    val idDevice: String
)