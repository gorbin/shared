package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class File(
        @SerializedName("uuid")
        val uuid: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("extension")
        val extension: String,
        @SerializedName("size")
        val size: Int,
        @SerializedName("link")
        val link: String
)