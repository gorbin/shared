package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class FileResponse(
    @SerializedName("key")
    val keyFile: String,
    @SerializedName("url")
    val urlFile: String,
    @SerializedName("uuid")
    val uuidFile: String
)