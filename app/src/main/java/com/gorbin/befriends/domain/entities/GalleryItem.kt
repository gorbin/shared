package com.gorbin.befriends.domain.entities

data class GalleryItem(
        val photo: GalleryPhoto? = null
)