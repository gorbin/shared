package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class GalleryPhoto(
        @SerializedName("urlPhoto")
        val photoUrl: String,
        @SerializedName("keyPhoto")
        val keyPhoto: String,
        @SerializedName("avatar")
        val isAvatarPhoto: Boolean
)
