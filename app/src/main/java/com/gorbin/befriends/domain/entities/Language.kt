package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Language(
        @SerializedName("idLanguage")
        val id: Long,
        @SerializedName("name", alternate = ["nameLanguage"])
        val name: String,
        @SerializedName("shortName")
        val shortName: String? = null
) : Serializable