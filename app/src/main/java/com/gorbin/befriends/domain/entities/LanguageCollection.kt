package com.gorbin.befriends.domain.entities

import java.io.Serializable

data class LanguageCollection(
        val languages: List<Language>
) : Serializable