package com.gorbin.befriends.domain.entities

enum class LocationCheckResults {
    PERMISSION_NOT_GRANTED,
    NOT_AVAILABLE,
    AVAILABLE
}