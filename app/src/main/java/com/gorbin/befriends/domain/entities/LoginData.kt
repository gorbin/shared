package com.gorbin.befriends.domain.entities

data class LoginData(
        val login: String,
        val password: String
)