package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import com.punicapp.mvvm.adapters.IIdentity
import java.io.Serializable

data class MyRequest(
    val idRequest: Long,
    val uuidRequest: String,
    val idEmployer: Long,
    val cost: Double,
    val idCurrency: Int,
    var status: RequestStatus,
    val idChat: Long,
    val idUser: Long,
    val uuidUser: String,
    val name: String,
    val surname: String,
    val type: ServiceType,
    val username: String,
    val usernameFlag: Boolean,
    val patronymic: String? = null,
    val urlPhoto: String? = null,
    val rating: Double,
    val lastUpdate: String? = null,
    @SerializedName("currency")
    val currency: Currency,
    @SerializedName("countMessages")
    val countMessages: Int? = 0
): Serializable, IIdentity {
    override fun getId(): String {
        return this.hashCode().toString()
    }

    fun getNameOrNickname(): String {
        return if (usernameFlag)
            username ?: ""
        else "$name $surname"
    }

    fun getFormattedPrice(): String {
        val cost = cost.toInt()
        if (cost == 0) {
            return "FREE"
        }
        return if (currency == Currency.RUB) {
            "${cost}${currency.sign}"
        } else {
            "${currency.sign}${cost}"
        }
    }

    fun isNewbie(): Boolean {
        return rating.isNaN() || rating.compareTo(1.0) < 0
    }
}