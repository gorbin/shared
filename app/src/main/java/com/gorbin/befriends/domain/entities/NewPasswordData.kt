package com.gorbin.befriends.domain.entities

data class NewPasswordData(
        val pass: String,
        val rememberToken: String
)