package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OtpInfo(
        @SerializedName("pin")
        val pin: String? = null,
        @SerializedName("pinAttempt")
        val attemptId: String? = null,
        @SerializedName("callId")
        val callId: String? = null
) : Serializable