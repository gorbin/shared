package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PersonalDataBody(
        val name: String? = null,
        val patronymic: String? = null,
        val surname: String? = null,
        val birthdate: String? = null,
        val email: String? = null,
        val phone: String? = null,
        var photo: PhotoBody? = null,
        val idCity: String? = null,
        val activity: String? = null,
        val specializations: List<SpecializationBody?>? = null,
        val languages: List<LanguagesBody?>? = null,
        val experience: String? = null,
        @SerializedName("place_work")
        val placeWork: String? = null,
        val about: String? = null,
        val username: String? = null,
        @SerializedName("usernameFlag")
        val showNickname: Boolean? = null
) : Serializable {
    fun setPhoto(photo: PhotoResponse) {
        this.photo = PhotoBody(photo.urlFile, photo.keyFile)
    }
}

data class PhotoBody(
        val urlPhoto: String?,
        val keyPhoto: String?
) : Serializable

data class SpecializationBody(
        val idSpecialization: String?,
        val services: List<String?>?,
        val spheres: List<UuidSphere>? = null
) : Serializable

data class LanguagesBody(
        val idLanguage: String?
) : Serializable