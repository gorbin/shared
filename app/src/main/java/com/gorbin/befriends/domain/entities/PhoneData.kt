package com.gorbin.befriends.domain.entities

data class PhoneData(
        val search: String
)