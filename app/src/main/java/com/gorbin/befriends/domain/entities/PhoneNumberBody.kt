package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PhoneNumberBody(
    @SerializedName("phone")
    val phoneNumber: String
)