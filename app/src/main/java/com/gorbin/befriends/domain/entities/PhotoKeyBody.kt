package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PhotoKeyBody (
    @SerializedName("keyPhoto")
    val photoKey: String
)