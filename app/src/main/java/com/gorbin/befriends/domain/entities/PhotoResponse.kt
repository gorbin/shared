package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PhotoResponse(
    @SerializedName("keyPhoto")
    val keyFile: String,
    @SerializedName("urlPhoto")
    val urlFile: String
)