package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PinData(
        @SerializedName("pin")
        var pin: String?,
        @SerializedName("pinAttempt")
        var pinAttempt: String?,
        @SerializedName("callId")
        var callId: String?
) : Serializable