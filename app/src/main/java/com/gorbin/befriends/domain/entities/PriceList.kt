package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PriceList (
        @SerializedName("items")
        val list: List<ServicePrice>
)