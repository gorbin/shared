package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PushAction (
        @SerializedName("role")
        val role: Role?,
        @SerializedName("type")
        val type: String,
        @SerializedName("room")
        val room: String
)