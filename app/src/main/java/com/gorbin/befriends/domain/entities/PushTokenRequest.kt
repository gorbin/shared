package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class PushTokenRequest (
    @SerializedName("token") val token: String,
    @SerializedName("idDevice") val idDevice: String
)