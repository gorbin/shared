package com.gorbin.befriends.domain.entities

import com.punicapp.mvvm.adapters.IIdentity
import java.io.Serializable

data class RegionInfo(
        val phoneCode: String,
        val countryCode: String,
        var countryName: String
) : Serializable, IIdentity {
    override fun getId(): String = countryName
}