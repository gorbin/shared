package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class RegisterRequestData(
        val login: String,
        val password: String,
        val name: String,
        val surname: String,
        val username: String? = null,
        @SerializedName("usernameFlag")
        val usernameOnly: Boolean = false
)