package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class RequestCreationBody(
        @SerializedName("uuidUser")
        val uuidUser: String,
        @SerializedName("idSpecialization")
        val idSpecialization: String,
        @SerializedName("idServiceType")
        val idServiceType: String
)