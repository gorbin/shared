package com.gorbin.befriends.domain.entities

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.R

enum class RequestStatus(
        @StringRes val nameRes: Int,
        @DrawableRes val bgRes: Int,
        @ColorRes val textColorRes: Int,
        @StringRes val descriptionRes: Int
) {
    @SerializedName("prepare")
    PREPARE(R.string.request_status_new, R.drawable.bg_request_status_new, R.color.colorWhite_80, R.string.empty),

    @SerializedName("new")
    NEW(R.string.request_status_new, R.drawable.bg_request_status_new, R.color.colorWhite_80, R.string.status_description_new),

    @SerializedName("moderation")
    MODERATION(R.string.request_status_moderation, R.drawable.bg_request_status_pending, R.color.colorGray_80, R.string.status_description_moderation),

    @SerializedName("rejectedEmployer")
    REJECTED_EMPLOYER(R.string.request_status_rejected_employer, R.drawable.bg_request_status_finished, R.color.colorWhite_80, R.string.status_description_rejected_employer),

    @SerializedName("rejectedExecutor")
    REJECTED_EXECUTOR(R.string.request_status_rejected_executor, R.drawable.bg_request_status_finished, R.color.colorWhite_80, R.string.status_description_rejected_executor),

    @SerializedName("closed")
    CLOSED(R.string.request_status_closed, R.drawable.bg_request_status_finished, R.color.colorWhite_80, R.string.status_description_closed),

    @SerializedName("inprogress")
    IN_PROGRESS(R.string.request_status_in_progress, R.drawable.bg_request_status_new, R.color.colorWhite_80, R.string.status_description_inProgress),

    @SerializedName("conflict")
    CONFLICT(R.string.request_status_conflict, R.drawable.bg_request_status_dispute, R.color.colorWhite_80, R.string.status_description_dispute),

    @SerializedName("conflictBef")
    CONFLICT_BEF(R.string.request_status_conflictBef, R.drawable.bg_request_status_dispute, R.color.colorWhite_80, R.string.status_description_dispute),

    @SerializedName("waiting")
    WAITING(R.string.request_status_waiting, R.drawable.bg_request_status_dispute, R.color.colorWhite_80, R.string.status_description_waiting)
}