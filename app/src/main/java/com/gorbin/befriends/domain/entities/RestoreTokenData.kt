package com.gorbin.befriends.domain.entities

data class RestoreTokenData(
        val rememberToken: String
)