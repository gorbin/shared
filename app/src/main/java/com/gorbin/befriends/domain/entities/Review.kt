package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.data.gson.ServerDateAtZDeserializer
import java.io.Serializable
import java.util.*

data class Review(
        @SerializedName("id")
        val id: Long,
        @SerializedName("qualification")
        val qualification: String,
        @SerializedName("speed_response")
        val responseSpeed: String,
        @SerializedName("courtesy")
        val politeness: String,
        @SerializedName("comment")
        val comment: String,
        @SerializedName("attachment")
        val attachment: String? = null,
        @JsonAdapter(ServerDateAtZDeserializer::class)
        @SerializedName("created_at")
        val created_at: Date,
        @SerializedName("name")
        val authorName: String,
        @SerializedName("surname")
        val authorSurname: String,
        @SerializedName("type")
        val type: ServiceType,
        @SerializedName("profile_photo")
        val profilePhoto: String? = null,
        @SerializedName("rating")
        val rating: Double
) : Serializable {
        val fullName: String
                get() = "$authorName $authorSurname"
}