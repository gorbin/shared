package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ReviewBody(
        @Transient
        val executorUuid: String,
        @SerializedName("comment")
        var comment: String? = null,
        @SerializedName("qualification")
        val professionalism: Int,
        @SerializedName("speed_response")
        val performance: Int,
        @SerializedName("courtesy")
        val politeness: Int,
        @SerializedName("attachment")
        val attachment: String? = null,
        @SerializedName("type")
        val type: ServiceType,
        @SerializedName("idRequest")
        val requestId: Long
) : Serializable