package com.gorbin.befriends.domain.entities

import androidx.annotation.StringRes
import com.gorbin.befriends.R

enum class ReviewSortBy(val value: String, @StringRes val labelRes: Int) {
    DATE("created_at", R.string.by_date),
    RATING("rating", R.string.by_rating)
}