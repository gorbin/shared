package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

enum class Role {
    @SerializedName("customer")
    CUSTOMER,
    @SerializedName("executor")
    EXECUTOR
}