package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchForm(
        @SerializedName("idSpecialization")
        var idSpecialization: String?,
        @SerializedName("idSphere")
        var idSphere: String? = null,
        @SerializedName("idService")
        var idService: String? = null,
        @SerializedName("idCountry")
        var idCountry: String?,
        @SerializedName("idCity")
        var idCity: String?,
        @SerializedName("idLanguage")
        var idLanguage: List<String>?,
        @SerializedName("rating")
        var rating: String?,
        @SerializedName("cost")
        var cost: String?
) : Serializable