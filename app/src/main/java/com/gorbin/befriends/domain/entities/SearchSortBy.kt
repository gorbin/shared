package com.gorbin.befriends.domain.entities

import androidx.annotation.StringRes
import com.gorbin.befriends.R

enum class SearchSortBy(val value: String, @StringRes val labelRes: Int) {
    POPULAR("popular", R.string.popular),
    RATING("rating", R.string.rating),
    COMMENTS("comments", R.string.reviews),
    PRICE("price", R.string.price_alt)
}