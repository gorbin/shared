package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServicePrice(
        @SerializedName("nameService")
        val nameService: String,
        @SerializedName("descService")
        val descService: String,
        @SerializedName("cost")
        val cost: String,
        @SerializedName("currency")
        val currency: ServiceCurrency
) : Serializable

enum class ServiceCurrency(val sign: String) {
        @SerializedName("USD")
        USD("\$"),
        @SerializedName("EUR")
        EUR("€"),
        @SerializedName("GBP")
        GBP("£"),
        @SerializedName("RUB")
        RUB("₽"),
        @SerializedName("UAH")
        UAH("₴"),
        @SerializedName("BYN")
        BYN("Br"),
        @SerializedName("KZT")
        KZT("₸"),
        @SerializedName("PKR")
        PKR("Rs"),
        @SerializedName("TRY")
        TRY("₺"),
        @SerializedName("TND")
        TND("د.ت"),
        @SerializedName("ZAR")
        ZAR("R"),
        @SerializedName("PYG")
        PYG("₲"),
        @SerializedName("VES")
        VES("Bs.S."),
        @SerializedName("AOA")
        AOA("Kz"),
        @SerializedName("XPF")
        XPF("₣"),
        @SerializedName("PEN")
        PEN("S/."),
        @SerializedName("BWP")
        BWP("P"),
        @SerializedName("HTG")
        HTG("G"),
        @SerializedName("GHS")
        GHS("₵"),
        @SerializedName("GTQ")
        GTQ("Q"),
        @SerializedName("HNL")
        HNL("L"),
        @SerializedName("PGK")
        PGK("K"),
        @SerializedName("KES")
        KES("Sh"),
        @SerializedName("KGS")
        KGS("С"),
        @SerializedName("MZN")
        MZN("MT"),
        @SerializedName("NGN")
        NGN("₦"),
        @SerializedName("PAB")
        PAB("B/."),
        @SerializedName("CRC")
        CRC("₡"),
        @SerializedName("TZS")
        TZS("Sh"),
        @SerializedName("UGX")
        UGX("Sh")
}