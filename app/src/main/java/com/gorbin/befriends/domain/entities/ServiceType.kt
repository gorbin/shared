package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

enum class ServiceType(val id: Int) {
    @SerializedName("consultancy")
    CONSULTANCY(1),
    @SerializedName("execution")
    EXECUTION(2),
    @SerializedName("training")
    TRAINING(3);

    companion object {
        fun getById(id: Int): ServiceType? {
            return values().find { it.id == id }
        }
    }
}