package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class SessionData(
        @SerializedName("tokens")
        var tokens: Tokens?
)