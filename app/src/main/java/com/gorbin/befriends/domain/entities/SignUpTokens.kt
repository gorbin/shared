package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SignUpTokens(
        @SerializedName("accessToken")
        val accessToken: String? = null,
        @SerializedName("refreshToken")
        val refreshToken: String? = null,
        @SerializedName("validationOtp")
        val otpInfo: OtpInfo? = null
) : Serializable