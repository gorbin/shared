package com.gorbin.befriends.domain.entities

import androidx.annotation.StringRes
import com.gorbin.befriends.R

enum class SortOrder(@StringRes val labelRes: Int) {
    ASC(R.string.asc),
    DESC(R.string.desc)
}