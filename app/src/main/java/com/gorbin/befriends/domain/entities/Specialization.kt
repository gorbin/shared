package com.gorbin.befriends.domain.entities

import androidx.annotation.StringRes
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.R
import com.punicapp.mvvm.adapters.IIdentity
import java.io.Serializable

data class ProfileSpecialization(
        @SerializedName("idSpecialization")
        val id: Long,
        @SerializedName("idArea")
        val areaId: Long,
        @SerializedName("nameArea")
        val areaName: String,
        @SerializedName("nameSpecialization")
        val name: String,
        @SerializedName("spheres")
        var spheres: List<Sphere>? = null,
        @SerializedName("services")
        val services: List<Int>
)

data class Specialization(
        @SerializedName("idSpecialization")
        val id: Long,
        @SerializedName("idArea")
        val areaId: Long,
        @SerializedName("nameSpecialization")
        val name: String,
        @SerializedName("spheres")
        var spheres: List<Sphere>? = null
) : Serializable, IIdentity {
    override fun getId(): String {
        return id.toString()
    }
}

enum class SkillSpecialization(
        @StringRes val titleRes: Int,
        @StringRes val shortTitleRes: Int,
        val serverId: Int,
        val filter: String
) : Serializable {
    CONSULTATION(R.string.consultation, R.string.consultation_short, 1, "consultancy"),
    WORK(R.string.work, R.string.work_short, 2, "execution"),
    TEACH(R.string.teach, R.string.teach, 3, "training");

    companion object {
        fun getById(id: Int): SkillSpecialization? {
            return values().find { it.serverId == id }
        }
    }
}

data class SpecializationItem(
        @SerializedName("idSpecialization")
        val id: Long,
        @SerializedName("area")
        val area: String,
        @SerializedName("nameSpecialization")
        val name: String
) : Serializable