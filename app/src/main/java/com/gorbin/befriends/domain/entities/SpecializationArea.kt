package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SpecializationArea(
        @SerializedName("idArea")
        val id: Long,
        @SerializedName("name", alternate = ["nameArea"])
        val name: String,
        @SerializedName("picture")
        val picture: String? = null
) : Serializable