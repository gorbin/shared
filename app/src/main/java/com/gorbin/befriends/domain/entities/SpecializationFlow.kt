package com.gorbin.befriends.domain.entities

enum class SpecializationFlow {
    PERSONAL_INFO,
    SEARCH,
    PROFILE,
    EMPTY_PROFILE
}