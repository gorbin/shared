package com.gorbin.befriends.domain.entities

import java.io.Serializable

class SpecializationTree : Serializable {
    var areas = listOf<SpecializationArea>()
    var selectedArea: SpecializationArea? = null
    var selectedSpecializations = mapOf<Specialization, MutableList<SkillSpecialization>>()
}