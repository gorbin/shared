package com.gorbin.befriends.domain.entities

import com.punicapp.mvvm.adapters.IIdentity

data class SpecializationWithSpheres (
        val specialization: Specialization,
        val sphereList: List<Sphere>,
        var expanded: Boolean
) : IIdentity {
    override fun getId(): String {
        return specialization.getId()
    }
}