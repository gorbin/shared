package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class SpecializationsBody(
        @SerializedName("specializations")
        val data: List<SpecializationBody>
)