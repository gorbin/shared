package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class Sphere (
        @SerializedName("uuid")
        val uuid: String,
        @SerializedName("name")
        val name: String
)