package com.gorbin.befriends.domain.entities

data class SphereItem(
        val sphere: Sphere,
        val specialization: Specialization
)