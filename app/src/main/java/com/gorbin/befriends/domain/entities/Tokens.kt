package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class Tokens(
    @SerializedName("accessToken")
    var accessToken: String?,
    @SerializedName("refreshToken")
    var refreshToken: String?
)