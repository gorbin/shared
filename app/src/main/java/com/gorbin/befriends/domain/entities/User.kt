package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.data.gson.ServerDateAtZDeserializer
import com.gorbin.befriends.data.gson.ServerFullDateDeserializer
import java.io.Serializable
import java.util.*

data class User(
        @SerializedName("uuidUser")
        val uuidUser: String,
        @SerializedName("idUser")
        val idUser: String,
        @SerializedName("username")
        val username: String? = null,
        @SerializedName("fullname")
        var fullName: String,
        @SerializedName("name")
        var name: String,
        @SerializedName("surname")
        var surname: String,
        @SerializedName("nameCity")
        var city: String?,
        @SerializedName("nameCountry")
        var country: String?,
        @SerializedName("experience")
        var experience: String,
        @SerializedName("place_work")
        var workPlace: String,
        @SerializedName("about")
        var about: String?,
        @SerializedName("lastActive")
        @JsonAdapter(ServerDateAtZDeserializer::class)
        var lastActive: Date,
        @SerializedName("language")
        var language: String,
        @SerializedName("languageList")
        var languageList: List<Language>,
        @SerializedName("specialization")
        var specialization: String,
        @SerializedName("specializationList")
        val specializationList: List<ProfileSpecialization>?,
        @SerializedName("cost_service")
        var costService: Double = 0.0,
        @SerializedName("request_closed")
        var requestClosed: Int,
        @SerializedName("comments")
        var comments: Int,
        @SerializedName("rating")
        var rating: Double,
        @SerializedName("birth_date")
        @JsonAdapter(ServerFullDateDeserializer::class)
        var birthDate: Date?,
        @SerializedName("logo")
        var logo: String?,
        @SerializedName("photoList")
        val photoList: List<GalleryPhoto>?,
        @SerializedName("phone")
        var phone: String,
        @SerializedName("activity")
        var activity: Boolean,
        @SerializedName("has_consultation")
        var hasConsultation: Boolean,
        @SerializedName("usernameFlag")
        var showNickname: Boolean,
        @SerializedName("status")
        val status: UserStatus,
        @SerializedName("has_price_list")
        val hasPriceList: Boolean,
        @SerializedName("countRequestsForMe")
        val requestsToMeCounter: Int? = null,
        @SerializedName("countMyRequests")
        val myRequestsCounter: Int? = null
) : Serializable {
    fun isRegistered(): Boolean {
        return (specialization.isNotBlank() || !city.isNullOrBlank() || !country.isNullOrBlank())
    }

    fun getNameOrNickname(): String {
        return if (showNickname)
            username ?: ""
        else fullName
    }

    fun isNewbie(): Boolean {
        return status == UserStatus.NEW || rating < 1.0
    }

    fun getGallery(): List<GalleryPhoto> {
        val result = mutableListOf<GalleryPhoto>()
        photoList?.find { it.isAvatarPhoto }?.let { result.add(it) }
        photoList?.filter { !it.isAvatarPhoto }?.reversed()?.let { result.addAll(it) }

        return result
    }

    fun getSpecializations(): String {
        return specializationList?.joinToString(separator = ", ") { it.name } ?: specialization
    }
}

enum class UserStatus {
    @SerializedName("new")
    NEW,
    @SerializedName("active")
    ACTIVE,
    @SerializedName("blocked")
    BLOCKED
}

data class UserShort(
        val uuidUser: String,
        val idUser: String
) : Serializable

data class UserExecutor(
        @SerializedName("uuidUser")
        val uuidUser: String,
        @SerializedName("idUser")
        val idUser: String,
        @SerializedName("urlPhoto")
        val url: String? = null,
        @SerializedName("name")
        val name: String,
        @SerializedName("surname")
        val surname: String,
        @SerializedName("rating")
        val rating: Double,
        @SerializedName("cost")
        val cost: Double,
        @SerializedName("requestClosed")
        val ordersCount: Int,
        @SerializedName("countComments")
        val reviewsCount: Int,
        @SerializedName("status")
        val status: UserStatus,
        @SerializedName("usernameFlag")
        val showNickname: Boolean,
        @SerializedName("username")
        val username: String? = null,
        @SerializedName("currency")
        val currency: Currency
) : Serializable {
    fun getNameOrNickname(): String {
        return if (showNickname)
            username ?: ""
        else "$name $surname"
    }

    fun getFormattedPrice(): String {
        val cost = cost.toInt()
        if (cost == 0) {
            return "FREE"
        }
        return if (currency == Currency.RUB) {
            "${cost}${currency.sign}"
        } else {
            "${currency.sign}${cost}"
        }
    }
    fun isNewbie(): Boolean {
        return rating.isNaN() || rating.compareTo(1.0) < 0
    }
}
