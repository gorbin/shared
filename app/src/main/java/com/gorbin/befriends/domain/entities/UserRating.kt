package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class UserRating(
        @SerializedName("ratingMain")
        val generalRating: String,
        @SerializedName("ratingConsultation")
        val ratingConsultation: String,
        @SerializedName("ratingWorks")
        val ratingWork: String,
        @SerializedName("ratingTraining")
        val ratingTraining: String,
        @SerializedName("consultationProf")
        val consultationProfessionalism: String,
        @SerializedName("consultationSpeed")
        val consultationPerformance: String,
        @SerializedName("consultationCourtesy")
        val consultationPoliteness: String,
        @SerializedName("worksProf")
        val workProfessionalism: String,
        @SerializedName("worksSpeed")
        val workPerformance: String,
        @SerializedName("worksCourtesy")
        val workPoliteness: String,
        @SerializedName("trainingProf")
        val trainingProfessionalism: String,
        @SerializedName("trainingSpeed")
        val trainingPerformance: String,
        @SerializedName("trainingCourtesy")
        val trainingPoliteness: String,
        @SerializedName("countCommentAll")
        val allCommentNumber: Int,
        @SerializedName("countRequestsConsultation")
        val consultationRequestsNumber: Int,
        @SerializedName("countCommentConsultation")
        val consultationCommentsNumber: Int,
        @SerializedName("countRequestsWorks")
        val workRequestsNumber: Int,
        @SerializedName("countCommentWorks")
        val workCommentsNumber: Int,
        @SerializedName("countRequestsTraining")
        val trainingRequestsNumber: Int,
        @SerializedName("countCommentTraining")
        val trainingCommentsNumber: Int

)