package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName

data class UserSpecializations(
        @SerializedName("area")
        val area: SpecializationArea,
        @SerializedName("specializations")
        val specializations: List<UserSpecialization>
)

data class UserSpecialization(
        @SerializedName("idSpecialization")
        val idSpecialization: Long,
        @SerializedName("nameSpecialization")
        val nameSpecialization: String,
        @SerializedName("spheres")
        val spheres: List<Sphere>,
        @SerializedName("services")
        val services: List<Int>? = null
)