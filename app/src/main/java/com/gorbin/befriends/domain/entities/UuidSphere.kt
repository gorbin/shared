package com.gorbin.befriends.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UuidSphere (
    @SerializedName("uuidSphere")
    val uuidSphere: String
) : Serializable