package com.gorbin.befriends.domain.entities

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.gorbin.befriends.R

enum class WelcomeOnboarding(
        @DrawableRes val icon: Int,
        @StringRes val title: Int,
        @StringRes val explanation: Int
) {
    WELCOME(R.drawable.ic_logo, R.string.welcome_to_be_friends, R.string.welcome_to_be_friends_explanation),
    COMMUNICATION(R.drawable.ic_conversation, R.string.request_assistance, R.string.request_assistance_explanation),
    ASSISTANCE(R.drawable.ic_purse, R.string.assist_and_earn_money, R.string.assist_and_earn_money_explanation)
}