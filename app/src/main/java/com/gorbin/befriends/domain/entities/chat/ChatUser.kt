package com.gorbin.befriends.domain.entities.chat

import com.stfalcon.chatkit.commons.models.IUser

open class ChatUser(var userId: String, var userName: String, var userAvatar: String?, var userAvatarRes: Int?) : IUser {
    constructor() : this("", "", null, null)

    override fun getId() = userId

    override fun getName() = userName

    override fun getAvatar() = userAvatar

    override fun getAvatarRes() = userAvatarRes
}