package com.gorbin.befriends.domain.entities.chat

import android.graphics.drawable.Drawable

data class CommandConfig(
        val commandAvailable: Boolean,
        val commandAction: String?,
        val commandButtonBg: Drawable?
)