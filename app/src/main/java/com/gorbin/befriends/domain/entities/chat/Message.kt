package com.gorbin.befriends.domain.entities.chat

import com.gorbin.befriends.domain.entities.File
import com.gorbin.befriends.domain.entities.RequestStatus
import com.gorbin.befriends.domain.entities.wsprotocol.WebSocketChatEvent
import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.Image
import java.util.*

sealed class Message(var mId: String, private val mUser: ChatUser, val chatAvailable: Boolean? = null) : IMessage {
    @Transient
    var isLastInChain: Boolean = false
    @Transient
    var date = Date(System.currentTimeMillis())

    override fun getId() = mId

    override fun getText(): String? = null

    override fun getUser() = mUser

    override fun getCreatedAt() = date

    override fun getBackgroundColor(): Int? = null

    override fun copy(): IMessage = this

    override fun getIsRead(): Boolean = true

    override fun setIsRead(isRead: Boolean) { }

    override fun noDate(): Boolean = false

    class TextMessage(
            mId: String,
            mUser: ChatUser,
            val bgColor: Int? = null,
            var read: Boolean? = null,
            var mText: String,
            val mDate: Date? = null,
            chatAvailable: Boolean? = null
    ) : Message(mId, mUser, chatAvailable) {
        override fun getText(): String = mText

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is TextMessage) return false
            return other.text == text
                    && other.id == id
                    && other.isRead == isRead
        }

        override fun hashCode(): Int {
            var result = 17
            result = 31 * result + text.hashCode()
            result = 31 * result + id.hashCode()
            result = 31 * result + isRead.hashCode()
            return result
        }

        override fun toString(): String {
            return "$id $mText"
        }

        override fun getCreatedAt(): Date {
            return mDate ?: date
        }

        override fun getBackgroundColor(): Int? = bgColor

        override fun getIsRead(): Boolean {
            return read ?: false
        }

        override fun setIsRead(isRead: Boolean) {
            read = isRead
        }
    }

    open class FileMessage(
            mId: String,
            mUser: ChatUser,
            val bgColor: Int? = null,
            var read: Boolean? = null,
            val file: File? = null,
            val mDate: Date? = null,
            chatAvailable: Boolean? = null
    ) : Message(mId, mUser, chatAvailable) {
        override fun getText(): String = file?.name ?: ""

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is ImageMessage) return false
            return other.file == file
                    && other.id == id
                    && other.isRead == isRead
        }

        override fun hashCode(): Int {
            var result = 17
            result = 31 * result + text.hashCode()
            result = 31 * result + file.hashCode()
            result = 31 * result + id.hashCode()
            result = 31 * result + isRead.hashCode()
            return result
        }

        override fun toString(): String {
            return "$id ${file?.name}"
        }

        override fun getCreatedAt(): Date {
            return mDate ?: date
        }

        override fun getBackgroundColor(): Int? = bgColor

        override fun getIsRead(): Boolean {
            return read ?: false
        }

        override fun setIsRead(isRead: Boolean) {
            read = isRead
        }
    }

    class ImageMessage(
            mId: String,
            mUser: ChatUser,
            bgColor: Int? = null,
            read: Boolean? = null,
            file: File? = null,
            mDate: Date? = null,
            chatAvailable: Boolean? = null
    ) : FileMessage(mId, mUser, bgColor, read, file, mDate, chatAvailable), Image {
        override fun getImageUrl(): String? {
            return file?.link
        }

        override fun needScroll(): Boolean {
            return true
        }
    }

    class StatusChangedMessage(
            val status: RequestStatus,
            val event: WebSocketChatEvent,
            val bgColor: Int,
            val silent: Boolean = false,
            val mText: String,
            val mDate: Date? = null,
            mUser: ChatUser,
            chatAvailable: Boolean? = null
    ) : Message(UUID.randomUUID().toString(), mUser, chatAvailable) {
        override fun getText(): String = mText

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is TextMessage) return false
            return other.text == text && other.id == id
        }

        override fun hashCode(): Int {
            var result = 17
            result = 31 * result + text.hashCode()
            result = 31 * result + id.hashCode()
            return result
        }

        override fun toString(): String {
            return "$id $mText"
        }

        override fun getCreatedAt(): Date {
            return mDate ?: date
        }

        override fun getBackgroundColor(): Int? = bgColor
    }

    class TypeInOutMessage(
            val typing: Boolean,
            val interlocutor: Boolean,
            mUser: ChatUser,
            chatAvailable: Boolean? = null
    ) : Message(UUID.randomUUID().toString(), mUser, chatAvailable) {
        override fun getText(): String = "$typing"
    }

    class StatusReadMessage(
            uuidMessage: String,
            chatAvailable: Boolean?
    ) : Message(uuidMessage, ChatUser(), chatAvailable)

    class HistoryPageMessage(
            val history: List<IMessage>,
            val totalMessages: Int,
            chatAvailable: Boolean? = null
    ) : Message(UUID.randomUUID().toString(), ChatUser(), chatAvailable)

    class ConnectionMessage(
            val connectionToken: String,
            mUser: ChatUser,
            chatAvailable: Boolean? = null
    ) : Message(UUID.randomUUID().toString(), mUser, chatAvailable)

    class RoomMessage(
            val room: String,
            val isSystem: Boolean,
            mUser: ChatUser,
            chatAvailable: Boolean? = null
    ) : Message(UUID.randomUUID().toString(), mUser, chatAvailable)

    class ErrorMessage(
            val errorMessage: String,
            val errorCode: String, mUser: ChatUser
    ) : Message(UUID.randomUUID().toString(), mUser) {
        override fun getText(): String = errorMessage
    }

    class TutorialMessage : Message(UUID.randomUUID().toString(), ChatUser(), null) {
        override fun noDate(): Boolean = true
    }
}