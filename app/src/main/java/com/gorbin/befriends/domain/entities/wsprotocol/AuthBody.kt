package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class AuthBody (
        @SerializedName("accessToken")
        val accessToken: String
)