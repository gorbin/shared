package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class ChatHistoryBody(
        @SerializedName("limit")
        val limit: Int,
        @SerializedName("beginningWith")
        val startMessageUuid: String? = null,
        @SerializedName("connectionToken")
        val connectionToken: String,
        @SerializedName("room")
        val room: String
)