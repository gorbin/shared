package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class CommandBody (
        @SerializedName("connectionToken")
        val connectionToken: String,
        @SerializedName("room")
        val room: String,
        @SerializedName("text")
        val text: String? = null,
        @SerializedName("comment")
        val comment: RejectionReview? = null
)