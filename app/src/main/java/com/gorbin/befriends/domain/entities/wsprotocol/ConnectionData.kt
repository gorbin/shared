package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class ConnectionData(
        @SerializedName("connectionToken")
        val connectionToken: String
)