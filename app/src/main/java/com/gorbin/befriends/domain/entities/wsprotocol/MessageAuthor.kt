package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

enum class MessageAuthor(val id: String) {
    @SerializedName("fromMe")
    ME("MessageAuthorMe"),
    @SerializedName("fromInterlocutor")
    OTHER("MessageAuthorOther")
}
