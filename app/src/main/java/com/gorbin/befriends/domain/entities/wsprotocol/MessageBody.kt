package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class MessageBody(
        @SerializedName("connectionToken")
        val connectionToken: String,
        @SerializedName("room")
        val room: String,
        @SerializedName("message")
        val message: ChatMessage
)

data class ChatMessage(
        @SerializedName("text")
        val text: String? = null,
        @SerializedName("files")
        val files: List<MessageFile>? = null
)

data class MessageFile(
        @SerializedName("uuid")
        val uuid: String
)