package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.data.gson.ServerDateAtZDeserializer
import com.gorbin.befriends.domain.entities.File
import java.util.*

data class MessageInfo(
        @SerializedName("id")
        val id: Long? = null,
        @SerializedName("dateTime")
        @JsonAdapter(ServerDateAtZDeserializer::class)
        val dateTime: Date? = null,
        @SerializedName("uuidMessage", alternate = ["uuid"])
        val uuidMessage: String? = null,
        @SerializedName("uuidRequest")
        val uuidRequest: String? = null,
        @SerializedName("text", alternate = ["message"])
        val text: String? = null,
        @SerializedName("files")
        val files: List<File>? = null,
        @SerializedName("readed", alternate = ["statusRead"])
        val read: Boolean? = null,
        @SerializedName("whoWrote")
        val author: MessageAuthor,
        @SerializedName("marker")
        val marker: MessageMarker
)
