package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

enum class MessageMarker {
    @SerializedName("%User")
    USER,
    @SerializedName("%RequestRejected")
    REQUEST_REJECTED,
    @SerializedName("%RequestAccepted")
    REQUEST_ACCEPTED,
    @SerializedName("%RequestWithdrawn")
    REQUEST_WITHDRAW,
    @SerializedName("%ExecutionConfirmationRequested")
    EXECUTION_CONFIRMATION_REQUESTED,
    @SerializedName("%ExecutionConfirmationRequestedTimer")
    EXECUTION_CONFIRMATION_REQUEST_TIMER,
    @SerializedName("%ExecutionConfirmed")
    EXECUTION_CONFIRMED,
    @SerializedName("%ExecutionConfirmedAuto")
    EXECUTION_CONFIRMED_AUTO,
    @SerializedName("%ExecutionRejected")
    EXECUTION_REJECTED,
    @SerializedName("%NewRequest")
    NEW_REQUEST
}