package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.domain.entities.ServiceType

data class RejectionReview(
        @SerializedName("text")
        val comment: String,
        @SerializedName("type")
        val type: ServiceType,
        @SerializedName("qualification")
        val professionalism: Int,
        @SerializedName("courtesy")
        val politeness: Int,
        @SerializedName("speedResponse")
        val performance: Int
)