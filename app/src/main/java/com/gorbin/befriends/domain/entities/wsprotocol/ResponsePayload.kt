package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.data.gson.ServerDateAtZDeserializer
import com.gorbin.befriends.domain.entities.RequestStatus
import java.util.*

data class ResponsePayload (
        @SerializedName("connectionToken")
        val connectionToken: String? = null,
        @SerializedName("room")
        val room: String? = null,
        @SerializedName("message")
        val message: MessageInfo? = null,
        @SerializedName("text")
        val text: String? = null,
        @SerializedName("messages")
        val messages: List<MessageInfo>? = null,
        @SerializedName("totalMessages")
        val totalMessages: Int? = null,
        @SerializedName("timeWaiting")
        @JsonAdapter(ServerDateAtZDeserializer::class)
        val timeWaiting: Date? = null,
        @SerializedName("timeClosed")
        @JsonAdapter(ServerDateAtZDeserializer::class)
        val timeClosed: Date? = null,
        @SerializedName("access")
        val chatAvailable: Boolean? = null,
        @SerializedName("statusRoom")
        val requestStatus: RequestStatus? = null,
        @SerializedName("marker")
        val marker: MessageMarker? = null,
        @SerializedName("dateTime")
        @JsonAdapter(ServerDateAtZDeserializer::class)
        val dateTime: Date? = null
)
