package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class RoomConnectionBody (
        @SerializedName("connectionToken")
        val connectionToken: String,
        @SerializedName("room")
        val room: String
)