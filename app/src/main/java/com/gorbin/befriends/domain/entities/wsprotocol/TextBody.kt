package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class TextBody(
        @SerializedName("text")
        val text: String
)