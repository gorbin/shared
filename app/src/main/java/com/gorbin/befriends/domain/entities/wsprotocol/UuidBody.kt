package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

data class UuidBody (
        @SerializedName("uuid")
        val uuid: String
)