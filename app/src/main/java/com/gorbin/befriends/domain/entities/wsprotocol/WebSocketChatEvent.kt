package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName

enum class WebSocketChatEvent {
    @SerializedName("auth")
    AUTH,
    @SerializedName("initRoom")
    ROOM_CONNECTION,
    @SerializedName("sendMessage")
    MESSAGE_SENDING,
    @SerializedName("newMessage")
    NEW_MESSAGE,
    @SerializedName("listMessageHistory")
    HISTORY,
    @SerializedName("imTyping")
    TYPING,
    @SerializedName("imTypingStop")
    STOP_TYPING,
    @SerializedName("interlocutorTyping")
    INTERLOCUTOR_TYPING,
    @SerializedName("interlocutorTypingStop")
    INTERLOCUTOR_TYPING_STOP,
    @SerializedName("setReaded", alternate = ["sendReaded"])
    MESSAGE_READ,
    @SerializedName("AcceptRequest")
    COMMAND_ACCEPT_REQUEST,
    @SerializedName("RejectRequest")
    COMMAND_REJECT_REQUEST,
    @SerializedName("WithdrawRequest")
    COMMAND_WITHDRAW_REQUEST,
    @SerializedName("AskForConfirmation")
    COMMAND_REQUEST_EXECUTION_CONFIRMATION,
    @SerializedName("ConfirmExecution")
    COMMAND_CONFIRM_EXECUTION,
    @SerializedName("RejectExecution")
    COMMAND_REJECT_EXECUTION
}