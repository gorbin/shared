package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.domain.websocket.MessageType

sealed class WrappedMessage(
    @SerializedName("event")
    val event: WebSocketChatEvent,
    @SerializedName("type")
    val type: MessageType
)

class MessageRequest<T>(
        event: WebSocketChatEvent,
        type: MessageType,
        @SerializedName("payload")
        val payload: T
) : WrappedMessage(event, type)

class MessageResponse(
        event: WebSocketChatEvent,
        type: MessageType,
        @SerializedName("payload")
        val payload: ResponsePayload,
        @SerializedName("state")
        val state: WsState? = null
) : WrappedMessage(event, type)