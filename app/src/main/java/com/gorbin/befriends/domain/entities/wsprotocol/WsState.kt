package com.gorbin.befriends.domain.entities.wsprotocol

import com.google.gson.annotations.SerializedName
import com.gorbin.befriends.data.api.model.ApiError

data class WsState (
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("error")
        val error: ApiError
)