package com.gorbin.befriends.domain.entities.wsprotocol

class WsStatus private constructor() {
    enum class Type {
        INTERNAL_ERROR,
        NONE,
        WS_ERROR,

        STATUS_CONNECTING,
        STATUS_CONNECTED,
        STATUS_DISCONNECTING,
        STATUS_DISCONNECTED,
        STATUS_OPEN
    }

    var cause: Throwable? = null
        private set
    var type: Type? = null
        private set

    class Builder {
        private val status: WsStatus = WsStatus()

        init {
            status.type = Type.NONE
        }

        fun with(cause: Throwable): Builder {
            status.cause = cause
            status.type = Type.INTERNAL_ERROR
            return this
        }

        fun with(type: Type): Builder {
            status.type = type
            return this
        }

        fun make(): WsStatus {
            return status
        }
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("WsStatus {")
        sb.append(type)
        if (cause != null) {
            sb.append(", cause= ")
            sb.append(cause)
        }
        sb.append("}")
        return sb.toString()
    }
}