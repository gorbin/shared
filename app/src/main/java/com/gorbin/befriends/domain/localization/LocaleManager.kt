package com.gorbin.befriends.domain.localization

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import com.google.common.base.Optional
import com.gorbin.befriends.presentation.base.activity.AppActivity
import com.gorbin.befriends.utils.GlobalValues
import com.jakewharton.processphoenix.ProcessPhoenix
import com.punicapp.rxrepocore.IKeyValueRepository
import com.punicapp.rxrepocore.IRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.*

class LocaleManager(
        private val localeContext: Context,
        private val localRepo: IKeyValueRepository<String>,
        private val langRepo: IRepository<Locale>
) {
    init {
        langRepo.saveAll(listOf(
                Locale.forLanguageTag("en"),
                Locale.forLanguageTag("ru"),
                Locale.forLanguageTag("es"),
                Locale.forLanguageTag("fr"),
                Locale.forLanguageTag("de"),
                Locale.forLanguageTag("pt"),
                Locale.forLanguageTag("hi"),
                Locale.forLanguageTag("id"),
                Locale.forLanguageTag("tr"),
                Locale.forLanguageTag("it")
        )).subscribe { _, _ ->  }
    }

    fun initConfig(context: Context): Context {
        val newLocale = findLanguage()
        Locale.setDefault(newLocale)

        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            val config = Configuration(context.resources.configuration)
            config.setLocale(newLocale)
            context.createConfigurationContext(config)
        } else {
            val res = context.resources
            val config = res.configuration
            config.locale = newLocale
            res.updateConfiguration(config, res.displayMetrics)
            context
        }
    }

    private fun onLanguageChanged() {
        val i = Intent(localeContext, AppActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        ProcessPhoenix.triggerRebirth(localeContext, i)
    }

    fun saveLanguage(tag: String): Disposable {
        return Single.zip(
                langRepo.fetch(null, null),
                localRepo[GlobalValues.LANGUAGE_KEY, String::class.java],
                BiFunction { languages: Optional<List<Locale>>, currentLang: Optional<String> ->
                    val availableLocales = languages.or(emptyList())
                    val currentLangTag = currentLang.get()
                    availableLocales
                            .find { it.toLanguageTag() == tag }
                            ?.toLanguageTag()
                            ?.let { it to (currentLangTag != tag) }
                            ?: Locale.ENGLISH.toLanguageTag().let {
                                it to false
                            }
                }).flatMap { result ->
            localRepo.save(GlobalValues.LANGUAGE_KEY, result.first).map { result.second }
        }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { langChanged, _ ->
                    if (langChanged)
                        onLanguageChanged()
                }
    }

    fun findLanguage(): Locale {
        val availableLanguages = langRepo.instantFetch(null, null)
                .or(emptyList())
        val defaultLanguage = availableLanguages.find { it.language == Locale.getDefault().language } ?: Locale.ENGLISH
        val tag = localRepo.instantGet<String>(GlobalValues.LANGUAGE_KEY, String::class.java).orNull()
        return tag?.let { Locale.forLanguageTag(it) }
                ?: localRepo.instantSave(GlobalValues.LANGUAGE_KEY, defaultLanguage.toLanguageTag())
                        .let { Locale.forLanguageTag(it) }
    }
}