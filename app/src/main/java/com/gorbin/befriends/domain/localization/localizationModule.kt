package com.gorbin.befriends.domain.localization

import org.koin.dsl.module

val localizationModule = module {
    single { LocaleManager(get(), get(), get()) }
}