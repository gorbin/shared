package com.gorbin.befriends.domain.usecases

import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single

interface ICommentsUseCase {
    fun sendReview(userUuid: String, review: ReviewBody): Single<Boolean>
    fun getMyRatingAndReviews(sortBy: ReviewSortBy = ReviewSortBy.DATE, sortOrder: SortOrder = SortOrder.DESC, specializations: List<SkillSpecialization>? = null): Single<Pair<UserRating, List<Review>>>
}