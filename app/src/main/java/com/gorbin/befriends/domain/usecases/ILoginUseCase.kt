package com.gorbin.befriends.domain.usecases

import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single

interface ILoginUseCase {
    fun login(data: LoginData, token: String): Single<Boolean>
    fun checkOtp(data: CodePhoneData): Single<SessionData>
    fun requestOtp(data: PhoneData): Single<SignUpTokens>
    fun requestCall(data: PhoneData): Single<PinData>
    fun requestCallCheck(data: CodePhoneData): Single<RestoreTokenData>
    fun requestSetPassword(data: NewPasswordData): Single<SessionData>
    fun newPassword(data: NewPasswordData, token: String): Single<Boolean>
}