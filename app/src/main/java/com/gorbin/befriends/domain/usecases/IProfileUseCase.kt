package com.gorbin.befriends.domain.usecases

import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single
import java.io.File

interface IProfileUseCase {
    fun getProfile(): Single<User>
    fun getGallery(): Single<List<GalleryPhoto>>
    fun setProfilePhoto(photoKey: String): Single<List<GalleryPhoto>>
    fun removePhoto(photoKey: String): Single<List<GalleryPhoto>>
    fun uploadPhoto(photo: File, isAvatar: Boolean): Single<PhotoResponse>
    fun getPriceList(): Single<List<ServicePrice>>
    fun savePriceList(data: PriceList): Single<BooleanResult>
    fun updateAbout(about: String): Single<User>
    fun updateProfile(data: PersonalDataBody): Single<User>
    fun updateCost(cost: String): Single<User>
    fun exit(): Single<String>
    fun updateActivity(activity: Boolean): Single<User>
    fun getAreasAndUserSpecializations(): Single<Pair<List<SpecializationArea>, UserSpecializations>>
    fun updateSpecializations(data: List<SpecializationBody>): Single<BooleanResult>
    fun getLanguages(): Single<List<Language>>
    fun getCountries(): Single<List<Country>>
    fun getEditInfo(): Single<Triple<User, List<Language>, List<Country>>>
}