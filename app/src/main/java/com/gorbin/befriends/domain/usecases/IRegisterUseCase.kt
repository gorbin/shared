package com.gorbin.befriends.domain.usecases

import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single
import java.io.File

interface IRegisterUseCase {
    fun register(data: RegisterRequestData): Single<SignUpTokens>
    fun getAdditionalInfoOptions(): Single<Pair<User, Triple<List<Country>, List<Language>, List<SpecializationArea>>>>
    fun getSpecializationsByArea(id: SpecializationArea): Single<List<Specialization>>
    fun getCityByCountry(id: Country, nameCity: String): Single<List<City>>
    fun registerProfile(photo: File?, personalData: PersonalDataBody): Single<UserShort>
    fun getCityByLocation(latLng: LatLng): Single<City>
}