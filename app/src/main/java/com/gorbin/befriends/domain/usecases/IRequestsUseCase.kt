package com.gorbin.befriends.domain.usecases

import com.gorbin.befriends.domain.entities.BooleanResult
import com.gorbin.befriends.domain.entities.FileResponse
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.ServiceType
import io.reactivex.Single
import java.io.File

interface IRequestsUseCase {
    companion object {
        const val PAGE_SIZE = 15
    }

    fun getMyRequests(page: Int): Single<List<MyRequest>>
    fun getRequestsToMe(page: Int): Single<List<MyRequest>>
    fun createRequest(uuidUser: String, idSpecialization: String, serviceType: ServiceType): Single<MyRequest>
    fun getRequestInfo(uuidRequest: String): Single<MyRequest>
    fun removeRequest(uuidRequest: String): Single<BooleanResult>
    fun uploadFile(file: File): Single<FileResponse>
}