package com.gorbin.befriends.domain.usecases

import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.domain.entities.*
import io.reactivex.Single

interface ISearchUseCase {
    fun getCountries(query: String): Single<List<Country>>
    fun getAdditionalInfoOptions(): Single<Triple<List<Country>, List<Language>, List<SpecializationArea>>>
    fun search(page: Int, sortOrder: SortOrder, sortBy: SearchSortBy, searchForm: SearchForm): Single<List<List<UserExecutor>>>
    fun getCityByLocation(latLng: LatLng): Single<City>
}