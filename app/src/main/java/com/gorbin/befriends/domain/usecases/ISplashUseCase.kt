package com.gorbin.befriends.domain.usecases

import io.reactivex.Single

interface ISplashUseCase {
    fun isRegistered(): Single<Boolean>
}