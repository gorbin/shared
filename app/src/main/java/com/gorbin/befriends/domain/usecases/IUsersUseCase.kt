package com.gorbin.befriends.domain.usecases

import com.gorbin.befriends.domain.entities.ComplaintType
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.ServicePrice
import com.gorbin.befriends.domain.entities.User
import io.reactivex.Single

interface IUsersUseCase {
    fun isAuthorized(): Single<Boolean>
    fun getProfile(userId: String): Single<Triple<User, List<ServicePrice>, List<Review>>>
    fun sendComplaint(uuidUser: String, comment: String, type: ComplaintType): Single<Boolean>
}