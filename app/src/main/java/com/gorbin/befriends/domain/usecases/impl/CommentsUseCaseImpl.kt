package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.ICommentsUseCase
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class CommentsUseCaseImpl(val api: IApiService) : ICommentsUseCase {
    override fun sendReview(userUuid: String, review: ReviewBody): Single<Boolean> {
        return api.sendReview(userUuid, review)
    }

    override fun getMyRatingAndReviews(sortBy: ReviewSortBy, sortOrder: SortOrder, specializations: List<SkillSpecialization>?): Single<Pair<UserRating, List<Review>>> {
        return api.profile().flatMap {
            Single.zip(api.getUserRating(it.uuidUser), api.getReviews(it.uuidUser, sortBy.value, sortOrder.name, specializations?.map { it.filter }),
                    BiFunction { rating: UserRating, reviews: List<Review> -> rating to reviews })
        }
    }
}