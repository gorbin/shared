package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.ILoginUseCase
import io.reactivex.Single

class LoginUseCaseImpl(val api: IApiService) :
        ILoginUseCase {
    override fun login(data: LoginData, token: String): Single<Boolean> {
        return api.login(data, token)
    }

    override fun checkOtp(data: CodePhoneData): Single<SessionData> = api.checkOtp(data)

    override fun requestOtp(data: PhoneData): Single<SignUpTokens> = api.resendOtp(data)

    override fun requestCall(data: PhoneData): Single<PinData> = api.requestCall(data)

    override fun requestCallCheck(data: CodePhoneData): Single<RestoreTokenData> = api.requestCallCheck(data)

    override fun requestSetPassword(data: NewPasswordData): Single<SessionData> = api.requestSetPassword(data)

    override fun newPassword(data: NewPasswordData, token: String): Single<Boolean> {
        return api.newPassword(data, token)
    }
}