package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import java.io.File

class ProfileUseCaseImpl(val api: IApiService, val localRepo: IKeyValueRepository<String>) :
        IProfileUseCase {

    override fun getProfile() = api.profile()
    override fun getGallery(): Single<List<GalleryPhoto>> = api.getGallery()
    override fun setProfilePhoto(photoKey: String): Single<List<GalleryPhoto>> = api.setProfilePhoto(photoKey)
            .flatMap { api.getGallery() }

    override fun removePhoto(photoKey: String): Single<List<GalleryPhoto>> = api.removePhoto(photoKey)
            .flatMap { api.getGallery() }

    override fun uploadPhoto(photo: File, isAvatar: Boolean): Single<PhotoResponse> = api.uploadUserPhoto(null, isAvatar, photo)
    override fun getPriceList(): Single<List<ServicePrice>> = api.profile()
            .flatMap { api.getServicePrices(it.uuidUser) }

    override fun savePriceList(data: PriceList): Single<BooleanResult> = api.saveServicePrices(data)
    override fun updateAbout(about: String) = api.updateAbout(about)
    override fun updateProfile(data: PersonalDataBody): Single<User> {
        return if (!data.phone.isNullOrBlank())
            api.updatePhoneNumber(data.phone)
                    .flatMap { api.updateProfile(data) }
        else
            api.updateProfile(data)
    }

    override fun updateCost(cost: String) = api.updateCost(cost)

    override fun exit(): Single<String> {
        return api.exit(localRepo.get<String>(GlobalValues.DEVICE_UUID, String::class.java).blockingGet().get())
                .flatMap { return@flatMap localRepo.remove(GlobalValues.SESSION_DATA) }
                .onErrorResumeNext { return@onErrorResumeNext Single.just("OK") }
    }

    override fun updateActivity(activity: Boolean): Single<User> = api.updateActivity(activity)
    override fun getAreasAndUserSpecializations(): Single<Pair<List<SpecializationArea>, UserSpecializations>> {
        return Single.zip(api.getSpecializationAreas(), api.getUserSpecializations(), BiFunction { areas, specializations ->
            areas to specializations
        })
    }

    override fun updateSpecializations(data: List<SpecializationBody>): Single<BooleanResult> {
        val body = SpecializationsBody(data)
        return api.updateSpecializations(body)
    }

    override fun getLanguages(): Single<List<Language>> = api.getLanguages()
    override fun getCountries(): Single<List<Country>> = api.getCountries()

    override fun getEditInfo(): Single<Triple<User, List<Language>, List<Country>>> {
        return Single.zip(getProfile(), getLanguages(), getCountries(),
                Function3 { profile, langList, countryList ->
                    Triple(profile, langList, countryList)
                })
    }
}