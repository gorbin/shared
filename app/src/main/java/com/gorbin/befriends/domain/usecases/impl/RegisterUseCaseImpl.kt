package com.gorbin.befriends.domain.usecases.impl

import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRegisterUseCase
import io.reactivex.Single
import io.reactivex.functions.Function4
import io.reactivex.schedulers.Schedulers
import java.io.File

class RegisterUseCaseImpl(val api: IApiService) : IRegisterUseCase {
    override fun register(data: RegisterRequestData) = api.register(data)

    override fun getAdditionalInfoOptions(): Single<Pair<User, Triple<List<Country>, List<Language>, List<SpecializationArea>>>> {
        return Single.zip(api.getCountries(), api.getLanguages(), api.getSpecializationAreas(), api.profile(),
                Function4 { countries: List<Country>, languages: List<Language>, areas: List<SpecializationArea>, profile: User ->
                    profile to Triple(countries, languages, areas)
                }).subscribeOn(Schedulers.io())
    }

    override fun getSpecializationsByArea(area: SpecializationArea): Single<List<Specialization>> {
        return api.getAreaSpecializations(area.id.toString())
    }

    override fun getCityByCountry(id: Country, nameCity: String): Single<List<City>> {
        return api.getCityByCountry(id.id.toString(), nameCity)
    }

    override fun registerProfile(photo: File?, personalData: PersonalDataBody): Single<UserShort> {
        return api.registerProfile(photo, personalData)
    }

    override fun getCityByLocation(latLng: LatLng): Single<City>{
        return api.getCityByLocation(latLng)
    }
}