package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import io.reactivex.Single
import java.io.File

class RequestsUseCaseImpl(private val api: IApiService) : IRequestsUseCase {
    override fun getMyRequests(page: Int): Single<List<MyRequest>> {
        return api.getMyRequests(IRequestsUseCase.PAGE_SIZE, IRequestsUseCase.PAGE_SIZE * (page - 1))
    }

    override fun getRequestsToMe(page: Int): Single<List<MyRequest>> {
        return api.getRequestsToMe(IRequestsUseCase.PAGE_SIZE, IRequestsUseCase.PAGE_SIZE * (page - 1))
    }

    override fun createRequest(uuidUser: String, idSpecialization: String, serviceType: ServiceType): Single<MyRequest> {
        return api.createRequest(RequestCreationBody(uuidUser, idSpecialization, serviceType.id.toString()))
    }

    override fun getRequestInfo(uuidRequest: String): Single<MyRequest> {
        return api.getRequestInfo(uuidRequest)
    }

    override fun removeRequest(uuidRequest: String): Single<BooleanResult> {
        return api.deleteRequest(uuidRequest)
    }

    override fun uploadFile(file: File): Single<FileResponse> {
        return api.uploadFile(file)
    }
}