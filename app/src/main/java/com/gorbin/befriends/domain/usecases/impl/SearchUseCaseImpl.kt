package com.gorbin.befriends.domain.usecases.impl

import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.domain.usecases.ISearchUseCase
import io.reactivex.Single
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers

class SearchUseCaseImpl(val api: IApiService) : ISearchUseCase {
    override fun getCountries(query: String): Single<List<Country>> {
        return api.getCountries(query)
    }

    override fun getAdditionalInfoOptions(): Single<Triple<List<Country>, List<Language>, List<SpecializationArea>>> {
        return Single.zip(api.getCountries(), api.getLanguages(), api.getSpecializationAreas(),
                Function3 { countries: List<Country>, languages: List<Language>, areas: List<SpecializationArea> ->
                    Triple(countries, languages, areas)
                }).subscribeOn(Schedulers.io())
    }

    override fun search(page: Int, sortOrder: SortOrder, sortBy: SearchSortBy, searchForm: SearchForm): Single<List<List<UserExecutor>>> {
        return api.search(IRequestsUseCase.PAGE_SIZE, IRequestsUseCase.PAGE_SIZE * (page - 1), sortBy.value, sortOrder.name, searchForm)
    }

    override fun getCityByLocation(latLng: LatLng): Single<City>{
        return api.getCityByLocation(latLng)
    }
}