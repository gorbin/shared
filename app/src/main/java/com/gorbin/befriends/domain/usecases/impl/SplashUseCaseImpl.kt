package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.usecases.ISplashUseCase
import io.reactivex.Single

class SplashUseCaseImpl(val api: IApiService) : ISplashUseCase {
    override fun isRegistered(): Single<Boolean> = api.profile().map { return@map it.isRegistered() }
}