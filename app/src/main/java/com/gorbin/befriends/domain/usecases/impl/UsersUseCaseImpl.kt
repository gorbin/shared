package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.data.api.IApiService
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IUsersUseCase
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class UsersUseCaseImpl(private val api: IApiService) : IUsersUseCase {
    override fun getProfile(userId: String): Single<Triple<User, List<ServicePrice>, List<Review>>> {
        return api.getProfile(userId).flatMap { user ->
            Single.zip(api.getServicePrices(user.uuidUser).onErrorReturn { emptyList() },
                    api.getReviews(user.uuidUser, ReviewSortBy.DATE.value, SortOrder.DESC.name, null),
                    BiFunction { prices: List<ServicePrice>, reviews: List<Review> ->
                        Triple(user, prices, reviews)
                    })
        }
    }

    override fun isAuthorized(): Single<Boolean> {
        return api.isAuthorized()
    }

    override fun sendComplaint(uuidUser: String, comment: String, type: ComplaintType) =
            api.registerComplaint(uuidUser, comment, type)
}