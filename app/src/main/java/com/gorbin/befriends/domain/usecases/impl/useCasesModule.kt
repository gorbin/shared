package com.gorbin.befriends.domain.usecases.impl

import com.gorbin.befriends.domain.usecases.*
import org.koin.dsl.module

val useCasesModule = module {
    single<ISplashUseCase> { SplashUseCaseImpl(api = get()) }
    single<ILoginUseCase> { LoginUseCaseImpl(api = get()) }
    single<IRegisterUseCase> { RegisterUseCaseImpl(api = get()) }
    single<IProfileUseCase> { ProfileUseCaseImpl(api = get(), localRepo = get()) }
    single<ISearchUseCase> { SearchUseCaseImpl(api = get()) }
    single<IRequestsUseCase> { RequestsUseCaseImpl(api = get()) }
    single<IUsersUseCase> { UsersUseCaseImpl(api = get()) }
    single<ICommentsUseCase> { CommentsUseCaseImpl(api = get()) }
}