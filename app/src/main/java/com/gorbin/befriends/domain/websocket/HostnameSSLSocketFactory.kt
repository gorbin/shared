package com.gorbin.befriends.domain.websocket

import android.net.SSLCertificateSocketFactory
import java.net.InetAddress
import java.net.Socket
import javax.net.ssl.SSLSocketFactory

class HostnameSSLSocketFactory(
        private val delegate: SSLCertificateSocketFactory,
        private val hostname: String
) : SSLSocketFactory() {
    private fun Socket.setHostname(): Socket = also { delegate.setHostname(it, hostname) }

    override fun createSocket(s: Socket?, host: String?, port: Int, autoClose: Boolean): Socket {
        return delegate.createSocket(s, host, port, autoClose).setHostname()
    }

    override fun createSocket(): Socket {
        return delegate.createSocket().setHostname()
    }

    override fun createSocket(host: String?, port: Int): Socket {
        return delegate.createSocket(host, port).setHostname()
    }

    override fun createSocket(host: String?, port: Int, localHost: InetAddress?, localPort: Int): Socket {
        return delegate.createSocket(host, port, localHost, localPort).setHostname()
    }

    override fun createSocket(host: InetAddress?, port: Int): Socket {
        return delegate.createSocket(host, port).setHostname()
    }

    override fun createSocket(address: InetAddress?, port: Int, localAddress: InetAddress?, localPort: Int): Socket {
        return delegate.createSocket(address, port, localAddress, localPort).setHostname()
    }

    override fun getDefaultCipherSuites(): Array<String> {
        return delegate.defaultCipherSuites
    }

    override fun getSupportedCipherSuites(): Array<String> {
        return delegate.supportedCipherSuites
    }
}