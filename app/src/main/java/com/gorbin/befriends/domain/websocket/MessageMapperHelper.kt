package com.gorbin.befriends.domain.websocket

import android.content.Context
import androidx.core.content.ContextCompat
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.RequestStatus
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.chat.ChatUser
import com.gorbin.befriends.domain.entities.chat.Message
import com.gorbin.befriends.domain.entities.wsprotocol.*
import com.stfalcon.chatkit.commons.models.IMessage
import java.util.*

class MessageMapperHelper(private val context: Context) {
    companion object {
        private const val OFFSET_MILLIS = 2 * 24 * 60 * 60 * 1000
        private val IMG_TYPES = listOf(".jpg", "jpg", ".jpeg", "jpeg", ".png", "png")
    }

    private val companion = ChatUser(userId = MessageAuthor.OTHER.id, userName = "", userAvatar = null, userAvatarRes = null)
    private val bf = ChatUser(userId = MessageAuthor.OTHER.id, userName = "", userAvatar = null, userAvatarRes = null)
    private val self = ChatUser(userId = MessageAuthor.ME.id, userName = "", userAvatar = null, userAvatarRes = null)
    private var role: Role = Role.CUSTOMER

    fun setRole(role: Role) {
        this.role = role
    }

    fun setInterlocutorName(name: String) {
        if (name.isBlank())
            return

        companion.userName = name
    }

    private fun MessageInfo.getSuitableUser(): ChatUser {
        return if (marker == MessageMarker.USER) {
            if (author == MessageAuthor.ME)
                self
            else
                companion
        } else {
            bf
        }
    }

    private fun MessageInfo.getSuitableBgColor(): Int? {
        return when (marker) {
            MessageMarker.NEW_REQUEST,
            MessageMarker.REQUEST_ACCEPTED,
            MessageMarker.EXECUTION_CONFIRMATION_REQUEST_TIMER,
            MessageMarker.EXECUTION_CONFIRMATION_REQUESTED,
            MessageMarker.EXECUTION_CONFIRMED,
            MessageMarker.EXECUTION_CONFIRMED_AUTO -> {
                ContextCompat.getColor(context, R.color.colorGreenWave)
            }

            MessageMarker.EXECUTION_REJECTED,
            MessageMarker.REQUEST_WITHDRAW,
            MessageMarker.REQUEST_REJECTED -> {
                ContextCompat.getColor(context, R.color.colorRedWave)
            }
            else -> null
        }
    }

    private fun getMessageByMarker(marker: MessageMarker, additionalText: String?, dateTime: Date?): String {
        return when (marker) {
            MessageMarker.NEW_REQUEST -> {
                when (role) {
                    Role.CUSTOMER -> context.getString(R.string.status_to_customer_new_request)
                    Role.EXECUTOR -> context.getString(R.string.status_to_executor_new_request)
                }
            }
            MessageMarker.REQUEST_ACCEPTED -> {
                when (role) {
                    Role.CUSTOMER -> String.format(context.getString(R.string.status_to_customer_request_accepted), companion.userName)
                    Role.EXECUTOR -> context.getString(R.string.status_to_executor_request_accepted)
                }
            }
            MessageMarker.REQUEST_REJECTED -> {
                val title = when (role) {
                    Role.CUSTOMER -> String.format(context.getString(R.string.status_to_customer_request_rejected), companion.userName)
                    Role.EXECUTOR -> context.getString(R.string.status_to_executor_request_rejected)
                }
                additionalText?.let { "$title\n\"$it\"" } ?: title
            }
            MessageMarker.REQUEST_WITHDRAW -> {
                val title = when (role) {
                    Role.CUSTOMER -> context.getString(R.string.status_to_customer_request_withdrawn)
                    Role.EXECUTOR -> String.format(context.getString(R.string.status_to_executor_request_withdrawn), companion.userName)
                }
                additionalText?.let { "$title\n\"$it\"" } ?: title
            }
            MessageMarker.EXECUTION_CONFIRMATION_REQUESTED -> {
                when (role) {
                    Role.CUSTOMER -> String.format(context.getString(R.string.status_to_customer_execution_confirmation_requested), companion.userName)
                    Role.EXECUTOR -> context.getString(R.string.status_to_executor_execution_confirmation_requested)
                }
            }
            MessageMarker.EXECUTION_CONFIRMATION_REQUEST_TIMER -> {
                var hoursLeft = "0"
                var minutesLeft = "0"
                dateTime?.let { date ->
                    val differenceInSeconds = (date.time + OFFSET_MILLIS - System.currentTimeMillis()) / 1000
                    if (differenceInSeconds < 0) return@let
                    val hours = (differenceInSeconds / 60.0 / 60).toInt()
                    hoursLeft = hours.toString()
                    val minutes = (differenceInSeconds / 60.0).toInt() - hours * 60
                    minutesLeft = minutes.toString()
                }
                String.format(context.getString(R.string.status_execution_confirmation_requested_timer), hoursLeft, minutesLeft)
            }
            MessageMarker.EXECUTION_CONFIRMED -> {
                when (role) {
                    Role.CUSTOMER -> context.getString(R.string.status_to_customer_execution_confirmed)
                    Role.EXECUTOR -> String.format(context.getString(R.string.status_to_executor_execution_confirmed), companion.userName)
                }
            }
            MessageMarker.EXECUTION_CONFIRMED_AUTO -> {
                when (role) {
                    Role.CUSTOMER -> context.getString(R.string.status_to_customer_execution_confirmed_auto)
                    Role.EXECUTOR -> String.format(context.getString(R.string.status_to_executor_execution_confirmed_auto), companion.userName)
                }
            }
            MessageMarker.EXECUTION_REJECTED -> {
                when (role) {
                    Role.CUSTOMER -> context.getString(R.string.status_to_customer_execution_rejected)
                    Role.EXECUTOR -> String.format(context.getString(R.string.status_to_executor_execution_rejected), companion.userName)
                }
            }
            else -> ""
        }
    }

    private fun MessageInfo.toChatMessage(chatAvailable: Boolean? = null): IMessage {
        val message: String = if (marker == MessageMarker.USER)
            text ?: ""
        else
            getMessageByMarker(marker, text, dateTime)

        return when {
            files.isNullOrEmpty() -> {
                Message.TextMessage(
                        uuidMessage ?: UUID.randomUUID().toString(),
                        getSuitableUser(),
                        getSuitableBgColor(),
                        read,
                        message,
                        dateTime,
                        chatAvailable
                )
            }
            IMG_TYPES.contains(files.first().extension) -> {
                Message.ImageMessage(
                        uuidMessage ?: UUID.randomUUID().toString(),
                        getSuitableUser(),
                        getSuitableBgColor(),
                        read,
                        files.first(),
                        dateTime,
                        chatAvailable
                )
            }
            else -> {
                Message.FileMessage(
                        uuidMessage ?: UUID.randomUUID().toString(),
                        getSuitableUser(),
                        getSuitableBgColor(),
                        read,
                        files.first(),
                        dateTime,
                        chatAvailable
                )
            }
        }
    }

    fun convertToIMessage(message: MessageResponse, photoUrl: String?): IMessage? {
        companion.userAvatar = photoUrl

        fun checkError(message: MessageResponse): Message.ErrorMessage? {
            return message.state
                    ?.takeIf {
                        !it.status || it.error.code != 0
                    }?.let {
                        Message.ErrorMessage(it.error.message, it.error.code.toString(), bf)
                    }
        }

        return checkError(message) ?: when (message.event) {
            WebSocketChatEvent.AUTH -> {
                Message.ConnectionMessage(message.payload.connectionToken!!, bf, message.payload.chatAvailable)
            }
            WebSocketChatEvent.ROOM_CONNECTION -> {
                if (message.payload.marker == MessageMarker.NEW_REQUEST) {
                    Message.StatusChangedMessage(
                            RequestStatus.NEW,
                            WebSocketChatEvent.NEW_MESSAGE,
                            ContextCompat.getColor(context, R.color.colorGreenWave),
                            false,
                            getMessageByMarker(message.payload.marker, null, null),
                            null,
                            bf,
                            null
                    )
                } else {
                    Message.RoomMessage(message.payload.room!!, message.type == MessageType.SYSTEM, bf, message.payload.chatAvailable)
                }
            }
            WebSocketChatEvent.MESSAGE_SENDING -> {
                message.payload.message!!.toChatMessage(message.payload.chatAvailable)
            }
            WebSocketChatEvent.NEW_MESSAGE -> {
                message.payload.message!!.toChatMessage(message.payload.chatAvailable)
            }
            WebSocketChatEvent.HISTORY -> {
                message.payload.messages?.map {
                    it.toChatMessage()
                }!!.let {
                    Message.HistoryPageMessage(it, message.payload.totalMessages!!, message.payload.chatAvailable)
                }
            }
            WebSocketChatEvent.INTERLOCUTOR_TYPING -> {
                Message.TypeInOutMessage(typing = true, interlocutor = true, mUser = companion, chatAvailable = message.payload.chatAvailable)
            }
            WebSocketChatEvent.INTERLOCUTOR_TYPING_STOP -> {
                Message.TypeInOutMessage(typing = false, interlocutor = true, mUser = companion, chatAvailable = message.payload.chatAvailable)
            }
            WebSocketChatEvent.TYPING -> {
                Message.TypeInOutMessage(typing = true, interlocutor = false, mUser = self, chatAvailable = message.payload.chatAvailable)
            }
            WebSocketChatEvent.STOP_TYPING -> {
                Message.TypeInOutMessage(typing = false, interlocutor = false, mUser = self, chatAvailable = message.payload.chatAvailable)
            }
            WebSocketChatEvent.MESSAGE_READ -> {
                Message.StatusReadMessage(message.payload.message?.uuidMessage!!, message.payload.chatAvailable)
            }
            WebSocketChatEvent.COMMAND_REQUEST_EXECUTION_CONFIRMATION -> {
                val marker = message.payload.marker!!
                Message.StatusChangedMessage(
                        message.payload.requestStatus!!,
                        message.event,
                        ContextCompat.getColor(context, R.color.colorGreenWave),
                        marker == MessageMarker.EXECUTION_CONFIRMATION_REQUEST_TIMER,
                        getMessageByMarker(
                                marker,
                                message.payload.text,
                                message.payload.dateTime ?: message.payload.timeWaiting
                        ),
                        message.payload.dateTime,
                        bf,
                        message.payload.chatAvailable
                )
            }
            WebSocketChatEvent.COMMAND_ACCEPT_REQUEST,
            WebSocketChatEvent.COMMAND_CONFIRM_EXECUTION -> {
                Message.StatusChangedMessage(
                        message.payload.requestStatus!!,
                        message.event,
                        ContextCompat.getColor(context, R.color.colorGreenWave),
                        false,
                        getMessageByMarker(
                                message.payload.marker!!,
                                message.payload.text,
                                message.payload.dateTime
                        ),
                        message.payload.dateTime,
                        bf,
                        message.payload.chatAvailable
                )
            }
            WebSocketChatEvent.COMMAND_REJECT_REQUEST,
            WebSocketChatEvent.COMMAND_WITHDRAW_REQUEST,
            WebSocketChatEvent.COMMAND_REJECT_EXECUTION -> {
                Message.StatusChangedMessage(
                        message.payload.requestStatus!!,
                        message.event,
                        ContextCompat.getColor(context, R.color.colorRedWave),
                        false,
                        getMessageByMarker(
                                message.payload.marker!!,
                                message.payload.text,
                                message.payload.dateTime
                        ),
                        message.payload.dateTime,
                        bf,
                        message.payload.chatAvailable
                )
            }
        }
    }
}
