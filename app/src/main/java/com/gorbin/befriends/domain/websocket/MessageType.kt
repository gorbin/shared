package com.gorbin.befriends.domain.websocket

import com.google.gson.annotations.SerializedName

enum class MessageType {
    @SerializedName("request")
    REQUEST,
    @SerializedName("response")
    RESPONSE,
    @SerializedName("system")
    SYSTEM
}