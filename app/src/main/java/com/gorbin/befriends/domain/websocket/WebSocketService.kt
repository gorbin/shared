package com.gorbin.befriends.domain.websocket

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.gorbin.befriends.BuildConfig
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.SessionData
import com.gorbin.befriends.domain.entities.chat.Message
import com.gorbin.befriends.domain.entities.wsprotocol.*
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.onChanged
import com.gorbin.befriends.utils.safeLet
import com.punicapp.rxrepocore.IKeyValueRepository
import com.stfalcon.chatkit.commons.models.IMessage
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class WebSocketService(val gson: Gson, val localRepo: IKeyValueRepository<String>, context: Context) {
    private var webSocketWrapper: WebSocketWrapper = WebSocketWrapper(gson, context)
            .apply {
                val d = getStatusWatcher()
                        .observeOn(AndroidSchedulers.mainThread())
                        .flatMap { status ->
                            if (BuildConfig.DEBUG) {
                                Log.d(WebSocketWrapper.SOCKET_CHAT, "wsStatus = $status")
                            }

                            val show = status.type != WsStatus.Type.STATUS_OPEN
                            localRepo.save(GlobalValues.SHOW_CHAT_PUSH_NOTIFICATIONS, show).toObservable()
                        }.subscribe({
                            //Do nothing
                        }, { err ->
                            Log.d("Exception", "err = $err", err)
                        })
            }
    private var connected: Boolean = false
    private var connectionToken: String? = null
    private var room = ObservableField<String>().onChanged { webSocketWrapper.updateRoom(it) }
    var baseUrl: String? = null

    fun hasConnectionToken(): Boolean = connectionToken != null
    fun hasRoom(): Boolean = room.get() != null

    fun clearSession() {
        connectionToken = null
        room.set(null)
    }

    fun setRoom(room: String) {
        this.room.set(room)
    }

    fun setInterlocutorPhoto(photoUri: String?) {
        webSocketWrapper.photoUri = photoUri
    }

    fun setRole(role: Role) {
        webSocketWrapper.setRole(role)
    }

    fun setInterlocutorName(name: String) {
        webSocketWrapper.setInterlocutorName(name)
    }

    fun startOrResumeSession() {
        if (connected)
            return

        connected = baseUrl?.let {
            webSocketWrapper.startOrResumeSession(it)
        } ?: false

        Log.d(WebSocketWrapper.SOCKET_CHAT, "Base url: $baseUrl")
    }

    fun stopSession() {
        webSocketWrapper.stopSession()
        connected = false
        room.set(null)
        connectionToken = null
    }

    fun markMessageRead(messageUuid: String) {
        safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    WebSocketChatEvent.MESSAGE_READ,
                    MessageType.REQUEST,
                    MessageReadBody(connectionToken, room, UuidBody(messageUuid))
            )
        }?.send()
    }

    fun sendCommand(event: WebSocketChatEvent, text: String? = null, comment: RejectionReview? = null) {
        safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    event,
                    MessageType.REQUEST,
                    CommandBody(connectionToken, room, text, comment)
            )
        }?.send()
    }

    fun sendFile(uuid: String) {
        wrapFileMessage(uuid)?.send()
    }

    fun sendMessage(text: String) {
        wrapTextMessage(text)?.send()
    }

    fun sendTypingStatus(isTyping: Boolean) {
        wrapTypingMessage(isTyping)?.send()
    }

    fun getStatusWatcher(): Observable<WsStatus> {
        return webSocketWrapper.getStatusWatcher()
    }

    fun getResponseChain(): Observable<IMessage> {
        return webSocketWrapper.getResponseChain()
                .doOnNext {
                    when (it) {
                        is Message.ConnectionMessage -> connectionToken = it.connectionToken
                        is Message.RoomMessage -> room.set(it.room)
                    }
                }
    }

    fun authorize(): MessageRequest<AuthBody>? {
        return if (connectionToken == null)
        localRepo.instantGet<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java).orNull()?.tokens?.accessToken?.let {
            MessageRequest(
                    WebSocketChatEvent.AUTH,
                    MessageType.REQUEST,
                    AuthBody(it)
            ).also { it.send() }
        } else null
    }

    fun connectToTheRoom(room: String): MessageRequest<RoomConnectionBody>? {
        return connectionToken?.let {
            MessageRequest(
                    WebSocketChatEvent.ROOM_CONNECTION,
                    MessageType.REQUEST,
                    RoomConnectionBody(it, room)
            ).also { it.send() }
        }
    }

    fun requestChatHistory(startMessageUuid: String? = null) {
        safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    WebSocketChatEvent.HISTORY,
                    MessageType.REQUEST,
                    ChatHistoryBody(
                            GlobalValues.CHAT_PAGE_SIZE,
                            startMessageUuid,
                            connectionToken,
                            room
                    )
            ).also { it.send() }
        }
    }

    private fun wrapTextMessage(text: String): MessageRequest<MessageBody>? {
        return safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    WebSocketChatEvent.MESSAGE_SENDING,
                    MessageType.REQUEST,
                    MessageBody(connectionToken, room, ChatMessage(text = text))
            )
        }
    }

    private fun wrapFileMessage(uuid: String): MessageRequest<MessageBody>? {
        return safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    WebSocketChatEvent.MESSAGE_SENDING,
                    MessageType.REQUEST,
                    MessageBody(connectionToken, room, ChatMessage(files = listOf(MessageFile(uuid))))
            )
        }
    }

    private fun wrapTypingMessage(isTyping: Boolean): MessageRequest<RoomConnectionBody>? {
        return safeLet(connectionToken, room.get()) { connectionToken, room ->
            MessageRequest(
                    if (isTyping) WebSocketChatEvent.TYPING else WebSocketChatEvent.STOP_TYPING,
                    MessageType.REQUEST,
                    RoomConnectionBody(connectionToken, room)
            )
        }
    }

    private fun MessageRequest<*>.send() {
        webSocketWrapper.queueMessage(gson.toJson(this).also {
            Log.d(WebSocketWrapper.SOCKET_CHAT, "Wrapped message: $it")
        })
    }
}