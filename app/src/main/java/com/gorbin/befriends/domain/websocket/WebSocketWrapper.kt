package com.gorbin.befriends.domain.websocket

import android.content.Context
import android.net.SSLCertificateSocketFactory
import android.os.Build
import android.util.Log
import com.google.gson.Gson
import com.gorbin.befriends.BuildConfig
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.wsprotocol.MessageResponse
import com.gorbin.befriends.domain.entities.wsprotocol.WsStatus
import com.neovisionaries.ws.client.*
import com.punicapp.core.utils.executors.ExecutorProvider
import com.stfalcon.chatkit.commons.models.IMessage
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.IOException

class WebSocketWrapper(private val gson: Gson, context: Context) {
    companion object {
        const val SOCKET_CHAT = "SOCKET_CHAT"
    }

    var photoUri: String? = null

    private val statusObserver = PublishSubject.create<WsStatus>()
    private val responseObserver = PublishSubject.create<String>()
    private val requestSender = PublishSubject.create<String>()

    private var responseChain: Observable<IMessage> = initResponseChain()

    private val messageHelper: MessageMapperHelper = MessageMapperHelper(context)
    private val wsFactory: WebSocketFactory = WebSocketFactory().apply {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q ) {
            val delegate = SSLCertificateSocketFactory.getDefault(1000) as SSLCertificateSocketFactory
            val factory = HostnameSSLSocketFactory(delegate, BuildConfig.SOCKET_HOSTNAME)
            sslSocketFactory = factory
        }
    }
    private var webSocket: WebSocket? = null
    private var requestDisposable: Disposable? = null
    private var room: String? = null

    fun updateRoom(room: String?) {
        this.room = room
    }

    fun setRole(role: Role) {
        messageHelper.setRole(role)
    }

    fun setInterlocutorName(name: String) {
        messageHelper.setInterlocutorName(name)
    }

    fun startOrResumeSession(baseUrl: String): Boolean {
        requestDisposable?.dispose()
        requestDisposable = initRequestChain()
                .subscribe(Functions.emptyConsumer(), Consumer<Throwable> { throwable ->
                    statusObserver.onNext(
                            WsStatus.Builder()
                                    .with(throwable)
                                    .make()
                    )
                })

        return try {
            if (!reconnect()) {
                webSocket = wsFactory.createSocket(baseUrl)
                        .addListener(buildWssAdapter())
                        .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                        .addExtension(WebSocketExtension.parse("client_max_window_bits"))
                        .connectAsynchronously()
            }
            true
        } catch (e: IOException) {
            val builder = WsStatus.Builder()
            statusObserver.onNext(builder.with(e).make())
            false
        }
    }

    fun stopSession() {
        webSocket?.disconnect()
        requestDisposable?.dispose()
        requestDisposable = null
    }

    private fun reconnect(): Boolean {
        val ws = webSocket
        return try {
            if (ws != null && ws.state == WebSocketState.CLOSED) {
                webSocket = ws.recreate().connectAsynchronously()
                true
            } else
                false
        } catch (e: IOException) {
            e.printStackTrace()
            false
        }
    }

    fun queueMessage(request: String) {
        requestSender.onNext(request)
    }

    private fun initRequestChain(): Observable<WebSocketFrame> {
        return requestSender
                .observeOn(Schedulers.from(ExecutorProvider.defaultHttpExecutor()))
                .filter { webSocket != null }
                .map { request ->
                    WebSocketFrame.createTextFrame(request)
                }
                .doOnNext { webSocketFrame ->
                    webSocket?.sendFrame(webSocketFrame)
                    webSocket?.flush()
                }
                .doOnError { throwable -> reportError(WsStatus.Type.WS_ERROR, throwable) }
    }

    private fun initResponseChain(): Observable<IMessage> {
        return responseObserver
                .observeOn(Schedulers.io())
                .map { data ->
                    gson.fromJson(data, MessageResponse::class.java)
                }.filter {
                    it.payload.room?.let { it == room } ?: true
                }.map {
                    messageHelper.convertToIMessage(it, null)
                }
    }

    private fun buildWssAdapter(): WebSocketAdapter {
        return object : WebSocketAdapter() {

            @Throws(Exception::class)
            override fun onConnectError(websocket: WebSocket?, exception: WebSocketException?) {
                reportError(WsStatus.Type.WS_ERROR, exception)
            }

            @Throws(Exception::class)
            override fun onStateChanged(websocket: WebSocket?, newState: WebSocketState?) {
                reportStatus(buildType(newState!!))
            }

            private fun buildType(newState: WebSocketState): WsStatus.Type {
                return when (newState) {
                    WebSocketState.OPEN -> WsStatus.Type.STATUS_OPEN
                    WebSocketState.CLOSING -> WsStatus.Type.STATUS_DISCONNECTING
                    WebSocketState.CLOSED -> WsStatus.Type.STATUS_DISCONNECTED
                    WebSocketState.CREATED -> WsStatus.Type.STATUS_CONNECTED
                    WebSocketState.CONNECTING -> WsStatus.Type.STATUS_CONNECTING
                    else -> WsStatus.Type.WS_ERROR
                }
            }

            @Throws(Exception::class)
            override fun onTextMessage(websocket: WebSocket?, text: String?) {
                super.onTextMessage(websocket, text)
                Log.d(SOCKET_CHAT, "Response: Text message received: $text")
                text?.let { responseObserver.onNext(it) }
            }

            @Throws(Exception::class)
            override fun onSendingFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                if (BuildConfig.DEBUG) {
                    Log.d(SOCKET_CHAT, "Request: ${frame?.payloadText}")
                }
            }
        }
    }

    private fun reportError(type: WsStatus.Type, exception: Throwable?) {
        val builder = WsStatus.Builder()
        statusObserver.onNext(builder
                .apply {
                    if (exception != null) with(exception)
                }
                .with(type)
                .make())
    }

    private fun reportStatus(type: WsStatus.Type) {
        val builder = WsStatus.Builder()
        statusObserver.onNext(builder
                .with(type)
                .make())
    }

    fun getStatusWatcher(): Observable<WsStatus> {
        return statusObserver
    }

    fun getResponseChain(): Observable<IMessage> {
        return responseChain
    }
}