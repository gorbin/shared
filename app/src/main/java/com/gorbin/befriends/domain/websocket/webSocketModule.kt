package com.gorbin.befriends.domain.websocket

import com.google.gson.Gson
import org.koin.dsl.module

val webSocketModule = module {
    single { WebSocketService(gson = Gson(), localRepo = get(), context = get())}
}