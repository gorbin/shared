package com.gorbin.befriends.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.gorbin.befriends.fcm.notifications.NotificationManager
import org.koin.android.ext.android.inject

class BFFirebaseMessagingService : FirebaseMessagingService() {
    companion object {
        const val BODY = "text"
        const val TITLE = "title"
        const val ACTION = "action"
    }

    private val notificationManager: NotificationManager by inject()

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("FCM", "From: " + remoteMessage.from)
        Log.d("FCM", "From: " + remoteMessage.data)

        val notification = remoteMessage.notification

        val data = remoteMessage.data
        if (data.isNotEmpty()) {
            // Log.d("FCM", "Message data payload: " + remoteMessage.getData());
            handleData(remoteMessage.data)
        } else if (notification != null) {
            // Log.d("FCM", "Message Notification Body: " + notification.getBody());
            handleNow(notification.body)
        }
    }

    private fun handleNow(body: String?) {
        // Log.d("FCM", "RECEIVED");
        notificationManager.notify(body, null, null)
        //  Log.d("FCM", "COMPLETED");
    }

    private fun handleData(data: Map<String, String>) {
        val body = data[BODY]
        val title = data[TITLE]
        val action = data[ACTION]
        notificationManager.notify(body, title, action)
    }

    override fun onNewToken(token: String) {
        Log.d("Refreshed token", "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(pushToken: String?) {
//        apiService.registerPushToken(pushToken)
//            .subscribe( { it -> Log.e("PushTokenRefresh", pushToken) },{ error -> Log.e("PushTokenRefresh", error.localizedMessage)})
    }
}
