package com.gorbin.befriends.fcm


import com.gorbin.befriends.fcm.notifications.NotificationManager
import org.koin.dsl.module

val fcmModule = module {
    single { NotificationManager(context = get()) }
}