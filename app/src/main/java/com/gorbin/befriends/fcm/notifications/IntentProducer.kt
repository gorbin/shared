package com.gorbin.befriends.fcm.notifications

import android.content.Context
import android.content.Intent
import com.gorbin.befriends.domain.entities.PushAction
import com.gorbin.befriends.utils.GlobalValues

object IntentProducer {
    fun produceIntent(context: Context, pushAction: PushAction? = null): Intent {
        return buildDefaultNotificationIntent(context).apply {
            if (pushAction == null)
                return@apply

            putExtra(GlobalValues.PENDING_CHAT_ROOM, pushAction.room to pushAction.role)
        }
    }

    private fun buildDefaultNotificationIntent(context: Context): Intent {
        return Intent(context, NotificationIntentService::class.java).apply {
            action = GlobalValues.PENDING_CHAT_ROOM
        }
    }
}
