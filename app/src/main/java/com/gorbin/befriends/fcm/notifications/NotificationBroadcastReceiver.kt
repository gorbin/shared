package com.gorbin.befriends.fcm.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NotificationBroadcastReceiver : BroadcastReceiver() {
    var foregroundIntentCallback: ((intent: Intent) -> Unit)? = null

    override fun onReceive(context: Context, intent: Intent) {
        foregroundIntentCallback?.invoke(intent)
    }
}