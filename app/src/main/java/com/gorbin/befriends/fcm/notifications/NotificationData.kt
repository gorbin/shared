package com.gorbin.befriends.fcm.notifications

import java.io.Serializable

class NotificationData : Serializable {
    var title: String? = null
    var message: String? = null
    var action: String? = null
}
