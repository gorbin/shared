package com.gorbin.befriends.fcm.notifications

import android.app.IntentService
import android.content.Intent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gorbin.befriends.presentation.base.activity.AppActivity
import com.gorbin.befriends.utils.isAppInForeground

class NotificationIntentService : IntentService("notificationIntentService") {
    override fun onHandleIntent(intent: Intent?) {
        val baseIntent = intent ?: return
        if (ProcessLifecycleOwner.get().lifecycle.isAppInForeground()) {
            if (!LocalBroadcastManager.getInstance(this).sendBroadcast(baseIntent)) {
                val newIntent = Intent(this, AppActivity::class.java)
                with(newIntent) {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    baseIntent.extras?.let { putExtras(it) }
                }
                startActivity(newIntent)
            }
        } else {
            val newIntent = Intent(this, AppActivity::class.java)
            with(newIntent) {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                baseIntent.extras?.let { putExtras(it) }
            }
            startActivity(newIntent)
        }
    }
}