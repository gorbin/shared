package com.gorbin.befriends.fcm.notifications

import android.app.*
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.PushAction
import java.util.*

class NotificationManager(private val context: Context) {
    companion object {
        private const val CHANNEL_ID = "channel_02"
    }

    private val gson = Gson()

    private fun buildFCMNotification(body: String?, title: String?, action: String?): Notification {
        val notificationData = NotificationData()
        notificationData.message = body
        notificationData.title = title
        notificationData.action = action
        return handleNotification(context, notificationData)
    }

    private fun notify(notification: Notification) {
        val mNotificationManager = context.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.app_name)
            val mChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)

            mNotificationManager.createNotificationChannel(mChannel)
        }
        mNotificationManager.notify(generateNotificationId(), notification)
    }

    fun notify(body: String?, title: String?, action: String?) {
        val notification = buildFCMNotification(body, title, action)
        notify(notification)
    }

    fun cancelAll() {
        val mNotificationManager = context.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.cancelAll()
    }

    private fun checkNotNull(string: String?): Boolean {
        return string != null && string.isNotEmpty()
    }

    private fun handleNotification(context: Context, notificationData: NotificationData): Notification {
        val pushAction = notificationData.action
                ?.let { gson.fromJson<PushAction>(it, PushAction::class.java) }
        val color = ContextCompat.getColor(context, R.color.colorPrimary)
        val mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setColor(color)
            .setSmallIcon(R.drawable.ic_logo)
            .setAutoCancel(true)

        if (checkNotNull(notificationData.title)) {
            mBuilder.setContentTitle(notificationData.title)
        } else {
            mBuilder.setContentTitle(context.getString(R.string.app_name))
        }
        if (checkNotNull(notificationData.message)) {
            mBuilder.setContentText(notificationData.message)
        }

        val resultIntent = IntentProducer.produceIntent(context, pushAction)
        val resultPendingIntent = PendingIntent.getService(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT)
        mBuilder.setContentIntent(resultPendingIntent)
        mBuilder.setChannelId(CHANNEL_ID)
        val build = mBuilder.build()
        build.flags = Notification.FLAG_AUTO_CANCEL
        return build
    }

    private fun generateNotificationId(): Int {
        val time = Date().time
        val tmpStr = time.toString()
        val notificationIdStringValue = tmpStr.substring(tmpStr.length - 5)
        return Integer.valueOf(notificationIdStringValue)
    }
}
