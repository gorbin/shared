package com.gorbin.befriends.presentation.base

import java.io.Serializable

class DataSelection<T> : Serializable {
    var selected: T? = null
    var listener: Listener<T>? = null

    fun setSelectedData(selected: T) {
        this.selected = selected
        if (listener != null) {
            (listener as Listener<T>).onChange(selected)
        }
    }

    interface Listener<T> {
        fun onChange(data: T)
    }
}