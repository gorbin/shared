package com.gorbin.befriends.presentation.base

interface IBackPressHandler {
    fun onBackPressed()
}