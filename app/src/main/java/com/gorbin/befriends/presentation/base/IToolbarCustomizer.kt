package com.gorbin.befriends.presentation.base

import com.gorbin.befriends.presentation.base.model.ToolbarSettings

interface IToolbarCustomizer {
    fun onUpActionTap(): Boolean
    fun provideToolbarSettings(): ToolbarSettings
    fun provideStatusBarColor(): Int?
}