package com.gorbin.befriends.presentation.base.activity

import android.content.Context
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.BaseAcBinding
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.localization.LocaleManager
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.fcm.notifications.NotificationBroadcastReceiver
import com.gorbin.befriends.presentation.base.IBackPressHandler
import com.gorbin.befriends.presentation.base.IToolbarCustomizer
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.base.viewmodel.ActivityViewModel
import com.gorbin.befriends.presentation.cicerone.ChatScreen
import com.gorbin.befriends.presentation.cicerone.DefaultAppNavigator
import com.gorbin.befriends.presentation.cicerone.SplashScreen
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.mvvm.android.BaseActivity
import com.punicapp.mvvm.android.BaseFragment
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.base_ac.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class AppActivity : BaseActivity<BaseAcBinding>() {
    lateinit var viewModel: ActivityViewModel
    private lateinit var activeTbCustomizer: IToolbarCustomizer

    private lateinit var toolbar: Toolbar

    private val navigatorHolder: NavigatorHolder by inject()
    private val router: Router by inject()
    private val localeManager: LocaleManager by inject()
    private var navigator: Navigator? = null
    private lateinit var broadcastManager: LocalBroadcastManager
    private val requestsUseCase: IRequestsUseCase by inject()
    private val broadcastReceiver = NotificationBroadcastReceiver()

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(localeManager.initConfig(newBase))
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            overrideConfiguration?.locale = localeManager.findLanguage()
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
            overrideConfiguration?.setLocale(localeManager.findLanguage())
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        navigator = createNavigator()
        configureStartScreen()
        initToolbar()
        registerBroadcastReceiver()
    }

    private fun registerBroadcastReceiver() {
        broadcastManager = LocalBroadcastManager.getInstance(this)
        broadcastReceiver.foregroundIntentCallback = { intent ->
            val pendingChatRoom = intent.extras?.getTypedSerializable<Pair<String, Role?>>(GlobalValues.PENDING_CHAT_ROOM)
            pendingChatRoom?.second?.let { role ->
                requestsUseCase.getRequestInfo(pendingChatRoom.first)
                        .subscribe { request, _ ->
                            request?.let {
                                router.navigateTo(ChatScreen(it, role))
                            }
                        }
            }
        }
        broadcastManager.registerReceiver(broadcastReceiver, IntentFilter(GlobalValues.PENDING_CHAT_ROOM))
    }

    private fun findCurrentFragment() =
            supportFragmentManager.findFragmentById(R.id.base_activity_container)

    private fun configureStartScreen() {
        router.replaceScreen(SplashScreen)
    }

    private fun initViewModel() {
        viewModel = createViewModel()
        lifecycle.addObserver(viewModel)

        val actionConsumer = UIActionConsumer()
        viewModel.listen(actionConsumer, Consumer { })
        binding.setVariable(BR.viewModel, viewModel)

    }

    private fun initToolbar() {
        val layout = LayoutInflater.from(this).inflate(R.layout.toolbar, toolbar_layout, true)
        toolbar = layout.toolbar
        setSupportActionBar(toolbar)

        val actionBar = supportActionBar ?: return

        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(false)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayShowCustomEnabled(false)
    }

    private fun createViewModel(): ActivityViewModel = getViewModel(ActivityViewModel::class)

    override fun onResume() {
        super.onResume()
        navigator?.let { navigatorHolder.setNavigator(it) }
        invalidateToolbar()
    }

    fun invalidateToolbar(customizer: IToolbarCustomizer = provideStubCustomizer()) {
        activeTbCustomizer = customizer

        val statusBarColor = customizer.provideStatusBarColor()
        if (statusBarColor != null) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = statusBarColor
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        val actionBar = supportActionBar ?: return
        val settings = customizer.provideToolbarSettings()

        if (settings.toolbarVisible) {
            toolbar_layout.visibility = View.VISIBLE
            actionBar.show()
        } else {
            toolbar_layout.visibility = View.GONE
            actionBar.hide()
            return
        }

        toolbar.title = settings.toolbarText
        toolbar.visibility = View.VISIBLE
        actionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, settings.iconId))
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private fun provideStubCustomizer() = object : IToolbarCustomizer {
        override fun onUpActionTap() = true
        override fun provideToolbarSettings() = ToolbarSettings()
        override fun provideStatusBarColor(): Int? = ContextCompat.getColor(baseContext, R.color.colorBackground)
    }

    override fun onPause() {
        super.onPause()
        navigator?.let { navigatorHolder.removeNavigator() }
    }

    override fun onDestroy() {
        super.onDestroy()
        fragment = null
        broadcastManager.unregisterReceiver(broadcastReceiver)
    }

    private fun createNavigator(): Navigator? {
        val fragmentManager = supportFragmentManager
        val containerId = base_activity_container.id
        return DefaultAppNavigator(this, containerId, fragmentManager)
    }

    override fun getFragment(): BaseFragment<*>? = null

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            return activeTbCustomizer.onUpActionTap()
        }

        return super.onOptionsItemSelected(item)
    }

    fun showHideProgress(show: Boolean) {
        viewModel.isBlockingProgressVisible.set(show)
    }

    override fun onBackPressed() {
        val fragment = findCurrentFragment()
        if (fragment is IBackPressHandler) {
            fragment.onBackPressed()
            return
        }

        super.onBackPressed()
    }
}