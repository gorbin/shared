package com.gorbin.befriends.presentation.base

import com.gorbin.befriends.presentation.base.viewmodel.ActivityViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val activityModule = module {
    viewModel { ActivityViewModel(application = get()) }
}