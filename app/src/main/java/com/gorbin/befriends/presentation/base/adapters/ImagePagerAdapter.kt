package com.gorbin.befriends.presentation.base.adapters

import android.content.Context
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso

class ImagePagerAdapter(
        private val ctx: Context,
        var images: List<String> = emptyList()
) : PagerAdapter() {
    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 === p1
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imgView = ImageView(ctx)
        Picasso.get()
                .load(Uri.parse(images[position]))
                .fit()
                .centerCrop()
                .into(imgView)
        container.addView(imgView)
        return imgView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}