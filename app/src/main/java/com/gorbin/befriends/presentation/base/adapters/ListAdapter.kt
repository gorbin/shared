package com.gorbin.befriends.presentation.base.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.gorbin.befriends.R

open class ListAdapter<T>(context: Context, list: List<T>, spinnerItemView: Int = R.layout.simple_spinner_item) : ArrayAdapter<T>(context, spinnerItemView, list) {
    init {
        setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent) as TextView
        ResourcesCompat.getFont(context, R.font.roboto_regular)?.let {
            view.typeface = it
        }
        return view
    }
}