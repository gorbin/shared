package com.gorbin.befriends.presentation.base.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.WelcomeOnboarding
import kotlinx.android.synthetic.main.welcome_onboarding_page.view.*

class WelcomeOnboardingPagerAdapter : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return WelcomeOnboarding.values().size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val view = inflater.inflate(R.layout.welcome_onboarding_page, null)
        WelcomeOnboarding.values().getOrNull(position)?.let { item ->
            view.illustration.setImageResource(item.icon)
            view.title.setText(item.title)
            view.explanation.setText(item.explanation)
        }
        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}