package com.gorbin.befriends.presentation.base.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.presentation.base.dialog.custom.*
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.android.IParentVMProvider
import com.punicapp.mvvm.dialogs.alert.AlertAction
import com.punicapp.mvvm.dialogs.alert.AlertDialogContent
import java.util.*

object DialogProvider {
    fun getLangSelectionDialog(
            context: Context,
            viewModel: IParentVMProvider,
            locales: List<Locale>,
            currentLocale: Locale?
    ): AlertDialogContent.Builder = AlertDialogContent.Builder(viewModel)
            .header(context.getString(R.string.select_language))
            .customBody(LanguageSelectionBodyViewModel().apply {
                this.locales.addAll(locales)
                this.currentLocale.set(currentLocale)
            })
            .buttonBarOrientation(AlertDialogContent.ButtonsOrientation.Horizontal)
            .okLabel(context.getString(android.R.string.ok), GlobalValues.LOCALE_SELECTED)
            .noLabel(context.getString(R.string.cancel))

    fun getLangMultiSelectionDialog(
            context: Context,
            viewModel: IParentVMProvider,
            languages: List<Language>,
            selectedLanguages: List<Language>,
            isSingleOption: Boolean = false
    ): AlertDialogContent.Builder = AlertDialogContent.Builder(viewModel)
            .header(context.getString(R.string.lang_proficiency))
            .customBody(LanguageMultiSelectionBodyViewModel().apply {
                this.languages.addAll(languages)
                this.singleOption = isSingleOption
                this.singleActionId = GlobalValues.SELECTED_LANGUAGES
                this.selectedLanguages.addAll(selectedLanguages)
            })
            .buttonBarOrientation(AlertDialogContent.ButtonsOrientation.Horizontal)
            .okLabel(context.getString(android.R.string.ok), positiveActionId = GlobalValues.SELECTED_LANGUAGES)
            .noLabel(context.getString(R.string.cancel))

    fun getImageSourceBottomSheetPickerDialog(cb: (Int) -> Unit): BottomSheetDialogFragment =
            ImageSourcePickerDialog().apply {
                this.cb = cb
            }

    fun getImageOptionsBottomSheetDialog(photo: GalleryPhoto, cb: (Int, GalleryPhoto) -> Unit): BottomSheetDialogFragment =
            ImageOptionsDialog(photo).apply {
                this.cb = cb
            }

    fun getRegisterDialog(context: Context, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(context.getString(R.string.account_created))
                    .message(context.getString(R.string.account_created_desc))
                    .buttonBarOrientation(AlertDialogContent.ButtonsOrientation.Horizontal)
                    .neutralLabel(context.getString(android.R.string.ok).toUpperCase(Locale.getDefault()), GlobalValues.GO_TO_PROFILE)
                    .okLabel(context.getString(R.string.rules), GlobalValues.SERVICE_RULES)

    fun getAboutDialog(context: Context, content: String, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(context.getString(R.string.about))
                    .message(content)
                    .okLabel(context.getString(android.R.string.ok))

    fun getMessageDialog(context: Context, @StringRes titleRes: Int, @StringRes messageRes: Int, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(context.getString(titleRes))
                    .message(context.getString(messageRes))
                    .okLabel(context.getString(android.R.string.ok))

    fun getConfirmationDialog(
            context: Context,
            @StringRes titleRes: Int? = null,
            message: String,
            viewModel: IParentVMProvider,
            @StringRes confirmLabelRes: Int = R.string.confirm,
            @StringRes neutralLabelRes: Int? = null,
            @StringRes declineLabelRes: Int? = null,
            confirmAction: String = GlobalValues.CONFIRM_ACTION,
            neutralAction: String = AlertAction.NEUTRAL.name,
            declineAction: String = AlertAction.NEGATIVE.name
    ): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(titleRes?.let { context.getString(it) } ?: "")
                    .message(message)
                    .customFooter(AlertColoredFooterViewModel().apply {
                        positiveActionId = confirmAction
                        neutralActionId = neutralAction
                        negativeActionId = declineAction
                        confirmButtonText.set(context.getString(confirmLabelRes))
                        neutralButtonText.set(neutralLabelRes?.let { context.getString(it) })
                        declineButtonText.set(declineLabelRes?.let { context.getString(it) })
                    })

    fun getTranslateDialog(context: Context, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(context.getString(R.string.translate))
                    .message(context.getString(R.string.translate_message))
                    .okLabel(context.getString(android.R.string.ok), positiveActionId = GlobalValues.TRANSLATE)
                    .noLabel(context.getString(R.string.cancel))

    fun getReviewDoneDialog(context: Context, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .header(context.getString(R.string.execution_confirmation))
                    .message(context.getString(R.string.request_closed_thanks))
                    .okLabel(context.getString(android.R.string.ok), positiveActionId = GlobalValues.REVIEW_OK)

    fun getReviewsFilterAndSortDialog(context: Context, data: Triple<ReviewSortBy?, SortOrder?, SkillSpecialization?>, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .customBody(ReviewsFilterAndSortBodyViewModel().apply {
                        initState(data)
                    })
                    .noLabel(context.getString(android.R.string.cancel))
                    .okLabel(context.getString(R.string.apply), positiveActionId = GlobalValues.APPLY_FILTER_AND_SORT)

    fun getSearchSortingDialog(context: Context, data: Triple<SortOrder, SearchSortBy, Boolean>, viewModel: IParentVMProvider): AlertDialogContent.Builder =
            AlertDialogContent.Builder(viewModel)
                    .customBody(SearchSortingBodyViewModel().apply {
                        initState(data)
                    })
                    .noLabel(context.getString(android.R.string.cancel))
                    .okLabel(context.getString(R.string.apply), positiveActionId = GlobalValues.APPLY_SEARCH_SORTING)

    fun getBottomSheetDialog(fragment: Fragment, @LayoutRes layoutId: Int, viewModel: BaseAppViewModel, fullHeight: Boolean = false): BottomSheetDialog {
        return BottomSheetDialog(fragment.requireContext()).apply {
            val inflater = LayoutInflater.from(context)
            val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, layoutId, null, false)
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
            setContentView(binding.root)
            viewModel.bottomSheetDialogDismissCallback = {
                dismiss()
            }

            (findViewById(R.id.design_bottom_sheet) as? FrameLayout)?.let {
                val behavior = BottomSheetBehavior.from(it)
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
                if (fullHeight) {
                    it.layoutParams = it.layoutParams.apply {
                        height = ViewGroup.LayoutParams.MATCH_PARENT
                    }
                }
                behavior.peekHeight = 0
                behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onStateChanged(var1: View, var2: Int) {
                        if (var2 == BottomSheetBehavior.STATE_COLLAPSED) {
                            dismiss()
                        }
                    }

                    override fun onSlide(var1: View, var2: Float) {}
                })
            }
        }
    }
}