package com.gorbin.befriends.presentation.base.dialog.custom

import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.punicapp.mvvm.dialogs.alert.AlertAction
import com.punicapp.mvvm.dialogs.vm.ComponentViewModel

class AlertColoredFooterViewModel : ComponentViewModel() {
    override val layout: Int = R.layout.alert_dialog_footer_colored
    var confirmButtonText = ObservableField<String>()
    var declineButtonText = ObservableField<String>()
    var neutralButtonText = ObservableField<String>()

    var positiveActionId: String = AlertAction.POSITIVE.name
    var negativeActionId: String = AlertAction.NEGATIVE.name
    var neutralActionId: String = AlertAction.NEUTRAL.name

    fun provideOnConfirmButtonClick() {
        observer?.onNext(positiveActionId)
    }

    fun provideOnDeclineButtonClick() {
        observer?.onNext(negativeActionId)
    }

    fun provideOnNeutralButtonClick() {
        observer?.onNext(neutralActionId)
    }
}