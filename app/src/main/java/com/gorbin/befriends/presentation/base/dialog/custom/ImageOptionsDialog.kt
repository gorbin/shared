package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.GalleryPhoto
import com.gorbin.befriends.utils.GlobalValues
import kotlinx.android.synthetic.main.bottom_sheet_image_options.view.*

class ImageOptionsDialog(private val photo: GalleryPhoto) : BottomSheetDialogFragment() {
    var cb: ((Int, GalleryPhoto) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_image_options, container, false)
        view.avatar.visibility = if (photo.isAvatarPhoto) View.GONE else View.VISIBLE
        view.avatar.setOnClickListener {
            cb?.invoke(GlobalValues.SET_AS_PROFILE_PHOTO, photo)
            dismiss()
        }
        view.remove.setOnClickListener {
            cb?.invoke(GlobalValues.REMOVE_PHOTO, photo)
            dismiss()
        }

        return view
    }
}