package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.gorbin.befriends.R
import com.gorbin.befriends.utils.GlobalValues
import kotlinx.android.synthetic.main.image_source_picker.view.*

class ImageSourcePickerDialog : BottomSheetDialogFragment() {
    var cb: ((Int) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.image_source_picker, container, false)
        view.gallery.setOnClickListener {
            cb?.invoke(GlobalValues.GALLERY_SOURCE)
            dismiss()
        }
        view.camera.setOnClickListener {
            cb?.invoke(GlobalValues.CAMERA_SOURCE)
            dismiss()
        }

        return view
    }
}