package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import androidx.databinding.ObservableArrayList
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.Language
import com.gorbin.befriends.domain.entities.LanguageCollection
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.dialogs.CollectResult
import com.punicapp.mvvm.dialogs.vm.ComponentViewModel

class LanguageMultiSelectionBodyViewModel : ComponentViewModel() {
    val languages = ObservableArrayList<Language>()
    val selectedLanguages = mutableListOf<Language>()
    var singleOption: Boolean = false
    var singleActionId: String? = null

    override val layout: Int
        get() = R.layout.dialog_lang_multi_selection_body

    fun getLanguageSelectionListener(): ((Pair<Language, Boolean>) -> Unit) = { (language, isSelected) ->
        if (isSelected) {
            if (singleOption)
                selectedLanguages.clear()
            selectedLanguages.add(language)
            if (singleOption) {
                singleActionId?.let { observer?.onNext(it) }
            }
        } else {
            selectedLanguages.remove(language)
        }
    }

    override fun collectData(actionId: String, bundle: Bundle): CollectResult {
        bundle.putSerializable(GlobalValues.SELECTED_LANGUAGES, LanguageCollection(selectedLanguages))
        return super.collectData(actionId, bundle)
    }
}