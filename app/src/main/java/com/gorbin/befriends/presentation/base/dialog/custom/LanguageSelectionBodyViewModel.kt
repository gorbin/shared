package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.dialogs.CollectResult
import com.punicapp.mvvm.dialogs.vm.ComponentViewModel
import java.util.*

class LanguageSelectionBodyViewModel : ComponentViewModel() {
    val locales = ObservableArrayList<Locale>()
    val currentLocale = ObservableField<Locale>()

    override val layout: Int
        get() = R.layout.dialog_lang_selection_body

    fun onLocaleSelected(locale: Locale) {
        currentLocale.set(locale)
    }

    override fun collectData(actionId: String, bundle: Bundle): CollectResult {
        bundle.putSerializable(GlobalValues.LOCALE, currentLocale.get())
        return super.collectData(actionId, bundle)
    }
}