package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import androidx.databinding.ObservableInt
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.ReviewSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.dialogs.CollectResult
import com.punicapp.mvvm.dialogs.vm.ComponentViewModel

class ReviewsFilterAndSortBodyViewModel : ComponentViewModel() {
    override val layout: Int = R.layout.reviews_filter_and_sort_dialog_body
    val sortOrder = ObservableInt()
    val sortBy = ObservableInt()
    val specialization = ObservableInt()

    fun initState(data: Triple<ReviewSortBy?, SortOrder?, SkillSpecialization?>) {
        sortBy.set(
                when (data.first) {
                    ReviewSortBy.DATE -> R.id.sortByDate
                    ReviewSortBy.RATING -> R.id.sortByRating
                    else -> R.id.sortByDate
                }
        )
        sortOrder.set(
                when (data.second) {
                    SortOrder.ASC -> R.id.sortAsc
                    SortOrder.DESC -> R.id.sortDesc
                    else -> R.id.sortDesc
                }
        )
        specialization.set(
                when (data.third) {
                    SkillSpecialization.CONSULTATION -> R.id.filterConsultancy
                    SkillSpecialization.TEACH -> R.id.filterTraining
                    SkillSpecialization.WORK -> R.id.filterExecution
                    else -> R.id.filterNone
                }
        )
    }

    override fun collectData(actionId: String, bundle: Bundle): CollectResult {
        if (actionId == GlobalValues.APPLY_FILTER_AND_SORT) {
            val sortBy = when (sortBy.get()) {
                R.id.sortByDate -> ReviewSortBy.DATE
                R.id.sortByRating -> ReviewSortBy.RATING
                else -> ReviewSortBy.DATE
            }
            bundle.putSerializable(GlobalValues.SORT_BY, sortBy)
            val sortOrder = when (sortOrder.get()) {
                R.id.sortAsc -> SortOrder.ASC
                R.id.sortDesc -> SortOrder.DESC
                else -> SortOrder.DESC
            }
            bundle.putSerializable(GlobalValues.SORT_ORDER, sortOrder)
            val filter = when (specialization.get()) {
                R.id.filterNone -> null
                R.id.filterConsultancy -> SkillSpecialization.CONSULTATION
                R.id.filterExecution -> SkillSpecialization.WORK
                R.id.filterTraining -> SkillSpecialization.TEACH
                else -> null
            }
            bundle.putSerializable(GlobalValues.SPECIALIZATION, filter)
        }
        return super.collectData(actionId, bundle)
    }
}