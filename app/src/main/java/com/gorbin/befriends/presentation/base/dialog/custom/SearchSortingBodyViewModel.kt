package com.gorbin.befriends.presentation.base.dialog.custom

import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.SearchSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.dialogs.CollectResult
import com.punicapp.mvvm.dialogs.vm.ComponentViewModel

class SearchSortingBodyViewModel : ComponentViewModel() {
    override val layout: Int = R.layout.search_sorting_dialog_body
    val sortOrder = ObservableInt()
    val sortBy = ObservableInt()
    val hideNewbies = ObservableBoolean()

    fun initState(data: Triple<SortOrder, SearchSortBy, Boolean>) {
        sortOrder.set(
                when (data.first) {
                    SortOrder.ASC -> R.id.sortAsc
                    SortOrder.DESC -> R.id.sortDesc
                }
        )
        sortBy.set(
                when (data.second) {
                    SearchSortBy.POPULAR -> R.id.sortByPopularity
                    SearchSortBy.RATING -> R.id.sortByRating
                    SearchSortBy.COMMENTS -> R.id.sortByReviews
                    SearchSortBy.PRICE -> R.id.sortByPrice
                }
        )

        hideNewbies.set(data.third)
    }

    override fun collectData(actionId: String, bundle: Bundle): CollectResult {
        if (actionId == GlobalValues.APPLY_SEARCH_SORTING) {
            val sortBy = when (sortBy.get()) {
                R.id.sortByPopularity -> SearchSortBy.POPULAR
                R.id.sortByRating -> SearchSortBy.RATING
                R.id.sortByReviews -> SearchSortBy.COMMENTS
                R.id.sortByPrice -> SearchSortBy.PRICE
                else -> SearchSortBy.POPULAR
            }
            bundle.putSerializable(GlobalValues.SORT_BY, sortBy)
            val sortOrder = when (sortOrder.get()) {
                R.id.sortAsc -> SortOrder.ASC
                R.id.sortDesc -> SortOrder.DESC
                else -> SortOrder.ASC
            }
            bundle.putSerializable(GlobalValues.SORT_ORDER, sortOrder)
            bundle.putSerializable(GlobalValues.HIDE_NEWBIES, hideNewbies.get())
        }
        return super.collectData(actionId, bundle)
    }
}