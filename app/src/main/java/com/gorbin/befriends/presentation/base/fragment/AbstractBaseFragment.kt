package com.gorbin.befriends.presentation.base.fragment

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.gorbin.befriends.R
import com.gorbin.befriends.presentation.base.IToolbarCustomizer
import com.gorbin.befriends.presentation.base.activity.AppActivity
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.core.utils.KeyBoardUtils
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.BaseFragment
import com.punicapp.mvvm.android.IParentVMProvider
import com.punicapp.mvvm.dialogs.alert.AlertDialogContent
import com.punicapp.mvvm.dialogs.vm.DialogContent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.android.viewmodel.ext.android.getViewModel
import retrofit2.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class AbstractBaseFragment<T : AppViewModel> : BaseFragment<T>(), IToolbarCustomizer {
    companion object {
        private val TAG: String = AbstractBaseFragment::class.java.simpleName
    }

    private var disposable: CompositeDisposable? = null

    override fun onResume() {
        super.onResume()
        checkDisposable()
        appActivity?.apply {
            invalidateToolbar(this@AbstractBaseFragment)
        }
    }

    override fun onPause() {
        disposable?.dispose()
        disposable = null
        super.onPause()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideKeyboardOnActivityCreated()
    }

    open fun hideKeyboardOnActivityCreated() {
        KeyBoardUtils.hideKeyboard(activity)
    }

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Boolean>(BaseAppViewModel.ACTION_PROGRESS_EVENT) { data ->
            showProgress(data)
        }.register<Throwable>(BaseAppViewModel.ACTION_ERROR_EVENT) { data ->
            showError(data)
        }
    }

    protected open fun showError(err: Throwable) {
        if (err is UnknownHostException || err is SocketTimeoutException || err is SocketException) {
            DialogProvider.getMessageDialog(requireContext(), R.string.attention, R.string.no_connection, IParentVMProvider { appViewModel })
                    .show()
        } else if (err is HttpException && err.code() == 500) {
            Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, err.message, Toast.LENGTH_LONG).show()
        }
    }

    val appActivity get() = activity as? AppActivity

    protected open fun showProgress(showProgress: Boolean) {
        appActivity?.showHideProgress(showProgress)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rootView = null
    }

    override fun initViewModel(activity: FragmentActivity, clazz: Class<T>): T {
        return activity.getViewModel(clazz.kotlin)
    }

    override fun handleSubjecriptionError(throwable: Throwable?) {
        Log.d(TAG, "subscription error $throwable")
    }

    override fun onUpActionTap(): Boolean {
        //do nothing
        return false
    }

    override fun provideToolbarSettings(): ToolbarSettings = ToolbarSettings()

    override fun provideStatusBarColor(): Int? = ContextCompat.getColor(requireContext(), R.color.colorBackground)

    open fun checkDisposable() {
        if (disposable == null || disposable?.isDisposed == true)
            disposable = CompositeDisposable()
    }

    open fun addDispose(dispose: Disposable) {
        disposable?.add(dispose)
    }

    fun AlertDialogContent.Builder.show() {
        if (activity?.isFinishing == true)
            return
        show(childFragmentManager)
    }

    fun DialogContent.Builder.show() {
        if (activity?.isFinishing == true)
            return
        show(childFragmentManager)
    }
}
