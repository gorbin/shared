package com.gorbin.befriends.presentation.base.model

import com.gorbin.befriends.R

data class ToolbarSettings(
        val toolbarVisible: Boolean = false,
        val iconId: Int = R.drawable.ic_arrow_back,
        val toolbarText: String = ""
)