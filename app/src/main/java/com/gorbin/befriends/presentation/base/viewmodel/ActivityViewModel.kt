package com.gorbin.befriends.presentation.base.viewmodel

import android.app.Application
import androidx.databinding.ObservableBoolean

class ActivityViewModel(application: Application) : BaseAppViewModel(application) {
    val isBlockingProgressVisible = ObservableBoolean(false)
}