package com.gorbin.befriends.presentation.base.viewmodel

import retrofit2.HttpException

interface IErrorHandler {
    fun handleError(error: HttpException): Boolean
}