package com.gorbin.befriends.presentation.base.viewmodel

import android.widget.SeekBar

interface ISeekBarHandler {
    fun getSeekBarListener(view: SeekBar): SeekBar.OnSeekBarChangeListener
}