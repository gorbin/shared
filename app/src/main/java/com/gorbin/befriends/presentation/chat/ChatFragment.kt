package com.gorbin.befriends.presentation.chat

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gorbin.befriends.BuildConfig
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.ChatFrBinding
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.ReviewBody
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.chat.Message
import com.gorbin.befriends.domain.entities.wsprotocol.MessageAuthor
import com.gorbin.befriends.domain.entities.wsprotocol.RejectionReview
import com.gorbin.befriends.domain.entities.wsprotocol.WebSocketChatEvent
import com.gorbin.befriends.domain.entities.wsprotocol.WsStatus
import com.gorbin.befriends.domain.websocket.WebSocketService
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ExecutorProfileScreen
import com.gorbin.befriends.presentation.cicerone.MarksScreen
import com.gorbin.befriends.presentation.cicerone.MyRequestsScreen
import com.gorbin.befriends.presentation.cicerone.RequestsToMeScreen
import com.gorbin.befriends.utils.ChatCommandHelper
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.miguelbcr.ui.rx_paparazzo2.entities.Response
import com.punicapp.core.utils.DimensUtils
import com.punicapp.mvvm.actions.UIActionConsumer
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.chatkit.utils.DateFormatter
import com.yalantis.ucrop.UCrop
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class ChatFragment : AbstractBaseFragment<ChatViewModel>() {
    companion object {
        const val REQUEST = "request"
        const val ROLE = "role"
        const val INITIAL = "initial"

        private const val TRANSLATE_PACKAGE = "com.google.android.apps.translate"
        private const val ADAPTER_TYPE_TUTORIAL: Byte = 110
        private const val ADAPTER_TYPE_FILE: Byte = 111

        private const val SOURCE_SELECTOR = "source_selector"
    }

    private val router: Router by inject()
    private val wsService: WebSocketService by inject()
    private var binding: ChatFrBinding? = null
    private lateinit var chatAdapter: MessagesListAdapter<IMessage>
    private var disposable: CompositeDisposable = CompositeDisposable()
    private var rejectionReview: RejectionReview? = null

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: ChatViewModel): View {
        val binding = ChatFrBinding.inflate(inflater, container, false)
        this.binding = binding
        binding.viewModel = appViewModel
        val request = arguments?.getTypedSerializable<MyRequest>(REQUEST)!!
        appViewModel.role = arguments?.getTypedSerializable<Role>(ROLE)!!

        showProgress(true)
        appViewModel.init(request)
        initChat()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        createStatusListeningChain()
                .also { disposable.add(it) }
        createResponseHandlingChain(wsService.getResponseChain()
                .observeOn(AndroidSchedulers.mainThread()))
                .also { disposable.add(it) }
        wsService.startOrResumeSession()
    }

    override fun onPause() {
        showProgress(false)
        wsService.stopSession()
        disposable.dispose()
        disposable = CompositeDisposable()
        super.onPause()
    }

    override fun onDestroyView() {
        binding = null
        wsService.clearSession()
        super.onDestroyView()
    }

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<String>(ChatViewModel.ACTION_SEND_MESSAGE) {
            wsService.sendMessage(it)
        }.register<Unit>(ChatViewModel.ACTION_BACK) {
            router.exit()
        }.register<Boolean>(ChatViewModel.ACTION_TYPING) {
            wsService.sendTypingStatus(it)
        }.register<Unit>(ChatCommandHelper.ACCEPT_REQUEST) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    titleRes = R.string.attention,
                    message = getString(R.string.confirm_request_acceptance),
                    viewModel = appViewModel,
                    confirmLabelRes = R.string.yes,
                    confirmAction = WebSocketChatEvent.COMMAND_ACCEPT_REQUEST.name,
                    declineLabelRes = R.string.no
            ).show()
        }.register<Unit>(ChatViewModel.ACTION_CONFIRM_REQUEST_ACCEPTANCE) {
            wsService.sendCommand(WebSocketChatEvent.COMMAND_ACCEPT_REQUEST)
        }.register<Unit>(ChatCommandHelper.REJECT_REQUEST) {
            if (activity?.isFinishing == true)
                return@register
            DialogProvider.getBottomSheetDialog(this, R.layout.bottom_sheet_request_rejection_options, appViewModel, true)
                    .show()
        }.register<Unit>(ChatCommandHelper.WITHDRAW_REQUEST) {
            if (activity?.isFinishing == true)
                return@register
            DialogProvider.getBottomSheetDialog(this, R.layout.bottom_sheet_request_withdraw_options, appViewModel, true)
                    .show()
        }.register<Unit>(ChatCommandHelper.CONFIRM_EXECUTION) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    message = getString(R.string.confirm_request_execution),
                    viewModel = appViewModel,
                    confirmLabelRes = R.string.yes,
                    confirmAction = WebSocketChatEvent.COMMAND_CONFIRM_EXECUTION.name,
                    neutralLabelRes = R.string.no
            ).show()
        }.register<Unit>(ChatViewModel.ACTION_CONFIRM_REQUEST_EXECUTION_CONFIRMATION) {
            wsService.sendCommand(WebSocketChatEvent.COMMAND_CONFIRM_EXECUTION)
        }.register<Unit>(ChatCommandHelper.REJECT_EXECUTION) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    titleRes = R.string.attention,
                    message = getString(R.string.reject_execution),
                    viewModel = appViewModel,
                    confirmLabelRes = R.string.yes,
                    confirmAction = WebSocketChatEvent.COMMAND_REJECT_EXECUTION.name,
                    declineLabelRes = R.string.no
            ).show()
        }.register<Unit>(ChatViewModel.ACTION_CONFIRM_REQUEST_EXECUTION_REJECTION) {
            val dataSelector = DataSelection<ReviewBody>()
            dataSelector.listener = object : DataSelection.Listener<ReviewBody> {
                override fun onChange(data: ReviewBody) {
                    rejectionReview = RejectionReview(
                            data.comment!!,
                            data.type,
                            data.professionalism,
                            data.politeness,
                            data.performance
                    )
                }
            }

            router.navigateTo(MarksScreen(appViewModel.data.uuidUser, appViewModel.data.idRequest, appViewModel.data.type, dataSelector))
        }.register<Unit>(ChatCommandHelper.REQUEST_EXECUTION_CONFIRMATION) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    message = getString(R.string.request_execution_confirmation),
                    viewModel = appViewModel,
                    confirmLabelRes = R.string.yes,
                    confirmAction = WebSocketChatEvent.COMMAND_REQUEST_EXECUTION_CONFIRMATION.name,
                    neutralLabelRes = R.string.no
            ).show()
        }.register<Unit>(ChatViewModel.ACTION_CONFIRM_REQUEST_EXECUTION_REQUEST) {
            wsService.sendCommand(WebSocketChatEvent.COMMAND_REQUEST_EXECUTION_CONFIRMATION)
        }.register<String>(ChatViewModel.ACTION_CONFIRM_REQUEST_REJECTION) {
            wsService.sendCommand(WebSocketChatEvent.COMMAND_REJECT_REQUEST, it)
        }.register<String>(ChatViewModel.ACTION_CONFIRM_REQUEST_WITHDRAWING) {
            wsService.sendCommand(WebSocketChatEvent.COMMAND_WITHDRAW_REQUEST, it)
        }.register<Unit>(ChatViewModel.ACTION_TRANSLATE) {
            DialogProvider.getTranslateDialog(requireContext(), appViewModel)
                    .show()
        }.register<Unit>(ChatViewModel.ACTION_LAUNCH_TRANSLATE) {
            val intent: Intent = requireContext().packageManager.getLaunchIntentForPackage(TRANSLATE_PACKAGE)
                    ?: Intent(Intent.ACTION_VIEW).apply {
                        data = Uri.parse("market://details?id=$TRANSLATE_PACKAGE")
                    }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }.register<Long>(ChatViewModel.ACTION_PROFILE) {
            router.navigateTo(ExecutorProfileScreen(it.toString()))
        }.register<Unit>(ChatViewModel.ACTION_BACK_TO_MY_REQUESTS) {
            router.backTo(MyRequestsScreen)
        }.register<Unit>(ChatViewModel.ACTION_BACK_TO_REQUESTS_TO_ME) {
            router.backTo(RequestsToMeScreen)
        }.register<Unit>(ChatCommandHelper.NEW_REQUEST) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    titleRes = R.string.new_request,
                    message = getString(R.string.accept_to_start),
                    viewModel = appViewModel,
                    confirmLabelRes = R.string.accept,
                    confirmAction = ChatCommandHelper.ACCEPT_REQUEST,
                    declineLabelRes = R.string.reject,
                    declineAction = ChatCommandHelper.REJECT_REQUEST
            ).show()
        }.register<Unit>(ChatViewModel.ACTION_ATTACHMENT) {
            if (activity?.isFinishing == true)
                return@register
            DialogProvider.getBottomSheetDialog(this, R.layout.bottom_sheet_attachment, appViewModel, false)
                    .show()
        }.register<Unit>(ChatViewModel.ACTION_PICTURE_SELECTION) {
            showImageSourceSelectionDialog()
        }.register<Unit>(ChatViewModel.ACTION_FILE_SELECTION) {
            getFile()
        }.register<String>(ChatViewModel.ACTION_FILE_UPLOADED) {
            wsService.sendFile(it)
        }
    }

    private fun initChat() {
        wsService.setInterlocutorName(appViewModel.data.getNameOrNickname())
        wsService.setInterlocutorPhoto(appViewModel.data.urlPhoto)
        wsService.setRole(appViewModel.role)
        wsService.baseUrl = BuildConfig.SOCKET_ADDRESS
        initChatAdapter()
    }

    private fun initChatAdapter() {
        val imageLoader = ImageLoader { imageView, url, drawableRes, _ ->

            Picasso.get()
                    .let {
                        if (drawableRes != null)
                            it.load(drawableRes)
                        else
                            it.load(url)
                    }
                    .resize(DimensUtils.dpToPx(225), DimensUtils.dpToPx(225))
                    .onlyScaleDown()
                    .centerCrop()
                    .into(imageView)
        }

        val dateFormatter: DateFormatter.Formatter = DateFormatter.Formatter { date ->
            when {
                DateFormatter.isToday(date) -> {
                    getString(R.string.today)
                }
                DateFormatter.isYesterday(date) -> {
                    getString(R.string.yesterday)
                }
                else -> {
                    DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_TIME)
                }
            }
        }

        val messageHolders = MessageHolders()
        val contentChecker = MessageHolders.ContentChecker<IMessage> { message, type ->
            when (type) {
                ADAPTER_TYPE_TUTORIAL -> message is Message.TutorialMessage
                ADAPTER_TYPE_FILE -> message is Message.FileMessage
                else -> false
            }
        }
        messageHolders.registerContentType<Message.TutorialMessage>(
                ADAPTER_TYPE_TUTORIAL,
                TutorialMessageViewHolder::class.java,
                R.layout.sequence_item,
                R.layout.sequence_item,
                contentChecker
        ).registerContentType<Message.FileMessage>(
                ADAPTER_TYPE_FILE,
                IncomingFileMessageViewHolder::class.java,
                R.layout.item_incoming_file_message,
                OutcomingFileMessageViewHolder::class.java,
                R.layout.item_outcoming_file_message,
                contentChecker
        )
        chatAdapter = MessagesListAdapter(MessageAuthor.ME.id, messageHolders, imageLoader, MessagesListAdapter.SimpleMessageMerger<IMessage>())
        chatAdapter.setLoadMoreListener { _, _ ->
            if (!appViewModel.hasNextPage)
                return@setLoadMoreListener

            val item = chatAdapter.items.findLast { it.item !is Date }?.item as? IMessage
            item?.id?.let { wsService.requestChatHistory(it) }
        }
        chatAdapter.setDateHeadersFormatter(dateFormatter)

        binding?.messagesList?.setAdapter(chatAdapter)
        binding?.messagesList?.itemAnimator = null
        binding?.messagesList?.recycledViewPool?.setMaxRecycledViews(0, 0)
        binding?.messagesList?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                (recyclerView.layoutManager as? LinearLayoutManager)?.apply {
                    chatAdapter.onLastVisibleItemPosition(findLastVisibleItemPosition())
                }
            }
        })
    }

    private fun createStatusListeningChain(): Disposable {
        return wsService.getStatusWatcher()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { wsStatus ->
                    when (wsStatus.type) {
                        WsStatus.Type.INTERNAL_ERROR, WsStatus.Type.WS_ERROR -> {
                            //TODO: handle error
                        }
                        WsStatus.Type.STATUS_OPEN -> {
                            if (!wsService.hasConnectionToken())
                                wsService.authorize()
                            else if (!wsService.hasRoom())
                                appViewModel.data.uuidRequest.let { wsService.connectToTheRoom(it) }
                            else
                                wsService.requestChatHistory()
                        }
                        else -> { }
                    }
                }
    }

    private fun createResponseHandlingChain(wsResponseObservable: Observable<IMessage>): Disposable {
        return wsResponseObservable
                .subscribe({ message ->
                    handleMessageReceiving(message)
                }, {
                    it.printStackTrace()
                })
    }

    override fun provideStatusBarColor(): Int = ContextCompat.getColor(requireContext(), R.color.colorDeepBlue)

    private fun handleMessageReceiving(message: IMessage?) {
        showProgress(false)
        if (message is Message && (message !is Message.HistoryPageMessage || message !is Message.TypeInOutMessage)) {
            message.chatAvailable?.let { appViewModel.chatAvailable.set(it) }
        }

        when (message) {
            is Message.ConnectionMessage -> {
                val initRequired = arguments?.getBoolean(INITIAL, false) ?: false
                wsService.setRoom(appViewModel.data.uuidRequest)
                if (initRequired) {
                    arguments?.remove(INITIAL)
                    appViewModel.data.uuidRequest.let { wsService.connectToTheRoom(it) }
                } else {
                    wsService.requestChatHistory()
                    rejectionReview?.let {
                        wsService.sendCommand(WebSocketChatEvent.COMMAND_REJECT_EXECUTION, null, it)
                        rejectionReview = null
                    }
                }
            }
            is Message.RoomMessage ->  {
                if (!message.isSystem)
                    wsService.requestChatHistory()
            }
            is Message.ErrorMessage -> Toast.makeText(requireContext(), message.errorMessage, Toast.LENGTH_LONG).show()
            is Message.HistoryPageMessage -> {
                appViewModel.hasNextPage = GlobalValues.CHAT_PAGE_SIZE == message.totalMessages
                val historyPage = if (appViewModel.hasNextPage || appViewModel.role == Role.EXECUTOR)
                    message.history
                else
                    message.history + Message.TutorialMessage()
                chatAdapter.mergeHistory(historyPage, false, appViewModel.historyMessagesLoaded == 0)
                val firstMessageId = (message.history.find { it is Message.TextMessage && it.user.id == MessageAuthor.OTHER.id } as? Message.TextMessage)?.id
                firstMessageId?.let {
                    wsService.markMessageRead(it)
                }
                appViewModel.historyMessagesLoaded =+ message.history.size
            }
            is Message.TypeInOutMessage -> {
                if (message.interlocutor)
                    appViewModel.typing.set(message.typing)
            }
            is Message.TextMessage, is Message.ImageMessage, is Message.FileMessage -> {
                chatAdapter.addToStartOrUpdate(message, true)
                if (message.user.id != MessageAuthor.ME.id)
                    wsService.markMessageRead(message.id)
            }
            is Message.StatusReadMessage -> {
                chatAdapter.setMessageRead(message.id)
            }
            is Message.StatusChangedMessage -> {
                appViewModel.data.status = message.status
                appViewModel.status.set(message.status)
                chatAdapter.addToStartOrUpdate(message, true)
                when (message.event) {
                    WebSocketChatEvent.COMMAND_WITHDRAW_REQUEST -> {
                        if (appViewModel.role == Role.CUSTOMER)
                            DialogProvider.getConfirmationDialog(
                                    context = requireContext(),
                                    titleRes = R.string.attention,
                                    message = getString(R.string.your_request_withdrawn),
                                    viewModel = appViewModel,
                                    confirmAction = WebSocketChatEvent.COMMAND_WITHDRAW_REQUEST.name,
                                    declineLabelRes = null
                            ).show()
                    }
                    WebSocketChatEvent.COMMAND_REJECT_REQUEST -> {
                        if (appViewModel.role == Role.EXECUTOR)
                            DialogProvider.getConfirmationDialog(
                                    context = requireContext(),
                                    titleRes = R.string.attention,
                                    message = getString(R.string.you_rejected_request),
                                    viewModel = appViewModel,
                                    confirmAction = WebSocketChatEvent.COMMAND_REJECT_REQUEST.name,
                                    declineLabelRes = null
                            ).show()
                    }
                    WebSocketChatEvent.COMMAND_REQUEST_EXECUTION_CONFIRMATION -> {
                        if (appViewModel.role == Role.EXECUTOR && !message.silent)
                            DialogProvider.getMessageDialog(
                                    context = requireContext(),
                                    titleRes = R.string.attention,
                                    messageRes = R.string.request_confirmation_notice,
                                    viewModel = appViewModel
                            ).show()
                    }
                    WebSocketChatEvent.COMMAND_CONFIRM_EXECUTION -> {
                        if (appViewModel.role == Role.CUSTOMER)
                            router.navigateTo(MarksScreen(appViewModel.data.uuidUser, appViewModel.data.idRequest, appViewModel.data.type))
                    }
                    else -> {  }
                }
            }
        }
    }

    private fun showImageSourceSelectionDialog() {
        if (activity?.isFinishing == true)
            return

        val options = UCrop.Options()
        options.setToolbarTitle("")
        options.withAspectRatio(1F, 1F)
        options.setStatusBarColor(Color.BLACK)
        options.setToolbarWidgetColor(Color.WHITE)
        options.setActiveWidgetColor(Color.BLACK)
        options.setToolbarColor(Color.BLACK)

        DialogProvider.getImageSourceBottomSheetPickerDialog { source ->
            when (source) {
                GlobalValues.GALLERY_SOURCE -> {
                    getPhotoFromGallery(options)
                }
                GlobalValues.CAMERA_SOURCE -> {
                    getPhotoFromCamera(options)
                }
            }
        }.show(childFragmentManager, SOURCE_SELECTOR)
    }

    private fun getPhotoFromCamera(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().onFileSelected(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getPhotoFromGallery(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().onFileSelected(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getFile(): Disposable {
        return RxPaparazzo.single(this)
                .usingFiles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().onFileSelected(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun onFileSelected(response: Response<ChatFragment, FileData>) {
        if (response.resultCode() == Activity.RESULT_OK) {
            appViewModel.uploadFile(response.data())
        }
    }
}