package com.gorbin.befriends.presentation.chat

import android.app.Application
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.RequestStatus
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.wsprotocol.WebSocketChatEvent
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.ChatCommandHelper
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.onChanged
import com.gorbin.befriends.utils.toDisplayRating
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import com.stfalcon.chatkit.messages.MessageInput

class ChatViewModel(application: Application, private val useCase: IRequestsUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_SEND_MESSAGE = "action_send_message"
        const val ACTION_BACK = "action_back"
        const val ACTION_TYPING = "action_typing"
        const val ACTION_CONFIRM_REQUEST_REJECTION = "action_confirm_request_rejection"
        const val ACTION_CONFIRM_REQUEST_WITHDRAWING = "action_confirm_request_withdrawing"
        const val ACTION_TRANSLATE = "action_translate"
        const val ACTION_LAUNCH_TRANSLATE = "action_launch_translate"
        const val ACTION_PROFILE = "action_profile"
        const val ACTION_BACK_TO_MY_REQUESTS = "action_back_to_my_requests"
        const val ACTION_BACK_TO_REQUESTS_TO_ME = "action_back_to_requests_to_me"
        const val ACTION_CONFIRM_REQUEST_ACCEPTANCE = "action_confirm_request_acceptance"
        const val ACTION_CONFIRM_REQUEST_EXECUTION_REQUEST = "action_confirm_request_execution_request"
        const val ACTION_CONFIRM_REQUEST_EXECUTION_CONFIRMATION = "action_confirm_request_execution_confirmation"
        const val ACTION_CONFIRM_REQUEST_EXECUTION_REJECTION = "action_confirm_request_execution_rejection"
        const val ACTION_ATTACHMENT = "action_attachment"
        const val ACTION_PICTURE_SELECTION = "action_picture_selection"
        const val ACTION_FILE_SELECTION = "action_file_selection"
        const val ACTION_FILE_UPLOADED = "action_file_sent"
    }

    val image = ObservableField<Uri>()
    val name = ObservableField<String>()
    val rating = ObservableField<String>()
    val isNewbie = ObservableBoolean()
    val typing = ObservableBoolean()
    var historyMessagesLoaded: Int = 0
    var hasNextPage = true
    lateinit var data: MyRequest
    lateinit var role: Role

    //Command section
    val commandButtonBackground = ObservableField<Drawable>()
    val commandAction = ObservableField<String>()
    val commandOptionAvailable = ObservableBoolean()

    val chatAvailable = ObservableBoolean(true)

    private val commandHelper = ChatCommandHelper()
    val status = ObservableField<RequestStatus>().onChanged { invalidateCommandButton() }

    fun init(data: MyRequest) {
        this.data = data
        historyMessagesLoaded = 0
        hasNextPage = true
        val validationRequired = status.get() == data.status
        status.set(data.status)
        if (validationRequired)
            invalidateCommandButton()
        image.set(data.urlPhoto?.let { Uri.parse(it) })
        name.set(data.getNameOrNickname())
        rating.set(data.rating.toDisplayRating())
        isNewbie.set(data.isNewbie())
    }

    private fun invalidateCommandButton() {
        val status = status.get()
        val config = status?.let { commandHelper.getCommandConfig(context, it, role) }
        commandButtonBackground.set(config?.commandButtonBg)
        commandAction.set(config?.commandAction)
        commandOptionAvailable.set(config?.commandAvailable ?: false)
    }

    fun onRequestRejectOptionSelected(reason: String) {
        dismissBottomSheetDialog()
        processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_REJECTION, reason))
    }

    fun onRequestWithdrawingOptionSelected(reason: String) {
        dismissBottomSheetDialog()
        processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_WITHDRAWING, reason))
    }

    fun onCommandButtonClick() {
        status.get()?.let {
            commandHelper.getAction(it, role)?.let { action ->
                processor.onNext(UIAction(action))
            }
        }
    }

    fun onAddAttachments() {
        processor.onNext(UIAction(ACTION_ATTACHMENT))
    }

    fun onPictureAttachmentSelected() {
        dismissBottomSheetDialog()
        processor.onNext(UIAction(ACTION_PICTURE_SELECTION))
    }

    fun onFileAttachmentSelected() {
        dismissBottomSheetDialog()
        processor.onNext(UIAction(ACTION_FILE_SELECTION))
    }

    fun uploadFile(data: FileData?) {
        data?.file?.let {
            useCase.uploadFile(it)
                    .appSubscribe {
                        processor.onNext(UIAction(ACTION_FILE_UPLOADED, it.uuidFile))
                    }
        }
    }

    fun openProfile() {
        processor.onNext(UIAction(ACTION_PROFILE, data.idUser))
    }

    fun onSubmit(message: CharSequence): Boolean {
        return message.toString()
                .trim()
                .takeIf { it.isNotBlank() }
                ?.let {
                    processor.onNext(UIAction(ACTION_SEND_MESSAGE, it))
                    true
                } ?: false
    }

    fun back() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onTranslateClick() {
        processor.onNext(UIAction(ACTION_TRANSLATE))
    }

    fun getTypingListener(): MessageInput.TypingListener = object : MessageInput.TypingListener {
        override fun onStartTyping() {
            processor.onNext(UIAction(ACTION_TYPING, true))
        }

        override fun onStopTyping() {
            processor.onNext(UIAction(ACTION_TYPING, false))
        }
    }

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction) {
        super.onActionFromChild(action)
        when (action.actionId) {
            GlobalValues.TRANSLATE -> {
                processor.onNext(UIAction(ACTION_LAUNCH_TRANSLATE))
            }
            WebSocketChatEvent.COMMAND_WITHDRAW_REQUEST.name -> {
                processor.onNext(UIAction(ACTION_BACK_TO_MY_REQUESTS))
            }
            WebSocketChatEvent.COMMAND_REJECT_REQUEST.name -> {
                processor.onNext(UIAction(ACTION_BACK_TO_REQUESTS_TO_ME))
            }
            WebSocketChatEvent.COMMAND_ACCEPT_REQUEST.name -> {
                processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_ACCEPTANCE))
            }
            WebSocketChatEvent.COMMAND_REQUEST_EXECUTION_CONFIRMATION.name -> {
                processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_EXECUTION_REQUEST))
            }
            WebSocketChatEvent.COMMAND_CONFIRM_EXECUTION.name -> {
                processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_EXECUTION_CONFIRMATION))
            }
            WebSocketChatEvent.COMMAND_REJECT_EXECUTION.name -> {
                processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_EXECUTION_REJECTION))
            }
            ChatCommandHelper.ACCEPT_REQUEST -> {
                processor.onNext(UIAction(ACTION_CONFIRM_REQUEST_ACCEPTANCE))
            }
            ChatCommandHelper.REJECT_REQUEST-> {
                processor.onNext(UIAction(action.actionId))
            }
        }
    }
}