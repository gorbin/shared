package com.gorbin.befriends.presentation.chat

import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.style.UnderlineSpan
import android.view.View
import com.gorbin.befriends.domain.entities.chat.Message
import com.stfalcon.chatkit.messages.MessageHolders

class IncomingFileMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.IncomingTextMessageViewHolder<Message.FileMessage>(itemView, payload) {
    override fun onBind(message: Message.FileMessage?) {
        super.onBind(message)
        text?.apply {
            val content = SpannableString(text)
            content.setSpan(UnderlineSpan(), 0, content.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            text = content
            setOnClickListener {
                message?.file?.link?.let {
                    itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
                }
            }
        }
    }
}

class OutcomingFileMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.OutcomingTextMessageViewHolder<Message.FileMessage>(itemView, payload) {
    override fun onBind(message: Message.FileMessage?) {
        super.onBind(message)
        text?.apply {
            val content = SpannableString(text)
            content.setSpan(UnderlineSpan(), 0, content.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            text = content
            setOnClickListener {
                message?.file?.link?.let {
                    itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
                }
            }
        }
    }
}