package com.gorbin.befriends.presentation.chat

import android.view.View
import com.gorbin.befriends.domain.entities.chat.Message
import com.stfalcon.chatkit.messages.MessageHolders

class TutorialMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.BaseMessageViewHolder<Message.TutorialMessage>(itemView, payload) {
    override fun onBind(data: Message.TutorialMessage?) {
        //DO NOTHING
    }
}