package com.gorbin.befriends.presentation.chat

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val chatModule = module {
    viewModel { ChatViewModel(application = get(), useCase = get()) }
}