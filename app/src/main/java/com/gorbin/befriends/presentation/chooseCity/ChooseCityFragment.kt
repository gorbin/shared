package com.gorbin.befriends.presentation.chooseCity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseCityFrBinding
import com.gorbin.befriends.domain.entities.City
import com.gorbin.befriends.domain.entities.CitySelectionFlow
import com.gorbin.befriends.domain.entities.Country
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.EditProfileScreen
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.SearchFormScreen
import com.gorbin.befriends.presentation.cicerone.SettingsScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.presentation.signIn.SignInViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseCityFragment : AbstractBaseFragment<ChooseCityViewModel>() {
    companion object {
        const val COUNTRY_EXTRA = "country_extra"
    }

    private var selectCity: DataSelection<Pair<Country, City>>? = DataSelection()
    private lateinit var selected: Pair<Country?, City?>
    private lateinit var flow: CitySelectionFlow
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SignInViewModel.ACTION_BACK) {
            router.exit()
        }.register<City>(ChooseCityViewModel.ACTION_SELECT_CITY) {
            selectCity?.setSelectedData(selected.first!! to it)
            when (flow) {
                CitySelectionFlow.PERSONAL_INFO -> router.backTo(PersonalInfoScreen())
                CitySelectionFlow.SEARCH -> router.backTo(SearchFormScreen)
                CitySelectionFlow.PROFILE_EDITING -> router.backTo(EditProfileScreen)
            }
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseCityViewModel): View {
        val binding = ChooseCityFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()

        val data = arguments?.getTypedSerializable<Country>(COUNTRY_EXTRA)!!
        selected = arguments?.getTypedSerializable<Pair<Country, City>>(PersonalInfoFragment.SELECTED_CITY_EXTRA)
                ?: Pair(null, null)
        selectCity = arguments?.getTypedSerializable<DataSelection<Pair<Country, City>>>(PersonalInfoFragment.SELECTION_CITY_EXTRA)
        flow = arguments?.getTypedSerializable<CitySelectionFlow>(GlobalValues.FLOW)!!

        appViewModel.init(data, selected.second)

        return binding.root
    }
}