package com.gorbin.befriends.presentation.chooseCity

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.City
import com.gorbin.befriends.domain.entities.Country
import com.gorbin.befriends.domain.usecases.IRegisterUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit

class ChooseCityViewModel(application: Application, private val useCase: IRegisterUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_BACK = "action_back"
        const val ACTION_SELECT_CITY = "action_select_city"

        const val SEARCH_DELAY = 500L
    }

    private lateinit var country: Country
    val query = ObservableField<String>().onChanged { search(it) }

    private val cities = mutableListOf<City>()
    private lateinit var dataObserver: Observer<List<*>>
    private val suggestionProcessor = PublishSubject.create<String>()
    val emptyIcon = ObservableField<Drawable>()
    val emptyString = ObservableField<String>()

    fun init(data: Country, city: City?) {
        country = data
        cities.clear()
        query.set(city?.name)
        emptyIcon.set(null)
        emptyString.set(context.getString(R.string.no_data_city))
        if (!query.get().isNullOrBlank()) {
            search(query.get())
        }
    }

    override fun doOnResume() {
        super.doOnResume()
        suggestionProcessor
                .debounce(SEARCH_DELAY, TimeUnit.MILLISECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { setProgressVisibility(true) }
                .observeOn(Schedulers.io())
                .flatMap {
                    val query = it.toLowerCase(Locale.getDefault())
                    if (query.isNotEmpty()) {
                        return@flatMap useCase.getCityByCountry(country, query)
                                .map { query to it }
                                .toObservable()
                    } else {
                        return@flatMap Single.just(listOf<City>())
                                .map { query to it }
                                .toObservable()
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { setProgressVisibility(false) }
                .subscribe {
                    if (it.first.isNotEmpty() && it.second.isEmpty()) {
                        emptyIcon.set(ContextCompat.getDrawable(context, R.drawable.ic_search))
                        emptyString.set(context.getString(R.string.city_not_found))
                    } else if (it.first.isEmpty()) {
                        emptyIcon.set(null)
                        emptyString.set(context.getString(R.string.no_data_city))
                    }
                    showCities(it.second)
                }.addVMDisposable()
    }

    private fun search(query: String?) {
        suggestionProcessor.onNext(query ?: "")
    }

    private fun showCities(data: List<City>) {
        dataObserver.onNext(data)
    }

    fun onItemSelect(data: City) {
        query.set(null)
        processor.onNext(UIAction(ACTION_SELECT_CITY, data))
    }

    fun onBackClick() {
        query.set(null)
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.city_item, City::class.java, CityItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also { dataObserver = it.dataReceiver }
}