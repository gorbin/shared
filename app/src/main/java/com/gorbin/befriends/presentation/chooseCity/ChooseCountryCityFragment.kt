package com.gorbin.befriends.presentation.chooseCity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseCountryCityFrBinding
import com.gorbin.befriends.domain.entities.City
import com.gorbin.befriends.domain.entities.CitySelectionFlow
import com.gorbin.befriends.domain.entities.Country
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ChooseCityScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.presentation.signIn.SignInViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseCountryCityFragment : AbstractBaseFragment<ChooseCountryCityViewModel>() {
    companion object {
        const val COUNTRIES_EXTRA = "cities_extra"
    }

    private lateinit var selected: Pair<Country?, City?>
    private var selectCity: DataSelection<Pair<Country, City>> = DataSelection()
    private val router: Router by inject()
    private lateinit var flow: CitySelectionFlow

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SignInViewModel.ACTION_BACK) {
            router.exit()
        }.register<Country>(ChooseCountryCityViewModel.ACTION_SELECT_ITEM) {
            val selectedPair = if (it.name == this.selected.first?.name)
                this.selected
            else
                it to null
            router.navigateTo(ChooseCityScreen(it, selectedPair, selectCity, flow))
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseCountryCityViewModel): View {
        val binding = ChooseCountryCityFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()

        val data = arguments?.getTypedSerializable<List<Country>>(COUNTRIES_EXTRA) ?: emptyList()
        selected = arguments?.getTypedSerializable<Pair<Country, City>>(PersonalInfoFragment.SELECTED_CITY_EXTRA)
                ?: Pair(null, null)
        selectCity = arguments?.getTypedSerializable<DataSelection<Pair<Country, City>>>(PersonalInfoFragment.SELECTION_CITY_EXTRA)
                ?: DataSelection()
        flow = arguments?.getTypedSerializable<CitySelectionFlow>(GlobalValues.FLOW)!!

        appViewModel.init(data, selected)

        return binding.root
    }
}