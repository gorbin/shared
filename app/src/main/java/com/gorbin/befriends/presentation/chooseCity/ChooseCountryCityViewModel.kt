package com.gorbin.befriends.presentation.chooseCity

import android.app.Application
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.City
import com.gorbin.befriends.domain.entities.Country
import com.gorbin.befriends.domain.usecases.ISearchUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit

class ChooseCountryCityViewModel(application: Application, private val useCase: ISearchUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_BACK = "action_back"
        const val ACTION_SELECT_ITEM = "action_select_item"

        const val SEARCH_DELAY = 500L
    }

    val query = ObservableField<String>().onChanged { search(it) }

    private var selected: Pair<Country?, City?>? = null
    private val countries = mutableListOf<Country>()
    private lateinit var dataObserver: Observer<List<*>>
    private val suggestionProcessor = PublishSubject.create<String>()

    fun init(data: List<Country>, selected: Pair<Country?, City?>?) {
        this.selected = selected
        countries.clear()
        countries.addAll(data)
        selected?.first?.let {
            query.set(it.name)
        }
        showCountries(countries)
    }

    override fun doOnResume() {
        super.doOnResume()
        suggestionProcessor
                .debounce(SEARCH_DELAY, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap {
                    val query = it.toLowerCase(Locale.getDefault()).trim()
                    if (query.isNotEmpty()) {
                        return@flatMap useCase.getCountries(query).toObservable()
                    } else {
                        return@flatMap Single.just(countries).toObservable()
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showCountries(it)
                }.addVMDisposable()
    }

    private fun search(query: String?) {
        suggestionProcessor.onNext(query ?: "")
    }

    private fun showCountries(data: List<Country>) {
        dataObserver.onNext(data)
    }

    fun onItemSelect(data: Country) {
        query.set(null)
        processor.onNext(UIAction(ACTION_SELECT_ITEM, data))
    }

    fun onBackClick() {
        query.set(null)
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.country_city_item, Country::class.java, CountryCityItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also { dataObserver = it.dataReceiver }
}