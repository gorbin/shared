package com.gorbin.befriends.presentation.chooseCity

import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.City
import com.punicapp.mvvm.adapters.AdapterItemViewModel


class CityItemViewModel : AdapterItemViewModel<City, ChooseCityViewModel>() {
    val city = ObservableField<String>()
    lateinit var data: City

    override fun bindData(data: City) {
        this.data = data
        city.set("${data.name}, ${data.region}")
    }

    fun onClick() {
        parent.onItemSelect(data)
    }
}