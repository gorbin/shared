package com.gorbin.befriends.presentation.chooseCity

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.Country
import com.punicapp.mvvm.adapters.AdapterItemViewModel


class CountryCityItemViewModel : AdapterItemViewModel<Country, ChooseCountryCityViewModel>() {
    val name = ObservableField<String>()
    val isAvailable = ObservableBoolean()
    lateinit var data: Country

    override fun bindData(data: Country) {
        this.data = data
        name.set(data.name)
        isAvailable.set(data.isAvailable ?: false)
    }

    fun onClick() {
        if (isAvailable.get())
            parent.onItemSelect(data)
    }
}