package com.gorbin.befriends.presentation.chooseCity

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val chooseCityModule = module {
    viewModel { ChooseCityViewModel(application = get(), useCase = get()) }
    viewModel { ChooseCountryCityViewModel(application = get(), useCase = get()) }
}