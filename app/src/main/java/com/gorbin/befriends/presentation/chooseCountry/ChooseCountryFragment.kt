package com.gorbin.befriends.presentation.chooseCountry

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseCountryFrBinding
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.signIn.SignInFragment
import com.gorbin.befriends.presentation.signIn.SignInViewModel
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseCountryFragment : AbstractBaseFragment<ChooseCountryViewModel>() {
    companion object {
        const val CHOOSE_COUNTRY_EXTRA = "choose_country_extra"
    }

    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ChooseCountryViewModel.ACTION_SELECT_COUNTRY) {
            router.exit()
        }.register<Unit>(SignInViewModel.ACTION_BACK) {
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseCountryViewModel): View {
        val binding = ChooseCountryFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()

        val data = arguments?.getTypedSerializable<List<RegionInfo>>(CHOOSE_COUNTRY_EXTRA) ?: emptyList()
        val selectedCountry = arguments?.getTypedSerializable<DataSelection<RegionInfo>>(SignInFragment.SELECTION)
        appViewModel.init(data, selectedCountry)

        return binding.root
    }
}