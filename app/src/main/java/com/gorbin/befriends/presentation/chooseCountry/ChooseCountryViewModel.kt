package com.gorbin.befriends.presentation.chooseCountry

import android.app.Application
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit

class ChooseCountryViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_SELECT_COUNTRY = "action_select_country"
        const val ACTION_BACK = "action_back"

        const val SEARCH_DELAY = 500L
    }

    val query = ObservableField<String>().onChanged { search(it) }

    private var selection: DataSelection<RegionInfo>? = null
    private val countries = mutableListOf<RegionInfo>()
    private lateinit var dataObserver: Observer<List<*>>
    private val suggestionProcessor = PublishSubject.create<String>()

    fun init(data: List<RegionInfo>, selection: DataSelection<RegionInfo>?) {
        this.selection = selection
        countries.clear()
        countries.addAll(data)
        showCountries()
    }

    override fun doOnResume() {
        super.doOnResume()
        suggestionProcessor
                .debounce(SEARCH_DELAY, TimeUnit.MILLISECONDS, Schedulers.io())
                .map {
                    val query = it.toLowerCase(Locale.getDefault())
                    countries.filter { regionInfo ->
                        regionInfo.countryCode.toLowerCase(Locale.getDefault()).contains(query)
                                || regionInfo.countryName.toLowerCase(Locale.getDefault()).contains(query)
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    showCountries(it)
                }.addVMDisposable()
    }

    private fun search(query: String?) {
        suggestionProcessor.onNext(query ?: "")
    }

    private fun showCountries(data: List<RegionInfo>? = null) {
        val countries = data ?: this.countries
        val list = mutableListOf<Any>()
        if (countries.isNullOrEmpty()) {
            dataObserver.onNext(emptyList<RegionInfo>())
        } else {
            countries.forEach {
                list.add(it)
            }
            dataObserver.onNext(list)
        }
    }

    fun onItemSelect(data: RegionInfo) {
        selection?.setSelectedData(data)
        query.set(null)
        processor.onNext(UIAction(ACTION_SELECT_COUNTRY))
    }

    fun onBackClick() {
        query.set(null)
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.country_item, RegionInfo::class.java, CountryItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also { dataObserver = it.dataReceiver }
}