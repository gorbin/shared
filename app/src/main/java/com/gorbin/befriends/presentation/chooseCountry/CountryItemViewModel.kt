package com.gorbin.befriends.presentation.chooseCountry

import android.net.Uri
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.RegionInfo
import com.punicapp.mvvm.adapters.AdapterItemViewModel


class CountryItemViewModel : AdapterItemViewModel<RegionInfo, ChooseCountryViewModel>() {
    companion object{
        const val FLAG_STRING = "https://flagpedia.net/data/flags/normal/"
    }
    val country = ObservableField<String>()
    val phoneCode = ObservableField<String>()
    val image = ObservableField<Uri>()

    private var data: RegionInfo? = null

    override fun bindData(data: RegionInfo?) {
        this.data = data
        country.set(data?.countryName)
        phoneCode.set("+${data?.phoneCode}")
        image.set(Uri.parse("$FLAG_STRING${data?.countryCode}.png"))
    }

    fun onClick() {
        data?.let {
            parent.onItemSelect(it)
        }
    }
}