package com.gorbin.befriends.presentation.chooseCountry

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val chooseCountryModule = module {
    viewModel { ChooseCountryViewModel(application = get()) }
}