package com.gorbin.befriends.presentation.chooseSpecialization

import android.net.Uri
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.SpecializationArea
import com.punicapp.mvvm.adapters.AdapterItemViewModel


class AreaItemViewModel : AdapterItemViewModel<SpecializationArea, ChooseAreaViewModel>() {
    val name = ObservableField<String>()
    val image = ObservableField<Uri>()
    lateinit var data: SpecializationArea

    override fun bindData(data: SpecializationArea) {
        this.data = data
        this.name.set(data.name)
        this.image.set( Uri.parse(data.picture))
    }

    fun onClick() {
        parent.onItemSelect(data)
    }
}