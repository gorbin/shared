package com.gorbin.befriends.presentation.chooseSpecialization

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseAreaFrBinding
import com.gorbin.befriends.domain.entities.SpecializationArea
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.IBackPressHandler
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.ProfileSpecializationScreen
import com.gorbin.befriends.presentation.cicerone.SearchFormScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseAreaFragment : AbstractBaseFragment<ChooseAreaViewModel>(), IBackPressHandler {
    companion object {
        const val AREAS_EXTRA = "areas_extra"
    }

    private lateinit var data: SpecializationTree
    private val router: Router by inject()
    private lateinit var selectedSpecifications: DataSelection<SpecializationTree>
    private lateinit var flow: SpecializationFlow

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ChooseAreaViewModel.ACTION_BACK) {
            onBackPressed()
        }.register<SpecializationArea>(ChooseAreaViewModel.ACTION_SELECT) { area ->
            if (data.selectedArea?.id != area.id)
                data.selectedSpecializations = mapOf()
            data.selectedArea = area
            router.navigateTo(SpecializationScreen(data, selectedSpecifications, flow))
        }.register<Unit>(ChooseAreaViewModel.ACTION_SKIP) {
            when (flow) {
                SpecializationFlow.SEARCH -> router.backTo(SearchFormScreen)
                SpecializationFlow.PERSONAL_INFO -> router.backTo(PersonalInfoScreen())
                SpecializationFlow.EMPTY_PROFILE,
                SpecializationFlow.PROFILE -> router.backTo(ProfileSpecializationScreen)
            }
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseAreaViewModel): View {
        val binding = ChooseAreaFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        flow = arguments?.getTypedSerializable<SpecializationFlow>(GlobalValues.FLOW)!!

        data = arguments?.getTypedSerializable<SpecializationTree>(AREAS_EXTRA)!!
        selectedSpecifications = arguments?.getTypedSerializable<DataSelection<SpecializationTree>>(PersonalInfoFragment.SELECTION)
                ?: DataSelection()
        appViewModel.init(data, flow)

        return binding.root
    }

    override fun onBackPressed() {
        when (flow) {
            SpecializationFlow.SEARCH -> router.backTo(SearchFormScreen)
            SpecializationFlow.PERSONAL_INFO -> router.backTo(PersonalInfoScreen())
            SpecializationFlow.EMPTY_PROFILE,
            SpecializationFlow.PROFILE -> router.backTo(ProfileSpecializationScreen)
        }
    }
}