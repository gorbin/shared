package com.gorbin.befriends.presentation.chooseSpecialization

import android.app.Application
import androidx.databinding.ObservableBoolean
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.City
import com.gorbin.befriends.domain.entities.SpecializationArea
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer

class ChooseAreaViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_BACK = "action_back"
        const val ACTION_SELECT = "action_select"
        const val ACTION_SKIP = "action_skip"
    }

    val isSearch = ObservableBoolean()
    private val data = mutableListOf<SpecializationArea>()
    private lateinit var dataObserver: Observer<List<*>>

    fun init(data: SpecializationTree, flow: SpecializationFlow) {
        this.isSearch.set(flow == SpecializationFlow.SEARCH)
        this.data.clear()
        this.data.addAll(data.areas)
        showData()
    }

    private fun showData() {
        val list = mutableListOf<Any>()
        if (data.isNullOrEmpty()) {
            dataObserver.onNext(emptyList<City>())
        } else {
            data.forEach {
                list.add(it)
            }
            dataObserver.onNext(list)
        }
    }

    fun onItemSelect(item: SpecializationArea) {
        processor.onNext(UIAction(ACTION_SELECT, item))
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onSkipClick() {
        processor.onNext(UIAction(ACTION_SKIP))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.area_item, SpecializationArea::class.java, AreaItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also { dataObserver = it.dataReceiver }
}