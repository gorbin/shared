package com.gorbin.befriends.presentation.chooseSpecialization

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseSkillFrBinding
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.Specialization
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.IBackPressHandler
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.ProfileSpecializationScreen
import com.gorbin.befriends.presentation.cicerone.SearchFormScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseSkillFragment : AbstractBaseFragment<ChooseSkillViewModel>(), IBackPressHandler {
    companion object {
        const val SKILL_EXTRA = "skill_extra"
    }

    private lateinit var data: SpecializationTree
    private val router: Router by inject()
    private lateinit var flow: SpecializationFlow
    private lateinit var selectedSkillSpecifications: DataSelection<SpecializationTree>

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ChooseSkillViewModel.ACTION_BACK) {
            onBackPressed()
        }.register<Map<Specialization, MutableList<SkillSpecialization>>>(ChooseSkillViewModel.ACTION_DONE) {
            data.selectedSpecializations = it
            selectedSkillSpecifications.setSelectedData(data)
            when (flow) {
                SpecializationFlow.PERSONAL_INFO -> router.backTo(PersonalInfoScreen())
                SpecializationFlow.SEARCH -> router.backTo(SearchFormScreen)
                SpecializationFlow.EMPTY_PROFILE,
                SpecializationFlow.PROFILE -> router.backTo(ProfileSpecializationScreen)
            }
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseSkillViewModel): View {
        val binding = ChooseSkillFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()

        flow = arguments?.getTypedSerializable<SpecializationFlow>(GlobalValues.FLOW)!!
        data = arguments?.getTypedSerializable<SpecializationTree>(SKILL_EXTRA)!!
        selectedSkillSpecifications = arguments?.getTypedSerializable<DataSelection<SpecializationTree>>(PersonalInfoFragment.SELECTION)
                ?: DataSelection()
        appViewModel.init(data, flow)

        return binding.root
    }

    override fun onBackPressed() {
        router.replaceScreen(SpecializationScreen(data, selectedSkillSpecifications, flow))
    }
}