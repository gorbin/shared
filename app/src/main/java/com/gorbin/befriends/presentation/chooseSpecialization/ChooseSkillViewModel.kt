package com.gorbin.befriends.presentation.chooseSpecialization

import android.app.Application
import androidx.databinding.ObservableBoolean
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.Specialization
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer

class ChooseSkillViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"
    }

    private val data = mutableListOf<Specialization>()
    private lateinit var dataObserver: Observer<List<*>>
    var selectedMap = mutableMapOf<Specialization, MutableList<SkillSpecialization>>()
    val isSearch = ObservableBoolean()

    fun init(data: SpecializationTree, flow: SpecializationFlow) {
        isSearch.set(flow == SpecializationFlow.SEARCH)
        this.data.clear()
        selectedMap.clear()
        this.data.addAll(data.selectedSpecializations.keys)
        this.data.forEach {
            if (this.isSearch.get())
                selectedMap[it] = mutableListOf()
            else
                selectedMap[it] = data.selectedSpecializations[it] ?: mutableListOf()
        }
        showData()
    }

    private fun showData() {
        val list = mutableListOf<Specialization>()
        if (data.isNullOrEmpty()) {
            dataObserver.onNext(emptyList<Specialization>())
        } else {
            data.forEach {
                list.add(it)
            }
            dataObserver.onNext(list)
        }
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onDoneClick() {
        selectedMap.forEach { item ->
            if (item.value.isNullOrEmpty()) {
                processor.onNext(UIAction(ACTION_ERROR_EVENT, Throwable(context.getString(R.string.need_to_set_skill))))
                return
            }
        }
        processor.onNext(UIAction(ACTION_DONE, selectedMap.toMap()))
        selectedMap.clear()
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.skill_item, Specialization::class.java, SkillItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also { dataObserver = it.dataReceiver }

    fun onChangeCheckbox(data: Specialization, skill: SkillSpecialization, selected: Boolean) {
        if (selectedMap[data]?.contains(skill) == true) {
            if (!selected) {
                selectedMap[data]?.remove(skill)
            }
        } else if (selected) {
            selectedMap[data]?.add(skill)
            if (isSearch.get())
                onDoneClick()
        }
    }
}