package com.gorbin.befriends.presentation.chooseSpecialization

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ChooseSpecializationFrBinding
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.Specialization
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.IBackPressHandler
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ProfileSpecializationScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationAreaScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationSkillScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ChooseSpecializationFragment : AbstractBaseFragment<ChooseSpecializationViewModel>(), IBackPressHandler {
    companion object {
        const val SPECIALIZATION_EXTRA = "specialization_extra"
    }

    private lateinit var data: SpecializationTree
    private var selector: DataSelection<SpecializationTree> = DataSelection()
    private val router: Router by inject()
    private lateinit var flow: SpecializationFlow

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ChooseSpecializationViewModel.ACTION_BACK) {
            onBackPressed()
        }.register<Map<Specialization, MutableList<SkillSpecialization>>>(ChooseSpecializationViewModel.ACTION_DONE) { specializations ->
            data.selectedSpecializations = specializations.toMap()
            selector.let {
                router.navigateTo(SpecializationSkillScreen(data, it, flow))
            }
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ChooseSpecializationViewModel): View {
        val binding = ChooseSpecializationFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        flow = arguments?.getTypedSerializable<SpecializationFlow>(GlobalValues.FLOW)!!
        data = arguments?.getTypedSerializable<SpecializationTree>(SPECIALIZATION_EXTRA)!!
        selector = arguments?.getTypedSerializable<DataSelection<SpecializationTree>>(PersonalInfoFragment.SELECTION) ?: DataSelection()

        appViewModel.init(data, flow)

        return binding.root
    }

    override fun onBackPressed() {
        when (flow) {
            SpecializationFlow.PROFILE -> router.backTo(ProfileSpecializationScreen)
            else -> router.replaceScreen(SpecializationAreaScreen(data, selector, flow))
        }
    }
}