package com.gorbin.befriends.presentation.chooseSpecialization

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRegisterUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.adapters.VmAdapterItem
import io.reactivex.Observer

class ChooseSpecializationViewModel(application: Application, val useCase: IRegisterUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_BACK = "action_back"
        const val ACTION_DONE = "action_done"

        private const val SPECIALIZATION = 1
        private const val SPECIALIZATION_OPTIONS = 2
    }

    val areaName = ObservableField<String>()
    val isClean = ObservableBoolean(true)

    val selected = mutableMapOf<Specialization, MutableList<SkillSpecialization>>()
    private var area: SpecializationArea? = null
    private lateinit var dataObserver: Observer<List<VmAdapterItem>>
    private var data: List<Specialization> = emptyList()
    private var multiSelect: Boolean = true

    fun init(data: SpecializationTree, flow: SpecializationFlow) {
        isClean.set(true)
        multiSelect = (flow != SpecializationFlow.SEARCH)
        if (area != data.selectedArea) {
            dataObserver.onNext(emptyList())
            this.data = emptyList()
        }
        area = data.selectedArea
        areaName.set(area?.name)
        selected.clear()
        selected.putAll(data.selectedSpecializations)
        showSpecialization()
    }

    private fun showSpecialization() {
        val area = area
        if (area == null) {
            dataObserver.onNext(emptyList())
            data = emptyList()
            isClean.set(false)
            return
        }

        useCase.getSpecializationsByArea(area)
                .doOnError {
                    isClean.set(false)
                    data = emptyList()
                }
                .appSubscribe {
                    val specs = it
                    if (specs.isNullOrEmpty()) {
                        dataObserver.onNext(emptyList())
                    } else {
                        data = specs
                        dataObserver.onNext(data.map {
                            if (it.spheres.isNullOrEmpty())
                                VmAdapterItem(it, SPECIALIZATION)
                            else
                                VmAdapterItem(SpecializationWithSpheres(it, it.spheres!!, false), SPECIALIZATION_OPTIONS)
                        })
                    }

                    isClean.set(false)
                }
    }

    fun onItemSelect(data: Specialization, isSelected: Boolean) {
        if (isSelected) {
            if (!multiSelect) {
                selected.clear()
            }
            selected[data] = mutableListOf()
            if (!multiSelect) {
                onDoneClick()
            }
        } else {
            selected.keys.find { it.id == data.id }?.let { selected.remove(it) }
        }
    }

    fun onSphereSelected(specialization: Specialization, sphere: Sphere, isSelected: Boolean) {
        if (!multiSelect) {
            selected.clear()
        }
        val spec = selected.keys.find { it.id == specialization.id } ?: selected.put(specialization.apply { spheres = null }, mutableListOf()).let { specialization }
        if (isSelected) {
            val spheres = spec.spheres?.toMutableList() ?: mutableListOf()
            spheres.add(sphere)
            spec.spheres = spheres.distinctBy { it.uuid }
            if (!multiSelect) {
                onDoneClick()
            }
        } else {
            spec.spheres = spec.spheres?.toMutableList()?.apply { remove(sphere) }
            if (spec.spheres.isNullOrEmpty()) {
                selected.keys.find { it.id == specialization.id }?.let { selected.remove(it) }
            }
        }
    }

    fun onDoneClick() {
        processor.onNext(UIAction(ACTION_DONE, selected))
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(SPECIALIZATION, R.layout.specialization_item, Specialization::class.java, SpecializationItemViewModel::class.java)
            .defaultProducer(SPECIALIZATION_OPTIONS, R.layout.specialization_options_item, SpecializationWithSpheres::class.java, SpecializationOptionsItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                dataObserver = it.dataWithTypeReceiver
            }
}