package com.gorbin.befriends.presentation.chooseSpecialization

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.Specialization
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class SkillItemViewModel : AdapterItemViewModel<Specialization, ChooseSkillViewModel>() {
    val name = ObservableField<String>()
    lateinit var data: Specialization

    val consultation = ObservableBoolean().onChanged {
        parent.onChangeCheckbox(data, SkillSpecialization.CONSULTATION, it)
    }
    val work = ObservableBoolean().onChanged {
        parent.onChangeCheckbox(data, SkillSpecialization.WORK, it)
    }
    val teach = ObservableBoolean().onChanged {
        parent.onChangeCheckbox(data, SkillSpecialization.TEACH, it)
    }

    override fun bindData(data: Specialization) {
        this.data = data
        var spheres = ""
        data.spheres?.let {
            if (it.isNotEmpty()) spheres = ": "
            for (sphere in it) {
                spheres += "${sphere.name}; "
            }
        }
        this.name.set("${data.name}$spheres")
        consultation.set(parent.selectedMap[data]?.contains(SkillSpecialization.CONSULTATION) == true)
        work.set(parent.selectedMap[data]?.contains(SkillSpecialization.WORK) == true)
        teach.set(parent.selectedMap[data]?.contains(SkillSpecialization.TEACH) == true)
    }
}