package com.gorbin.befriends.presentation.chooseSpecialization

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.Specialization
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class SpecializationItemViewModel : AdapterItemViewModel<Specialization, ChooseSpecializationViewModel>() {
    val name = ObservableField<String>()
    val checkbox = ObservableBoolean()
    lateinit var data: Specialization

    override fun bindData(data: Specialization) {
        this.data = data
        name.set(data.name)
        checkbox.set(parent.selected.keys.any { it.id == data.id })
    }

    fun onClick() {
        checkbox.set(!checkbox.get())
        parent.onItemSelect(data, checkbox.get())
    }
}