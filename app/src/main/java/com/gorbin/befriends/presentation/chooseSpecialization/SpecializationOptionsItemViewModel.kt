package com.gorbin.befriends.presentation.chooseSpecialization

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.SpecializationWithSpheres
import com.gorbin.befriends.domain.entities.SphereItem
import com.punicapp.mvvm.adapters.AdapterItemViewModel
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer


class SpecializationOptionsItemViewModel : AdapterItemViewModel<SpecializationWithSpheres, ChooseSpecializationViewModel>() {
    val name = ObservableField<String>()
    val expanded = ObservableBoolean()
    lateinit var data: SpecializationWithSpheres
    private var observer: Observer<List<*>>? = null

    override fun bindData(data: SpecializationWithSpheres) {
        this.data = data
        name.set(data.specialization.name)
        expanded.set(data.expanded)
        observer?.onNext(data.sphereList.map { SphereItem(it, data.specialization) })
    }

    fun onClick() {
        expanded.set(!expanded.get())
        data.expanded = expanded.get()
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(parent, BR.viewModel)
            .defaultProducer(1, R.layout.sphere_item, SphereItem::class.java, SphereItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                if (observer == null) {
                    observer = it.dataReceiver
                    it.dataReceiver.onNext(data.sphereList.map { SphereItem(it, data.specialization) })
                }
            }
}