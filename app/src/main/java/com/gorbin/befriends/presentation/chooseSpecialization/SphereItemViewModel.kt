package com.gorbin.befriends.presentation.chooseSpecialization

import android.view.View
import android.widget.CheckBox
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.SphereItem
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class SphereItemViewModel : AdapterItemViewModel<SphereItem, ChooseSpecializationViewModel>() {
    val name = ObservableField<String>()
    val checked = ObservableBoolean()
    lateinit var data: SphereItem

    override fun bindData(data: SphereItem) {
        this.data = data
        name.set(data.sphere.name)
        checked.set(parent.selected.keys.find { data.specialization.id == it.id }?.spheres?.any { it.uuid == data.sphere.uuid } ?: false)
    }

    fun onClick(v: View) {
        checked.set(!(v as CheckBox).isChecked)
        parent.onSphereSelected(data.specialization, data.sphere, checked.get())
    }
}