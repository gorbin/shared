package com.gorbin.befriends.presentation.chooseSpecialization

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val chooseSpecializationModule = module {
    viewModel { ChooseAreaViewModel(application = get()) }
    viewModel { ChooseSpecializationViewModel(application = get(), useCase = get()) }
    viewModel { ChooseSkillViewModel(application = get()) }

}