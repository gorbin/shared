package com.gorbin.befriends.presentation.cicerone

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Replace

class DefaultAppNavigator(
        activity: FragmentActivity,
        containerId: Int,
        fragmentManager: FragmentManager = activity.supportFragmentManager
) : SupportAppNavigator(activity, fragmentManager, containerId) {
    override fun backToUnexisting(screen: SupportAppScreen?) {
        fragmentReplace(Replace(screen))
    }
}