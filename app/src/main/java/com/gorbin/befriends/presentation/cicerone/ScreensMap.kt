package com.gorbin.befriends.presentation.cicerone

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.chat.ChatFragment
import com.gorbin.befriends.presentation.chooseCity.ChooseCityFragment
import com.gorbin.befriends.presentation.chooseCity.ChooseCountryCityFragment
import com.gorbin.befriends.presentation.chooseCountry.ChooseCountryFragment
import com.gorbin.befriends.presentation.chooseSpecialization.ChooseAreaFragment
import com.gorbin.befriends.presentation.chooseSpecialization.ChooseSkillFragment
import com.gorbin.befriends.presentation.chooseSpecialization.ChooseSpecializationFragment
import com.gorbin.befriends.presentation.editProfile.EditProfileFragment
import com.gorbin.befriends.presentation.enterPassword.EnterPasswordFragment
import com.gorbin.befriends.presentation.executorProfile.ExecutorProfileFragment
import com.gorbin.befriends.presentation.gallery.GalleryFragment
import com.gorbin.befriends.presentation.myRequests.MyRequestsFragment
import com.gorbin.befriends.presentation.passwordRecovery.NewPasswordFragment
import com.gorbin.befriends.presentation.passwordRecovery.PasswordRecoveryFragment
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoFragment
import com.gorbin.befriends.presentation.phoneCallError.PhoneCallErrorFragment
import com.gorbin.befriends.presentation.priceList.PriceListFragment
import com.gorbin.befriends.presentation.priceList.PricePositionFragment
import com.gorbin.befriends.presentation.profile.AboutFragment
import com.gorbin.befriends.presentation.profile.ConsultFragment
import com.gorbin.befriends.presentation.profile.ProfileFragment
import com.gorbin.befriends.presentation.profile.ProfileSpecializationFragment
import com.gorbin.befriends.presentation.requestsToMe.RequestsToMeFragment
import com.gorbin.befriends.presentation.review.*
import com.gorbin.befriends.presentation.search.SearchFormFragment
import com.gorbin.befriends.presentation.search.SearchResultFragment
import com.gorbin.befriends.presentation.settings.SettingsFragment
import com.gorbin.befriends.presentation.signIn.SignInFragment
import com.gorbin.befriends.presentation.signUp.SignUpFragment
import com.gorbin.befriends.presentation.smsCode.SmsCodeFragment
import com.gorbin.befriends.presentation.splash.SplashFragment
import com.gorbin.befriends.presentation.welcome.WelcomeFragment
import com.gorbin.befriends.utils.GlobalValues
import ru.terrakok.cicerone.android.support.SupportAppScreen

sealed class Screen(
        private val clazz: Class<out AbstractBaseFragment<*>>,
        private val args: Bundle? = null
) : SupportAppScreen() {
    override fun getFragment(): Fragment {
        val fragment = clazz.newInstance()
        fragment.arguments = args
        return fragment
    }
}

object SplashScreen : Screen(SplashFragment::class.java)
object WelcomeScreen : Screen(WelcomeFragment::class.java)
object SignInScreen : Screen(SignInFragment::class.java)
object SignUpScreen : Screen(SignUpFragment::class.java)
class ChooseCountryScreen(data: List<RegionInfo>, dataSelection: DataSelection<RegionInfo>) : Screen(ChooseCountryFragment::class.java, bundleOf(ChooseCountryFragment.CHOOSE_COUNTRY_EXTRA to data, SignInFragment.SELECTION to dataSelection))
class SighInPasswordScreen(phone: String) : Screen(EnterPasswordFragment::class.java, bundleOf(GlobalValues.PHONE to phone))
class ForgotPasswordScreen(phone: String, data: PinData) : Screen(PasswordRecoveryFragment::class.java, bundleOf(GlobalValues.PHONE to phone, PasswordRecoveryFragment.PIN_DATA to data))
object PhoneCallErrorScreen : Screen(PhoneCallErrorFragment::class.java)
class PersonalInfoScreen(isNewLaunch: Boolean = false) : Screen(PersonalInfoFragment::class.java, bundleOf(PersonalInfoFragment.IS_NEW_LAUNCH to isNewLaunch))
class ChooseCountryCityScreen(data: List<Country>, selected: Pair<Country?, City?>?, dataSelection: DataSelection<Pair<Country, City>>, flow: CitySelectionFlow) : Screen(ChooseCountryCityFragment::class.java, bundleOf(ChooseCountryCityFragment.COUNTRIES_EXTRA to data, PersonalInfoFragment.SELECTED_CITY_EXTRA to selected, PersonalInfoFragment.SELECTION_CITY_EXTRA to dataSelection, GlobalValues.FLOW to flow))
class ChooseCityScreen(data: Country, selected: Pair<Country?, City?>?, dataSelection: DataSelection<Pair<Country, City>>, flow: CitySelectionFlow) : Screen(ChooseCityFragment::class.java, bundleOf(ChooseCityFragment.COUNTRY_EXTRA to data, PersonalInfoFragment.SELECTED_CITY_EXTRA to selected, PersonalInfoFragment.SELECTION_CITY_EXTRA to dataSelection, GlobalValues.FLOW to flow))
class SmsCodeScreen(data: Pair<String, SignUpTokens>) : Screen(SmsCodeFragment::class.java, bundleOf(SmsCodeFragment.SIGN_UP_TOKENS to data.second, GlobalValues.PHONE to data.first))
class SpecializationAreaScreen(data: SpecializationTree, selector: DataSelection<SpecializationTree>, flow: SpecializationFlow) : Screen(ChooseAreaFragment::class.java, bundleOf(ChooseAreaFragment.AREAS_EXTRA to data, PersonalInfoFragment.SELECTION to selector, GlobalValues.FLOW to flow))
class SpecializationScreen(data: SpecializationTree, selector: DataSelection<SpecializationTree>, flow: SpecializationFlow) : Screen(ChooseSpecializationFragment::class.java, bundleOf(ChooseSpecializationFragment.SPECIALIZATION_EXTRA to data, PersonalInfoFragment.SELECTION to selector, GlobalValues.FLOW to flow))
class SpecializationSkillScreen(data: SpecializationTree, selector: DataSelection<SpecializationTree>, flow: SpecializationFlow) : Screen(ChooseSkillFragment::class.java, bundleOf(ChooseSkillFragment.SKILL_EXTRA to data, PersonalInfoFragment.SELECTION to selector, GlobalValues.FLOW to flow))
object ProfileScreen : Screen(ProfileFragment::class.java)
object MyRequestsScreen : Screen(MyRequestsFragment::class.java)
class ExecutorProfileScreen(userId: String, searchForm: SearchForm? = null, isFromSearch: Boolean = false) : Screen(ExecutorProfileFragment::class.java, bundleOf(ExecutorProfileFragment.USER_ID to userId, ExecutorProfileFragment.SEARCH_FORM to searchForm, ExecutorProfileFragment.IS_FROM_SEARCH to isFromSearch))
object SearchFormScreen : Screen(SearchFormFragment::class.java)
class SearchResultScreen(searchForm: SearchForm) : Screen(SearchResultFragment::class.java, bundleOf(SearchResultFragment.DATA to searchForm))
object RequestsToMeScreen : Screen(RequestsToMeFragment::class.java)
object SettingsScreen : Screen(SettingsFragment::class.java)
class MarksScreen(userUUID: String, requestId: Long? = null, type: ServiceType, selector: DataSelection<ReviewBody>? = null) : Screen(MarksFragment::class.java, bundleOf(MarksFragment.USER_UUID to userUUID, MarksFragment.REQUEST_ID to requestId, MarksFragment.SERVICE_TYPE to type, MarksFragment.DATA_SELECTOR to selector))
class CommentScreen(data: ReviewBody, selector: DataSelection<ReviewBody>? = null) : Screen(CommentFragment::class.java, bundleOf(CommentFragment.REVIEW_DATA to data, CommentFragment.DATA_SELECTOR to selector))
object MyRatingAndReviewsScreen : Screen(MyRatingAndReviewsFragment::class.java)
class FullReviewScreen(review: Review) : Screen(FullReviewFragment::class.java, bundleOf(FullReviewFragment.REVIEW to review))
object AllReviewsScreen : Screen(AllReviewsFragment::class.java)
class ChatScreen(request: MyRequest, role: Role, initial: Boolean = false) : Screen(ChatFragment::class.java, bundleOf(ChatFragment.REQUEST to request, ChatFragment.ROLE to role, ChatFragment.INITIAL to initial))
object PriceListScreen : Screen(PriceListFragment::class.java)
class PricePositionScreen(priceList: List<ServicePrice>? = null, editPosition: Int? = null) : Screen(PricePositionFragment::class.java, bundleOf(PricePositionFragment.PRICE_LIST to priceList, PricePositionFragment.PRICE_POSITION to editPosition))
object AboutScreen : Screen(AboutFragment::class.java)
object ProfileSpecializationScreen : Screen(ProfileSpecializationFragment::class.java)
object GalleryScreen : Screen(GalleryFragment::class.java)
object ConsultScreen : Screen(ConsultFragment::class.java)
object EditProfileScreen : Screen(EditProfileFragment::class.java)
class NewPasswordScreen(rememberToken: String) : Screen(NewPasswordFragment::class.java, bundleOf(GlobalValues.REMEMBER_TOKEN to rememberToken))

