package com.gorbin.befriends.presentation.editProfile

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.ConfigurationCompat
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.EditProfileFrBinding
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.ChooseCountryCityScreen
import com.gorbin.befriends.presentation.cicerone.ChooseCountryScreen
import com.gorbin.befriends.presentation.personalInfo.PersonalInfoViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.PhoneFormattedUtils
import com.gorbin.befriends.utils.PhoneNumberValidityChangeListener
import com.gorbin.befriends.utils.RegionChangeListener
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.miguelbcr.ui.rx_paparazzo2.entities.Response
import com.punicapp.mvvm.actions.UIActionConsumer
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class EditProfileFragment : AbstractBaseFragment<EditProfileViewModel>() {
    companion object {
        private const val SOURCE_SELECTOR = "source_selector"
    }

    private var binding: EditProfileFrBinding? = null
    private val router: Router by inject()
    private var dataSelection: DataSelection<RegionInfo> = DataSelection()
    private val citySelector = DataSelection<Pair<Country, City>>()
    private lateinit var formatter: PhoneFormattedUtils

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(EditProfileViewModel.ACTION_DONE) {
            router.exit()
        }.register<Unit>(EditProfileViewModel.ACTION_IMAGE_SELECTION) {
            showImageSourceSelectionDialog()
        }.register<Unit>(EditProfileViewModel.ACTION_COUNTRY_CODE_SELECTION) {
            router.navigateTo(ChooseCountryScreen(formatter.codes, dataSelection))
        }.register<Pair<List<Language>, List<Language>>>(EditProfileViewModel.ACTION_LANGUAGE_SELECTION) {
            showLanguageSelectionDialog(it)
        }.register<Pair<List<Country>, Pair<Country, City>>>(PersonalInfoViewModel.ACTION_COUNTRY_SELECTION) {
            router.navigateTo(ChooseCountryCityScreen(it.first, it.second, citySelector, CitySelectionFlow.PROFILE_EDITING))
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: EditProfileViewModel): View {
        if (binding != null) {
            return binding!!.root
        }

        binding = EditProfileFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel
        citySelector.listener = appViewModel.getCitySelectionListener()
        formatter = PhoneFormattedUtils(context!!).apply {
            registerEditText(binding!!.phone, binding!!.countryCode)
            setPhoneNumberValidityChangeListener(object : PhoneNumberValidityChangeListener {
                override fun onValidityChanged(isValidNumber: Boolean) {
                    appViewModel.updatePhoneValidity(isValidNumber)
                }
            })
            setRegionChangeListener(object : RegionChangeListener {
                override fun onText(string: String) {
                    appViewModel.onText(string)
                }

                override fun onRegion(region: RegionInfo) {
                    appViewModel.onRegion(region)
                }

                override fun onNotFoundRegion() {
                    appViewModel.onNotFoundRegion()
                }
            })
        }

        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
        formatter.setPhoneCode(locale.country.toLowerCase(Locale.getDefault()))

        dataSelection.listener = object : DataSelection.Listener<RegionInfo> {
            override fun onChange(data: RegionInfo) {
                formatter.setPhoneCode(data.countryCode)
            }
        }

        appViewModel.init(formatter)
        return binding!!.root
    }

    private fun showImageSourceSelectionDialog() {
        if (activity?.isFinishing == true)
            return

        val options = UCrop.Options()
        options.setToolbarTitle("")
        options.withAspectRatio(1F, 1F)
        options.setStatusBarColor(Color.BLACK)
        options.setToolbarWidgetColor(Color.WHITE)
        options.setActiveWidgetColor(Color.BLACK)
        options.setToolbarColor(Color.BLACK)

        DialogProvider.getImageSourceBottomSheetPickerDialog { source ->
            when (source) {
                GlobalValues.GALLERY_SOURCE -> {
                    getPhotoFromGallery(options)
                }
                GlobalValues.CAMERA_SOURCE -> {
                    getPhotoFromCamera(options)
                }
            }
        }.show(childFragmentManager, SOURCE_SELECTOR)
    }

    private fun getPhotoFromCamera(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getPhotoFromGallery(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getImage(response: Response<*, FileData>) {
        if (response.resultCode() == Activity.RESULT_OK) {
            appViewModel.uploadImage(response.data())
        }
    }

    private fun showLanguageSelectionDialog(data: Pair<List<Language>, List<Language>>) {
        DialogProvider.getLangMultiSelectionDialog(requireContext(), appViewModel, data.first, data.second)
                .show()
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarText = getString(R.string.profile_editing),
            toolbarVisible = true,
            iconId = R.drawable.ic_arrow_back_24
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}