package com.gorbin.befriends.presentation.editProfile

import android.app.Application
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.PhoneFormattedUtils
import com.gorbin.befriends.utils.onChanged
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider

class EditProfileViewModel(application: Application, private val useCase: IProfileUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_IMAGE_SELECTION = "action_image_selection"
        const val ACTION_COUNTRY_CODE_SELECTION = "action_country_code_selection"
        const val ACTION_LANGUAGE_SELECTION = "action_language_selection"
        const val ACTION_COUNTRY_SELECTION = "action_city_selection"

        const val NAME_ERROR = "name_error"
        const val SURNAME_ERROR = "surname_error"
        const val PHONE_NUMBER_ERROR = "phone_number_error"
        const val NICKNAME_ERROR = "nickname_error"
        const val LANGUAGE_ERROR = "language_error"
        const val CITY_ERROR = "city_error"
    }

    val avatar = ObservableField<Uri>()
    val name = ObservableField<String>().onChanged { errors[NAME_ERROR] = null }
    val surname = ObservableField<String>().onChanged { errors[SURNAME_ERROR] = null }
    val phoneNumber = ObservableField<String>()
    val city = ObservableField<String>().onChanged { errors[CITY_ERROR] = null }
    val nickname = ObservableField<String>().onChanged { errors[NICKNAME_ERROR] = null }
    val languageProficiency = ObservableField<String>().onChanged { errors[LANGUAGE_ERROR] = null }
    val showNickname = ObservableBoolean()
    var phone: String = ""

    val errors = ObservableArrayMap<String, String>()
    var data: User? = null
    private val countries = mutableListOf<Country>()
    private var isPhoneValid: Boolean = true
    private var selectedCity: Pair<Country, City>? = null
    private val languages = mutableListOf<Language>()
    private val selectedLanguages = mutableListOf<Language>()
    private lateinit var formatter: PhoneFormattedUtils

    fun init(formatter: PhoneFormattedUtils) {
        this.formatter = formatter
        errors.clear()
        useCase.getEditInfo()
                .appSubscribe { (profile, langList, countryList) ->
                    this.data = profile
                    this.languages.clear()
                    this.languages.addAll(langList)
                    this.countries.clear()
                    this.countries.addAll(countryList)
                    selectedLanguages.clear()
                    selectedLanguages.addAll(langList.filter { language -> profile.languageList.any { it.id == language.id } })
                    avatar.set(profile.logo?.let { Uri.parse(it) })
                    name.set(profile.name)
                    surname.set(profile.surname)
                    city.set("${profile.country}, ${profile.city}")
                    phone = profile.phone
                    phoneNumber.set(profile.phone)
                    languageProficiency.set(profile.language)
                    nickname.set(profile.username)
                    showNickname.set(profile.showNickname)
                }
    }

    fun updatePhoneValidity(isValid: Boolean) {
        isPhoneValid = isValid
        errors[PHONE_NUMBER_ERROR] = null
    }

    fun loadPhoto() {
        processor.onNext(UIAction(ACTION_IMAGE_SELECTION))
    }

    fun uploadImage(data: FileData?) {
        data?.file?.let { file ->
            useCase.uploadPhoto(file, true)
                    .appSubscribe {
                        avatar.set(Uri.parse(it.urlFile))
                    }
        }
    }

    fun selectCountry() {
        processor.onNext(UIAction(ACTION_COUNTRY_CODE_SELECTION))
    }

    fun onDoneClick() {
        if (!isValid())
            return

        if (data == null)
            return

        val personalDataBody = PersonalDataBody(
                phone = formatter.getNormalizePhone().takeIf { it.isNotBlank() && it.length > 5 },
                name = name.get(),
                surname = surname.get(),
                username = nickname.get(),
                showNickname = showNickname.get(),
                languages = selectedLanguages.toServerFormat(),
                idCity = selectedCity?.second?.getId()
        )
        useCase.updateProfile(personalDataBody).appSubscribe {
            processor.onNext(UIAction(ACTION_DONE))
        }
    }

    fun onText(string: String) {
        if (string == "+") {
            errors[PHONE_NUMBER_ERROR] = context.getString(R.string.choose_country)
        }
    }

    fun selectCity() {
        processor.onNext(UIAction(ACTION_COUNTRY_SELECTION, countries to selectedCity))
    }

    fun selectLanguages() {
        processor.onNext(UIAction(ACTION_LANGUAGE_SELECTION, languages to selectedLanguages))
    }

    fun onRegion(region: RegionInfo) {
        errors.remove(PHONE_NUMBER_ERROR)
    }

    fun onNotFoundRegion() {
        errors[PHONE_NUMBER_ERROR] = context.getString(R.string.register_region_code_incorrect)
    }

    fun getCitySelectionListener(): DataSelection.Listener<Pair<Country, City>> = object : DataSelection.Listener<Pair<Country, City>> {
        override fun onChange(data: Pair<Country, City>) {
            Handler().postDelayed({
                selectedCity = data
                city.set("${data.first.name}, ${data.second.name}")
            }, 100)
        }
    }

    private fun isValid(): Boolean {
        var result = true
        if (name.get().isNullOrBlank()) {
            result = false
            errors[NAME_ERROR] = context.getString(R.string.value_is_missing)
        }
        if (surname.get().isNullOrBlank()) {
            result = false
            errors[SURNAME_ERROR] = context.getString(R.string.value_is_missing)
        }
        if (showNickname.get() && nickname.get().isNullOrBlank()) {
            result = false
            errors[NICKNAME_ERROR] = context.getString(R.string.value_is_missing)
        }
        if (languageProficiency.get().isNullOrBlank()) {
            result = false
            errors[LANGUAGE_ERROR] = context.getString(R.string.value_is_missing)
        }
        if (city.get().isNullOrBlank()) {
            result = false
            errors[CITY_ERROR] = context.getString(R.string.value_is_missing)
        }
//        if (formatter.getNormalizePhone().isNotBlank() && !isPhoneValid) {
//            result = false
//            errors[PHONE_NUMBER_ERROR] = context.getString(R.string.empty_phone_error)
//        }

        return result
    }

    private fun List<Language>.toServerFormat(): List<LanguagesBody?>? {
        if (this.isEmpty())
            return null

        val result = mutableListOf<LanguagesBody>()
        this.forEach { language ->
            result.add(LanguagesBody(language.id.toString()))
        }

        return result
    }

    private fun List<Language>.toDisplayFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        this.forEachIndexed { index, language ->
            result += if (index == 0)
                language.name
            else
                ", ${language.name}"
        }

        return result
    }

    override fun onActionFromChild(action: UIAction?) {
        super.onActionFromChild(action)
        when (action?.actionId) {
            GlobalValues.SELECTED_LANGUAGES -> {
                val langCollection = (action.data as? Bundle)?.getSerializable(GlobalValues.SELECTED_LANGUAGES) as? LanguageCollection
                langCollection?.languages?.let {
                    selectedLanguages.clear()
                    selectedLanguages.addAll(it)
                    languageProficiency.set(selectedLanguages.toDisplayFormat())
                }
            }
        }
    }

    override fun provideVm(): AppViewModel {
        return this
    }
}