package com.gorbin.befriends.presentation.editProfile

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val editProfileModule = module {
    viewModel { EditProfileViewModel(application = get(), useCase = get()) }
}