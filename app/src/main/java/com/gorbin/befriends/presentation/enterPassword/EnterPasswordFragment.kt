package com.gorbin.befriends.presentation.enterPassword

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.EnterPasswordFrBinding
import com.gorbin.befriends.domain.entities.PinData
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ForgotPasswordScreen
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.ProfileScreen
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class EnterPasswordFragment : AbstractBaseFragment<EnterPasswordViewModel>() {
    private var binding: EnterPasswordFrBinding? = null
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<PinData>(EnterPasswordViewModel.ACTION_FORGOT_PASSWORD) {
            router.navigateTo(ForgotPasswordScreen(appViewModel.phone, it))
        }.register<Boolean>(EnterPasswordViewModel.ACTION_DONE) {
            if (it)
                router.newRootScreen(ProfileScreen)
            else
                router.newRootScreen(PersonalInfoScreen(true))
        }.register<Unit>(EnterPasswordViewModel.ACTION_BACK) {
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: EnterPasswordViewModel): View {
        binding = EnterPasswordFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        val phone = arguments?.getString(GlobalValues.PHONE)
        if (phone == null) {
            router.exit()
        } else {
            appViewModel.init(phone)
        }

        return binding!!.root
    }
}