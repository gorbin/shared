package com.gorbin.befriends.presentation.enterPassword

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.gorbin.befriends.R
import com.gorbin.befriends.data.api.model.ErrorList
import com.gorbin.befriends.domain.entities.LoginData
import com.gorbin.befriends.domain.entities.PhoneData
import com.gorbin.befriends.domain.usecases.ILoginUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.presentation.base.viewmodel.IErrorHandler
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import retrofit2.HttpException

class EnterPasswordViewModel(application: Application, private val useCase: ILoginUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"
        const val ACTION_FORGOT_PASSWORD = "action_forgot_password"

        private const val LOGIN_NOT_FOUND = 1021L
        private const val CALL_FAILED = 1022L
    }

    val password = ObservableField<String>().onChanged { check() }
    val isDoneEnabled = ObservableBoolean(true)
    lateinit var phone: String
    val error = ObservableField<String>()
    private val isValid = ObservableBoolean()

    fun init(phone: String) {
        this.phone = phone
        password.set("")
        error.set(null)
    }

    private fun check() {
        error.set(null)
    }

    fun onForgotClick() {
        useCase.requestCall(PhoneData(phone))
                .appSubscribe(
                        errorHandler = getRecoveryErrorHandler()
                ) {
                    if (it.pinAttempt != null) {
                        processor.onNext(UIAction(ACTION_FORGOT_PASSWORD, it))
                    }
                }
    }

    fun onDoneClick() {
        Log.e("CheckMz", "onDoneClick() + ${password.get()}")
        isValid.set((password.get()?.length ?: 0) > 0)

        if (!isValid.get()) {
            this.error.set(context.getString(R.string.error_password_valid))
            return
        }

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { result ->
            val data = LoginData(phone, password.get()!!)
            useCase.login(data, result.token).appSubscribe {
                processor.onNext(UIAction(ACTION_DONE, it))
            }
        }.addOnFailureListener {
            Log.e("CheckMz", "addOnFailureListener() ${password.get()} = $it")
            val data = LoginData(phone, password.get()!!)
            useCase.login(data, "").appSubscribe {
                processor.onNext(UIAction(ACTION_DONE, it))
            }
        }
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    private fun getRecoveryErrorHandler(): IErrorHandler = object : IErrorHandler {
        override fun handleError(error: HttpException): Boolean {
            return when (error.code()) {
                409 -> {
                    val body = error.response().errorBody()?.source()?.buffer()?.readUtf8()
                            ?: return false
                    val errorList = Gson().fromJson(body, ErrorList::class.java)
                    var errorProcessed = false
                    errorList.errorList.forEach {
                        when (it.code) {
                            LOGIN_NOT_FOUND -> {
                                this@EnterPasswordViewModel.error.set(context.getString(R.string.login_not_found))
                                errorProcessed = true
                            }
                            CALL_FAILED -> {
                                errorProcessed = true
                            }
                            else -> handleError(Throwable(it.message))
                        }
                    }
                    errorProcessed
                }
                else -> false
            }
        }
    }

    override fun handleError(error: Throwable) {
        if (error is HttpException) {
            if (error.code() == 403) {
                this.error.set(context.getString(R.string.error_wrong_password))
            } else if (error.code() == 404) {
                this.error.set(context.getString(R.string.error_wrong_user))
            }
            return
        }

        super.handleError(error)
    }
}