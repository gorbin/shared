package com.gorbin.befriends.presentation.enterPassword

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val enterPasswordModule = module {
    viewModel { EnterPasswordViewModel(application = get(), useCase = get()) }
}