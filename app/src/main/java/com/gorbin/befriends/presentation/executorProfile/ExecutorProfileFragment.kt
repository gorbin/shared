package com.gorbin.befriends.presentation.executorProfile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.ExecutorProfileFrBinding
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.SearchForm
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ChatScreen
import com.gorbin.befriends.presentation.cicerone.FullReviewScreen
import com.gorbin.befriends.presentation.cicerone.SignInScreen
import com.gorbin.befriends.presentation.review.ReviewViewModel
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ExecutorProfileFragment : AbstractBaseFragment<ExecutorProfileViewModel>() {
    companion object {
        const val USER_ID = "user_id"
        const val IS_FROM_SEARCH = "is_from_search"
        const val SEARCH_FORM = "search_form"
    }

    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ExecutorProfileViewModel.ACTION_BACK) {
            router.exit()
        }.register<String>(ExecutorProfileViewModel.ACTION_ABOUT) {
            DialogProvider.getAboutDialog(requireContext(), it, appViewModel)
                    .show()
        }.register<MyRequest>(ExecutorProfileViewModel.ACTION_NEW_REQUEST) {
            router.replaceScreen(ChatScreen(it, Role.CUSTOMER, true))
        }.register<Unit>(ExecutorProfileViewModel.ACTION_AUTH) {
            router.navigateTo(SignInScreen)
        }.register<Review>(ReviewViewModel.ACTION_FULL_REVIEW) {
            router.navigateTo(FullReviewScreen(it))
        }.register<Unit>(ExecutorProfileViewModel.ACTION_COMPLAINT_REGISTERED) {
            DialogProvider.getMessageDialog(requireContext(), R.string.complaint_registered_title, R.string.complaint_registered_message, appViewModel)
                    .show()
        }.register<Unit>(ExecutorProfileViewModel.ACTION_COMPLAIN) {
            if (activity?.isFinishing == true)
                return@register

            DialogProvider.getBottomSheetDialog(this, R.layout.bottom_sheet_complaint, appViewModel, true)
                    .show()
        }
    }

    override fun initBinding(
            inflater: LayoutInflater,
            container: ViewGroup?,
            appViewModel: ExecutorProfileViewModel
    ): View {
        val binding = ExecutorProfileFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        val isFromSearch = arguments?.getBoolean(IS_FROM_SEARCH, false) ?: false
        val searchForm = arguments?.getTypedSerializable<SearchForm>(SEARCH_FORM)
        arguments?.getString(USER_ID)?.let {
            appViewModel.init(it, searchForm, isFromSearch)
        }

        return binding.root
    }

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}