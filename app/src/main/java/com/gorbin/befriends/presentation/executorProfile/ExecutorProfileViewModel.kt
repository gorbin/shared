package com.gorbin.befriends.presentation.executorProfile

import android.app.Application
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.domain.usecases.IUsersUseCase
import com.gorbin.befriends.presentation.review.ProfileReviewItemViewModel
import com.gorbin.befriends.presentation.review.ReviewViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.onChanged
import com.gorbin.befriends.utils.safeLet
import com.gorbin.befriends.utils.toDisplayRating
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class ExecutorProfileViewModel(application: Application, private val useCase: IUsersUseCase, private val requestsUseCase: IRequestsUseCase) : ReviewViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_BACK = "action_back"
        const val ACTION_ABOUT = "action_about"
        const val ACTION_NEW_REQUEST = "action_new_request"
        const val ACTION_AUTH = "action_auth"
        const val ACTION_COMPLAINT_REGISTERED = "action_complaint_registered"
        const val ACTION_COMPLAIN = "action_complain"
    }

    val complaintComment = ObservableField<String>().onChanged { complaintError.set(null) }
    val complaintError = ObservableField<String>()
    private var complaintReason: ComplaintType? = null

    val gallery = ObservableField<List<String>>(emptyList())
    val name = ObservableField<String>()
    val rating = ObservableField<String>()
    val about = ObservableField<String>()
    val cost = ObservableField<String>()
    val experience = ObservableField<String>()
    val orders = ObservableField<String>()
    val location = ObservableField<String>()
    val languages = ObservableField<String>()
    val isAuthorized = ObservableBoolean()
    var profile: User? = null
    val aboutVisible = ObservableBoolean(true)
    val specializationVisible = ObservableBoolean()
    val pricesVisible = ObservableBoolean()
    val commentsVisible = ObservableBoolean()
    val isFromSearch = ObservableBoolean()
    val idSpecialization = ObservableField<String>()
    val isNewbie = ObservableBoolean()
    val showPlaceholder = ObservableBoolean(false)
    private var serviceType: ServiceType? = null
    private lateinit var specObserver: Observer<List<*>>
    private lateinit var pricesObserver: Observer<List<*>>
    private lateinit var reviewsObserver: Observer<List<*>>

    fun init(userId: String, searchForm: SearchForm?, isFromSearch: Boolean) {
        wipeBottomComplaint()
        gallery.set(emptyList())
        this.isFromSearch.set(isFromSearch)
        this.idSpecialization.set(searchForm?.idSpecialization)
        this.serviceType = searchForm?.idService?.toIntOrNull()?.let { ServiceType.getById(it) }
        Single.zip(useCase.isAuthorized(), useCase.getProfile(userId), BiFunction { isAuthorized: Boolean, profileData: Triple<User, List<ServicePrice>, List<Review>> ->
            isAuthorized to profileData
        }).appSubscribe {
            this.isAuthorized.set(it.first)
            val profile = it.second.first
            this.profile = profile
            gallery.set(profile.getGallery().take(GlobalValues.GALLERY_SIZE).map { it.photoUrl })
            showPlaceholder.set(profile.getGallery().isNullOrEmpty())
            name.set(profile.getNameOrNickname())
            rating.set(if (profile.comments == 0) "-" else profile.rating.toDisplayRating())
            about.set(if (profile.about.isNullOrBlank()) null else profile.about)
            cost.set("$ ${profile.costService.toInt()}")
            experience.set(profile.experience)
            orders.set(profile.requestClosed.toString())
            location.set("${profile.country}, ${profile.city}")
            languages.set(profile.language)
            isNewbie.set(profile.isNewbie())

            specObserver.onNext(profile.specializationList ?: emptyList<ProfileSpecialization>())
            pricesObserver.onNext(it.second.second)
            reviewsObserver.onNext(it.second.third)
        }
    }

    private fun wipeBottomComplaint() {
        complaintComment.set("")
    }

    fun back() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onAboutClick() {
        aboutVisible.set(true)
        specializationVisible.set(false)
        pricesVisible.set(false)
        commentsVisible.set(false)
    }

    fun onSpecClick() {
        aboutVisible.set(false)
        specializationVisible.set(true)
        pricesVisible.set(false)
        commentsVisible.set(false)
    }

    fun onPricesClick() {
        aboutVisible.set(false)
        specializationVisible.set(false)
        pricesVisible.set(true)
        commentsVisible.set(false)
    }

    fun onCommentsClick() {
        aboutVisible.set(false)
        specializationVisible.set(false)
        pricesVisible.set(false)
        commentsVisible.set(true)
    }

    fun makeRequest() {
        if (isAuthorized.get()) {
            safeLet(profile?.uuidUser, idSpecialization.get(), serviceType) { uuidUser, idSpecialization, serviceType ->
                requestsUseCase.createRequest(uuidUser, idSpecialization, serviceType)
                        .appSubscribe {
                            processor.onNext(UIAction(ACTION_NEW_REQUEST, it))
                        }
            }
        } else {
            processor.onNext(UIAction(ACTION_AUTH))
        }
    }

    fun complain() {
        processor.onNext(UIAction(ACTION_COMPLAIN))
    }

    fun sendComplaint() {
        if (complaintComment.get().isNullOrBlank() && complaintReason == ComplaintType.OTHER) {
            complaintError.set(context.getString(R.string.value_is_missing))
            return
        }

        safeLet(complaintReason, profile?.uuidUser) { reason, uuidUser ->
            useCase.sendComplaint(uuidUser, complaintComment.get()?:"null", reason)
                    .appSubscribe {
                        if (it) {
                            dismissBottomSheetDialog()
                            processor.onNext(UIAction(ACTION_COMPLAINT_REGISTERED))
                        }
                    }
        }
    }

    override fun provideVm(): AppViewModel {
        return this
    }

    fun getComplaintReasonListener(): RadioGroup.OnCheckedChangeListener =
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val reasonNumber = when (checkedId) {
                    R.id.complaint_reason_0 -> 0
                    R.id.complaint_reason_1 -> 1
                    R.id.complaint_reason_2 -> 2
                    R.id.complaint_reason_3 -> 3
                    R.id.complaint_reason_4 -> 4
                    R.id.complaint_reason_5 -> 5
                    else -> -1
                }
                complaintReason = ComplaintType.values().getOrNull(reasonNumber)
            }

    fun getSpecAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.profile_specialization_item, ProfileSpecialization::class.java, ProfileSpecializationItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                this.specObserver = it.dataReceiver
            }

    fun getPriceAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.profile_prices_item, ServicePrice::class.java, ProfilePriceItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                this.pricesObserver = it.dataReceiver
            }

    fun getReviewsAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(1, R.layout.profile_review_item, Review::class.java, ProfileReviewItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                this.reviewsObserver = it.dataReceiver
            }
}
