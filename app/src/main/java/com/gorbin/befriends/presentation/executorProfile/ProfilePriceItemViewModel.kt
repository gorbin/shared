package com.gorbin.befriends.presentation.executorProfile

import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.ServicePrice
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class ProfilePriceItemViewModel : AdapterItemViewModel<ServicePrice, ExecutorProfileViewModel>() {
    val cost = ObservableField<String>()
    val body = ObservableField<String>()

    override fun bindData(data: ServicePrice) {
        cost.set("${data.cost}${data.currency.sign}")
        body.set(data.nameService)
    }
}