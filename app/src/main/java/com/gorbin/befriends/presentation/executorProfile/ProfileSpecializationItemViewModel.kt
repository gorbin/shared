package com.gorbin.befriends.presentation.executorProfile

import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.ProfileSpecialization
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class ProfileSpecializationItemViewModel : AdapterItemViewModel<ProfileSpecialization, ExecutorProfileViewModel>() {
    val title = ObservableField<String>()
    val body = ObservableField<String>()

    override fun bindData(data: ProfileSpecialization) {
        title.set(data.areaName)
        body.set(data.name)
    }
}