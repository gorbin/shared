package com.gorbin.befriends.presentation.executorProfile

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val executorProfileModule = module {
    viewModel { ExecutorProfileViewModel(application = get(), useCase = get(), requestsUseCase = get()) }
}