package com.gorbin.befriends.presentation.gallery

import android.app.Activity
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.GalleryFrBinding
import com.gorbin.befriends.domain.entities.GalleryPhoto
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.utils.GlobalValues
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.miguelbcr.ui.rx_paparazzo2.entities.Response
import com.punicapp.core.utils.DimensUtils.dpToPx
import com.punicapp.mvvm.actions.UIActionConsumer
import com.squareup.picasso.Picasso
import com.stfalcon.imageviewer.StfalconImageViewer
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class GalleryFragment : AbstractBaseFragment<GalleryViewModel>() {
    companion object {
        private const val SOURCE_SELECTOR = "source_selector"
        private const val IMAGE_OPTIONS = "image_options"
        private const val IMAGE_MARGIN_DP = 16
    }

    private var binding: GalleryFrBinding? = null
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(GalleryViewModel.ACTION_DONE) {
            router.exit()
        }.register<Unit>(GalleryViewModel.ACTION_IMAGE_SELECTION) {
            showImageSourceSelectionDialog()
        }.register<Triple<List<GalleryPhoto>, Int, ImageView>>(GalleryViewModel.ACTION_SHOW_IMAGE) {
            StfalconImageViewer.Builder<GalleryPhoto>(context, it.first) { view, image ->
                Picasso.get().load(Uri.parse(image.photoUrl)).into(view)
            }.withStartPosition(it.second)
                    .withBackgroundColorResource(R.color.colorBackground)
                    .allowZooming(true)
                    .allowSwipeToDismiss(true)
                    .withHiddenStatusBar(false)
                    .withImageMarginPixels(dpToPx(IMAGE_MARGIN_DP))
                    .withTransitionFrom(it.third)
                    .show()
        }.register<GalleryPhoto>(GalleryViewModel.ACTION_SHOW_OPTIONS) {
            showImageOptionsDialog(it)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: GalleryViewModel): View {
        binding = GalleryFrBinding.inflate(inflater, container, false).apply {
            viewModel = appViewModel
            executePendingBindings()
        }

        appViewModel.loadGallery()
        return binding!!.root
    }

    private fun showImageOptionsDialog(data: GalleryPhoto) {
        if (activity?.isFinishing == true)
            return

        DialogProvider.getImageOptionsBottomSheetDialog(data) { action, photo ->
            when (action) {
                GlobalValues.SET_AS_PROFILE_PHOTO -> {
                    appViewModel.setProfilePhoto(photo)
                }
                GlobalValues.REMOVE_PHOTO -> {
                    appViewModel.removePhoto(photo)
                }
            }
        }.show(childFragmentManager, IMAGE_OPTIONS)
    }

    private fun showImageSourceSelectionDialog() {
        if (activity?.isFinishing == true)
            return

        val options = UCrop.Options()
        options.setToolbarTitle("")
        options.withAspectRatio(1F, 1F)
        options.setStatusBarColor(Color.BLACK)
        options.setToolbarWidgetColor(Color.WHITE)
        options.setActiveWidgetColor(Color.BLACK)
        options.setToolbarColor(Color.BLACK)

        DialogProvider.getImageSourceBottomSheetPickerDialog { source ->
            when (source) {
                GlobalValues.GALLERY_SOURCE -> {
                    getPhotoFromGallery(options)
                }
                GlobalValues.CAMERA_SOURCE -> {
                    getPhotoFromCamera(options)
                }
            }
        }.show(childFragmentManager, SOURCE_SELECTOR)
    }

    private fun getPhotoFromCamera(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getPhotoFromGallery(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getImage(response: Response<GalleryFragment, FileData>) {
        if (response.resultCode() == Activity.RESULT_OK) {
            appViewModel.uploadImage(response.data())
        }
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarText = getString(R.string.gallery),
            toolbarVisible = true,
            iconId = R.drawable.ic_arrow_back_24
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}