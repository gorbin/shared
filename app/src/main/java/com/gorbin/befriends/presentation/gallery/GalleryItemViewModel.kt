package com.gorbin.befriends.presentation.gallery

import android.net.Uri
import android.widget.ImageView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.GalleryItem
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class GalleryItemViewModel : AdapterItemViewModel<GalleryItem, GalleryViewModel>() {
    val image = ObservableField<Uri>()
    val isAvatar = ObservableBoolean()
    lateinit var data: GalleryItem

    override fun bindData(data: GalleryItem) {
        this.data = data
        isAvatar.set(data.photo?.isAvatarPhoto ?: false)
        image.set(data.photo?.photoUrl?.let { Uri.parse(it) })
    }

    fun showOptions() {
        if (image.get() == null)
            return

        data.photo?.let {
            parent.showImageOptions(it)
        }
    }

    fun onPhotoClick(view: ImageView) {
        if (image.get() == null) {
            parent.pickImage()
        } else {
            parent.showImage(adapterPosition.get(), view)
        }
    }
}
