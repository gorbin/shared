package com.gorbin.befriends.presentation.gallery

import android.app.Application
import android.widget.ImageView
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.GalleryItem
import com.gorbin.befriends.domain.entities.GalleryPhoto
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer

class GalleryViewModel(application: Application, private val useCase: IProfileUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_IMAGE_SELECTION = "action_image_selection"
        const val ACTION_SHOW_IMAGE = "action_show_image"
        const val ACTION_SHOW_OPTIONS = "action_show_options"

        const val GALLERY_SIZE = 9
    }

    lateinit var observer: Observer<List<*>>
    private var images: List<GalleryPhoto> = emptyList()

    fun loadGallery() {
        images = emptyList()
        useCase.getGallery()
                .appSubscribe {
                    showGallery(it)
                }
    }

    private fun showGallery(gallery: List<GalleryPhoto>) {
        images = gallery.reversed()
        val data = MutableList(GALLERY_SIZE) { GalleryItem() }
        images.forEachIndexed { index, galleryPhoto ->
            if (index < GALLERY_SIZE) {
                data[index] = GalleryItem(galleryPhoto)
            }
        }
        observer.onNext(data)
    }

    fun uploadImage(data: FileData?) {
        data?.file?.let { file ->
            useCase.uploadPhoto(file, false)
                    .flatMap { useCase.getGallery() }
                    .appSubscribe {
                        showGallery(it)
                    }
        }
    }

    fun showImage(position: Int, view: ImageView) {
        images.takeIf { it.isNotEmpty() }?.take(GALLERY_SIZE)?.let { gallery ->
            processor.onNext(UIAction(ACTION_SHOW_IMAGE, Triple(gallery, position, view)))
        }
    }

    fun setProfilePhoto(photo: GalleryPhoto) {
        useCase.setProfilePhoto(photo.keyPhoto)
                .appSubscribe {
                    showGallery(it)
                }
    }

    fun removePhoto(photo: GalleryPhoto) {
        useCase.removePhoto(photo.keyPhoto)
                .appSubscribe {
                    showGallery(it)
                }
    }

    fun showImageOptions(data: GalleryPhoto) {
        processor.onNext(UIAction(ACTION_SHOW_OPTIONS, data))
    }

    fun onDoneClick() {
        processor.onNext(UIAction(ACTION_DONE))
    }

    fun pickImage() {
        processor.onNext(UIAction(ACTION_IMAGE_SELECTION))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(0, R.layout.gallery_item, GalleryItem::class.java, GalleryItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                observer = it.dataReceiver
            }
}