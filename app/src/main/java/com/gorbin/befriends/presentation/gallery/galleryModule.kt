package com.gorbin.befriends.presentation.gallery

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val galleryModule = module {
    viewModel { GalleryViewModel(application = get(), useCase = get()) }
}