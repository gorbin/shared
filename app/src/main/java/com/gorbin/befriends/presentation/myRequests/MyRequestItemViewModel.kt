package com.gorbin.befriends.presentation.myRequests

import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.RequestStatus
import com.gorbin.befriends.utils.toDisplayRating
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class MyRequestItemViewModel : AdapterItemViewModel<MyRequest, MyRequestsViewModel>() {
    val image = ObservableField<Uri>()
    val name = ObservableField<String>()
    val rating = ObservableField<String>()
    val price = ObservableField<String>()
    val status = ObservableField<String>()
    val statusColor = ObservableField<Drawable>()
    val statusTextColor = ObservableInt()
    val description = ObservableField<String>()
    val lastActive = ObservableField<String>()
    val isNewbie = ObservableBoolean()
    val countMessages = ObservableField<Int>(0)
    val free = ObservableBoolean()
    lateinit var data: MyRequest

    override fun bindData(data: MyRequest) {
        this.data = data
        image.set(data.urlPhoto?.let { Uri.parse(it) })
        name.set(data.getNameOrNickname())
        rating.set(data.rating.toDisplayRating())
        price.set(data.getFormattedPrice())
        status.set(parent.context.getString(data.status.nameRes))
        statusColor.set(ContextCompat.getDrawable(parent.context, data.status.bgRes))
        statusTextColor.set(ContextCompat.getColor(parent.context, data.status.textColorRes))
        description.set(parent.context.getString(data.status.descriptionRes))
        isNewbie.set(data.isNewbie())
        countMessages.set(data.countMessages)
        free.set(price.get().equals("FREE"))
        //TODO: set last active
    }

    fun openProfile() {
        parent.openProfile(data.idUser)
    }

    fun openChat() {
        parent.openChat(data)
    }

    fun deleteRequest() {
        if (data.status == RequestStatus.CLOSED)
            parent.requestRemovingConfirmation(data.uuidRequest)
    }

}
