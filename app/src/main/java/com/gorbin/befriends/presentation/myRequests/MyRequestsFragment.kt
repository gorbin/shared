package com.gorbin.befriends.presentation.myRequests

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.MyRequestsFrBinding
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.ChatScreen
import com.gorbin.befriends.presentation.cicerone.ExecutorProfileScreen
import com.gorbin.befriends.presentation.cicerone.SearchFormScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class MyRequestsFragment : AbstractBaseFragment<MyRequestsViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Long>(MyRequestsViewModel.ACTION_PROFILE) {
            router.navigateTo(ExecutorProfileScreen(it.toString()))
        }.register<MyRequest>(MyRequestsViewModel.ACTION_CHAT) {
            router.navigateTo(ChatScreen(it, Role.CUSTOMER))
        }.register<Unit>(MyRequestsViewModel.ACTION_NEW_REQUEST) {
            router.navigateTo(SearchFormScreen)
        }.register<Unit>(MyRequestsViewModel.ACTION_REMOVE_REQUEST) {
            DialogProvider.getConfirmationDialog(
                    context = requireContext(),
                    titleRes = R.string.attention,
                    message = getString(R.string.remove_request),
                    viewModel = appViewModel
            ).show()
        }
    }

    override fun initBinding(
            inflater: LayoutInflater,
            container: ViewGroup?,
            appViewModel: MyRequestsViewModel
    ): View {
        val binding = MyRequestsFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init()

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = requireContext().getString(R.string.my_requests)
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}