package com.gorbin.befriends.presentation.myRequests

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val myRequestsModule = module {
    viewModel { MyRequestsViewModel(application = get(), useCase = get()) }
}