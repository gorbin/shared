package com.gorbin.befriends.presentation.passwordRecovery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.NewPasswordFrBinding
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ProfileScreen
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class NewPasswordFragment : AbstractBaseFragment<NewPasswordViewModel>() {

    private var binding: NewPasswordFrBinding? = null
    private val router: Router by inject()
    var rememberToken: String? = null

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<String>(NewPasswordViewModel.ACTION_DONE) {
            router.newRootScreen(ProfileScreen)
        }.register<Unit>(NewPasswordViewModel.ACTION_BACK) {
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: NewPasswordViewModel): View {
        binding = NewPasswordFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        rememberToken = arguments?.getString(GlobalValues.REMEMBER_TOKEN)
        if (rememberToken == null) {
            router.exit()
        }
        appViewModel.init(rememberToken!!)
        return binding!!.root
    }
}