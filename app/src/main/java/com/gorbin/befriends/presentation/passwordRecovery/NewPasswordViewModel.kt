package com.gorbin.befriends.presentation.passwordRecovery

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.firebase.iid.FirebaseInstanceId
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.NewPasswordData
import com.gorbin.befriends.domain.usecases.ILoginUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import retrofit2.HttpException

class NewPasswordViewModel(application: Application, private val useCase: ILoginUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"
    }

    val password = ObservableField<String>().onChanged { check() }
    val isDoneEnabled = ObservableBoolean(true)
    val error = ObservableField<String>()
    private val isValid = ObservableBoolean()

    lateinit var rememberToken: String

    fun init(rememberToken: String) {
        this.rememberToken = rememberToken
        password.set("")
        error.set(null)
    }

    private fun check() {
        error.set(null)
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onDoneClick() {
        isValid.set((password.get()?.length ?: 0) > 0)

        if (!isValid.get()) {
            this.error.set(context.getString(R.string.error_password_valid))
            return
        }

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { result ->
            val data = NewPasswordData(password.get()!!, rememberToken)
            useCase.newPassword(data, result.token).appSubscribe {
                processor.onNext(UIAction(ACTION_DONE))
            }
        }
    }

    override fun handleError(error: Throwable) {
        if (error is HttpException) {
            if (error.code() == 403) {
                this.error.set(context.getString(R.string.error_wrong_password))
            } else if (error.code() == 404) {
                this.error.set(context.getString(R.string.error_wrong_user))
            }
            return
        }

        super.handleError(error)
    }
}