package com.gorbin.befriends.presentation.passwordRecovery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gorbin.befriends.databinding.PasswordRecoveryFrBinding
import com.gorbin.befriends.domain.entities.PinData
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.NewPasswordScreen
import com.gorbin.befriends.presentation.cicerone.PhoneCallErrorScreen
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class PasswordRecoveryFragment : AbstractBaseFragment<PasswordRecoveryViewModel>() {
    companion object {
        const val PIN_DATA = "pin_data"
    }

    private var binding: PasswordRecoveryFrBinding? = null
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<String>(PasswordRecoveryViewModel.ACTION_DONE) {
            router.navigateTo(NewPasswordScreen(it))
        }.register<Unit>(PasswordRecoveryViewModel.ACTION_BACK) {
            router.exit()
        }.register<Unit>(PasswordRecoveryViewModel.PHONE_CALL_ERROR) {
            router.navigateTo(PhoneCallErrorScreen)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: PasswordRecoveryViewModel): View {
        binding = PasswordRecoveryFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        val phone = arguments?.getString(GlobalValues.PHONE)
        val pinData = arguments?.getTypedSerializable<PinData>(PIN_DATA)!!
        if (phone == null) {
            router.exit()
        } else {
            appViewModel.init(phone, pinData)
        }

        return binding!!.root
    }

    override fun onDestroyView() {
        appViewModel.wipeData()
        super.onDestroyView()
    }
}