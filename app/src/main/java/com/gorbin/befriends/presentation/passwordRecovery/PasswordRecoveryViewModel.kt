package com.gorbin.befriends.presentation.passwordRecovery

import android.app.Application
import android.os.CountDownTimer
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.google.gson.Gson
import com.gorbin.befriends.R
import com.gorbin.befriends.data.api.model.ErrorList
import com.gorbin.befriends.domain.entities.CodePhoneData
import com.gorbin.befriends.domain.entities.PhoneData
import com.gorbin.befriends.domain.entities.PinData
import com.gorbin.befriends.domain.usecases.ILoginUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.presentation.base.viewmodel.IErrorHandler
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import retrofit2.HttpException

class PasswordRecoveryViewModel(application: Application, private val useCase: ILoginUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"

        const val COUNT = 4
        const val MIN_RESEND_TIME_MILLIS = 30_000
        const val COUNT_DOWN_TIMER_STEP = 1000
        const val COUNT_DOWN_TIMER_DIVIDER = 1000
        const val PHONE_CALL_ERROR = "PHONE_CALL_ERROR"

        const val ONE = "one"
        const val TWO = "two"
        const val THREE = "three"
        const val FOUR = "four"

        private const val LOGIN_NOT_FOUND = 1021L
        private const val CALL_FAILED = 1022L
    }

    var code = ObservableField("").onChanged { handleCode(it) }
    var error = ObservableField<String>()
    val codeKeyboard = ObservableInt(InputMethodManager.SHOW_IMPLICIT)
    val resendCodeText = ObservableField("")
    val resendCodeEnabled = ObservableBoolean(false)
    var values = ObservableArrayMap<String, String>()

    private var timerTime = MIN_RESEND_TIME_MILLIS
    private var timer: CountDownTimer? = null
    private var isRunning = false
    private var attempt: String? = null

    lateinit var phone: String

    fun init(phone: String, data: PinData) {
        this.phone = phone
        attempt = data.pinAttempt
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    private fun requestCode() {
        useCase.requestCall(PhoneData(phone))
                .appSubscribe(
                        errorHandler = getRecoveryErrorHandler()
                ) {
                    if (it.pinAttempt != null) {
                        attempt = it.pinAttempt!!
                    } else {
                        processor.onNext(UIAction(PHONE_CALL_ERROR))
                    }
                }
    }

    fun handleResendCodeButtonText(count: Long): String {
        return if (count > 0) {
            context.getString(R.string.resend_code_in, count)
        } else {
            context.getString(R.string.resend_code)
        }
    }

    fun onResendClick() {
        startTimer()
        requestCode()
    }

    private fun checkCode() {
        if (attempt == null) {
            processor.onNext(UIAction(PHONE_CALL_ERROR))
            return
        }
        code.get()?.let {
            useCase.requestCallCheck(CodePhoneData(it, attempt!!))
                    .appSubscribe { sessionData ->
                        processor.onNext(UIAction(ACTION_DONE, sessionData.rememberToken))
                        code.set("")
                    }
        }
    }

    fun show() {
        codeKeyboard.set(0)
        codeKeyboard.set(InputMethodManager.SHOW_IMPLICIT)
    }

    private fun startTimer() {
        if (timer == null) {
            timer = object : CountDownTimer(timerTime.toLong(), COUNT_DOWN_TIMER_STEP.toLong()) {
                override fun onTick(millisUntilFinished: Long) {
                    timerTime = millisUntilFinished.toInt()
                    resendCodeText.set(handleResendCodeButtonText(millisUntilFinished / COUNT_DOWN_TIMER_DIVIDER))
                }

                override fun onFinish() {
                    isRunning = false
                    resendCodeText.set(handleResendCodeButtonText(0))
                    resendCodeEnabled.set(true)
                    timerTime = MIN_RESEND_TIME_MILLIS
                }
            }
        }
        if (!isRunning) {
            isRunning = true
            resendCodeEnabled.set(false)
            timer?.start()
        }
    }

    override fun doOnStart() {
        startTimer()
    }

    private fun getRecoveryErrorHandler(): IErrorHandler = object : IErrorHandler {
        override fun handleError(error: HttpException): Boolean {
            return when (error.code()) {
                500 -> {
                    processor.onNext(UIAction(PHONE_CALL_ERROR))
                    true
                }
                409 -> {
                    val body = error.response().errorBody()?.source()?.buffer()?.readUtf8()
                            ?: return false
                    val errorList = Gson().fromJson(body, ErrorList::class.java)
                    var errorProcessed = false
                    errorList.errorList.forEach {
                        when (it.code) {
                            LOGIN_NOT_FOUND -> {
                                this@PasswordRecoveryViewModel.error.set(context.getString(R.string.login_not_found))
                                errorProcessed = true
                            }
                            CALL_FAILED -> {
                                Toast.makeText(context, context.getText(R.string.call_failed), Toast.LENGTH_LONG).show()
                                errorProcessed = true
                            }
                            else -> handleError(Throwable(it.message))
                        }
                    }
                    errorProcessed
                }
                else -> false
            }
        }
    }

    private fun handleCode(value: String?) {
        error.set(null)

        values[ONE] = value?.getOrNull(0)?.toString()
        values[TWO] = value?.getOrNull(1)?.toString()
        values[THREE] = value?.getOrNull(2)?.toString()
        values[FOUR] = value?.getOrNull(3)?.toString()

        if (value?.length == COUNT)
            checkCode()
    }

    fun wipeData() {
        code.set("")
        error.set(null)
        values = ObservableArrayMap()
    }
}