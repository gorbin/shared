package com.gorbin.befriends.presentation.passwordRecovery

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val passwordRecoveryModule = module {
    viewModel { PasswordRecoveryViewModel(application = get(), useCase = get()) }
    viewModel { NewPasswordViewModel(application = get(), useCase = get()) }
}