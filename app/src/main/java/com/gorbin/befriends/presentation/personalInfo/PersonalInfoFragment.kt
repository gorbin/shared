package com.gorbin.befriends.presentation.personalInfo

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.common.base.Optional
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.PersonalInfoFrBinding
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.IBackPressHandler
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.*
import com.gorbin.befriends.utils.FusedLocationTracker
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.safeLet
import com.gorbin.befriends.utils.toLatLng
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.miguelbcr.ui.rx_paparazzo2.entities.Response
import com.punicapp.mvvm.actions.UIActionConsumer
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*
import java.util.concurrent.TimeUnit

class PersonalInfoFragment : AbstractBaseFragment<PersonalInfoViewModel>(), IBackPressHandler {
    companion object {
        private const val SOURCE_SELECTOR = "source_selector"
        const val SELECTION = "selection"
        const val SELECTION_CITY_EXTRA = "selection_city_extra"
        const val SELECTED_CITY_EXTRA = "SELECTED_CITY_EXTRA"
        const val IS_NEW_LAUNCH = "is_new_launch"

        const val TERMS = "https://bf-mobile.develophost.ru/terms"
    }

    private val router: Router by inject()
    private val selector = DataSelection<Pair<Country, City>>()
    private var binding: PersonalInfoFrBinding? = null
    private val selectorSpecialization = DataSelection<SpecializationTree>()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Calendar>(PersonalInfoViewModel.ACTION_DATE_PICKER) {
            showDatePickerDialog(it)
        }.register<Pair<List<Country>, Pair<Country, City>>>(PersonalInfoViewModel.ACTION_COUNTRY_SELECTION) {
            router.navigateTo(ChooseCountryCityScreen(it.first, it.second, selector, CitySelectionFlow.PERSONAL_INFO))
        }.register<Pair<List<Language>, List<Language>>>(PersonalInfoViewModel.ACTION_LANGUAGE_SELECTION) {
            showLanguageSelectionDialog(it)
        }.register<Unit>(PersonalInfoViewModel.ACTION_IMAGE_SELECTION) {
            showImageSourceSelectionDialog()
        }.register<SpecializationTree>(PersonalInfoViewModel.ACTION_SPECIALIZATION_SELECTION) {
            if (it.selectedSpecializations.isNotEmpty()) {
                router.navigateTo(SpecializationSkillScreen(it, selectorSpecialization, SpecializationFlow.PERSONAL_INFO))
            } else {
                router.navigateTo(SpecializationAreaScreen(it, selectorSpecialization, SpecializationFlow.PERSONAL_INFO))
            }
        }.register<Unit>(PersonalInfoViewModel.ACTION_DONE) {
            if (activity?.isFinishing == false)
                safeLet(context, fragmentManager) { ctx, fm ->
                    DialogProvider.getRegisterDialog(ctx, appViewModel).show(fm)
                }
        }.register<Unit>(PersonalInfoViewModel.ACTION_TO_PROFILE) {
            router.newRootScreen(ProfileScreen)
        }.register<Unit>(PersonalInfoViewModel.ACTION_TO_RULES) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(TERMS))
            startActivity(browserIntent)
        }.register<Unit>(PersonalInfoViewModel.ACTION_GET_LOCATION) {
            getLocation()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: PersonalInfoViewModel): View {
        if (arguments?.getBoolean(IS_NEW_LAUNCH, false) == true) {
            appViewModel.wipeData()
            arguments?.remove(IS_NEW_LAUNCH)
        }

        binding?.let {
            return it.root
        }

        val binding = PersonalInfoFrBinding.inflate(inflater, container, false)
        this.binding = binding
        binding.viewModel = appViewModel
        selector.listener = appViewModel.getCitySelectionListener()
        selectorSpecialization.listener = appViewModel.getSpecializationSelectionListener()
        appViewModel.init()

        return binding.root
    }

    private fun getLocation(): Disposable? {
        val act = activity ?: return null
        val fusedLocationTracker = FusedLocationTracker(act)
        return fusedLocationTracker.isLocationAvailable().map {
            if (it) LocationCheckResults.AVAILABLE else LocationCheckResults.NOT_AVAILABLE
        }.subscribe { result, _ ->
            when (result) {
                LocationCheckResults.AVAILABLE -> {
                    getLatLng(fusedLocationTracker)
                }
                else -> {
                }
            }
        }
    }

    private fun getLatLng(fusedLocationTracker: FusedLocationTracker): Disposable {
        return fusedLocationTracker.getLocationLatLngObservable()
                .timeout(10L, TimeUnit.SECONDS)
                .firstOrError()
                .map { Optional.of(it) }
                .subscribe { result, throwable ->
                    if (throwable == null && result.isPresent) {
                        val latLng = result.get().toLatLng()
                        appViewModel.onLocation(latLng)
                    }
                }
    }

    private fun showDatePickerDialog(calendar: Calendar) {
        val context = context
        if (activity?.isFinishing == true || context == null)
            return

        val dialog = DatePickerDialog(
                context,
                R.style.DatePickerDialogTheme,
                appViewModel.getDateSetListener(),
                calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
        )
        dialog.datePicker.maxDate = System.currentTimeMillis()
        dialog.show()
    }

    private fun showLanguageSelectionDialog(data: Pair<List<Language>, List<Language>>) {
        DialogProvider.getLangMultiSelectionDialog(requireContext(), appViewModel, data.first, data.second)
                .show()
    }

    private fun showImageSourceSelectionDialog() {
        if (activity?.isFinishing == true)
            return

        val options = UCrop.Options()
        options.setToolbarTitle("")
        options.withAspectRatio(1F, 1F)
        options.setStatusBarColor(Color.BLACK)
        options.setToolbarWidgetColor(Color.WHITE)
        options.setActiveWidgetColor(Color.BLACK)
        options.setToolbarColor(Color.BLACK)

        DialogProvider.getImageSourceBottomSheetPickerDialog { source ->
            when (source) {
                GlobalValues.GALLERY_SOURCE -> {
                    getPhotoFromGallery(options)
                }
                GlobalValues.CAMERA_SOURCE -> {
                    getPhotoFromCamera(options)
                }
            }
        }.show(childFragmentManager, SOURCE_SELECTOR)
    }

    private fun getPhotoFromCamera(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getPhotoFromGallery(options: UCrop.Options): Disposable {
        return RxPaparazzo.single(this)
                .crop(options)
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response.targetUI().getImage(response)
                }, { error ->
                    error.printStackTrace()
                })
    }

    private fun getImage(response: Response<PersonalInfoFragment, FileData>) {
        if (response.resultCode() == Activity.RESULT_OK) {
            appViewModel.loadImage(response.data())
        }
    }

    override fun onBackPressed() {
        router.newRootScreen(WelcomeScreen)
    }
}