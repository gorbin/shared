package com.gorbin.befriends.presentation.personalInfo

import android.app.Application
import android.app.DatePickerDialog
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IRegisterUseCase
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.DateFormats
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.onChanged
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import java.io.File
import java.util.*

class PersonalInfoViewModel(application: Application, private val useCase: IRegisterUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_TO_PROFILE = "action_to_profile"
        const val ACTION_DATE_PICKER = "action_date_picker"
        const val ACTION_DONE = "action_done"
        const val ACTION_COUNTRY_SELECTION = "action_city_selection"
        const val ACTION_LANGUAGE_SELECTION = "action_language_selection"
        const val ACTION_IMAGE_SELECTION = "action_image_selection"
        const val ACTION_TO_RULES = "action_to_rules"
        const val ACTION_SPECIALIZATION_SELECTION = "action_specialization_selection"
        const val ACTION_GET_LOCATION = "ACTION_GET_LOCATION"

        const val DATE_BIRTH = "DATE_BIRTH"
        const val CITY = "CITY"
        const val LANGUAGE = "LANGUAGE"
    }

    val imageUri = ObservableField<Uri>()
    val birthDate = ObservableField<String>().onChanged { errors[DATE_BIRTH] = null }
    val city = ObservableField<String>().onChanged { errors[CITY] = null }
    val languageProficiency = ObservableField<String>().onChanged { errors[LANGUAGE] = null }
    val selectSpecializations = ObservableField<String>()
    val experience = ObservableField<String>()
    val errors = ObservableArrayMap<String, String>()

    private val countries = mutableListOf<Country>()
    private var selectedCity: Pair<Country, City>? = null
    private val languages = mutableListOf<Language>()
    private val selectedLanguages = mutableListOf<Language>()
    private val specializationAreas = mutableListOf<SpecializationArea>()
    private val selectedSpecializations = mutableListOf<Specialization>()
    private var selectedSpecializationsSkills = mutableMapOf<Specialization, List<SkillSpecialization>>()
    private var calendar = Calendar.getInstance()
    private var user: User? = null
    private var specializationTree: SpecializationTree? = null

    fun init() {
        useCase.getAdditionalInfoOptions()
                .appSubscribe { (user, data) ->
                    val countries = data.first
                    val languages = data.second
                    val areas = data.third
                    this.countries.clear()
                    this.countries.addAll(countries)
                    this.languages.clear()
                    this.languages.addAll(languages)
                    this.specializationAreas.clear()
                    this.specializationAreas.addAll(areas)
                    this.user = user
                    processor.onNext(UIAction(ACTION_GET_LOCATION))
                }
    }

    fun wipeData() {
        calendar = Calendar.getInstance()
        imageUri.set(null)
        birthDate.set(null)
        selectedCity = null
        city.set(null)
        selectedLanguages.clear()
        languageProficiency.set(null)
        selectedSpecializationsSkills.clear()
        selectSpecializations.set(null)
        specializationTree = null
        experience.set(null)
    }

    fun onDoneClick() {
        if (!isValid()) {
            return
        }

        val personalDataBody = PersonalDataBody(
                name = user?.name,
                surname = user?.surname,
                birthdate = birthDate.get(),
                idCity = selectedCity?.second?.getId(),
                activity = "1",
                specializations = selectedSpecializationsSkills.toServerSpecsFormat(),
                languages = selectedLanguages.toServerFormat(),
                experience = experience.get() ?: "0"
        )
        useCase.registerProfile(imageUri.get()?.path?.let { File(it) }, personalDataBody).appSubscribe {
            processor.onNext(UIAction(ACTION_DONE))
        }
    }

    private fun isValid(): Boolean {
        fun str(id: Int) = context.getString(id)
        errors.clear()
        if (birthDate.get().isNullOrBlank()) {
            errors[DATE_BIRTH] = str(R.string.empty_error)
        }
        if (city.get().isNullOrBlank()) {
            errors[CITY] = str(R.string.empty_error)
        }
        if (languageProficiency.get().isNullOrBlank()) {
            errors[LANGUAGE] = str(R.string.empty_error)
        }
        return errors.size == 0
    }

    fun pickDate() {
        processor.onNext(UIAction(ACTION_DATE_PICKER, calendar))
    }

    fun selectCity() {
        processor.onNext(UIAction(ACTION_COUNTRY_SELECTION, countries to selectedCity))
    }

    fun selectLanguages() {
        processor.onNext(UIAction(ACTION_LANGUAGE_SELECTION, languages to selectedLanguages))
    }

    fun selectSpecialization() {
        val tree = specializationTree ?: SpecializationTree().apply {
            areas = specializationAreas
            selectedSpecializations = this@PersonalInfoViewModel.selectedSpecializations.map { it to mutableListOf<SkillSpecialization>() }.toMap()
        }
        processor.onNext(UIAction(ACTION_SPECIALIZATION_SELECTION, tree))
    }

    fun onImageClick() {
        processor.onNext(UIAction(ACTION_IMAGE_SELECTION))
    }

    fun getDateSetListener(): DatePickerDialog.OnDateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                birthDate.set(DateFormats.numericDateFormat.format(calendar.time))
            }

    fun getCitySelectionListener(): DataSelection.Listener<Pair<Country, City>> = object : DataSelection.Listener<Pair<Country, City>> {
        override fun onChange(data: Pair<Country, City>) {
            Handler().postDelayed({
                selectedCity = data
                city.set("${data.first.name}, ${data.second.name}")
            }, 100)
        }
    }

    fun getSpecializationSelectionListener(): DataSelection.Listener<SpecializationTree> = object : DataSelection.Listener<SpecializationTree> {
        override fun onChange(data: SpecializationTree) {
            specializationTree = data
            selectedSpecializationsSkills.clear()
            selectedSpecializationsSkills.putAll(data.selectedSpecializations)
            selectSpecializations.set(data.selectedSpecializations.toDisplaySpecsFormat())
        }
    }

    fun loadImage(data: FileData?) {
        imageUri.set(Uri.fromFile(data?.file))
    }

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction?) {
        super.onActionFromChild(action)
        when (action?.actionId) {
            GlobalValues.SELECTED_LANGUAGES -> {
                val langCollection = (action.data as? Bundle)?.getSerializable(GlobalValues.SELECTED_LANGUAGES) as? LanguageCollection
                langCollection?.languages?.let {
                    selectedLanguages.clear()
                    selectedLanguages.addAll(it)
                    languageProficiency.set(selectedLanguages.toDisplayFormat())
                }
            }
            GlobalValues.GO_TO_PROFILE -> {
                processor.onNext(UIAction(ACTION_TO_PROFILE))
            }
            GlobalValues.SERVICE_RULES -> {
                processor.onNext(UIAction(ACTION_TO_RULES))
            }
        }
    }

    private fun List<Language>.toDisplayFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        this.forEachIndexed { index, language ->
            result += if (index == 0)
                language.name
            else
                ", ${language.name}"
        }

        return result
    }

    private fun List<Language>.toServerFormat(): List<LanguagesBody?>? {
        if (this.isEmpty())
            return null

        val result = mutableListOf<LanguagesBody>()
        this.forEach { language ->
            result.add(LanguagesBody(language.id.toString()))
        }

        return result
    }

    private fun List<SkillSpecialization>.toDisplaySpecsFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        this.forEachIndexed { index, data ->
            result += if (index == 0)
                context.getString(data.titleRes)
            else
                ", ${context.getString(data.titleRes)}"
        }

        return result
    }

    private fun Map<Specialization, List<SkillSpecialization>>.toDisplaySpecsFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        for (entry in this) {
            result += entry.key.name.trim() + ": " + entry.value.toDisplaySpecsFormat() + "; "
        }

        return result
    }

    private fun Map<Specialization, List<SkillSpecialization>>.toServerSpecsFormat(): List<SpecializationBody>? {
        if (this.isEmpty())
            return null

        val result = mutableListOf<SpecializationBody>()
        for (entry in this) {
            val skills = mutableListOf<String>()
            for (skill in entry.value) {
                skills.add(skill.serverId.toString())
            }
            result.add(SpecializationBody(entry.key.id.toString(), skills, entry.key.spheres?.map { UuidSphere(it.uuid) }?.takeIf { it.isNotEmpty() }))
        }

        return result
    }

    override fun handleError(error: Throwable) {
        //Handle error
    }

    fun onLocation(latLng: LatLng) {
        useCase.getCityByLocation(latLng).appSubscribe { countryCity ->
            if (countryCity.id != null) {
                val country = Country(null, countryCity.country, countryCity.region, null, null, null, null, true)
                val city = City(countryCity.id.toLong(), countryCity.name, countryCity.region, countryCity.country)

                selectedCity = country to city
                this.city.set("${countryCity.country}, ${countryCity.name}")
            }
        }
    }
}