package com.gorbin.befriends.presentation.personalInfo

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val personalInfoModule = module {
    viewModel { PersonalInfoViewModel(application = get(), useCase = get()) }
}