package com.gorbin.befriends.presentation.phoneCallError

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.PhoneCallErrorFrBinding
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.WelcomeScreen
import com.gorbin.befriends.presentation.passwordRecovery.PasswordRecoveryViewModel
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class PhoneCallErrorFragment : AbstractBaseFragment<PhoneCallErrorViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<String>(PhoneCallErrorViewModel.ACTION_DONE) {
            router.backTo(WelcomeScreen)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: PhoneCallErrorViewModel): View {
        val binding = PhoneCallErrorFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        return binding.root
    }
}