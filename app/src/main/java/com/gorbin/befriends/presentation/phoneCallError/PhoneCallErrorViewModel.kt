package com.gorbin.befriends.presentation.phoneCallError

import android.app.Application
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction

class PhoneCallErrorViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
    }

    fun onDoneClick() {
        processor.onNext(UIAction(ACTION_DONE))
    }
}