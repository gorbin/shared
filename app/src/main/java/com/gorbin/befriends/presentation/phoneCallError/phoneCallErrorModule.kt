package com.gorbin.befriends.presentation.phoneCallError

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val phoneCallErrorModule = module {
    viewModel { PhoneCallErrorViewModel(application = get()) }
}