package com.gorbin.befriends.presentation.priceList

import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.ServicePrice
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class PriceItemViewModel : AdapterItemViewModel<ServicePrice, PriceListViewModel>() {
    val name = ObservableField<String>()
    val price = ObservableField<String>()
    lateinit var data: ServicePrice

    override fun bindData(data: ServicePrice) {
        this.data = data
        name.set(data.nameService)
        price.set(String.format(parent.context.getString(R.string.from_value), data.cost, data.currency.sign))
    }

    fun onOptionsClick() {
        parent.onOptionsClick(adapterPosition.get())
    }
}