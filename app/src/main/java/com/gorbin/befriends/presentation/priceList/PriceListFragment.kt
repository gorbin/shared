package com.gorbin.befriends.presentation.priceList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.PriceListFrBinding
import com.gorbin.befriends.domain.entities.ServicePrice
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.PricePositionScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class PriceListFragment : AbstractBaseFragment<PriceListViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(PriceListViewModel.ACTION_PRICE_OPTIONS) {
            DialogProvider.getBottomSheetDialog(
                    this,
                    R.layout.bottom_sheet_price_options,
                    appViewModel
            ).show()
        }.register<List<ServicePrice>>(PriceListViewModel.ACTION_CREATE_PRICE_POSITION) {
            router.navigateTo(PricePositionScreen(it))
        }.register<Pair<List<ServicePrice>, Int>>(PriceListViewModel.ACTION_EDIT_ITEM) {
            router.navigateTo(PricePositionScreen(it.first, it.second))
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: PriceListViewModel): View {
        val binding = PriceListFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init()

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarText = getString(R.string.your_price_list),
            toolbarVisible = true,
            iconId = R.drawable.ic_arrow_back_24
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}