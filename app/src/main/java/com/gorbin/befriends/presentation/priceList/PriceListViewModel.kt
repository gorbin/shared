package com.gorbin.befriends.presentation.priceList

import android.app.Application
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.PriceList
import com.gorbin.befriends.domain.entities.ServicePrice
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import io.reactivex.Observer

class PriceListViewModel(application: Application, val useCase: IProfileUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_PRICE_OPTIONS = "action_price_options"
        const val ACTION_CREATE_PRICE_POSITION = "action_create_price_position"
        const val ACTION_EDIT_ITEM = "action_edit_item"
    }

    lateinit var observer: Observer<List<*>>
    private var selectedData: Pair<List<ServicePrice>, Int>? = null
    var data: List<ServicePrice>? = null

    fun init() {
        useCase.getPriceList()
                .appSubscribe {
                    data = it
                    observer.onNext(it)
                }
    }

    fun editItem() {
        val data = selectedData ?: return
        dismissBottomSheetDialog()
        processor.onNext(UIAction(ACTION_EDIT_ITEM, data))
    }

    fun removeItem() {
        val data = selectedData ?: return
        val list = data.first.toMutableList()
        list.removeAt(data.second)
        useCase.savePriceList(PriceList(list))
                .appSubscribe {
                    dismissBottomSheetDialog()
                    init()
                }
    }

    fun newPricePosition() {
        processor.onNext(UIAction(ACTION_CREATE_PRICE_POSITION, data ?: emptyList<ServicePrice>()))
    }

    fun onOptionsClick(position: Int) {
        data?.let {
            selectedData = it to position
            processor.onNext(UIAction(ACTION_PRICE_OPTIONS))
        }
    }

    override fun dismissBottomSheetDialog() {
        super.dismissBottomSheetDialog()
        selectedData = null
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(0, R.layout.price_item, ServicePrice::class.java, PriceItemViewModel::class.java)
            .build()
            .also {
                observer = it.dataReceiver
            }

    override fun provideVm(): AppViewModel {
        return this
    }
}