package com.gorbin.befriends.presentation.priceList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.PricePositionFrBinding
import com.gorbin.befriends.domain.entities.ServicePrice
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class PricePositionFragment : AbstractBaseFragment<PricePositionViewModel>() {
    companion object {
        const val PRICE_POSITION = "price_position"
        const val PRICE_LIST = "price_list"
    }

    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(PricePositionViewModel.ACTION_DONE) {
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: PricePositionViewModel): View {
        val binding = PricePositionFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        val list = arguments?.getTypedSerializable<List<ServicePrice>>(PRICE_LIST) ?: emptyList()
        val position = arguments?.getTypedSerializable<Int>(PRICE_POSITION)
        appViewModel.init(list, position)

        return binding.root
    }

    override fun onDestroyView() {
        appViewModel.wipeData()
        super.onDestroyView()
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = if (arguments?.getTypedSerializable<Int>(PRICE_POSITION) != null)
                getString(R.string.edit_position)
            else
                getString(R.string.new_position),
            iconId = R.drawable.ic_arrow_back_24
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}