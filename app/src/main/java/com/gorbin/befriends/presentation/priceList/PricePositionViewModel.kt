package com.gorbin.befriends.presentation.priceList

import android.app.Application
import android.view.View
import android.widget.AdapterView
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.PriceList
import com.gorbin.befriends.domain.entities.ServiceCurrency
import com.gorbin.befriends.domain.entities.ServicePrice
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.adapters.ListAdapter
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction

class PricePositionViewModel(application: Application, private val useCase: IProfileUseCase) : BaseAppViewModel(application) {
    companion object {
        const val NAME_ERROR = "name_error"
        const val PRICE_ERROR = "price_error"

        const val ACTION_DONE = "action_done"
    }

    val name = ObservableField<String>().onChanged { errors[NAME_ERROR] = null }
    val price = ObservableField<String>().onChanged { errors[PRICE_ERROR] = null }
    val currency = ObservableField<ServiceCurrency>(ServiceCurrency.USD).onChanged {
        selectedCurrencyPosition.set(ServiceCurrency.values().indexOf(it))
    }
    val selectedCurrencyPosition = ObservableInt(-1)
    val buttonAction = ObservableField<String>()
    val errors = ObservableArrayMap<String, String>()
    private val data = mutableListOf<ServicePrice>()
    private var position: Int? = null

    fun init(data: List<ServicePrice>, position: Int?) {
        this.data.clear()
        this.data.addAll(data)
        this.position = position
        if (position == null) {
            buttonAction.set(context.getString(R.string.add))
        } else {
            val item = data[position]
            name.set(item.nameService)
            price.set(item.cost)
            currency.set(item.currency)
            buttonAction.set(context.getString(R.string.save))
        }
    }

    fun done() {
        if (!checkDataValid())
            return

        val priceList = data
        val position = position
        val newItem = ServicePrice(name.get()!!, "", price.get()!!, currency.get()!!)
        if (position != null && position < priceList.size) {
            priceList[position] = newItem
        } else {
            priceList.add(newItem)
        }

        useCase.savePriceList(PriceList(priceList))
                .appSubscribe {
                    processor.onNext(UIAction(ACTION_DONE))
                }
    }

    fun wipeData() {
        currency.set(ServiceCurrency.USD)
        name.set(null)
        price.set(null)
        errors.clear()
    }

    private fun checkDataValid(): Boolean {
        var result = true
        if (name.get().isNullOrBlank()) {
            result = false
            errors[NAME_ERROR] = context.getString(R.string.value_is_missing)
        }
        if (price.get().isNullOrBlank()) {
            result = false
            errors[PRICE_ERROR] = context.getString(R.string.value_is_missing)
        }

        return result
    }

    fun getCurrencyAdapter(): ListAdapter<String> {
        return ListAdapter(context, ServiceCurrency.values().map { it.sign })
    }

    fun getCurrencyListener(): AdapterView.OnItemSelectedListener {
        return object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ServiceCurrency.values().getOrNull(position)?.let { currency.set(it) }
            }
        }
    }
}
