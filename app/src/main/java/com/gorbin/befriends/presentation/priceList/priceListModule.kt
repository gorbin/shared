package com.gorbin.befriends.presentation.priceList

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val priceListModule = module {
    viewModel { PriceListViewModel(application = get(), useCase = get()) }
    viewModel { PricePositionViewModel(application = get(), useCase = get()) }
}