package com.gorbin.befriends.presentation.profile

import android.app.Application
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction

class AboutViewModel(application: Application, val useCase: IProfileUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
    }

    val about = ObservableField<String>()

    fun init() {
        initProfile()
    }

    private fun initProfile() {
        useCase.getProfile().appSubscribe {
            about.set(it.about)
        }
    }

    fun onDoneClick() {
        useCase.updateAbout(about.get() ?: "").appSubscribe {
            processor.onNext(UIAction(ACTION_DONE))
        }
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_DONE))
    }

}