package com.gorbin.befriends.presentation.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ConsultFrBinding
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ProfileScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ConsultFragment : AbstractBaseFragment<ConsultViewModel>() {
    private var binding: ConsultFrBinding? = null
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(AboutViewModel.ACTION_DONE) {
            router.backTo(ProfileScreen)
        }
    }


    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ConsultViewModel): View {
        binding = ConsultFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        appViewModel.init()
        return binding!!.root
    }
}