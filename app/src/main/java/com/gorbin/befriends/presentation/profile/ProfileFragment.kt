package com.gorbin.befriends.presentation.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.ProfileFrBinding
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.*
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.gorbin.befriends.utils.onShare
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ProfileFragment : AbstractBaseFragment<ProfileViewModel>() {
    private var binding: ProfileFrBinding? = null
    private val requestsUseCase: IRequestsUseCase by inject()
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ProfileViewModel.ACTION_MY_REQUESTS) {
            router.navigateTo(MyRequestsScreen)
        }.register<Unit>(ProfileViewModel.ACTION_SEARCH) {
            router.navigateTo(SearchFormScreen)
        }.register<Unit>(ProfileViewModel.ACTION_REQUESTS_TO_ME) {
            router.navigateTo(RequestsToMeScreen)
        }.register<Unit>(ProfileViewModel.ACTION_SETTINGS) {
            router.navigateTo(SettingsScreen)
        }.register<Unit>(ProfileViewModel.ACTION_REVIEWS) {
            router.navigateTo(MyRatingAndReviewsScreen)
        }.register<Unit>(ProfileViewModel.ACTION_PRICE_LIST) {
            router.navigateTo(PriceListScreen)
        }.register<Unit>(ProfileViewModel.ACTION_ABOUT) {
            router.navigateTo(AboutScreen)
        }.register<Unit>(ProfileViewModel.ACTION_SPECIALIZATION) {
            router.navigateTo(ProfileSpecializationScreen)
        }.register<Unit>(ProfileViewModel.ACTION_GALLERY) {
            router.navigateTo(GalleryScreen)
        }.register<Unit>(ProfileViewModel.ACTION_CONSULT) {
            router.navigateTo(ConsultScreen)
        }.register<Unit>(ProfileViewModel.ACTION_CURRENCY_TIP) {
            Toast.makeText(requireContext(), getString(R.string.currency_tip), Toast.LENGTH_SHORT).show()
        }.register<Unit>(ProfileViewModel.ACTION_SHARE) {
            onShare(requireContext())
        }.register<Unit>(ProfileViewModel.ACTION_NO_PRICE_LIST) {
            DialogProvider.getMessageDialog(requireContext(), R.string.attention, R.string.no_price_list, appViewModel)
                    .show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val pendingChatRoom = activity?.intent?.extras
                ?.getTypedSerializable<Pair<String, Role?>>(GlobalValues.PENDING_CHAT_ROOM) ?: return
        pendingChatRoom.second?.let { role ->
            requestsUseCase.getRequestInfo(pendingChatRoom.first)
                    .subscribe { request, _ ->
                        request?.let {
                            router.navigateTo(ChatScreen(it, role))
                        }
                    }
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ProfileViewModel): View {
        binding = ProfileFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        appViewModel.initProfile()
        return binding!!.root
    }
}