package com.gorbin.befriends.presentation.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.ProfileSpecFrBinding
import com.gorbin.befriends.domain.entities.SpecializationFlow
import com.gorbin.befriends.domain.entities.SpecializationTree
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ProfileScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationAreaScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class ProfileSpecializationFragment : AbstractBaseFragment<ProfileSpecializationViewModel>() {
    private var binding: ProfileSpecFrBinding? = null
    private val router: Router by inject()
    private val selectorSpecialization = DataSelection<SpecializationTree>()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(ProfileSpecializationViewModel.ACTION_DONE) {
            router.backTo(ProfileScreen)
        }.register<SpecializationTree>(ProfileSpecializationViewModel.ACTION_SPECIALIZATION_SELECTION) {
            if (it.selectedArea != null)
                router.navigateTo(SpecializationScreen(it, selectorSpecialization, SpecializationFlow.PROFILE))
            else
                router.navigateTo(SpecializationAreaScreen(it, selectorSpecialization, SpecializationFlow.EMPTY_PROFILE))
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: ProfileSpecializationViewModel): View {
        if (binding != null) {
            return binding!!.root
        }

        binding = ProfileSpecFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel
        selectorSpecialization.listener = appViewModel.getSpecializationSelectionListener()

        appViewModel.init()
        return binding!!.root
    }
}