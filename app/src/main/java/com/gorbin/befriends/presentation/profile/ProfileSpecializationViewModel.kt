package com.gorbin.befriends.presentation.profile

import android.app.Application
import android.os.Handler
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction

class ProfileSpecializationViewModel(application: Application, val useCase: IProfileUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_SPECIALIZATION_SELECTION = "action_specialization_selection"
    }

    val area = ObservableField<String>()
    private var data: Pair<List<SpecializationArea>, UserSpecializations>? = null
    private var specializationTree: SpecializationTree? = null
    private var selectedSpecializationsSkills = mutableMapOf<Specialization, List<SkillSpecialization>>()

    fun init() {
        data = null
        selectedSpecializationsSkills.clear()
        specializationTree = null
        initProfile()
    }

    private fun initProfile() {
        useCase.getAreasAndUserSpecializations().appSubscribe {
            data = it
            area.set(it.second.toDisplayFormat())
        }
    }

    fun onAreaClick() {
        data?.let {
            val tree = specializationTree ?: SpecializationTree().apply {
                areas = it.first
                selectedArea = it.second.area.takeIf { it.id != 0L }
                selectedSpecializations = if (selectedArea != null)
                    it.second.specializations.map { spec ->
                        val specialization = Specialization(
                                spec.idSpecialization,
                                it.second.area.id,
                                spec.nameSpecialization,
                                spec.spheres
                        )
                        val skills = spec.services
                                ?.mapNotNull { SkillSpecialization.getById(it) }
                                ?.toMutableList()
                                ?: mutableListOf()
                        specialization to skills
                    }.toMap()
                else emptyMap()
            }
            processor.onNext(UIAction(ACTION_SPECIALIZATION_SELECTION, tree))
        }
    }

    fun onDoneClick() {
        if (selectedSpecializationsSkills.isEmpty()) {
            processor.onNext(UIAction(ACTION_DONE))
        } else {
            selectedSpecializationsSkills.toServerSpecsFormat()?.let {
                useCase.updateSpecializations(it)
                        .appSubscribe {
                            processor.onNext(UIAction(ACTION_DONE))
                        }
            }
        }
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_DONE))
    }

    fun getSpecializationSelectionListener() = object : DataSelection.Listener<SpecializationTree> {
        override fun onChange(data: SpecializationTree) {
            specializationTree = data
            selectedSpecializationsSkills.clear()
            selectedSpecializationsSkills.putAll(data.selectedSpecializations)
            Handler().postDelayed({area.set(data.toDisplayFormat())}, 300L)
        }
    }

    private fun SpecializationTree.toDisplayFormat(): String? {
        val selectedArea = selectedArea
        val selectedSpecializations = selectedSpecializations
        if (selectedArea == null || selectedSpecializations.isEmpty())
            return null

        var result = selectedArea.name
        selectedSpecializations.entries.forEachIndexed { index, entry ->
            result += if (index == 0) {
                ": ${entry.key.name}"
            } else {
                ", ${entry.key.name}"
            }

            if (entry.value.isNotEmpty()) {
                var skills = ""
                entry.value.forEachIndexed { skillIndex, skill ->
                    skills += if (skillIndex == 0) {
                        context.getString(skill.shortTitleRes)
                    } else {
                        ", ${context.getString(skill.shortTitleRes)}"
                    }
                }
                result += " ($skills)"
            }
        }

        return result
    }

    private fun Map<Specialization, List<SkillSpecialization>>.toServerSpecsFormat(): List<SpecializationBody>? {
        if (this.isEmpty())
            return null

        val result = mutableListOf<SpecializationBody>()
        for (entry in this) {
            val skills = mutableListOf<String>()
            for (skill in entry.value) {
                skills.add(skill.serverId.toString())
            }
            result.add(SpecializationBody(
                    entry.key.id.toString(),
                    skills,
                    entry.key.spheres?.map {
                        UuidSphere(it.uuid)
                    }?.takeIf {
                        it.isNotEmpty()
                    }
            ))
        }

        return result
    }

    private fun UserSpecializations.toDisplayFormat(): String {
        var result = context.getString(R.string.your_area) + area.name + "\n"
        if(specializations.size > 0){
            result += context.getString(R.string.specializations)
        }
        specializations.forEachIndexed { index, specialization ->
            result += specialization.nameSpecialization + "\n"

            val skills = specialization.services?.mapNotNull { SkillSpecialization.getById(it) }
            if (!skills.isNullOrEmpty()) {
                var skillsLabel = ""
                skills.forEachIndexed { skillIndex, skill ->
                    skillsLabel += if (skillIndex == 0) {
                        context.getString(skill.shortTitleRes)
                    } else {
                        ", ${context.getString(skill.shortTitleRes)}"
                    }
                }
                result += "($skillsLabel)\n"
            }
        }
        result = result.substringBeforeLast("\n")

        return result
    }
}