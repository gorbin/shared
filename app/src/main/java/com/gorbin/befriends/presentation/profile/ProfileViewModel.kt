package com.gorbin.befriends.presentation.profile

import android.app.Application
import android.net.Uri
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.User
import com.gorbin.befriends.domain.entities.UserStatus
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider

class ProfileViewModel(application: Application, val useCase: IProfileUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_MY_REQUESTS = "action_my_requests"
        const val ACTION_REQUESTS_TO_ME = "action_requests_to_me"
        const val ACTION_SEARCH = "action_search"
        const val ACTION_SETTINGS = "action_settings"
        const val ACTION_REVIEWS = "action_reviews"
        const val ACTION_PRICE_LIST = "action_price_list"
        const val ACTION_NO_PRICE_LIST = "action_no_price_list"
        const val ACTION_ABOUT = "action_about"
        const val ACTION_SPECIALIZATION = "action_specialization"
        const val ACTION_GALLERY = "action_gallery"
        const val ACTION_CONSULT = "action_consult"
        const val ACTION_CURRENCY_TIP = "action_currency_tip"
        const val ACTION_SHARE = "action_share"
    }

    val logo = ObservableField<Uri>()
    val fullName = ObservableField<String>()
    val nickname = ObservableField<String>()
    val rating = ObservableField<String>()
    val consult = ObservableField<String>()
    val readyStatus = ObservableField<String>()
    val myRequestsCounter = ObservableField<String>()
    val requestsToMeCounter = ObservableField<String>()
    val active = ObservableBoolean(true).onChanged { updateReadyStatus() }
    val blocked = ObservableBoolean()
    val isRed = ObservableBoolean(false)
    val hasConsult = ObservableBoolean(true)
    val isNewbie = ObservableBoolean()
    private var data: User? = null

    fun initProfile() {
        useCase.getProfile()
                .appSubscribe {
                    this.data = it
                    blocked.set(it.status == UserStatus.BLOCKED)
                    logo.set(it.logo?.let { Uri.parse(it) })
                    fullName.set(it.fullName)
                    nickname.set(it.username)
                    rating.set(it.rating.toString())
                    consult.set("$ ${it.costService.toInt()}")
                    active.set(if (blocked.get()) false else it.activity)
                    updateReadyStatus()
                    hasConsult.set(it.hasConsultation)
                    myRequestsCounter.set(it.myRequestsCounter?.takeIf { it > 0 }?.toString())
                    requestsToMeCounter.set(it.requestsToMeCounter?.takeIf { it > 0 }?.toString())
                    isRed.set(isRed())
                }
    }

    private fun updateReadyStatus() {
        readyStatus.set(
                when {
                    blocked.get() -> context.getString(R.string.account_blocked)
                    active.get() -> context.getString(R.string.ready_title)
                    else -> context.getString(R.string.on_vacation)
                }
        )
    }

    fun onActivityChanged(checked: Boolean) {
        useCase.updateActivity(checked).appSubscribe {
            active.set(it.activity)
            isRed.set(isRed())

        }
    }

    fun onShareClick() {
        processor.onNext(UIAction(ACTION_SHARE))
    }

    fun onMyRequestsClick() {
        processor.onNext(UIAction(ACTION_MY_REQUESTS))
    }

    fun onRequestsToMeClick() {
        processor.onNext(UIAction(ACTION_REQUESTS_TO_ME))
    }

    fun onSearchFormClick() {
        if (blocked.get())
            return
        processor.onNext(UIAction(ACTION_SEARCH))
    }

    fun onSettingsClick() {
        processor.onNext(UIAction(ACTION_SETTINGS))
    }

    fun onReviewsClick() {
        processor.onNext(UIAction(ACTION_REVIEWS))
    }

    fun onAboutClick() {
        processor.onNext(UIAction(ACTION_ABOUT))
    }

    fun onSpecializationClick() {
        processor.onNext(UIAction(ACTION_SPECIALIZATION))
    }

    fun onGalleryClick() {
        processor.onNext(UIAction(ACTION_GALLERY))
    }

    fun onConsultClick() {
        processor.onNext(UIAction(ACTION_CONSULT))
    }

    fun onPriceListClick() {
        if (data?.hasPriceList == true) {
            processor.onNext(UIAction(ACTION_PRICE_LIST))
        } else {
            processor.onNext(UIAction(ACTION_NO_PRICE_LIST))
        }
    }

    fun currencyTip() {
        processor.onNext(UIAction(ACTION_CURRENCY_TIP))
    }

    fun isRed(): Boolean = blocked.get() || !active.get()
    override fun provideVm(): AppViewModel {
        return this
    }
}