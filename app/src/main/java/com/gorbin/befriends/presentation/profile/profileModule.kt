package com.gorbin.befriends.presentation.profile

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val profileModule = module {
    viewModel { ProfileViewModel(application = get(), useCase = get()) }
}

val aboutModule = module {
    viewModel { AboutViewModel(application = get(), useCase = get()) }
}
val consultModule = module {
    viewModel { ConsultViewModel(application = get(), useCase = get()) }
}
val profileSpecializationModule = module {
    viewModel { ProfileSpecializationViewModel(application = get(), useCase = get()) }
}