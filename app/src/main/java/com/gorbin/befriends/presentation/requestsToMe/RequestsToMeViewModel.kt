package com.gorbin.befriends.presentation.requestsToMe

import android.app.Application
import androidx.databinding.ObservableBoolean
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.MyRequest
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsIdentityDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import io.reactivex.Observer

class RequestsToMeViewModel(application: Application, private val useCase: IRequestsUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val REQUEST_TYPE = 1

        const val ACTION_PROFILE = "action_profile"
        const val ACTION_CHAT = "action_chat"
        const val ACTION_REMOVE_REQUEST = "action_remove_request"
    }

    private lateinit var observer: Observer<List<*>>
    private var isPageInProgress: Boolean = false
    private var currentPage: Int = 0
    private val requestList = mutableListOf<MyRequest>()
    private var requestToRemove: String? = null
    val isClean = ObservableBoolean(true)

    fun init() {
        isClean.set(true)
        currentPage = 0
        requestList.clear()
        isPageInProgress = false
        loadNewPage(0)
    }

    fun loadNewPage(itemCount: Int) {
        currentPage = itemCount / IRequestsUseCase.PAGE_SIZE + 1
        loadNextPage()
    }

    private fun loadNextPage() {
        if (isPageInProgress)
            return

        isPageInProgress = true

        useCase.getRequestsToMe(currentPage)
                .doOnError {
                    isClean.set(false)
                    isPageInProgress = false
                }.appSubscribe {
                    if (currentPage == 1)
                        requestList.clear()
                    if (it.size >= IRequestsUseCase.PAGE_SIZE)
                        isPageInProgress = false
                    requestList.addAll(it)
                    observer.onNext(requestList)
                    isClean.set(false)
                }
    }

    fun openProfile(userId: Long) {
        processor.onNext(UIAction(ACTION_PROFILE, userId))
    }

    fun openChat(request: MyRequest) {
        processor.onNext(UIAction(ACTION_CHAT, request))
    }

    fun requestRemovingConfirmation(uuidRequest: String) {
        requestToRemove = uuidRequest
        processor.onNext(UIAction(ACTION_REMOVE_REQUEST))
    }

    override fun onActionFromChild(action: UIAction) {
        when (action.actionId) {
            GlobalValues.CONFIRM_ACTION -> {
                requestToRemove?.let { uuidRequest ->
                    useCase.removeRequest(uuidRequest).appSubscribe {
                        val request = requestList.find { it.uuidRequest == uuidRequest }
                        request?.let { requestList.remove(it) }
                        observer.onNext(requestList)
                    }
                }
            }
        }
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(REQUEST_TYPE, R.layout.request_to_me_item, MyRequest::class.java, RequestsToMeItemViewModel::class.java)
            .processor(DiffUtilsIdentityDataProcessor())
            .build()
            .also {
                this.observer = it.dataReceiver
            }

    override fun provideVm(): AppViewModel {
        return this
    }
}