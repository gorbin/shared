package com.gorbin.befriends.presentation.requestsToMe

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val requestsToMeModule = module {
    viewModel { RequestsToMeViewModel(application = get(), useCase = get()) }
}