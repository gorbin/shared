package com.gorbin.befriends.presentation.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.AllReviewsFrBinding
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.ReviewSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.FullReviewScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class AllReviewsFragment : AbstractBaseFragment<AllReviewsViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Triple<ReviewSortBy?, SortOrder?, SkillSpecialization?>>(AllReviewsViewModel.ACTION_FILTER_AND_SORT) {
            DialogProvider.getReviewsFilterAndSortDialog(requireContext(), it, appViewModel)
                    .show()
        }.register<Review>(ReviewViewModel.ACTION_FULL_REVIEW) {
            router.navigateTo(FullReviewScreen(it))
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: AllReviewsViewModel): View {
        val binding = AllReviewsFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init()

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            iconId = R.drawable.ic_close,
            toolbarText = requireContext().getString(R.string.all_reviews)
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}