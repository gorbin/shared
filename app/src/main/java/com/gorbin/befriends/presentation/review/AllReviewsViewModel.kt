package com.gorbin.befriends.presentation.review

import android.app.Application
import android.os.Bundle
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.domain.entities.ReviewSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.domain.usecases.ICommentsUseCase
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import io.reactivex.Observer

class AllReviewsViewModel(application: Application, private val useCase: ICommentsUseCase) : ReviewViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_FILTER_AND_SORT = "action_filter_and_sort"
    }

    val rating = ObservableField<String>()
    val filterAndSort = ObservableField<String>()
    private val sortBy = ObservableField<ReviewSortBy>(ReviewSortBy.DATE).onChanged { updateFilterAndSort() }
    private val sortOrder = ObservableField<SortOrder>(SortOrder.DESC).onChanged { updateFilterAndSort() }
    private val specialization = ObservableField<SkillSpecialization>().onChanged { updateFilterAndSort() }
    private lateinit var observer: Observer<List<*>>

    fun init() {
        updateFilterAndSort()
    }

    fun onFilterAndSortClick() {
        processor.onNext(UIAction(ACTION_FILTER_AND_SORT, Triple(sortBy.get(), sortOrder.get(), specialization.get())))
    }

    private fun updateFilterAndSort() {
        val sortBy = sortBy.get()?.labelRes?.let { context.getString(it) } ?: ""
        val sortOrder = sortOrder.get()?.labelRes?.let { " (${context.getString(it)})" } ?: ""
        filterAndSort.set(sortBy + sortOrder)
        useCase.getMyRatingAndReviews(
                this.sortBy.get() ?: ReviewSortBy.DATE,
                this.sortOrder.get() ?: SortOrder.DESC,
                specialization.get()?.let { listOf(it) }
        ).appSubscribe {
            rating.set(
                    when (specialization.get()) {
                        SkillSpecialization.CONSULTATION -> it.first.ratingConsultation
                        SkillSpecialization.WORK -> it.first.ratingWork
                        SkillSpecialization.TEACH -> it.first.ratingTraining
                        else -> it.first.generalRating
                    }
            )
            observer.onNext(it.second)
        }
    }

    fun getRecyclerAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(0, R.layout.review_item, Review::class.java, AllReviewsReviewItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                observer = it.dataReceiver
            }

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction) {
        when (action.actionId) {
            GlobalValues.APPLY_FILTER_AND_SORT -> {
                val bundle = action.data as? Bundle ?: return
                bundle.getTypedSerializable<ReviewSortBy>(GlobalValues.SORT_BY)?.let { sortBy.set(it) }
                bundle.getTypedSerializable<SortOrder>(GlobalValues.SORT_ORDER)?.let { sortOrder.set(it) }
                bundle.getTypedSerializable<SkillSpecialization>(GlobalValues.SPECIALIZATION).let { specialization.set(it) }
            }
        }
    }
}