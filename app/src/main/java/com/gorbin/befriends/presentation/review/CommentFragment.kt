package com.gorbin.befriends.presentation.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.CommentFrBinding
import com.gorbin.befriends.domain.entities.ReviewBody
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class CommentFragment : AbstractBaseFragment<CommentViewModel>() {
    companion object {
        const val REVIEW_DATA = "review_data"
        const val DATA_SELECTOR = "data_selector"
    }

    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(CommentViewModel.ACTION_REVIEW_SENT) {
            DialogProvider.getReviewDoneDialog(requireContext(), appViewModel)
                    .modal(true)
                    .show()
        }.register<Unit>(CommentViewModel.ACTION_DONE) {
            appViewModel.comment.set(null)
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: CommentViewModel): View {
        val binding = CommentFrBinding.inflate(layoutInflater, container, false)
        binding.viewModel = appViewModel
        val dataSelector = arguments?.getTypedSerializable<DataSelection<ReviewBody>>(DATA_SELECTOR)
        val reviewBody = arguments?.getTypedSerializable<ReviewBody>(REVIEW_DATA)!!
        appViewModel.init(reviewBody, dataSelector)

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = requireContext().getString(R.string.leave_a_review),
            iconId = R.drawable.ic_close
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}