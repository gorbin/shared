package com.gorbin.befriends.presentation.review

import android.app.Application
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.ReviewBody
import com.gorbin.befriends.domain.usecases.ICommentsUseCase
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider

class CommentViewModel(application: Application, private val useCase: ICommentsUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_REVIEW_SENT = "action_review_sent"
        const val ACTION_DONE = "action_done"
    }

    val comment = ObservableField<String>()
    private lateinit var data: ReviewBody
    private var selector: DataSelection<ReviewBody>? = null

    fun init(data: ReviewBody, selector: DataSelection<ReviewBody>?) {
        this.data = data
        this.selector = selector
    }

    fun send() {
        val comment = comment.get() ?: return
        val selector = selector
        data.comment = comment
        if (selector == null) {
            useCase.sendReview(data.executorUuid, data)
                    .appSubscribe {
                        processor.onNext(UIAction(ACTION_REVIEW_SENT))
                    }
        } else {
            selector.setSelectedData(data)
            processor.onNext(UIAction(ACTION_REVIEW_SENT))
        }
    }

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction) {
        super.onActionFromChild(action)
        when (action.actionId) {
            GlobalValues.REVIEW_OK -> processor.onNext(UIAction(ACTION_DONE))
        }
    }
}