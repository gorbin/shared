package com.gorbin.befriends.presentation.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.FullReviewFrBinding
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class FullReviewFragment : AbstractBaseFragment<FullReviewViewModel>() {
    companion object {
        const val REVIEW = "review"
    }

    private val router: Router by inject()

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: FullReviewViewModel): View {
        val binding = FullReviewFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        (arguments?.getSerializable(REVIEW) as? Review)?.let {
            appViewModel.init(it)
        }

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            iconId = R.drawable.ic_close,
            toolbarText = requireContext().getString(R.string.reviews)

    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}