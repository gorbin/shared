package com.gorbin.befriends.presentation.review

import android.app.Application
import android.net.Uri
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.ServiceType
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.DateFormats
import com.gorbin.befriends.utils.toDisplayRating

class FullReviewViewModel(application: Application) : BaseAppViewModel(application) {
    val image = ObservableField<Uri>()
    val name = ObservableField<String>()
    val date = ObservableField<String>()
    val rating = ObservableField<String>()
    val review = ObservableField<String>()
    val serviceType = ObservableField<String>()

    fun init(review: Review) {
        this.image.set(review.profilePhoto?.let { Uri.parse(it) })
        this.name.set(review.fullName)
        this.date.set(DateFormats.numericDateFormat.format(review.created_at))
        this.rating.set(review.rating.toDisplayRating())
        this.review.set(review.comment)
        serviceType.set(
                when (review.type) {
                    ServiceType.CONSULTANCY -> context.getString(R.string.consultation)
                    ServiceType.EXECUTION -> context.getString(R.string.work)
                    ServiceType.TRAINING -> context.getString(R.string.teach)
                }
        )
    }
}