package com.gorbin.befriends.presentation.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.MarksFrBinding
import com.gorbin.befriends.domain.entities.ReviewBody
import com.gorbin.befriends.domain.entities.ServiceType
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.CommentScreen
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class MarksFragment : AbstractBaseFragment<MarksViewModel>() {
    companion object {
        const val REQUEST_ID = "request_id"
        const val SERVICE_TYPE = "service_type"
        const val USER_UUID = "user_uuid"
        const val DATA_SELECTOR = "data_selector"
    }

    private val router: Router by inject()
    private var requestId: Long? = null
    private lateinit var executorUuid: String
    private lateinit var serviceType: ServiceType

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Triple<Int, Int, Int>>(MarksViewModel.ACTION_NEXT) {
            requestId?.let { requestId ->
                val data = ReviewBody(
                        executorUuid = executorUuid,
                        requestId = requestId,
                        type = serviceType,
                        professionalism = it.first,
                        performance = it.second,
                        politeness = it.third
                )
                router.replaceScreen(CommentScreen(data, arguments?.getTypedSerializable<DataSelection<ReviewBody>>(DATA_SELECTOR)))
            }
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: MarksViewModel): View {
        val binding = MarksFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        requestId = arguments?.getLong(REQUEST_ID)?.takeIf { it != 0L }
        serviceType = arguments?.getTypedSerializable<ServiceType>(SERVICE_TYPE)!!
        executorUuid = arguments?.getString(USER_UUID)!!

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = requireContext().getString(R.string.leave_a_review),
            iconId = R.drawable.ic_close
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}