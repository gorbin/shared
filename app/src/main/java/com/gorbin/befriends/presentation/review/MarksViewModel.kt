package com.gorbin.befriends.presentation.review

import android.app.Application
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.presentation.base.viewmodel.ISeekBarHandler
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import java.util.*

class MarksViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_NEXT = "action_next"
    }

    val mark = ObservableField<String>("0.0")
    val workTitle = ObservableField<String>()
    private val professionalism = ObservableInt().onChanged { calculateRating() }
    private val performance = ObservableInt().onChanged { calculateRating() }
    private val politeness = ObservableInt().onChanged { calculateRating() }

    private fun calculateRating() {
        val rating = listOf(professionalism.get(), performance.get(), politeness.get()).average()
        mark.set(String.format(Locale.US, "%.1f", rating))
    }

    fun next() {
        processor.onNext(UIAction(ACTION_NEXT, Triple(
                professionalism.get(),
                performance.get(),
                politeness.get())
        ))

        professionalism.set(10)
        performance.set(10)
        politeness.set(10)
    }

    fun getProfessionalismSeekBarHandler(): ISeekBarHandler =
            RatingSeekBarHandler { professionalism.set(it) }

    fun getPerformanceSeekBarHandler(): ISeekBarHandler =
            RatingSeekBarHandler { performance.set(it) }

    fun getPolitenessSeekBarHandler(): ISeekBarHandler =
            RatingSeekBarHandler { politeness.set(it) }
}