package com.gorbin.befriends.presentation.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.MyRatingAndReviewsFrBinding
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.AllReviewsScreen
import com.gorbin.befriends.presentation.cicerone.FullReviewScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class MyRatingAndReviewsFragment : AbstractBaseFragment<MyRatingAndReviewsViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(MyRatingAndReviewsViewModel.ACTION_ALL_REVIEWS) {
            router.navigateTo(AllReviewsScreen)
        }.register<Review>(ReviewViewModel.ACTION_FULL_REVIEW) {
            router.navigateTo(FullReviewScreen(it))
        }
    }

    override fun initBinding(inflater: LayoutInflater,
                             container: ViewGroup?,
                             appViewModel: MyRatingAndReviewsViewModel): View {
        val binding = MyRatingAndReviewsFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        binding.executePendingBindings()
        appViewModel.init()

        return binding.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = requireContext().getString(R.string.rating_and_reviews)
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }
}