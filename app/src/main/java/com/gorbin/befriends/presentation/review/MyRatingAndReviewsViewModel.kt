package com.gorbin.befriends.presentation.review

import android.app.Application
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.google.android.material.tabs.TabLayout
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.ICommentsUseCase
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import io.reactivex.Observer

class MyRatingAndReviewsViewModel(application: Application, private val useCase: ICommentsUseCase) : ReviewViewModel(application) {
    companion object {
        const val ACTION_ALL_REVIEWS = "action_all_reviews"
    }

    val rating = ObservableField<String>("0")
    val categoryRating = ObservableField<String>("0")
    val categoryOrders = ObservableField<String>("0")
    val categoryComments = ObservableField<String>("0")
    val categoryProfessionalism = ObservableField<String>("0")
    val categoryPerformance = ObservableField<String>("0")
    val categoryPoliteness = ObservableField<String>("0")
    val reviewsNumber = ObservableInt()
    val tabs = SkillSpecialization.values().asList()
    val specialization = ObservableField<SkillSpecialization>().onChanged { onSpecializationChanged(it) }
    private var data: UserRating? = null

    lateinit var observer: Observer<List<*>>

    fun init() {
        useCase.getMyRatingAndReviews()
                .appSubscribe {
                    data = it.first
                    displayData(it)
                }
    }

    private fun onSpecializationChanged(spec: SkillSpecialization?) {
        if (spec == null)
            return

        val userRating = data ?: return

        rating.set(userRating.generalRating)
        reviewsNumber.set(userRating.allCommentNumber)

        when (spec) {
            SkillSpecialization.CONSULTATION -> {
                categoryRating.set(userRating.ratingConsultation)
                categoryOrders.set(userRating.consultationRequestsNumber.toString())
                categoryComments.set(userRating.consultationCommentsNumber.toString())
                categoryProfessionalism.set(userRating.consultationProfessionalism)
                categoryPerformance.set(userRating.consultationPerformance)
                categoryPoliteness.set(userRating.consultationPoliteness)
            }
            SkillSpecialization.WORK -> {
                categoryRating.set(userRating.ratingWork)
                categoryOrders.set(userRating.workRequestsNumber.toString())
                categoryComments.set(userRating.workCommentsNumber.toString())
                categoryProfessionalism.set(userRating.workProfessionalism)
                categoryPerformance.set(userRating.workPerformance)
                categoryPoliteness.set(userRating.workPoliteness)
            }
            SkillSpecialization.TEACH -> {
                categoryRating.set(userRating.ratingTraining)
                categoryOrders.set(userRating.trainingRequestsNumber.toString())
                categoryComments.set(userRating.trainingCommentsNumber.toString())
                categoryProfessionalism.set(userRating.trainingProfessionalism)
                categoryPerformance.set(userRating.trainingPerformance)
                categoryPoliteness.set(userRating.trainingPoliteness)
            }
        }
    }

    private fun displayData(data: Pair<UserRating, List<Review>>) {
        onSpecializationChanged(specialization.get())
        observer.onNext(data.second)
    }

    fun showAllReviews() {
        processor.onNext(UIAction(ACTION_ALL_REVIEWS))
    }

    fun getRecyclerAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(0, R.layout.review_item, Review::class.java, MyRatingReviewItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                observer = it.dataReceiver
            }

    fun getTabSelectedListener(): TabLayout.OnTabSelectedListener {
        return object : TabLayout.OnTabSelectedListener {
            init {
                specialization.set(SkillSpecialization.values().first())
            }

            override fun onTabReselected(tab: TabLayout.Tab) { }

            override fun onTabUnselected(tab: TabLayout.Tab) { }

            override fun onTabSelected(tab: TabLayout.Tab) {
                specialization.set(tab.tag as? SkillSpecialization)
            }
        }
    }
}