package com.gorbin.befriends.presentation.review

import android.widget.SeekBar
import com.gorbin.befriends.presentation.base.viewmodel.ISeekBarHandler
import kotlin.math.roundToInt

class RatingSeekBarHandler(val callback: (Int) -> Unit) : ISeekBarHandler {
    companion object {
        private const val MAX_SEEK_BAR_PROGRESS_VALUE = 90
    }

    override fun getSeekBarListener(view: SeekBar): SeekBar.OnSeekBarChangeListener {
        view.max = MAX_SEEK_BAR_PROGRESS_VALUE
        view.progress = MAX_SEEK_BAR_PROGRESS_VALUE
        callback.invoke(calculateValue(view.progress))

        return object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) { }

            override fun onStartTrackingTouch(seekBar: SeekBar) { }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                seekBar.progress = ((seekBar.progress + 10) / 10.0).roundToInt() * 10 - 10
                callback.invoke(calculateValue(seekBar.progress))
            }
        }
    }

    private fun calculateValue(progress: Int): Int {
        return progress / 10 + 1
    }
}