package com.gorbin.befriends.presentation.review

import android.net.Uri
import androidx.databinding.ObservableField
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.domain.entities.ServiceType
import com.gorbin.befriends.presentation.executorProfile.ExecutorProfileViewModel
import com.gorbin.befriends.utils.DateFormats
import com.gorbin.befriends.utils.toDisplayRating
import com.punicapp.mvvm.adapters.AdapterItemViewModel

open class ReviewItemViewModel <T> : AdapterItemViewModel<Review, T>() where T : ReviewViewModel {
    val image = ObservableField<Uri>()
    val name = ObservableField<String>()
    val date = ObservableField<String>()
    val rating = ObservableField<String>()
    val review = ObservableField<String>()
    val serviceType = ObservableField<String>()
    lateinit var data: Review

    override fun bindData(data: Review) {
        this.data = data
        image.set(data.profilePhoto?.let { Uri.parse(it) })
        name.set(data.fullName)
        date.set(DateFormats.numericDateFormat.format(data.created_at))
        rating.set(data.rating.toDisplayRating())
        review.set(data.comment)
        serviceType.set(
                when (data.type) {
                    ServiceType.CONSULTANCY -> parent.context.getString(R.string.consultation)
                    ServiceType.EXECUTION -> parent.context.getString(R.string.work)
                    ServiceType.TRAINING -> parent.context.getString(R.string.teach)
                }
        )
    }

    fun showFullReview() {
        parent.showFullReview(data)
    }
}

class MyRatingReviewItemViewModel : ReviewItemViewModel<MyRatingAndReviewsViewModel>()
class AllReviewsReviewItemViewModel : ReviewItemViewModel<AllReviewsViewModel>()
class ProfileReviewItemViewModel : ReviewItemViewModel<ExecutorProfileViewModel>()