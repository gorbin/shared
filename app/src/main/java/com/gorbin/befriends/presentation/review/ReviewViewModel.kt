package com.gorbin.befriends.presentation.review

import android.app.Application
import com.gorbin.befriends.domain.entities.Review
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction

open class ReviewViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_FULL_REVIEW = "action_full_review"
    }

    fun showFullReview(data: Review) {
        processor.onNext(UIAction(ACTION_FULL_REVIEW, data))
    }
}