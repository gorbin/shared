package com.gorbin.befriends.presentation.review

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val reviewModule = module {
    viewModel { MarksViewModel(application = get()) }
    viewModel { CommentViewModel(application = get(), useCase = get()) }
    viewModel { MyRatingAndReviewsViewModel(application = get(), useCase = get()) }
    viewModel { FullReviewViewModel(application = get()) }
    viewModel { AllReviewsViewModel(application = get(), useCase = get()) }
}