package com.gorbin.befriends.presentation.search

import android.net.Uri
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.UserExecutor
import com.gorbin.befriends.domain.entities.UserStatus
import com.gorbin.befriends.utils.toDisplayRating
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class SearchExecutorItemViewModel : AdapterItemViewModel<UserExecutor, SearchResultViewModel>() {
    val image = ObservableField<Uri>()
    val name = ObservableField<String>()
    val rating = ObservableField<String>()
    val price = ObservableField<String>()
    val ordersCount = ObservableField<String>()
    val reviewsCount = ObservableField<String>()
    val isNewbie = ObservableBoolean()
    val showNickname = ObservableBoolean()

    lateinit var data: UserExecutor

    override fun bindData(data: UserExecutor) {
        this.data = data
        image.set(data.url?.let { Uri.parse(it) })
        name.set(data.getNameOrNickname())
        rating.set(data.rating.toDisplayRating())
        price.set(data.getFormattedPrice())
        ordersCount.set(data.ordersCount.toString())
        reviewsCount.set(data.reviewsCount.toString())
        isNewbie.set(data.isNewbie())
        showNickname.set(data.showNickname)
    }

    fun openProfile() {
        parent.onProfileClick(data.idUser)
    }

    fun free():Boolean {
        return price.get().equals("FREE")
    }
}
