package com.gorbin.befriends.presentation.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.common.base.Optional
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.SearchFormFrBinding
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.ChooseCountryCityScreen
import com.gorbin.befriends.presentation.cicerone.SearchResultScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationAreaScreen
import com.gorbin.befriends.presentation.cicerone.SpecializationSkillScreen
import com.gorbin.befriends.utils.FusedLocationTracker
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.toLatLng
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

class SearchFormFragment : AbstractBaseFragment<SearchFormViewModel>() {
    private var binding: SearchFormFrBinding? = null
    private val router: Router by inject()
    private val localRepo: IKeyValueRepository<String> by inject()
    private val selector = DataSelection<Pair<Country, City>>()
    private val selectorSpecialization = DataSelection<SpecializationTree>()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<SearchForm>(SearchFormViewModel.ACTION_DONE) {
            router.navigateTo(SearchResultScreen(it))
        }.register<Pair<List<Country>, Pair<Country, City>>>(SearchFormViewModel.ACTION_COUNTRY_SELECTION) {
            router.navigateTo(ChooseCountryCityScreen(it.first, it.second, selector, CitySelectionFlow.SEARCH))
        }.register<Pair<List<Language>, List<Language>>>(SearchFormViewModel.ACTION_LANGUAGE_SELECTION) {
            showProgress(true)
            showLanguageSelectionDialog(it)
        }.register<SpecializationTree>(SearchFormViewModel.ACTION_SPECIALIZATION_SELECTION) {
            if (it.selectedSpecializations.isNotEmpty()) {
                router.navigateTo(SpecializationSkillScreen(it, selectorSpecialization, SpecializationFlow.SEARCH))
            } else {
                router.navigateTo(SpecializationAreaScreen(it, selectorSpecialization, SpecializationFlow.SEARCH))
            }
        }.register<Unit>(SearchFormViewModel.ACTION_GET_LOCATION) {
            getLocation()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SearchFormViewModel): View {
        binding?.let {
            return it.root
        }

        binding = SearchFormFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        selector.listener = appViewModel.getCitySelectionListener()
        selectorSpecialization.listener = appViewModel.getSpecializationSelectionListener()
        val session = localRepo.instantGet<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java).orNull()
        appViewModel.init(session != null)
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        hideKeyboardOnActivityCreated()
    }

    private fun showLanguageSelectionDialog(data: Pair<List<Language>, List<Language>>) {
        DialogProvider.getLangMultiSelectionDialog(requireContext(), appViewModel, data.first, data.second)
                .show()
        showProgress(false)
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarVisible = true,
            toolbarText = requireContext().getString(R.string.new_request)
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        appViewModel.clear()
        return true
    }

    private fun getLocation(): Disposable? {
        val act = activity ?: return null
        val fusedLocationTracker = FusedLocationTracker(act)
        return fusedLocationTracker.isLocationAvailable().map {
            if (it) LocationCheckResults.AVAILABLE else LocationCheckResults.NOT_AVAILABLE
        }.subscribe { result, _ ->
            when (result) {
                LocationCheckResults.AVAILABLE -> {
                    getLatLng(fusedLocationTracker)
                }
                else -> {
                }
            }
        }
    }

    private fun getLatLng(fusedLocationTracker: FusedLocationTracker): Disposable {
        return fusedLocationTracker.getLocationLatLngObservable()
                .timeout(10L, TimeUnit.SECONDS)
                .firstOrError()
                .map { Optional.of(it) }
                .subscribe { result, throwable ->
                    if (throwable == null && result.isPresent) {
                        val latLng = result.get().toLatLng()
                        appViewModel.onLocation(latLng)
                    }
                }
    }
}