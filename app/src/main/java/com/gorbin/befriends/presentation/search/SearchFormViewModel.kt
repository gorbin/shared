package com.gorbin.befriends.presentation.search

import android.app.Application
import android.os.Bundle
import android.os.Handler
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.android.gms.maps.model.LatLng
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.*
import com.gorbin.befriends.domain.usecases.ISearchUseCase
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider

class SearchFormViewModel(application: Application, private val useCase: ISearchUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_COUNTRY_SELECTION = "action_city_selection"
        const val ACTION_LANGUAGE_SELECTION = "action_language_selection"
        const val ACTION_SPECIALIZATION_SELECTION = "action_specialization_selection"
        const val ACTION_GET_LOCATION = "action_get_location"

        const val LANGUAGE = "language"
        const val SPECIALIZATION = "specialization"
    }

    val selectSpecializations = ObservableField<String>().onChanged { errors[SPECIALIZATION] = null }
    val city = ObservableField<String>()
    val languageProficiency = ObservableField<String>().onChanged { errors[LANGUAGE] = null }
    val ratingField = ObservableField<String>()
    val priceField = ObservableField<String>().onChanged { price = it?.toIntOrNull() ?: 0 }
    val serviceType = ObservableField<SkillSpecialization>()
    val buttonText = ObservableField<String>()
    val clearButtonVisible = ObservableBoolean()
    val errors = ObservableArrayMap<String, String>()

    private val countries = mutableListOf<Country>()
    private var selectedCity: Pair<Country, City>? = null
    private val languages = mutableListOf<Language>()
    private val selectedLanguages = mutableListOf<Language>()
    private val specializationAreas = mutableListOf<SpecializationArea>()
    private val selectedSpecializations = mutableListOf<Specialization>()
    private var rating: Int = 0
    private var selectedSpecialization: Specialization? = null
    private var price: Int = 0
    private val clearButtonObservable = ObservableField<String>(
            selectSpecializations,
            city,
            languageProficiency,
            priceField
    ).onChanged {
        clearButtonVisible.set(
                !selectSpecializations.get().isNullOrBlank()
                        || !city.get().isNullOrBlank()
                        || !languageProficiency.get().isNullOrBlank()
                        || !priceField.get().isNullOrBlank()
        )
    }
    private var tree: SpecializationTree? = null

    override fun provideVm(): AppViewModel {
        return this
    }

    fun init(isAuth: Boolean) {
        initSearchForm(isAuth)
    }

    private fun initSearchForm(isAuth: Boolean) {
        useCase.getAdditionalInfoOptions()
                .appSubscribe { (countries, languages, areas) ->
                    this.countries.clear()
                    this.countries.addAll(countries)
                    this.languages.clear()
                    this.languages.addAll(languages)
                    this.specializationAreas.clear()
                    this.specializationAreas.addAll(areas)
                    processor.onNext(UIAction(ACTION_GET_LOCATION))
                }
        buttonText.set(if (isAuth) context.getString(R.string.search) else context.getString(R.string.find_executor))
        ratingField.set(rating.toString())
    }

    fun getCitySelectionListener(): DataSelection.Listener<Pair<Country, City>> = object : DataSelection.Listener<Pair<Country, City>> {
        override fun onChange(data: Pair<Country, City>) {
            Handler().postDelayed({
                selectedCity = data
                city.set("${data.first.name}, ${data.second.name}")
            }, 100)
        }
    }

    fun getSpecializationSelectionListener(): DataSelection.Listener<SpecializationTree> = object : DataSelection.Listener<SpecializationTree> {
        override fun onChange(data: SpecializationTree) {
            Handler().postDelayed({
                tree = data
                selectedSpecialization = data.selectedSpecializations.keys.first()
                selectSpecializations.set(data.selectedSpecializations.toDisplaySpecsFormat())
                serviceType.set(data.selectedSpecializations.entries.firstOrNull()?.value?.firstOrNull())
                if (serviceType.get() == SkillSpecialization.TEACH || serviceType.get() == SkillSpecialization.WORK) {
                    priceField.set(null)
                    price = 0
                }
            }, 100)
        }
    }

    fun clearCity() {
        selectedCity = null
        city.set(null)
    }

    fun selectCity() {
        processor.onNext(UIAction(ACTION_COUNTRY_SELECTION, countries to selectedCity))
    }

    fun selectLanguages() {
        processor.onNext(UIAction(ACTION_LANGUAGE_SELECTION, languages to selectedLanguages))
    }

    fun selectSpecialization() {
        val data = tree ?: SpecializationTree().apply {
            areas = specializationAreas
            selectedSpecializations = this@SearchFormViewModel.selectedSpecializations.map { it to mutableListOf<SkillSpecialization>() }.toMap()
        }
        processor.onNext(UIAction(ACTION_SPECIALIZATION_SELECTION, data))
    }

    fun onRatingAddClick() {
        if (rating < 10)
            rating += 1
        ratingField.set(rating.toString())
    }

    fun onRatingRemoveClick() {
        if (rating > 0)
            rating -= 1
        ratingField.set(rating.toString())
    }

    fun onFree() {
        price = 0
        priceField.set(price.toString())
    }

    private fun Map<Specialization, List<SkillSpecialization>>.toDisplaySpecsFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        for (entry in this) {
            val sphere = entry.key.spheres?.firstOrNull()?.name
            val sphereString = if(sphere == null) "" else  " $sphere"
            result += specializationAreas.find { it.id == entry.key.areaId }?.name + ": ${entry.key.name}$sphereString"
            if (entry.value.isNotEmpty()) {
                var skills = ""
                entry.value.forEachIndexed { index, skill ->
                    skills += if (index == 0) {
                        context.getString(skill.shortTitleRes)
                    } else {
                        ", ${context.getString(skill.shortTitleRes)}"
                    }
                }
                result += " ($skills)"
            }
        }

        return result
    }

    fun onDoneClick() {
        if (!checkValid())
            return

        val data = SearchForm(
                idSpecialization = selectedSpecialization?.id?.toString(),
                idSphere = selectedSpecialization?.spheres?.firstOrNull()?.uuid,
                idService = serviceType.get()?.serverId?.toString(),
                idCountry = selectedCity?.first?.getId(),
                idCity = selectedCity?.second?.getId(),
                idLanguage = selectedLanguages.map { it.id.toString() },
                rating = rating.toString(),
                cost = price.toString()
        )
        processor.onNext(UIAction(ACTION_DONE, data))
    }

    fun clear() {
        selectedCity = null
        city.set(null)
        selectedLanguages.clear()
        languageProficiency.set(null)
        selectedSpecializations.clear()
        selectSpecializations.set(null)
        selectedSpecialization = null
        price = 0
        priceField.set(null)
        errors.clear()
        serviceType.set(null)
        tree = null
    }

    private fun checkValid(): Boolean {
        var result = true

        if (selectSpecializations.get().isNullOrBlank()) {
            result = false
            errors[SPECIALIZATION] = context.getString(R.string.value_is_missing)
        }
        if (languageProficiency.get().isNullOrBlank()) {
            result = false
            errors[LANGUAGE] = context.getString(R.string.value_is_missing)
        }

        return result
    }

    override fun onActionFromChild(action: UIAction?) {
        super.onActionFromChild(action)
        when (action?.actionId) {
            GlobalValues.SELECTED_LANGUAGES -> {
                val langCollection = (action.data as? Bundle)?.getSerializable(GlobalValues.SELECTED_LANGUAGES) as? LanguageCollection
                langCollection?.languages?.let {
                    selectedLanguages.clear()
                    selectedLanguages.addAll(it)
                    languageProficiency.set(selectedLanguages.toDisplayFormat())
                }
            }
        }
    }

    private fun List<Language>.toDisplayFormat(): String? {
        if (this.isEmpty())
            return null

        var result = ""
        this.forEachIndexed { index, language ->
            result += if (index == 0)
                language.name
            else
                ", ${language.name}"
        }

        return result
    }

    fun onLocation(latLng: LatLng) {
        useCase.getCityByLocation(latLng).appSubscribe { countryCity ->
            if (countryCity.id != null) {
                val country = Country(null, countryCity.country, countryCity.region, null, null, null, null, true)
                val city = City(countryCity.id.toLong(), countryCity.name, countryCity.region, countryCity.country)

                selectedCity = country to city
                this.city.set("${countryCity.country}, ${countryCity.name}")
            }
        }
    }
}