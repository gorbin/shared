package com.gorbin.befriends.presentation.search

import androidx.databinding.ObservableField
import com.punicapp.mvvm.adapters.AdapterItemViewModel

class SearchHeaderItemViewModel : AdapterItemViewModel<String, SearchResultViewModel>() {
    val header = ObservableField<String>()

    override fun bindData(data: String) {
        header.set(data)
    }
}
