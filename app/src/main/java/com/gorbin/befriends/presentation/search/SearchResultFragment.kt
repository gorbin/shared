package com.gorbin.befriends.presentation.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.SearchResultFrBinding
import com.gorbin.befriends.domain.entities.SearchForm
import com.gorbin.befriends.domain.entities.SearchSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ExecutorProfileScreen
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SearchResultFragment : AbstractBaseFragment<SearchResultViewModel>() {
    companion object {
        const val DATA = "DATA"
    }

    private var binding: SearchResultFrBinding? = null
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Pair<String, SearchForm>>(SearchResultViewModel.ACTION_PROFILE) {
            router.navigateTo(ExecutorProfileScreen(it.first, it.second, true))
        }.register<Unit>(SearchResultViewModel.ACTION_BACK) {
            router.exit()
        }.register<Triple<SortOrder, SearchSortBy, Boolean>>(SearchResultViewModel.ACTION_SORTING) {
            DialogProvider.getSearchSortingDialog(requireContext(), it, appViewModel)
                    .show()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SearchResultViewModel): View {
        binding = SearchResultFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel
        val data = arguments?.getSerializable(DATA) as SearchForm

        appViewModel.init(data)
        return binding!!.root
    }
}