package com.gorbin.befriends.presentation.search

import android.app.Application
import android.os.Bundle
import androidx.databinding.ObservableField
import com.gorbin.befriends.BR
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.SearchForm
import com.gorbin.befriends.domain.entities.SearchSortBy
import com.gorbin.befriends.domain.entities.SortOrder
import com.gorbin.befriends.domain.entities.UserExecutor
import com.gorbin.befriends.domain.usecases.IRequestsUseCase
import com.gorbin.befriends.domain.usecases.ISearchUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.adapters.DiffUtilsDataProcessor
import com.punicapp.mvvm.adapters.VmAdapter
import com.punicapp.mvvm.adapters.VmAdapterItem
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import io.reactivex.Observer

class SearchResultViewModel(application: Application, private val useCase: ISearchUseCase) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_PROFILE = "action_profile"
        const val ACTION_BACK = "action_back"
        const val ACTION_SORTING = "action_sorting"

        private const val EXECUTOR_ITEM = 1
        private const val HEADER_ITEM = 2
    }

    private var currentPage: Int = 0
    private lateinit var data: SearchForm
    private lateinit var dataObserver: Observer<List<VmAdapterItem>>
    val emptyString = ObservableField<String>("")
    private var isPageInProgress: Boolean = false
    private val dataList = List<MutableList<UserExecutor>>(5) { mutableListOf() }
    private var sortOrder: SortOrder = SortOrder.ASC
    private var sortBy: SearchSortBy = SearchSortBy.POPULAR
    private var hideNewbies: Boolean = false

    fun init(data: SearchForm) {
        this.data = data
        currentPage = 0
        dataList.forEach { it.clear() }
        isPageInProgress = false
        emptyString.set("")
        loadNewPage(0)
    }

    fun loadNewPage(itemCount: Int) {
        currentPage = itemCount / IRequestsUseCase.PAGE_SIZE + 1
        loadNextPage()
    }

    private fun loadNextPage() {
        if (isPageInProgress)
            return

        isPageInProgress = true

        useCase.search(currentPage, sortOrder, sortBy, data)
                .doOnError {
                    isPageInProgress = false
                }.appSubscribe { result ->

                    emptyString.set(context.getString(R.string.not_found))
                    if (currentPage == 1)
                        dataList.forEach { it.clear() }
                    if (result.sumBy { it.size } >= IRequestsUseCase.PAGE_SIZE)
                        isPageInProgress = false
                    dataList.forEachIndexed { index, mutableList ->
                        mutableList.addAll(result.getOrElse(index) { emptyList() })
                    }
                    showResult()
                }
    }

    private fun showResult() {
        val data = mutableListOf<VmAdapterItem>()
        if (this.data.idCity != null) {
            dataList.forEachIndexed { index, mutableList ->
                if (mutableList.isNotEmpty()) {
                    val header = when (index) {
                        0 -> R.string.search_result_group_0
                        1 -> R.string.search_result_group_1
                        2 -> R.string.search_result_group_2
                        3 -> R.string.search_result_group_3
                        4 -> R.string.search_result_group_4
                        else -> null
                    }
                    if (header != null) {
                        data.add(VmAdapterItem(context.getString(header), HEADER_ITEM))
                        data.addAll(mutableList.map { VmAdapterItem(it, EXECUTOR_ITEM) })
                    }
                }
            }
        } else {
            data.addAll(dataList.flatten().map { VmAdapterItem(it, EXECUTOR_ITEM) })
        }

        dataObserver.onNext(data)
    }

    fun onProfileClick(id: String) {
        processor.onNext(UIAction(ACTION_PROFILE, id to data))
    }

    fun onSortClick() {
        processor.onNext(UIAction(ACTION_SORTING, Triple(sortOrder, sortBy, hideNewbies)))
    }

    fun onBack() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun getAdapter(): VmAdapter = VmAdapter.Builder(this, BR.viewModel)
            .defaultProducer(EXECUTOR_ITEM, R.layout.search_executor_item, UserExecutor::class.java, SearchExecutorItemViewModel::class.java)
            .defaultProducer(HEADER_ITEM, R.layout.search_header_item, String::class.java, SearchHeaderItemViewModel::class.java)
            .processor(DiffUtilsDataProcessor())
            .build()
            .also {
                dataObserver = it.dataWithTypeReceiver
            }

    override fun onActionFromChild(action: UIAction) {
        super.onActionFromChild(action)
        when (action.actionId) {
            GlobalValues.APPLY_SEARCH_SORTING -> {
                val data = action.data as? Bundle ?: return
                var resetPage = false
                val sortOrder = data.getTypedSerializable<SortOrder>(GlobalValues.SORT_ORDER)
                sortOrder?.let {
                    if (this.sortOrder != it)
                        resetPage = true
                    this.sortOrder = it
                }
                val sortBy = data.getTypedSerializable<SearchSortBy>(GlobalValues.SORT_BY)
                sortBy?.let {
                    if (this.sortBy != it)
                        resetPage = true
                    this.sortBy = it
                }
                val hideNewbies = data.getTypedSerializable<Boolean>(GlobalValues.HIDE_NEWBIES)
                hideNewbies?.let {
                    if (this.hideNewbies != it)
                        resetPage = true
                    this.hideNewbies = it
                }
                if (resetPage) {
                    currentPage = 0
                    dataList.forEach { it.clear() }
                    isPageInProgress = false
                    loadNewPage(0)
                }
            }
        }
    }

    override fun provideVm(): AppViewModel {
        return this
    }
}