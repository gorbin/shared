package com.gorbin.befriends.presentation.search

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchModule = module {
    viewModel { SearchFormViewModel(application = get(), useCase = get()) }
    viewModel { SearchResultViewModel(application = get(), useCase = get()) }
}