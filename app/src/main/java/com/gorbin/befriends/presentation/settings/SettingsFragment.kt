package com.gorbin.befriends.presentation.settings

import android.os.Bundle
import android.view.*
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.SettingsFrBinding
import com.gorbin.befriends.fcm.notifications.NotificationManager
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.base.model.ToolbarSettings
import com.gorbin.befriends.presentation.cicerone.EditProfileScreen
import com.gorbin.befriends.presentation.cicerone.WelcomeScreen
import com.gorbin.befriends.utils.onShare
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SettingsFragment : AbstractBaseFragment<SettingsViewModel>() {
    private var binding: SettingsFrBinding? = null
    private val router: Router by inject()
    private val notificationManager: NotificationManager by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SettingsViewModel.ACTION_EXIT) {
            notificationManager.cancelAll()
            router.newRootScreen(WelcomeScreen)
        }.register<Unit>(SettingsViewModel.ACTION_EDIT_USER) {
            router.navigateTo(EditProfileScreen)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SettingsViewModel): View {
        binding = SettingsFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel

        appViewModel.init()
        return binding!!.root
    }

    override fun provideToolbarSettings() = ToolbarSettings(
            toolbarText = getString(R.string.settings),
            toolbarVisible = true,
            iconId = R.drawable.ic_arrow_back_24
    )

    override fun onUpActionTap(): Boolean {
        router.exit()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.share_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.share -> {
                context?.let {
                    onShare(it)
                }
            }
        }
        return true
    }
}