package com.gorbin.befriends.presentation.settings

import android.app.Application
import android.net.Uri
import androidx.databinding.ObservableField
import com.gorbin.befriends.domain.entities.User
import com.gorbin.befriends.domain.usecases.IProfileUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.DateFormats
import com.gorbin.befriends.utils.PhoneEmailUtils
import com.punicapp.mvvm.actions.UIAction

class SettingsViewModel(application: Application, private val useCase: IProfileUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_EXIT = "action_exit"
        const val ACTION_EDIT_USER = "action_edit_user"
    }

    val fullName = ObservableField<String>()
    val nickname = ObservableField<String>()
    val birthDate = ObservableField<String>()
    val phone = ObservableField<String>()
    val logo = ObservableField<Uri>()
    val location = ObservableField<String>()
    val lang = ObservableField<String>()

    fun init() {
        useCase.getProfile().appSubscribe { user: User ->
            logo.set(user.logo?.let { Uri.parse(it) })
            fullName.set(user.fullName)
            nickname.set(user.username.takeIf { !it.isNullOrBlank() })
            phone.set(PhoneEmailUtils.getFormattedPhoneString(user.phone))
            birthDate.set(user.birthDate?.let { DateFormats.numericDateFormat.format(it) })
            location.set("${user.country}, ${user.city}")
            lang.set(user.language)
        }
    }

    fun onExitClick() {
        useCase.exit().appSubscribe {
            processor.onNext(UIAction(ACTION_EXIT))
        }
    }

    fun onEditProfileClick() {
        processor.onNext(UIAction(ACTION_EDIT_USER))
    }
}