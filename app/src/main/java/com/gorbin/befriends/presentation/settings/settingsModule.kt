package com.gorbin.befriends.presentation.settings

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val settingsModule = module {
    viewModel { SettingsViewModel(application = get(), useCase = get()) }
}