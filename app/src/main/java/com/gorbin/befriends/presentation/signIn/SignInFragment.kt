package com.gorbin.befriends.presentation.signIn

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.os.ConfigurationCompat
import com.gorbin.befriends.databinding.SignInFrBinding
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ChooseCountryScreen
import com.gorbin.befriends.presentation.cicerone.SighInPasswordScreen
import com.gorbin.befriends.utils.PhoneFormattedUtils
import com.gorbin.befriends.utils.PhoneNumberValidityChangeListener
import com.gorbin.befriends.utils.RegionChangeListener
import com.punicapp.core.utils.KeyBoardUtils
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class SignInFragment : AbstractBaseFragment<SignInViewModel>() {
    companion object {
        const val PHONE = "PHONE"
        const val SELECTION = "SELECTION"
    }

    private var countryCode: String = "+7"
    private var binding: SignInFrBinding? = null
    private val router: Router by inject()
    private lateinit var formatter: PhoneFormattedUtils
    var dataSelection: DataSelection<RegionInfo> = DataSelection()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SignInViewModel.ACTION_CHOSE_COUNTRY) {
            router.navigateTo(ChooseCountryScreen(formatter.codes, dataSelection))
        }.register<Unit>(SignInViewModel.ACTION_DONE) {
            KeyBoardUtils.hideKeyboard(activity)
            val data = formatter.getNormalizePhone()
            router.navigateTo(SighInPasswordScreen(data))
        }.register<Unit>(SignInViewModel.ACTION_BACK) {
            router.exit()
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SignInViewModel): View {
        if (binding != null) {
            return binding!!.root
        }
        binding = SignInFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel


        appViewModel.values[SignInViewModel.PHONE_FIELD] = ""
        formatter = PhoneFormattedUtils(context!!).apply {
            registerEditText(binding!!.phone, binding!!.countryCode)
            setPhoneNumberValidityChangeListener(object : PhoneNumberValidityChangeListener {
                override fun onValidityChanged(isValidNumber: Boolean) {
                    appViewModel.isPhoneValid = isValidNumber
                }
            })
            setRegionChangeListener(object : RegionChangeListener {
                override fun onText(string: String) {
                    appViewModel.onText(string)
                }

                override fun onRegion(region: RegionInfo) {
                    appViewModel.onRegion(region)
                }

                override fun onNotFoundRegion() {
                    appViewModel.onNotFoundRegion()
                }
            })
        }
        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
        findCode(locale.country.toLowerCase(Locale.getDefault()))

        dataSelection.listener = object : DataSelection.Listener<RegionInfo> {
            override fun onChange(data: RegionInfo) {
                countryCode = "+${data.phoneCode}"
                formatter.setPhoneCode(data.countryCode)
            }
        }

        return binding!!.root
    }

    private fun findCode(countryCode: String) {
        formatter.setPhoneCode(countryCode)
    }

    override fun onPause() {
        super.onPause()
        val v = activity!!.currentFocus
        if (v != null) {
            v.clearFocus()
            context?.let {
                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
            }
        }
        binding?.let {
            binding!!.phone.clearFocus()
        }
    }
}