package com.gorbin.befriends.presentation.signIn

import android.app.Application
import android.view.inputmethod.InputMethodManager
import androidx.databinding.*
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.punicapp.mvvm.actions.UIAction

class SignInViewModel(application: Application) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"
        const val ACTION_CHOSE_COUNTRY = "action_chose_country"
        const val PHONE_FIELD = "phone"
    }

    val error = ObservableField<String>("")
    val agreementAccepted = ObservableBoolean()
    val codeKeyboard = ObservableInt(InputMethodManager.SHOW_IMPLICIT)

    val isDoneEnable = ObservableField<Boolean>(true)
    var isPhoneValid: Boolean = false
    val values = ObservableArrayMap<String, String>().apply {

        addOnMapChangedCallback(object : ObservableMap.OnMapChangedCallback<ObservableArrayMap<String, String>, String, String>() {
            override fun onMapChanged(sender: ObservableArrayMap<String, String>, key: String) {
                error.set("")
            }
        })
    }

    fun choseCountry() {
        processor.onNext(UIAction(ACTION_CHOSE_COUNTRY))
    }

    fun onDoneClick() {
        fun str(id: Int) = context.getString(id)

        error.set("")
        if (!isPhoneValid) {
            error.set(str(R.string.empty_phone_error))
            return
        }
        if (!agreementAccepted.get()) {
            error.set(str(R.string.empty_checkbox_error))
            return
        }
        processor.onNext(UIAction(ACTION_DONE))
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun onAgreementClick() {
        agreementAccepted.set(!agreementAccepted.get())
    }

    fun onText(string: String) {
        if (string == "+") {
            error.set(context.getString(R.string.choose_country))
        }
    }

    fun onRegion(region: RegionInfo) {
        error.set("")
    }

    fun onNotFoundRegion() {
        error.set(context.getString(R.string.register_region_code_incorrect))
    }

    override fun doOnResume() {
        super.doOnResume()
        codeKeyboard.set(0)
        codeKeyboard.set(InputMethodManager.SHOW_IMPLICIT)
    }

    override fun doOnPause() {
        super.doOnPause()
        codeKeyboard.set(0)
    }
}