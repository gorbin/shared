package com.gorbin.befriends.presentation.signIn

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val signInModule = module {
    viewModel { SignInViewModel(application = get()) }
}