package com.gorbin.befriends.presentation.signUp

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.ConfigurationCompat
import com.gorbin.befriends.databinding.SignUpFrBinding
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.domain.entities.SignUpTokens
import com.gorbin.befriends.presentation.base.DataSelection
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.ChooseCountryScreen
import com.gorbin.befriends.presentation.cicerone.SmsCodeScreen
import com.gorbin.befriends.utils.PhoneFormattedUtils
import com.gorbin.befriends.utils.PhoneNumberValidityChangeListener
import com.gorbin.befriends.utils.RegionChangeListener
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class SignUpFragment : AbstractBaseFragment<SignUpViewModel>() {
    private lateinit var formatter: PhoneFormattedUtils
    private var dataSelection: DataSelection<RegionInfo> = DataSelection()
    private val router: Router by inject()
    private var countryCode: String = "+7"
    private var binding: SignUpFrBinding? = null

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SignUpViewModel.ACTION_BACK) {
            router.exit()
        }.register<Unit>(SignUpViewModel.ACTION_COUNTRY_CODE_SELECTION) {
            router.navigateTo(ChooseCountryScreen(formatter.codes, dataSelection))
        }.register<Pair<String, SignUpTokens>>(SignUpViewModel.ACTION_DONE) {
            router.navigateTo(SmsCodeScreen(it))
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SignUpViewModel): View {
        if (binding != null) {
            return binding!!.root
        }

        binding = SignUpFrBinding.inflate(inflater, container, false)
        binding!!.viewModel = appViewModel
        formatter = PhoneFormattedUtils(requireContext()).apply {
            registerEditText(binding!!.phone, binding!!.countryCode)
            setPhoneNumberValidityChangeListener(object : PhoneNumberValidityChangeListener {
                override fun onValidityChanged(isValidNumber: Boolean) {
                    appViewModel.isPhoneValid = isValidNumber
                }
            })
            setRegionChangeListener(object : RegionChangeListener {
                override fun onText(string: String) {
                    appViewModel.onText(string)
                }

                override fun onRegion(region: RegionInfo) {
                    appViewModel.onRegion(region)
                }

                override fun onNotFoundRegion() {
                    appViewModel.onNotFoundRegion()
                }
            })
        }

        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
        findCode(locale.country.toLowerCase(Locale.getDefault()))

        dataSelection.listener = object : DataSelection.Listener<RegionInfo> {
            override fun onChange(data: RegionInfo) {
                countryCode = "+${data.phoneCode}"
                formatter.setPhoneCode(data.countryCode)
            }
        }
        appViewModel.init(formatter)

        return binding!!.root
    }

    private fun findCode(countryCode: String) {
        formatter.setPhoneCode(countryCode)
    }
}