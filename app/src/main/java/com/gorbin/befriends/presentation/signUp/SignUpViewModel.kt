package com.gorbin.befriends.presentation.signUp

import android.app.Application
import android.widget.Toast
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.gorbin.befriends.R
import com.gorbin.befriends.data.api.model.ErrorList
import com.gorbin.befriends.domain.entities.RegionInfo
import com.gorbin.befriends.domain.entities.RegisterRequestData
import com.gorbin.befriends.domain.usecases.IRegisterUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.presentation.base.viewmodel.IErrorHandler
import com.gorbin.befriends.utils.PhoneFormattedUtils
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import retrofit2.HttpException

class SignUpViewModel(application: Application, private val useCase: IRegisterUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"
        const val ACTION_COUNTRY_CODE_SELECTION = "action_country_code_selection"

        const val FIRST_NAME_ERROR = "FIRST_NAME_ERROR"
        const val LAST_NAME_ERROR = "LAST_NAME_ERROR"
        const val PASSWORD_ERROR = "BIRTH_DATE_ERROR"
        const val PHONE_ERROR = "PHONE_ERROR"

        private const val SHORT_PASSWORD_ERROR_CODE = 1002L
        private const val WRONG_LOGIN_ERROR_CODE = 1011L
        private const val LOGIN_ERROR_CODE = 1008L
        private const val DB_ERROR_CODE = 1010L
    }

    val errors = ObservableArrayMap<String, String>()
    val rulesAccepted = ObservableBoolean().onChanged {
        showRulesError.set(false)}
    val privacyPolicyAccepted = ObservableBoolean().onChanged {
        showPrivacyError.set(false)}
    val nickname = ObservableField<String>()
    val useNickname = ObservableBoolean()
    val firstName = ObservableField<String>().onChanged { errors[FIRST_NAME_ERROR] = null }
    val lastName = ObservableField<String>().onChanged { errors[LAST_NAME_ERROR] = null }
    val password = ObservableField<String>().onChanged { errors[PASSWORD_ERROR] = null }
    var isPhoneValid: Boolean = false

    private lateinit var formatter: PhoneFormattedUtils
    private var keepData: Boolean = false
    val showRulesError = ObservableBoolean(false)
    val showPrivacyError = ObservableBoolean(false)

    fun init(formatter: PhoneFormattedUtils) {
        errors[FIRST_NAME_ERROR] = null
        errors[LAST_NAME_ERROR] = null
        errors[PASSWORD_ERROR] = null
        errors[PHONE_ERROR] = null
        this.formatter = formatter
    }

    override fun doOnResume() {
        super.doOnResume()
        if (keepData) {
            keepData = false
        } else {
            wipeData()
        }
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    fun selectCountry() {
        keepData = true
        processor.onNext(UIAction(ACTION_COUNTRY_CODE_SELECTION))
    }

    fun onRulesClick() {
        showRulesError.set(false)
        rulesAccepted.set(!rulesAccepted.get())
    }

    fun onPrivacyPolicyClick() {
        showPrivacyError.set(false)
        privacyPolicyAccepted.set(!privacyPolicyAccepted.get())
    }

    fun onDoneClick() {
        val name = firstName.get()?.trim()
        val lastName = lastName.get()?.trim()
        val pass = password.get()?.trim()

        fun str(id: Int) = context.getString(id)

        errors.clear()

        if (name.isNullOrEmpty())
            errors[FIRST_NAME_ERROR] = str(R.string.empty_error)
        if (lastName.isNullOrEmpty())
            errors[LAST_NAME_ERROR] = str(R.string.empty_error)
        if (pass.isNullOrEmpty())
            errors[PASSWORD_ERROR] = str(R.string.empty_error)
        if (!isPhoneValid)
            errors[PHONE_ERROR] = str(R.string.empty_phone_error)
        if (!rulesAccepted.get()){
            showRulesError.set(true)
            return
        }
        if (!privacyPolicyAccepted.get()) {
            showPrivacyError.set(true)
            return
        }

        if (errors.size == 0) {
            val login = formatter.getNormalizePhone()
            useCase.register(RegisterRequestData(login, pass!!, name!!, lastName!!, nickname.get(), useNickname.get()))
                    .appSubscribe(
                            errorHandler = getRegisterErrorHandler()
                    ) {
                        processor.onNext(UIAction(ACTION_DONE, login to it))
                    }
        }
    }

    fun onText(string: String) {
        if (string == "+") {
            errors[PHONE_ERROR] = context.getString(R.string.choose_country)
        }
    }

    fun onRegion(region: RegionInfo) {
        errors.remove(PHONE_ERROR)
    }

    fun onNotFoundRegion() {
        errors[PHONE_ERROR] = context.getString(R.string.register_region_code_incorrect)
    }

    private fun wipeData() {
        errors.clear()
        rulesAccepted.set(false)
        privacyPolicyAccepted.set(false)
        isPhoneValid = false
        nickname.set(null)
        useNickname.set(false)
        firstName.set(null)
        lastName.set(null)
        password.set(null)
        formatter.reset()
    }

    private fun getRegisterErrorHandler(): IErrorHandler = object : IErrorHandler {
        override fun handleError(error: HttpException): Boolean {
            return when (error.code()) {
                409 -> {
                    val body = error.response().errorBody()?.source()?.buffer()?.readUtf8() ?: return false
                    val errorList = Gson().fromJson(body, ErrorList::class.java)
                    var errorProcessed = false
                    errorList.errorList.forEach {
                        when (it.code) {
                            SHORT_PASSWORD_ERROR_CODE -> {
                                errors[PASSWORD_ERROR] = context.getString(R.string.incorrect_password_error_message)
                                errorProcessed = true
                            }
                            WRONG_LOGIN_ERROR_CODE, LOGIN_ERROR_CODE -> {
                                errors[PHONE_ERROR] = context.getString(R.string.user_exists_error_message)
                                errorProcessed = true
                            }
                            DB_ERROR_CODE -> {
                                Toast.makeText(context, context.getText(R.string.db_error_message), Toast.LENGTH_LONG).show()
                                errorProcessed = true
                            }
                            else -> handleError(Throwable(it.message))
                        }
                    }
                    errorProcessed
                }
                else -> false
            }
        }
    }
}