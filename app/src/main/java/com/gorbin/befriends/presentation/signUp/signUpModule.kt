package com.gorbin.befriends.presentation.signUp

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val signUpModule = module {
    viewModel { SignUpViewModel(application = get(), useCase = get()) }
}