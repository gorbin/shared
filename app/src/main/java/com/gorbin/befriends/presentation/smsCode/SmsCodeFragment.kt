package com.gorbin.befriends.presentation.smsCode

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.SmsCodeFrBinding
import com.gorbin.befriends.domain.entities.SignUpTokens
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.PhoneCallErrorScreen
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.punicapp.mvvm.actions.UIActionConsumer
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SmsCodeFragment : AbstractBaseFragment<SmsCodeViewModel>() {
    companion object {
        const val SIGN_UP_TOKENS = "sign_up_tokens"
    }

    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SmsCodeViewModel.ACTION_DONE) {
            router.newRootScreen(PersonalInfoScreen(true))
        }.register<Unit>(SmsCodeViewModel.ACTION_BACK) {
            router.exit()
        }.register<Unit>(SmsCodeViewModel.PHONE_CALL_ERROR) {
            router.navigateTo(PhoneCallErrorScreen)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: SmsCodeViewModel): View {
        val binding = SmsCodeFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel

        val data = arguments?.getTypedSerializable<SignUpTokens>(SIGN_UP_TOKENS)
        val phone = arguments?.getString(GlobalValues.PHONE)
        if (phone == null || data?.otpInfo?.attemptId == null) {
            router.exit()
        }
        appViewModel.init(phone!!, data!!)

        return binding.root
    }
}