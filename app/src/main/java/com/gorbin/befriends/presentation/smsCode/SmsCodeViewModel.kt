package com.gorbin.befriends.presentation.smsCode

import android.app.Application
import android.os.CountDownTimer
import android.view.inputmethod.InputMethodManager
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.CodePhoneData
import com.gorbin.befriends.domain.entities.PhoneData
import com.gorbin.befriends.domain.entities.SignUpTokens
import com.gorbin.befriends.domain.usecases.ILoginUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.onChanged
import com.punicapp.mvvm.actions.UIAction
import retrofit2.HttpException

class SmsCodeViewModel(application: Application, private val useCase: ILoginUseCase) : BaseAppViewModel(application) {
    companion object {
        const val ACTION_DONE = "action_done"
        const val ACTION_BACK = "action_back"

        const val COUNT = 4
        const val MIN_RESEND_TIME_MILLIS = 30_000
        const val COUNT_DOWN_TIMER_STEP = 1000
        const val COUNT_DOWN_TIMER_DIVIDER = 1000
        const val PHONE_CALL_ERROR = "PHONE_CALL_ERROR"

        const val ONE = "one"
        const val TWO = "two"
        const val THREE = "three"
        const val FOUR = "four"
    }

    var code = ObservableField("").onChanged { handleCode(it) }
    var error = ObservableField<String>()
    val codeKeyboard = ObservableInt(InputMethodManager.SHOW_IMPLICIT)
    val resendCodeText = ObservableField("")
    val resendCodeEnabled = ObservableBoolean(false)
    val values = ObservableArrayMap<String, String>()

    private var timerTime = MIN_RESEND_TIME_MILLIS
    private var timer: CountDownTimer? = null
    private var isRunning = false
    private var attempt: String? = null

    lateinit var phone: String

    fun init(phone: String, data: SignUpTokens) {
        this.phone = phone
        this.attempt = data.otpInfo?.attemptId
    }

    fun onBackClick() {
        processor.onNext(UIAction(ACTION_BACK))
    }

    private fun requestCode() {
        useCase.requestOtp(PhoneData(phone))
                .appSubscribe {
                    if (it.otpInfo?.attemptId != null) {
                        attempt = it.otpInfo.attemptId
                    } else {
                        processor.onNext(UIAction(PHONE_CALL_ERROR))
                    }
                }
    }

    fun handleResendCodeButtonText(count: Long): String {
        return if (count > 0) {
            context.getString(R.string.resend_code_in, count)
        } else {
            context.getString(R.string.resend_code)
        }
    }

    fun onResendClick() {
        startTimer()
        requestCode()
    }

    private fun checkCode() {
        val attemptId = attempt
        if (attemptId == null) {
            processor.onNext(UIAction(PHONE_CALL_ERROR))
            return
        }
        code.get()?.let {
            useCase.checkOtp(CodePhoneData(it, attemptId))
                    .appSubscribe { sessionData ->
                        processor.onNext(UIAction(ACTION_DONE))
                        code.set("")
                    }
        }
    }

    fun show() {
        codeKeyboard.set(0)
        codeKeyboard.set(InputMethodManager.SHOW_IMPLICIT)
    }

    private fun startTimer() {
        if (timer == null) {
            timer = object : CountDownTimer(timerTime.toLong(), COUNT_DOWN_TIMER_STEP.toLong()) {
                override fun onTick(millisUntilFinished: Long) {
                    timerTime = millisUntilFinished.toInt()
                    resendCodeText.set(handleResendCodeButtonText(millisUntilFinished / COUNT_DOWN_TIMER_DIVIDER))
                }

                override fun onFinish() {
                    isRunning = false
                    resendCodeText.set(handleResendCodeButtonText(0))
                    resendCodeEnabled.set(true)
                    timerTime = MIN_RESEND_TIME_MILLIS
                }
            }
        }
        if (!isRunning) {
            isRunning = true
            resendCodeEnabled.set(false)
            timer?.start()
        }
    }

    override fun doOnStart() {
        startTimer()
    }

    override fun handleError(error: Throwable) {
        if (error is HttpException) {
            if (error.code() == 500) {
                processor.onNext(UIAction(PHONE_CALL_ERROR))
            } else {
                this.error.set(context.getString(R.string.error_code))
            }
        } else {
            this.error.set(context.getString(R.string.error_code))
        }
    }

    private fun handleCode(value: String?) {
        error.set(null)

        values[ONE] = value?.getOrNull(0)?.toString()
        values[TWO] = value?.getOrNull(1)?.toString()
        values[THREE] = value?.getOrNull(2)?.toString()
        values[FOUR] = value?.getOrNull(3)?.toString()

        if (value?.length == COUNT)
            checkCode()
    }


}