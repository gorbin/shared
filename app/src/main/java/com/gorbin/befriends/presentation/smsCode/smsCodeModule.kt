package com.gorbin.befriends.presentation.smsCode

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val smsCodeModule = module {
    viewModel { SmsCodeViewModel(application = get(), useCase = get()) }
}