package com.gorbin.befriends.presentation.splash

import android.Manifest
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.R
import com.gorbin.befriends.databinding.SplashFrBinding
import com.gorbin.befriends.domain.entities.LocationCheckResults
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.PersonalInfoScreen
import com.gorbin.befriends.presentation.cicerone.ProfileScreen
import com.gorbin.befriends.presentation.cicerone.WelcomeScreen
import com.gorbin.befriends.utils.FusedLocationTracker
import com.punicapp.mvvm.actions.UIActionConsumer
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SplashFragment : AbstractBaseFragment<SplashViewModel>() {
    private val router: Router by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(SplashViewModel.ACTION_TO_PROFILE) {
            router.newRootScreen(ProfileScreen)
        }.register<Unit>(SplashViewModel.ACTION_TO_REGISTER) {
            router.newRootScreen(PersonalInfoScreen(true))
        }.register<Unit>(SplashViewModel.ACTION_TO_WELCOME) {
            router.newRootScreen(WelcomeScreen)
        }.register<Unit>(SplashViewModel.ACTION_GET_LOCATION) {
            getLocation()
        }
    }

    override fun initBinding(
            inflater: LayoutInflater,
            container: ViewGroup?,
            appViewModel: SplashViewModel
    ): View {
        val binding = SplashFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        appViewModel.sync()

        return binding.root
    }

    private fun getLocation(): Disposable? {
        val act = activity ?: return null
        val fusedLocationTracker = FusedLocationTracker(act)
        return RxPermissions(act)
                .request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .any { it }
                .flatMap { granted ->
                    if (granted) {
                        fusedLocationTracker.isLocationAvailable().map {
                            if (it) LocationCheckResults.AVAILABLE else LocationCheckResults.NOT_AVAILABLE
                        }
                    } else {
                        Single.just(LocationCheckResults.PERMISSION_NOT_GRANTED)
                    }
                }
                .subscribe { result, _ ->
                    when (result) {
                        LocationCheckResults.NOT_AVAILABLE -> {
//                            showNoLocationPermissionDialog()
                            appViewModel.checkWhereToGo()
                        }
                        else -> {
                            appViewModel.checkWhereToGo()
                        }
                    }
                }
    }

    private fun showNoLocationPermissionDialog() {
        DialogProvider.getMessageDialog(
                requireContext(),
                R.string.attention,
                R.string.no_location_permission_message,
                appViewModel
        ).show()
    }
}