package com.gorbin.befriends.presentation.splash

import android.app.Application
import com.gorbin.befriends.domain.entities.SessionData
import com.gorbin.befriends.domain.usecases.ISplashUseCase
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SplashViewModel(application: Application, private val useCase: ISplashUseCase, private val localRepo: IKeyValueRepository<String>) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_TO_PROFILE = "action_to_profile"
        const val ACTION_TO_REGISTER = "action_to_register"
        const val ACTION_TO_WELCOME = "action_to_welcome"
        const val ACTION_GET_LOCATION = "action_get_location"
    }

    fun sync(): Any {
        //TODO: stub implementation
        return Single.just(true)
                .delay(1000L, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    processor.onNext(UIAction(ACTION_GET_LOCATION))
                }, {
                    it.printStackTrace()
                })
    }

    fun checkWhereToGo() {
        val session = localRepo.instantGet<SessionData>(GlobalValues.SESSION_DATA, SessionData::class.java).orNull()
        if (session != null) {
            useCase.isRegistered().appSubscribe(isShowProgress = false) {
                if (it) {
                    processor.onNext(UIAction(ACTION_TO_PROFILE))
                } else {
                    processor.onNext(UIAction(ACTION_TO_REGISTER))
                }
            }
        } else {
            processor.onNext(UIAction(ACTION_TO_WELCOME))
        }
    }

    override fun handleError(error: Throwable) {
        processor.onNext(UIAction(ACTION_TO_WELCOME))
    }

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction?) {
        super.onActionFromChild(action)
        checkWhereToGo()
    }

}