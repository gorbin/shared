package com.gorbin.befriends.presentation.splash

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val splashModule = module {
    viewModel { SplashViewModel(application = get(), useCase = get(), localRepo = get()) }
}