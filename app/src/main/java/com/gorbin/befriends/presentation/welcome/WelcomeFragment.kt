package com.gorbin.befriends.presentation.welcome

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gorbin.befriends.databinding.WelcomeFrBinding
import com.gorbin.befriends.domain.localization.LocaleManager
import com.gorbin.befriends.presentation.base.dialog.DialogProvider
import com.gorbin.befriends.presentation.base.fragment.AbstractBaseFragment
import com.gorbin.befriends.presentation.cicerone.SearchFormScreen
import com.gorbin.befriends.presentation.cicerone.SignInScreen
import com.gorbin.befriends.presentation.cicerone.SignUpScreen
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.safeLet
import com.punicapp.mvvm.actions.UIActionConsumer
import com.punicapp.rxrepocore.IKeyValueRepository
import com.punicapp.rxrepocore.IRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class WelcomeFragment : AbstractBaseFragment<WelcomeViewModel>() {
    private val router: Router by inject()
    private val localesRepo: IRepository<Locale> by inject()
    private val localRepo: IKeyValueRepository<String> by inject()
    private val localeManager: LocaleManager by inject()

    override fun fillHandlers(consumer: UIActionConsumer) {
        super.fillHandlers(consumer)
        consumer.register<Unit>(WelcomeViewModel.ACTION_SIGN_IN) {
            localRepo.instantRemove(GlobalValues.SESSION_DATA)
            router.navigateTo(SignInScreen)
        }.register<Unit>(WelcomeViewModel.ACTION_SIGN_UP) {
            localRepo.instantRemove(GlobalValues.SESSION_DATA)
            router.navigateTo(SignUpScreen)
        }.register<Unit>(WelcomeViewModel.ACTION_LANGUAGE_SELECTION) {
            showLangSelectionDialog()
        }.register<String>(WelcomeViewModel.ACTION_RESTART_WITH_NEW_LANGUAGE) {
            localeManager.saveLanguage(it)
        }.register<Unit>(WelcomeViewModel.ACTION_SEARCH) {
            router.navigateTo(SearchFormScreen)
        }
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?, appViewModel: WelcomeViewModel): View {
        val binding = WelcomeFrBinding.inflate(inflater, container, false)
        binding.viewModel = appViewModel
        appViewModel.selectedLang.set(localeManager.findLanguage())

        return binding.root
    }

    private fun showLangSelectionDialog(): Disposable {
        return localesRepo.fetch(null, null)
                .map {
                    it.or(emptyList())
                }.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { localeList, _ ->
                    localeList.takeIf { it.isNotEmpty() }?.let { supportedLanguages ->
                        DialogProvider.getLangSelectionDialog(requireContext(), appViewModel,
                                supportedLanguages, appViewModel.selectedLang.get())
                                .show()
                    }
                }
    }
}