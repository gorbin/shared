package com.gorbin.befriends.presentation.welcome

import android.app.Application
import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.viewpager.widget.PagerAdapter
import com.gorbin.befriends.presentation.base.adapters.WelcomeOnboardingPagerAdapter
import com.gorbin.befriends.presentation.base.viewmodel.BaseAppViewModel
import com.gorbin.befriends.utils.GlobalValues
import com.gorbin.befriends.utils.getTypedSerializable
import com.gorbin.befriends.utils.onChanged
import com.gorbin.befriends.utils.toDisplayLanguage
import com.punicapp.mvvm.actions.UIAction
import com.punicapp.mvvm.android.AppViewModel
import com.punicapp.mvvm.android.IParentVMProvider
import java.util.*

class WelcomeViewModel(application: Application) : BaseAppViewModel(application), IParentVMProvider {
    companion object {
        const val ACTION_SIGN_IN = "action_sign_in"
        const val ACTION_SEARCH = "action_search"
        const val ACTION_SIGN_UP = "action_sign_up"
        const val ACTION_LANGUAGE_SELECTION = "action_language_selection"
        const val ACTION_RESTART_WITH_NEW_LANGUAGE = "action_restart_with_new_language"
    }

    val language = ObservableField<String>()
    val selectedLang = ObservableField<Locale>().onChanged {
        language.set(it?.displayLanguage?.toDisplayLanguage())
        it?.toLanguageTag()?.let {
            processor.onNext(UIAction(ACTION_RESTART_WITH_NEW_LANGUAGE, it))
        }
    }

    fun signUp() {
        processor.onNext(UIAction(ACTION_SIGN_UP))
    }

    fun signIn() {
        processor.onNext(UIAction(ACTION_SIGN_IN))
    }

    fun search() {
        processor.onNext(UIAction(ACTION_SEARCH))
    }

    fun selectLanguage() {
        processor.onNext(UIAction(ACTION_LANGUAGE_SELECTION))
    }

    fun getViewPagerAdapter(): PagerAdapter = WelcomeOnboardingPagerAdapter()

    override fun provideVm(): AppViewModel {
        return this
    }

    override fun onActionFromChild(action: UIAction?) {
        if (action?.actionId == GlobalValues.LOCALE_SELECTED) {
            val locale = (action.data as Bundle).getTypedSerializable<Locale>(GlobalValues.LOCALE)
            selectedLang.set(locale)
        }
        super.onActionFromChild(action)
    }
}