package com.gorbin.befriends.presentation.welcome

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val welcomeModule = module {
    viewModel { WelcomeViewModel(application = get()) }
}