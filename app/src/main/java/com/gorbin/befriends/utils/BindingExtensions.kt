package com.gorbin.befriends.utils

import androidx.databinding.*

fun <T> ObservableField<T>.onChanged(f: ObservableField<T>.(newVal: T?) -> Unit): ObservableField<T> {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            f(get())
        }
    })
    return this
}

fun ObservableInt.onChanged(f: ObservableInt.(newVal: Int) -> Unit): ObservableInt {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            f(get())
        }
    })
    return this
}

fun ObservableDouble.onChanged(f: ObservableDouble.(newVal: Double) -> Unit): ObservableDouble {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            f(get())
        }
    })
    return this
}

fun ObservableBoolean.onChanged(f: ObservableBoolean.(newVal: Boolean) -> Unit): ObservableBoolean {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            f(get())
        }
    })
    return this
}