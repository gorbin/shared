package com.gorbin.befriends.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.RequestStatus
import com.gorbin.befriends.domain.entities.Role
import com.gorbin.befriends.domain.entities.chat.CommandConfig

class ChatCommandHelper {
    companion object {
        const val WITHDRAW_REQUEST = "WITHDRAW_REQUEST"

        const val ACCEPT_REQUEST = "ACCEPT_REQUEST"
        const val REJECT_REQUEST = "REJECT_REQUEST"
        const val NEW_REQUEST = "NEW_REQUEST"

        const val CONFIRM_EXECUTION = "CONFIRM_EXECUTION"
        const val REJECT_EXECUTION = "REJECT_EXECUTION"
        const val REQUEST_EXECUTION_CONFIRMATION = "REQUEST_EXECUTION_CONFIRMATION"
    }

    fun getAction(status: RequestStatus, role: Role): String? {
        return when (status) {
            RequestStatus.PREPARE -> {
                when (role) {
                    Role.CUSTOMER -> WITHDRAW_REQUEST
                    Role.EXECUTOR -> null
                }
            }
            RequestStatus.NEW -> {
                when (role) {
                    Role.CUSTOMER -> WITHDRAW_REQUEST
                    Role.EXECUTOR -> NEW_REQUEST
                }
            }
            RequestStatus.IN_PROGRESS -> {
                when (role) {
                    Role.CUSTOMER -> null
                    Role.EXECUTOR -> REQUEST_EXECUTION_CONFIRMATION
                }
            }
            RequestStatus.WAITING -> {
                when (role) {
                    Role.CUSTOMER -> CONFIRM_EXECUTION
                    Role.EXECUTOR -> null
                }
            }
            else -> null
        }
    }

    fun getCommandConfig(
            context: Context,
            status: RequestStatus,
            role: Role
    ): CommandConfig {
        var commandAvailable = false
        var commandAction: String? = null
        var commandBackground: Drawable? = null

        when (status) {
            RequestStatus.PREPARE -> {
                when (role) {
                    Role.CUSTOMER -> {
                        commandAction = context.getString(R.string.withdraw_request)
                        commandBackground = ContextCompat.getDrawable(context, R.drawable.bg_button_red)
                        commandAvailable = true
                    }
                    Role.EXECUTOR -> {
                        //Nothing for executor
                    }
                }
            }
            RequestStatus.NEW -> {
                commandAvailable = true
                when (role) {
                    Role.CUSTOMER -> {
                        commandAction = context.getString(R.string.withdraw_request)
                        commandBackground = ContextCompat.getDrawable(context, R.drawable.bg_button_red)
                    }
                    Role.EXECUTOR -> {
                        commandAction = context.getString(R.string.new_request)
                        commandBackground = ContextCompat.getDrawable(context, R.drawable.bg_button)
                    }
                }
            }
            RequestStatus.MODERATION -> {

            }
            RequestStatus.REJECTED_EMPLOYER -> {

            }
            RequestStatus.REJECTED_EXECUTOR -> {

            }
            RequestStatus.CLOSED -> {

            }
            RequestStatus.IN_PROGRESS -> {
                when (role) {
                    Role.CUSTOMER -> {

                    }
                    Role.EXECUTOR -> {
                        commandAvailable = true
                        commandAction = context.getString(R.string.request_confirmation)
                        commandBackground = ContextCompat.getDrawable(context, R.drawable.bg_button)
                    }
                }
            }
            RequestStatus.CONFLICT -> {

            }
            RequestStatus.CONFLICT_BEF -> {

            }
            RequestStatus.WAITING -> {
                when (role) {
                    Role.CUSTOMER -> {
                        commandAvailable = true
                        commandAction = context.getString(R.string.confirm_execution)
                        commandBackground = ContextCompat.getDrawable(context, R.drawable.bg_button)
                    }
                    Role.EXECUTOR -> {

                    }
                }
            }
        }

        return CommandConfig(
                commandAvailable = commandAvailable,
                commandAction = commandAction,
                commandButtonBg = commandBackground
        )
    }
}