package com.gorbin.befriends.utils

import android.content.Context
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.text.util.Linkify
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputLayout
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.Language
import com.gorbin.befriends.domain.entities.SkillSpecialization
import com.gorbin.befriends.presentation.base.adapters.ImagePagerAdapter
import com.punicapp.core.utils.DimensUtils
import com.punicapp.mvvm.adapters.VmAdapter
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.messages.MessageInput
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.util.*

@BindingAdapter(value = ["layoutManager"], requireAll = false)
fun setLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager?) {
    view.layoutManager = layoutManager
}

@BindingAdapter(value = ["spanSizeLookup"], requireAll = false)
fun setSpanSizeLookup(view: RecyclerView, spanSizeLookup: GridLayoutManager.SpanSizeLookup?) {
    val layoutManager = view.layoutManager
    if (layoutManager is GridLayoutManager && spanSizeLookup != null)
        layoutManager.spanSizeLookup = spanSizeLookup
}

@BindingAdapter(value = ["recyclerAdapter"], requireAll = false)
fun setRecyclerAdapter(view: RecyclerView, adapter: VmAdapter?) {
    view.adapter = adapter
}

@BindingAdapter(value = ["clipToOutline"], requireAll = true)
fun setClipping(view: View, clip: Boolean) {
    view.clipToOutline = clip
}

@BindingAdapter(value = ["tabSelectedListener", "removeOtherTabSelectionListeners"], requireAll = false)
fun setTabSelectedListener(view: TabLayout, listener: TabLayout.OnTabSelectedListener?, remove: Boolean?) {
    if (remove == true)
        view.clearOnTabSelectedListeners()
    listener?.let { view.addOnTabSelectedListener(it) }
}

@BindingAdapter("textWatcher")
fun setTextWatcher(view: EditText, watcher: TextWatcher) {
    view.addTextChangedListener(watcher)
}

@BindingAdapter(value = ["bottomSheetState"], requireAll = false)
fun setBottomSheetState(view: View, state: Int?) {
    val behavior = BottomSheetBehavior.from(view)
    state?.let { behavior.state = it }
}

interface OnLongClickListener {
    fun onLongClick()
}

@BindingAdapter(value = ["onLongClick"], requireAll = false)
fun onLongClick(view: View, listener: OnLongClickListener) {
    view.setOnLongClickListener {
        listener.onLongClick()
        true
    }
}

@BindingAdapter(value = ["inputMask"], requireAll = false)
fun setInputMask(view: TextView, mask: String) {
    val maskImpl = MaskImpl.createTerminated(UnderscoreDigitSlotsParser().parseSlots(mask))
    MaskFormatWatcher(maskImpl).installOn(view)
}

@BindingAdapter(value = ["pager_adapter", "pager_indicator", "pager_images"], requireAll = false)
fun setViewPagerAdapter(view: ViewPager, adapter: PagerAdapter?, indicator: TabLayout?, images: List<String>?) {
    adapter?.let { view.adapter = it }
    indicator?.setupWithViewPager(view)
    images?.takeIf { it.isNotEmpty() }?.let {
        val pagerAdapter = ImagePagerAdapter(view.context, emptyList())
        pagerAdapter.images = it
        view.adapter = pagerAdapter
    }
}

interface OnLocaleSelectionListener {
    fun onLocaleSelected(locale: Locale)
}

@BindingAdapter(value = ["locales", "selectedLocale", "localeListener"], requireAll = false)
fun setLocales(view: RadioGroup, locales: List<Locale>?, selectedLocale: Locale?, onLocaleSelectionListener: OnLocaleSelectionListener?) {
    view.removeAllViews()
    val inflater = LayoutInflater.from(view.context)
    locales?.forEach { locale ->
        val option = inflater.inflate(R.layout.radio_button, view, false) as RadioButton
        val language = locale.getDisplayLanguage(locale).toDisplayLanguage()
        option.text = language
        option.isChecked = locale == selectedLocale
        option.setOnClickListener {
            onLocaleSelectionListener?.onLocaleSelected(locale)
        }

        view.addView(option)
    }
}

@BindingAdapter(value = ["languages", "selectedLanguages", "languageSelectionListener"], requireAll = true)
fun setLanguages(view: LinearLayout, languages: List<Language>, selectedLanguages: List<Language>, listener: (Pair<Language, Boolean>) -> Unit) {
    view.removeAllViews()
    val inflater = LayoutInflater.from(view.context)
    languages.forEach { language ->
        val option = inflater.inflate(R.layout.check_box_button, view, false) as CheckBox
        option.text = language.name
        option.isChecked = selectedLanguages.contains(language)
        option.setOnCheckedChangeListener { _, isChecked ->
            listener.invoke(language to isChecked)
        }

        view.addView(option)
    }
}

@BindingAdapter(value = ["htmlText", "removeLinksUnderline"], requireAll = false)
fun setHtmlText(textView: TextView, htmlText: String, removeLinksUnderline: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        textView.text = Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY)
    } else {
        textView.text = Html.fromHtml(htmlText)
    }
    if (removeLinksUnderline) {
        Linkify.addLinks(textView, Linkify.ALL)
        val s = SpannableString(textView.text)
        val spans = s.getSpans(0, s.length, URLSpan::class.java)
        for (span: URLSpan in spans) {
            val start = s.getSpanStart(span)
            val end = s.getSpanEnd(span)
            s.removeSpan(span)
            val newSpan = URLSpanNoUnderline(span.url)
            s.setSpan(newSpan, start, end, 0)
        }
        textView.text = s
    }
    textView.movementMethod = LinkMovementMethod.getInstance()
}

@BindingAdapter(
        value = ["picasso_uri", "picasso_error", "picasso_placeholder", "picasso_circle_shape", "picasso_rounded"],
        requireAll = false
)
fun setImagePicasso(
        image: ImageView,
        uri: Uri?, @DrawableRes error: Int?, @DrawableRes placeholder: Int?,
        circle: Boolean = false, rounded: Boolean = false) {
    if (uri == null) {
        image.setImageDrawable(if(placeholder!=null) {ContextCompat.getDrawable(image.context, placeholder)} else {null})
        return
    }
    Picasso
            .get()
            .load(uri)
            .apply {
                if (circle) transform(CircleTransform())
                if (rounded) transform(RoundedCornersTransformation(DimensUtils.dpToPx(8), 0))
                if (error != null) error(error)
            }
            .into(image)
}

@BindingAdapter(value = ["track", "trackerId", "trackError"], requireAll = true)
fun setTracking(view: View, track: String, trackerId: Int?, trackError: String?) {
    view.setBackgroundResource(
            when {
                trackError != null -> R.drawable.track_error
                trackerId == null -> R.drawable.track_regular
                trackerId == (track.length + 1) -> R.drawable.track_highlight
                else -> R.drawable.track_regular
            }
    )
}

@BindingAdapter(value = ["hintLayout", "hintExpanded", "hintCollapsed"], requireAll = true)
fun setChangingHint(view: EditText, hintLayout: TextInputLayout, hintExpanded: String, hintCollapsed: String) {
    view.setOnFocusChangeListener { _, hasFocus ->
        if (hasFocus) {
            hintLayout.hint = hintCollapsed
        } else {
            if (view.text.isNullOrEmpty())
                hintLayout.hint = hintExpanded
            else
                hintLayout.hint = hintCollapsed
        }
    }
}

@BindingAdapter(value = ["underline"], requireAll = false)
fun bindUnderline(view: EditText, underlineView: View) {
    view.setOnFocusChangeListener { _, hasFocus ->
        if (hasFocus) {
            underlineView.setBackgroundResource(R.drawable.bg_edit_text_activated)
        } else {
            underlineView.setBackgroundResource(R.drawable.bg_edit_text_normal)
        }
    }
}

@BindingAdapter(value = ["viewRequestFocus"], requireAll = false)
fun setViewFocus(view: EditText, flags: Int) {
    if (view.requestFocus())
        view.context?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, flags)
            view.setSelection(view.text.length)
        }
}

@BindingAdapter(value = ["emptyHintHack"], requireAll = true)
fun emptyHintHack(view: EditText, emptyHintHack: Boolean) {
    view.hint = ""
}

@BindingAdapter(value = ["error", "error_view"], requireAll = true)
fun setError(view: TextInputLayout, error: String?, error_view: TextView) {
    view.error = if (error != null) " " else null
    error_view.text = error
}

@BindingAdapter(value = ["disableScrolling"], requireAll = true)
fun disableScrolling(view: EditText, disable: Boolean) {
    if (disable)
        view.keyListener = null
}

interface PageUpdateListener {
    fun onPage(itemCount: Int)
}

@BindingAdapter(value = ["page_listener", "removeOtherScrollListeners"], requireAll = false)
fun setPageListener(view: RecyclerView, listener: PageUpdateListener?, remove: Boolean? = false) {
    val layoutManager = view.layoutManager as LinearLayoutManager
    val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val position = layoutManager.findLastVisibleItemPosition()
            view.adapter?.let {
                val updatePosition = it.itemCount - 3
                if (position >= updatePosition) {
                    listener?.onPage(it.itemCount)
                }
            }
        }
    }
    if (remove == true)
        view.clearOnScrollListeners()
    view.addOnScrollListener(scrollListener)
}

@BindingAdapter(value = ["seekBarListener"], requireAll = false)
fun setSeekBarListener(view: SeekBar, listener: SeekBar.OnSeekBarChangeListener) {
    view.setOnSeekBarChangeListener(listener)
}

@BindingAdapter(value = ["width_margin"], requireAll = false)
fun setCustomWidth(view: ViewGroup, margin: Int = 0) {
    val wm = view.context.getSystemService(Context.WINDOW_SERVICE) as? WindowManager
    val metrics = DisplayMetrics()
    wm?.defaultDisplay?.getMetrics(metrics)
    val width = metrics.widthPixels - 2 * DimensUtils.dpToPx(margin)
    view.layoutParams = view.layoutParams.apply {
        this.width = width
    }
    view.visibility = View.VISIBLE
}

@BindingAdapter(value = ["spinner_listener"], requireAll = false)
fun setSpinnerListener(view: Spinner, listener: AdapterView.OnItemSelectedListener?) {
    listener?.let {
        view.onItemSelectedListener = it
    }
}

@BindingAdapter(value = ["spinner_adapter", "spinner_selected_item"], requireAll = false)
fun spinnerAdapter(view: Spinner, adapter: ArrayAdapter<*>?, selectedItemPosition: Int?) {
    adapter?.let {
        view.adapter = it

        selectedItemPosition?.let {
            view.setSelection(it)
        }
    }
}

@BindingAdapter(value = ["withSnapHelper"], requireAll = false)
fun setPagerBehaviour(view: RecyclerView, withPager: Boolean) {
    if (withPager)
        PagerSnapHelper().apply { attachToRecyclerView(view) }
}

@BindingAdapter(value = ["tabs"], requireAll = false)
fun setTabs(view: TabLayout, tabs: List<SkillSpecialization>) {
    view.removeAllTabs()
    val inflater = LayoutInflater.from(view.context)
    tabs.forEachIndexed { i, it ->
        val tab = view.newTab()
        tab.tag = it
        val tabView = inflater.inflate(R.layout.spec_tab, view, false) as TextView
        tabView.setText(it.shortTitleRes)
        tab.text = tabView.text
        tab.customView = tabView
        view.addTab(tab)
        val tabContainer = (view.getChildAt(0) as ViewGroup).getChildAt(i)
        (tabContainer.layoutParams as ViewGroup.MarginLayoutParams).setMargins(DimensUtils.dpToPx(6), 0, DimensUtils.dpToPx(6), 0)
        tabContainer.setPadding(0, 0, 0, 0)
        tabContainer.requestLayout()
    }
}

@BindingAdapter(value = ["messageInput_input_listener", "messageInput_attachments_listener", "messageInput_typing_listener", "messageInput_translate_listener"], requireAll = false)
fun setMessageInputListeners(view: MessageInput, iListener: MessageInput.InputListener?, aListener: MessageInput.AttachmentsListener?, typingListener: MessageInput.TypingListener?, tListener: MessageInput.TranslateListener?) {
    view.setInputListener {
        iListener?.onSubmit(it) ?: false
    }

    view.setAttachmentsListener {
        aListener?.onAddAttachments()
    }

    view.setTypingListener(typingListener)

    view.setTranslateListener {
        tListener?.onTranslate()
    }
}

@BindingAdapter("messageInput_typeface")
fun setInputTypeface(view: MessageInput, @FontRes typeface: Int) {
    view.setTypeface(ResourcesCompat.getFont(view.context, typeface))
}

interface IRadioOptionListener {
    fun onOptionSelected(reason: String)
}

@BindingAdapter(value = ["radioGroup", "onOptionSelected"], requireAll = false)
fun setOptionSelectedListener(view: Button, radioGroup: RadioGroup?, listener: IRadioOptionListener?) {
    view.setOnClickListener {
        val option = radioGroup?.checkedRadioButtonId?.let {
            radioGroup.findViewById<RadioButton>(it)
        }?.text?.toString() ?: return@setOnClickListener

        listener?.onOptionSelected(option)
    }
}

@BindingAdapter(value = ["radioSelectionListener"], requireAll = false)
fun setRadioGroupListener(view: RadioGroup, listener: RadioGroup.OnCheckedChangeListener?) {
    listener?.let { view.setOnCheckedChangeListener(it) }
}
