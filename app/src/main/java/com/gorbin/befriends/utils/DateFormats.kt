package com.gorbin.befriends.utils

import java.text.SimpleDateFormat
import java.util.*

object DateFormats {
    val numericDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
}