package com.gorbin.befriends.utils

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.lifecycle.Lifecycle
import com.google.android.gms.maps.model.LatLng
import com.google.gson.reflect.TypeToken
import com.gorbin.befriends.R
import com.punicapp.rxrepolocal.LocalRepository
import io.reactivex.Single
import java.io.File
import java.io.Serializable
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList

private object ExtensionsData {
    const val GEO_ACTION_VIEW = "geo:0,0?q=%s,%s(%s)"
}

// Local repo extensions
inline fun <reified T> getType(): Type = object : TypeToken<T>() {}.type!!

inline fun <reified T> LocalRepository.get(key: String) = get<T>(key, getType<T>())

inline fun <reified T> LocalRepository.get(key: String, default: T): Single<T> = get<T>(key)
        .map { if (it.isPresent) it.get() else default }

inline fun <reified T> LocalRepository.instantGet(key: String): T? = instantGet<T>(key, getType<T>()).orNull()

inline fun <reified T> LocalRepository.instantGet(key: String, default: T): T = instantGet<T>(key, getType<T>())
        .let { if (it.isPresent) it.get() else default }

inline fun <reified T> Bundle.getTypedSerializable(key: String): T? {
    val data = this.getSerializable(key)
    return if (data is T)
        data
    else
        null
}

// other
fun <T> List<T>.asSerializable(): Serializable = (this as? Serializable) ?: ArrayList<T>(this)

fun countTime(block: () -> Unit): Long {
    val timeStart = System.currentTimeMillis()
    block()
    return System.currentTimeMillis() - timeStart
}

fun printTime(prefix: String = "", block: () -> Unit) = countTime(block).let { Log.d("Exception", "$prefix $it"); }

fun Pair<Double, Double>.getUri(geoName: String): Uri {
    return Uri.parse(String.format(ExtensionsData.GEO_ACTION_VIEW, first, second, geoName))
}

fun String.toDisplayLanguage(): String {
    return this.substring(0, 1).toUpperCase(Locale.getDefault()) + this.substring(1)
}

fun Double.toDisplayRating(): String {
    return String.format("%.1f", this)
}

fun onShare(context: Context) {
    val i = Intent(Intent.ACTION_SEND)
    i.type = "text/plain"
    i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_title))
    val appPackageName = context.packageName
    i.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=$appPackageName")
    context.startActivity(Intent.createChooser(i, context.getString(R.string.share)))
}

fun Lifecycle.isAppInForeground(): Boolean {
    return this.currentState.isAtLeast(Lifecycle.State.STARTED)
}

fun Location.toLatLng() = LatLng(latitude, longitude)

fun File.getMimeType(): String? {
    return MimeTypeMap
            .getFileExtensionFromUrl(this.path)
            ?.let {
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(it)
            }
}
