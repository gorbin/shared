package com.gorbin.befriends.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class FusedLocationTracker(private val context: Context) : LocationCallback() {
    companion object {
        private const val UPDATE_INTERVAL = 10 * 1000L
        private const val FASTEST_INTERVAL = 2000L
    }

    private val locationSubject = PublishSubject.create<Location>()

    fun isLocationAvailable(): Single<Boolean> {
        return Single.create { emitter ->
            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            emitter.onSuccess(gpsEnabled || networkEnabled)
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocationLatLngObservable(accuracy: Int = LocationRequest.PRIORITY_HIGH_ACCURACY): Observable<Location> {
        val client = getFusedLocationProviderClient(context)!!
        return Observable.fromCallable {
            val mLocationRequest = LocationRequest()
                    .setPriority(accuracy)
                    .setInterval(UPDATE_INTERVAL)
                    .setFastestInterval(FASTEST_INTERVAL)

            val locationSettingsRequest = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
                    .build()

            val settingsClient = LocationServices.getSettingsClient(context)
            settingsClient.checkLocationSettings(locationSettingsRequest)

            client.requestLocationUpdates(mLocationRequest, this, Looper.getMainLooper())
            client.lastLocation.addOnCompleteListener {
                if (!locationSubject.hasObservers() || locationSubject.hasComplete() || locationSubject.hasThrowable())
                    return@addOnCompleteListener

                if (it.isSuccessful
                        && it.result != null
                        && checkValidLocation(it.result!!)) locationSubject.onNext(it.result!!)
                else locationSubject.onError(RuntimeException("Could not identify your location"))
            }
            Unit
        }.flatMap {
            locationSubject
        }.doFinally {
            client.removeLocationUpdates(this)
        }
    }

    override fun onLocationResult(locationResult: LocationResult) {
        locationResult.lastLocation?.let {
            if (checkValidLocation(it)) {
                locationSubject.onNext(it)
            }
        }
    }

    private fun checkValidLocation(location: Location): Boolean = location.latitude != 0.0 && location.longitude != 0.0

    fun buildLocationRequest(): Single<Location> {
        return getLocationLatLngObservable(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .firstOrError()
                .compose { upstream ->
                    upstream
                            .onErrorResumeNext {
                                upstream.retry(4)
                            }
                }
    }
}