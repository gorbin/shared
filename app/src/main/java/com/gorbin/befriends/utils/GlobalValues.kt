package com.gorbin.befriends.utils

object GlobalValues {
    const val REMEMBER_TOKEN = "REMEMBER_TOKEN"
    const val SERVICE_RULES = "SERVICE_RULES"
    const val GO_TO_PROFILE = "GO_TO_PROFILE"
    const val PIN_DATA = "PIN_DATA"
    const val SESSION_DATA = "SESSION_DATA"
    const val SELECTED_LANGUAGES = "SELECTED_LANGUAGES"
    const val PHONE = "PHONE"
    const val GALLERY_SOURCE = 100_100
    const val CAMERA_SOURCE = 100_101
    const val SET_AS_PROFILE_PHOTO = 100_200
    const val REMOVE_PHOTO = 100_201
    const val LOCALE = "LOCALE"
    const val LOCALE_SELECTED = "LOCALE_SELECTED"
    const val LANGUAGE_KEY = "LANGUAGE_KEY"
    const val REVIEW_OK = "REVIEW_OK"
    const val DEVICE_UUID = "DEVICE_UUID"

    const val APPLY_FILTER_AND_SORT = "APPLY_FILTER_AND_SORT"
    const val SORT_BY = "SORT_BY"
    const val SORT_ORDER = "SORT_ORDER"
    const val SPECIALIZATION = "SPECIALIZATION"

    const val SHOW_CHAT_PUSH_NOTIFICATIONS = "SHOW_CHAT_PUSH_NOTIFICATIONS"
    const val CHAT_PAGE_SIZE = 50

    const val PENDING_CHAT_ROOM = "PENDING_CHAT_ROOM"
    const val FLOW = "FLOW"
    const val TRANSLATE = "TRANSLATE"
    const val APPLY_SEARCH_SORTING = "APPLY_SEARCH_SORTING"
    const val HIDE_NEWBIES = "HIDE_NEWBIES"

    const val CONFIRM_ACTION = "CONFIRM_ACTION"
    const val GALLERY_SIZE = 9
}