package com.gorbin.befriends.utils


class MaskedFormatter {
    companion object {
        const val PHONE_MASK_FORMATTED = "+7 (###) ###-##-##"
        const val PLUS_KEY = '+'
        const val DASH_KEY = '-'
        const val LEFT_BRACKET_KEY = '('
        const val RIGHT_BRACKET_KEY = ')'
        private const val DIGIT_KEY = '#'
        private const val LITERAL_KEY = '\''
        private const val UPPERCASE_KEY = 'U'
        private const val LOWERCASE_KEY = 'L'
        private const val ALPHA_NUMERIC_KEY = 'A'
        private const val CHARACTER_KEY = '?'
        private const val ANYTHING_KEY = '*'
        private const val HEX_KEY = 'H'
    }

    /** The user specified mask.  */
    private var mask: String? = null

    /** Indicates if the value contains the literal characters.  */
    private var containsLiteralChars: Boolean = false

    private val emptyMaskChars = arrayOfNulls<MaskCharacter>(0)

    /** List of valid characters.  */
    private var validCharacters: String? = null

    /** List of invalid characters.  */
    private var invalidCharacters: String? = null

    /** String used to represent characters not present.  */
    private var placeholder: Char = ' '

    /** String used for the passed in value if it does not completely
     * fill the mask.  */
    private var placeholderString: String? = null

    @Transient
    private var maskChars: Array<MaskCharacter?>? = null


    /** Indicates if the value being edited must match the mask.  */
    private var allowsInvalid: Boolean = false

    /**
     * Creates a MaskFormatter with no mask.
     */
    constructor() {
        setAllowsInvalid(false)
        containsLiteralChars = true
        maskChars = emptyMaskChars
        placeholder = ' '
    }

    /**
     * Creates a MaskFormatter with the specified mask.
     * A ParseException
     * will be thrown if mask is an invalid mask.
     *
     * @throws ParseException if mask does not contain valid mask characters
     */
    //@Throws(ParseException::class)
    constructor(mask: String) : this() {
        setMask(mask)
    }

    /**
     * Sets the mask dictating the legal characters.
     * This will throw a ParseException if mask is
     * not valid.
     *
     * @throws ParseException if mask does not contain valid mask characters
     */
    fun setMask(mask: String) {
        this.mask = mask
        updateInternalMask()
    }

    /**
     * Returns the formatting mask.
     *
     * @return Mask dictating legal character values.
     */
    fun getMask(): String? {
        return mask
    }

    /**
     * Updates the internal representation of the mask.
     */
    private fun updateInternalMask() {
        val mask = getMask()
        val fixed = mutableListOf<MaskCharacter>()

        if (mask != null) {
            var counter = 0
            val maxCounter = mask.length
            while (counter < maxCounter) {
                var maskChar = mask[counter]

                when (maskChar) {
                    DIGIT_KEY -> fixed.add(DigitMaskCharacter())
                    LITERAL_KEY -> if (++counter < maxCounter) {
                        maskChar = mask[counter]
                        fixed.add(LiteralCharacter(maskChar))
                    }
                    UPPERCASE_KEY -> fixed.add(UpperCaseCharacter())
                    LOWERCASE_KEY -> fixed.add(LowerCaseCharacter())
                    ALPHA_NUMERIC_KEY -> fixed.add(AlphaNumericCharacter())
                    CHARACTER_KEY -> fixed.add(CharCharacter())
                    ANYTHING_KEY -> fixed.add(MaskCharacter())
                    HEX_KEY -> fixed.add(HexCharacter())
                    else -> fixed.add(LiteralCharacter(maskChar))
                }
                counter++
            }
        }
        maskChars = if (fixed.size == 0) {
            emptyMaskChars
        } else {
            fixed.toTypedArray()
        }
    }


    /**
     * Sets whether or not the value being edited is allowed to be invalid
     * for a length of time (that is, stringToValue throws
     * a ParseException).
     * It is often convenient to allow the user to temporarily input an
     * invalid value.
     *
     * @param allowsInvalid Used to indicate if the edited value must always
     * be valid
     */
    fun setAllowsInvalid(allowsInvalid: Boolean) {
        this.allowsInvalid = allowsInvalid
    }


    /**
     * Allows for further restricting of the characters that can be input.
     * Only characters specified in the mask, not in the
     * invalidCharacters, and in
     * validCharacters will be allowed to be input. Passing
     * in null (the default) implies the valid characters are only bound
     * by the mask and the invalid characters.
     *
     * @param validCharacters If non-null, specifies legal characters.
     */
    fun setValidCharacters(validCharacters: String) {
        this.validCharacters = validCharacters
    }

    /**
     * Returns the valid characters that can be input.
     *
     * @return Legal characters
     */
    fun getValidCharacters(): String? {
        return validCharacters
    }

    /**
     * Allows for further restricting of the characters that can be input.
     * Only characters specified in the mask, not in the
     * invalidCharacters, and in
     * validCharacters will be allowed to be input. Passing
     * in null (the default) implies the valid characters are only bound
     * by the mask and the valid characters.
     *
     * @param invalidCharacters If non-null, specifies illegal characters.
     */
    fun setInvalidCharacters(invalidCharacters: String) {
        this.invalidCharacters = invalidCharacters
    }

    /**
     * Returns the characters that are not valid for input.
     *
     * @return illegal characters.
     */
    fun getInvalidCharacters(): String? {
        return invalidCharacters
    }

    /**
     * If true, the returned value and set value will also contain the literal
     * characters in mask.
     *
     * For example, if the mask is '(###) ###-####', the
     * current value is '(415) 555-1212', and
     * valueContainsLiteralCharacters is
     * true stringToValue will return
     * '(415) 555-1212'. On the other hand, if
     * valueContainsLiteralCharacters is false,
     * stringToValue will return '4155551212'.
     *
     * @param containsLiteralChars Used to indicate if literal characters in
     * mask should be returned in stringToValue
     */
    fun setValueContainsLiteralCharacters(containsLiteralChars: Boolean) {
        this.containsLiteralChars = containsLiteralChars
    }

    /**
     * Returns true if stringToValue should return literal
     * characters in the mask.
     *
     * @return True if literal characters in mask should be returned in
     * stringToValue
     */
    fun getValueContainsLiteralCharacters(): Boolean {
        return containsLiteralChars
    }

    /**
     * Sets the character to use in place of characters that are not present
     * in the value, ie the user must fill them in. The default value is
     * a space.
     *
     * This is only applicable if the placeholder string has not been
     * specified, or does not completely fill in the mask.
     *
     * @param placeholder Character used when formatting if the value does not
     * completely fill the mask
     */
    fun setPlaceholderCharacter(placeholder: Char) {
        this.placeholder = placeholder
    }

    /**
     * Returns the character to use in place of characters that are not present
     * in the value, ie the user must fill them in.
     *
     * @return Character used when formatting if the value does not
     * completely fill the mask
     */
    fun getPlaceholderCharacter(): Char {
        return placeholder
    }

    /**
     * Sets the string to use if the value does not completely fill in
     * the mask. A null value implies the placeholder char should be used.
     *
     * @param placeholder String used when formatting if the value does not
     * completely fill the mask
     */
    fun setPlaceholder(placeholder: String) {
        this.placeholderString = placeholder
    }

    /**
     * Returns the String to use if the value does not completely fill
     * in the mask.
     *
     * @return String used when formatting if the value does not
     * completely fill the mask
     */
    fun getPlaceholder(): String? {
        return placeholderString
    }

    /**
     * Returns a String representation of the Object value
     * based on the mask.  Refer to
     * [.setValueContainsLiteralCharacters] for details
     * on how literals are treated.
     *
     * @throws ParseException if there is an error in the conversion
     * @param value Value to convert
     * @see .setValueContainsLiteralCharacters
     *
     * @return String representation of value
     */
    fun valueToString(value: Any?): String {
        val result = StringBuilder()
        val placeholder = getPlaceholder()
        val valueCounter = intArrayOf(0)

        safeLet(maskChars, value) { maskChars, value ->
            append(result, value.toString(), valueCounter, placeholder, maskChars)
        }
        return result.toString()
    }

    /**
     * Invokes append on the mask characters in
     * mask.
     */
    private fun append(result: StringBuilder, value: String, index: IntArray,
                       placeholder: String?, mask: Array<MaskCharacter?>) {
        var counter = 0
        val maxCounter = mask.size
        while (counter < maxCounter) {
            mask[counter]?.append(result, value, index, placeholder)
            counter++
        }
    }

    private open inner class MaskCharacter {
        /**
         * Subclasses should override this returning true if the instance
         * represents a literal character. The default implementation
         * returns false.
         */
        open val isLiteral: Boolean
            get() = false

        /**
         * Returns true if `aChar` is a valid reprensentation of
         * the receiver. The default implementation returns true if the
         * receiver represents a literal character and `getChar`
         * == aChar. Otherwise, this will return true is `aChar`
         * is contained in the valid characters and not contained
         * in the invalid characters.
         */
        open fun isValidCharacter(aChar: Char): Boolean {
            var aChar = aChar
            if (isLiteral) {
                return getChar(aChar) == aChar
            }

            aChar = getChar(aChar)

            var filter: String? = getValidCharacters()

            if (filter != null && filter.indexOf(aChar) == -1) {
                return false
            }
            filter = getInvalidCharacters()
            return !(filter != null && filter.indexOf(aChar) != -1)
        }

        /**
         * Returns the character to addAttribute for `aChar`. The
         * default implementation returns `aChar`. Subclasses
         * that wish to do some sort of mapping, perhaps lower case to upper
         * case should override this and do the necessary mapping.
         */
        open fun getChar(aChar: Char): Char {
            return aChar
        }

        /**
         * Appends the necessary character in `formatting` at
         * `index` to `buff`.
         */
        open fun append(buff: StringBuilder, formatting: String, index: IntArray,
                        placeholder: String?) {
            val inString = index[0] < formatting.length
            val aChar = if (inString) formatting[index[0]] else '0'

            if (isLiteral) {
                buff.append(getChar(aChar))
                if (getValueContainsLiteralCharacters()) {
                    if (inString && aChar != getChar(aChar)) {
                        throw Throwable("Invalid character: $aChar, index: ${index[0]}")
                    }
                    index[0] = index[0] + 1
                }
            } else if (index[0] >= formatting.length) {
                if (placeholder != null && index[0] < placeholder.length) {
                    buff.append(placeholder[index[0]])
                } else {
                    buff.append(getPlaceholderCharacter())
                }
                index[0] = index[0] + 1
            } else if (isValidCharacter(aChar)) {
                buff.append(getChar(aChar))
                index[0] = index[0] + 1
            } else {
                throw Throwable("Invalid character: $aChar, index: ${index[0]}")
            }
        }
    }

    /**
     * Used to represent a fixed character in the mask.
     */
    private inner class LiteralCharacter(private val fixedChar: Char) : MaskCharacter() {
        override val isLiteral: Boolean
            get() = true

        override fun getChar(aChar: Char): Char {
            return fixedChar
        }
    }


    /**
     * Represents a number, uses `Character.isDigit`.
     */
    private inner class DigitMaskCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return Character.isDigit(aChar) && super.isValidCharacter(aChar)
        }
    }


    /**
     * Represents a character, lower case letters are mapped to upper case
     * using `Character.toUpperCase`.
     */
    private inner class UpperCaseCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return Character.isLetter(aChar) && super.isValidCharacter(aChar)
        }

        override fun getChar(aChar: Char): Char {
            return Character.toUpperCase(aChar)
        }
    }


    /**
     * Represents a character, upper case letters are mapped to lower case
     * using `Character.toLowerCase`.
     */
    private inner class LowerCaseCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return Character.isLetter(aChar) && super.isValidCharacter(aChar)
        }

        override fun getChar(aChar: Char): Char {
            return Character.toLowerCase(aChar)
        }
    }


    /**
     * Represents either a character or digit, uses
     * `Character.isLetterOrDigit`.
     */
    private inner class AlphaNumericCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return Character.isLetterOrDigit(aChar) && super.isValidCharacter(aChar)
        }
    }


    /**
     * Represents a letter, uses `Character.isLetter`.
     */
    private inner class CharCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return Character.isLetter(aChar) && super.isValidCharacter(aChar)
        }
    }


    /**
     * Represents a hex character, 0-9a-fA-F. a-f is mapped to A-F
     */
    private inner class HexCharacter : MaskCharacter() {
        override fun isValidCharacter(aChar: Char): Boolean {
            return (aChar == '0' || aChar == '1' ||
                    aChar == '2' || aChar == '3' ||
                    aChar == '4' || aChar == '5' ||
                    aChar == '6' || aChar == '7' ||
                    aChar == '8' || aChar == '9' ||
                    aChar == 'a' || aChar == 'A' ||
                    aChar == 'b' || aChar == 'B' ||
                    aChar == 'c' || aChar == 'C' ||
                    aChar == 'd' || aChar == 'D' ||
                    aChar == 'e' || aChar == 'E' ||
                    aChar == 'f' || aChar == 'F') && super.isValidCharacter(aChar)
        }

        override fun getChar(aChar: Char): Char {
            return if (Character.isDigit(aChar)) {
                aChar
            } else Character.toUpperCase(aChar)
        }
    }
}