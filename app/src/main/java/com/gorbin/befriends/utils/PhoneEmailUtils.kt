package com.gorbin.befriends.utils

import java.text.ParseException
import java.util.regex.Pattern

object PhoneEmailUtils {
     const val SEVEN = "7"
     const val EIGHT = "8"
     const val PHONE_LENGTH = 11
     const val MIN_PHONE_LENGTH = 12
     const val PHONE_PREFIX_LENGTH = 4
     const val PHONE_POSTFIX_LENGTH = 3
     private const val ASTERISK = '*'
     private const val FORMATTER_PLACEHOLDER = 'f'
     const val MASK = MaskedFormatter.PHONE_MASK_FORMATTED

     private var formatter: MaskedFormatter? = null

     fun isPhoneNumberString(s: String?): Boolean {
         var s = s
         if (s != null) {
             s = s.replaceFirst("\\+7".toRegex(),
                 SEVEN
             )
             return s.matches("[78] ?\\(?[0-9]*\\)? ?[0-9]*-?[0-9]*-?[0-9]*".toRegex())
         } else {
             return false
         }
     }

     fun getPhoneString(s: String?): String {
         return if (s != null && !s.isEmpty()) {
             "+" + clearString(
                 s.replaceFirst(
                     "\\+7".toRegex(),
                     SEVEN
                 )
             )
         } else {
             ""
         }
     }

     fun isPhoneEmailValid(s: String?): Boolean {
         if (s == null || s.length == 0) {
             return false
         }

         if (isPhoneNumberString(s)) {
             val digits = clearString(s)
             return digits.length == PHONE_LENGTH
         } else
             return isEmailValid(s)
     }

     fun isEmailValid(email: String): Boolean {
         val expression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,64})$"
         val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
         val matcher = pattern.matcher(email)
         return matcher.matches()
     }

     fun isPhoneValid(s: String?): Boolean {
         return s == null || s.length < MIN_PHONE_LENGTH
     }

     fun clearString(s: String): String {
         return s.replace("[\\D]".toRegex(), "")
     }

     fun getPhoneExtraString(phone: String): String {
         var phone = phone
         phone = getPhoneString(phone)
         return phone
     }

    @JvmStatic
     fun getFormattedPhoneString(phone: String?): String {
         return if (phone != null) {
             getFormattedPhoneString(
                 phone,
                 MASK
             )
         } else ""
     }

     fun getFormattedPhoneString(phone: String, mask: String): String {
        var phone = phone
        var formattedPhone: String

        try {
            formatter =
                MaskedFormatter(mask)
            formatter!!.setValueContainsLiteralCharacters(false)
            formatter!!.setPlaceholderCharacter(FORMATTER_PLACEHOLDER)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        if (phone.length == PHONE_LENGTH) {
            if (phone.startsWith(SEVEN) || phone.startsWith(
                    EIGHT
                )) {
                phone = phone.substring(1, phone.length)
            }
        }

        formattedPhone = phone.replaceFirst("\\+7".toRegex(), "").replace("[^\\w.@]".toRegex(), "").trim { it <= ' ' }
        try {
            formattedPhone = formatter!!.valueToString(formattedPhone)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        try {
            formattedPhone = formattedPhone.substring(0, formattedPhone.indexOf(FORMATTER_PLACEHOLDER))
            if (formattedPhone[formattedPhone.length - 1] == mask[formattedPhone.length - 1]) {
                formattedPhone = formattedPhone.substring(0, formattedPhone.length - 1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return formattedPhone
    }

    fun getLockedPhoneString(phone: String): String {
        val formattedString = StringBuilder(
            getPhoneString(
                phone
            )
        )
        for (i in PHONE_PREFIX_LENGTH until formattedString.length - PHONE_POSTFIX_LENGTH) {
            formattedString.setCharAt(i, ASTERISK)
        }
        return formattedString.toString()
    }
}