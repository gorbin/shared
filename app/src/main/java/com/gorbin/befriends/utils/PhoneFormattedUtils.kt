package com.gorbin.befriends.utils

import android.content.Context
import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.ConfigurationCompat
import com.gorbin.befriends.R
import com.gorbin.befriends.domain.entities.RegionInfo
import io.michaelrocks.libphonenumber.android.NumberParseException
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import java.util.*


interface PhoneNumberValidityChangeListener {
    fun onValidityChanged(isValidNumber: Boolean)
}

interface RegionChangeListener {
    fun onText(string: String)
    fun onRegion(region: RegionInfo)
    fun onNotFoundRegion()
}

class PhoneFormattedUtils(val context: Context) {
    private var editText: EditText? = null
    private var regionEditText: TextView? = null
    private var formattingTextWatcher: InternationalPhoneTextWatcher? = null
    private var phoneNumberValidityChangeListener: PhoneNumberValidityChangeListener? = null
    private var regionChangeListener: RegionChangeListener? = null
    private var validityTextWatcher: TextWatcher? = null
    private var regionTextWatcher: TextWatcher? = null
    private val phoneUtil: PhoneNumberUtil = PhoneNumberUtil.createInstance(context)
    private var isUserUpdate = false
    var country: RegionInfo? = null
    var codes: MutableList<RegionInfo> = mutableListOf()
    var countries: Map<String, List<RegionInfo>>

    init {
        countries = regions()
        countries.forEach { (_, country) ->
            codes.addAll(country)
        }
        codes.forEach {
            it.countryName = Locale("", it.countryCode).displayCountry
        }
        codes = codes.sortedWith(compareBy { it.countryName }).toMutableList()

        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
        setPhoneCode(locale.country.toLowerCase(Locale.getDefault()))
    }

    fun reset() {
        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)
        setPhoneCode(locale.country.toLowerCase(Locale.getDefault()))

        editText?.setText(R.string.empty)
    }

    fun registerEditText(phone: EditText, region: TextView) {
        this.editText = phone
        this.regionEditText = region

        phone.setOnKeyListener { _, _, _ -> false }

        updateFormattingRegionTextWatcher()
        updateValidityTextWatcher()
        updateFormattingTextWatcher()
    }

    fun setPhoneNumberValidityChangeListener(phoneNumberValidityChangeListener: PhoneNumberValidityChangeListener) {
        this.phoneNumberValidityChangeListener = phoneNumberValidityChangeListener
    }

    fun setRegionChangeListener(regionChangeListener: RegionChangeListener) {
        this.regionChangeListener = regionChangeListener
    }

    private fun updateFormattingTextWatcher() {
        if (country == null || editText == null) return

        formattingTextWatcher?.let {
            editText?.removeTextChangedListener(it)
        }

        formattingTextWatcher = InternationalPhoneTextWatcher(context, country!!.countryCode.toUpperCase(), country!!.phoneCode.toInt(), true)
        editText?.addTextChangedListener(formattingTextWatcher)

        val digitsValue = PhoneNumberUtil.normalizeDigitsOnly(editText?.text)
        editText?.setText("")
        editText?.setText(digitsValue)
        editText?.setSelection(editText!!.text.length)
    }

    fun clearMask() {
        formattingTextWatcher?.let {
            editText?.removeTextChangedListener(it)
        }
        formattingTextWatcher = null
        val digitsValue = PhoneNumberUtil.normalizeDigitsOnly(editText?.text)
        editText?.setText("")
        editText?.setText(digitsValue)
    }

    fun setPhoneCode(regionInfo: RegionInfo) {
        country = regionInfo
        isUserUpdate = true
        updateRegion()
        editText?.setText("")
    }

    fun setPhoneCode(countryCode: String) {
        codes.find { it.countryCode == countryCode }?.let {
            setPhoneCode(it)
        }
    }

    private fun updatePhone(regionInfo: RegionInfo) {
        if (!isUserUpdate) {
            country = regionInfo
        }
        isUserUpdate = false
        updateValidityTextWatcher()
        updateFormattingTextWatcher()
    }

    private fun updateValidityTextWatcher() {
        validityTextWatcher?.let {
            editText?.removeTextChangedListener(it)
        }

        validityTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                phoneNumberValidityChangeListener?.onValidityChanged(isPhoneValid())
            }
        }

        editText?.addTextChangedListener(validityTextWatcher)
    }

    fun isPhoneValid(): Boolean {
        if (editText?.text?.isEmpty() == true || regionEditText?.text?.isEmpty() == true) return false
        return try {
            return safeLet(editText?.text, regionEditText?.text, country?.countryCode) { phone, region, code ->
                val phoneNumber = phoneUtil.parse("$region$phone", code)
                phoneUtil.isValidNumber(phoneNumber)
            } ?: false
        } catch (e: NumberParseException) {
            false
        }
    }

    private fun updateRegion() {
        val code = "${country?.countryCode?.toUpperCase(Locale.getDefault())} +${country?.phoneCode}"
        regionEditText?.text = code
    }

    private fun updateFormattingRegionTextWatcher() {
        regionTextWatcher?.let {
            regionEditText?.removeTextChangedListener(regionTextWatcher)
        }

        regionTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (country == null || !s.contains(" ") || s.length > s.indexOf(" ") + 1) {
                    return
                }
                countries[s.substring(s.indexOf(" ") + 1)]?.let {
                    updatePhone(it[0])
                    country?.let {
                        regionChangeListener?.onRegion(it)
                    }
                    editText?.requestFocus()
                    Unit
                } ?: run {
                    regionChangeListener?.onNotFoundRegion()
                    clearMask()
                }
                regionChangeListener?.onText(s.toString())
            }
        }

        regionEditText?.addTextChangedListener(regionTextWatcher)
    }

    fun getNormalizePhone(): String {
        return safeLet(editText?.text, regionEditText?.text) { phone, code ->
            "+${PhoneNumberUtil.normalizeDigitsOnly("$code$phone")}"
        } ?: ""
    }

    fun getPhone(number: String, region: String): String {
        val phone = PhoneNumberUtil.normalizeDigitsOnly(number)
        return phoneUtil.parse(phone, region.toUpperCase()).nationalNumber.toString()
    }

    fun regions() = mapOf(
            "+1" to listOf(
                    RegionInfo("1", "us", "United States"),
                    RegionInfo("1", "mp", "Northern Mariana Islands"),
                    RegionInfo("1", "ms", "Montserrat"),
                    RegionInfo("1", "ky", "Cayman Islands"),
                    RegionInfo("1", "lc", "Saint Lucia"),
                    RegionInfo("1", "kn", "Saint Kitts and Nevis"),
                    RegionInfo("1", "tt", "Trinidad & Tobago"),
                    RegionInfo("1", "pr", "Puerto Rico"),
                    RegionInfo("1", "tc", "Turks and Caicos Islands"),
                    RegionInfo("1", "vc", "Saint Vincent &amp; The Grenadines"),
                    RegionInfo("1", "vg", "British Virgin Islands"),
                    RegionInfo("1", "vi", "US Virgin Islands"),
                    RegionInfo("1", "sx", "Grenada"),
                    RegionInfo("1", "gd", "Grenada"),
                    RegionInfo("1", "gu", "Guam"),
                    RegionInfo("1", "jm", "Jamaica"),
                    RegionInfo("1", "ag", "Antigua and Barbuda"),
                    RegionInfo("1", "ai", "Anguilla"),
                    RegionInfo("1", "as", "American Samoa"),
                    RegionInfo("1", "bb", "Barbados"),
                    RegionInfo("1", "bm", "Bermuda"),
                    RegionInfo("1", "bs", "Bahamas"),
                    RegionInfo("1", "ca", "Canada"),
                    RegionInfo("1", "dm", "Dominica"),
                    RegionInfo("1", "do", "Dominican Republic")
            ),
            "+7" to listOf(
                    RegionInfo("7", "ru", "Russian Federation"),
                    RegionInfo("7", "kz", "Kazakhstan")
            ),
            "+20" to listOf(RegionInfo("20", "eg", "Egypt")),
            "+27" to listOf(RegionInfo("27", "za", "South Africa")),
            "+30" to listOf(RegionInfo("30", "gr", "Greece")),
            "+31" to listOf(RegionInfo("31", "nl", "Netherlands")),
            "+32" to listOf(RegionInfo("32", "be", "Belgium")),
            "+33" to listOf(RegionInfo("33", "fr", "France")),
            "+34" to listOf(RegionInfo("34", "es", "Spain")),
            "+36" to listOf(RegionInfo("36", "hu", "Hungary")),
            "+39" to listOf(RegionInfo("39", "it", "Italy")),
            "+40" to listOf(RegionInfo("40", "ro", "Romania")),
            "+41" to listOf(RegionInfo("41", "ch", "Switzerland")),
            "+43" to listOf(RegionInfo("43", "at", "Austria")),
            "+44" to listOf(
                    RegionInfo("44", "gb", "United Kingdom"),
                    RegionInfo("44", "im", "Isle Of Man"),
                    RegionInfo("44", "je", "Jersey")
            ),
            "+45" to listOf(RegionInfo("45", "dk", "Denmark")),
            "+46" to listOf(RegionInfo("46", "se", "Sweden")),
            "+47" to listOf(RegionInfo("47", "no", "Norway")),
            "+48" to listOf(RegionInfo("48", "pl", "Norway")),
            "+49" to listOf(RegionInfo("49", "de", "Germany")),
            "+51" to listOf(RegionInfo("51", "pe", "Peru")),
            "+52" to listOf(RegionInfo("52", "mx", "Mexico")),
            "+53" to listOf(RegionInfo("53", "cu", "Cuba")),
            "+54" to listOf(RegionInfo("54", "ar", "Argentina")),
            "+55" to listOf(RegionInfo("55", "br", "Brazil")),
            "+56" to listOf(RegionInfo("56", "cl", "Chile")),
            "+57" to listOf(RegionInfo("57", "co", "Colombia")),
            "+58" to listOf(RegionInfo("58", "ve", "Venezuela, Bolivarian Republic Of")),
            "+60" to listOf(RegionInfo("60", "my", "Malaysia")),
            "+61" to listOf(
                    RegionInfo("61", "au", "Australia"),
                    RegionInfo("61", "cc", "Cocos (keeling) Islands"),
                    RegionInfo("61", "cx", "Christmas Island")
            ),
            "+62" to listOf(RegionInfo("62", "id", "Indonesia")),
            "+63" to listOf(RegionInfo("63", "ph", "Philippines")),
            "+64" to listOf(RegionInfo("64", "nz", "New Zealand")),
            "+65" to listOf(RegionInfo("65", "sg", "Singapore")),
            "+66" to listOf(RegionInfo("66", "th", "Thailand")),
            "+81" to listOf(RegionInfo("81", "jp", "Japan")),
            "+82" to listOf(RegionInfo("82", "kr", "South Korea")),
            "+84" to listOf(RegionInfo("84", "vn", "Vietnam")),
            "+86" to listOf(RegionInfo("86", "cn", "China")),
            "+90" to listOf(RegionInfo("90", "tr", "Turkey")),
            "+91" to listOf(RegionInfo("91", "in", "India")),
            "+92" to listOf(RegionInfo("92", "pk", "Pakistan")),
            "+93" to listOf(RegionInfo("93", "af", "Afghanistan")),
            "+94" to listOf(RegionInfo("94", "lk", "Sri Lanka")),
            "+95" to listOf(RegionInfo("95", "mm", "Myanmar")),
            "+98" to listOf(RegionInfo("98", "ir", "Iran, Islamic Republic Of Iran")),
            "+211" to listOf(RegionInfo("211", "ss", "South Sudan")),
            "+212" to listOf(RegionInfo("212", "ma", "Morocco")),
            "+213" to listOf(RegionInfo("213", "dz", "Algeria")),
            "+216" to listOf(RegionInfo("216", "tn", "Tunisia")),
            "+218" to listOf(RegionInfo("218", "ly", "Libya")),
            "+220" to listOf(RegionInfo("220", "gm", "Gambia")),
            "+221" to listOf(RegionInfo("221", "sn", "Senegal")),
            "+222" to listOf(RegionInfo("222", "mr", "Mauritania")),
            "+223" to listOf(RegionInfo("223", "ml", "Mali")),
            "+224" to listOf(RegionInfo("224", "gn", "Guinea")),
            "+225" to listOf(RegionInfo("225", "ci", "Côte D'ivoire")),
            "+226" to listOf(RegionInfo("226", "bf", "Burkina Faso")),
            "+227" to listOf(RegionInfo("227", "ne", "Niger")),
            "+228" to listOf(RegionInfo("228", "tg", "Togo")),
            "+229" to listOf(RegionInfo("229", "bj", "Benin")),
            "+230" to listOf(RegionInfo("230", "mu", "Mauritius")),
            "+231" to listOf(RegionInfo("231", "lr", "Liberia")),
            "+232" to listOf(RegionInfo("232", "sl", "Sierra Leone")),
            "+234" to listOf(RegionInfo("234", "ng", "Nigeria")),
            "+233" to listOf(RegionInfo("233", "gh", "Ghana")),
            "+235" to listOf(RegionInfo("235", "td", "Chad")),
            "+236" to listOf(RegionInfo("236", "cf", "Central African Republic")),
            "+237" to listOf(RegionInfo("237", "cm", "Cameroon")),
            "+239" to listOf(RegionInfo("239", "st", "Sao Tome And Principe")),
            "+238" to listOf(RegionInfo("238", "cv", "Cape Verde")),
            "+240" to listOf(RegionInfo("240", "gq", "Equatorial Guinea")),
            "+241" to listOf(RegionInfo("241", "ga", "Gabon")),
            "+242" to listOf(RegionInfo("242", "cg", "Congo")),
            "+243" to listOf(RegionInfo("243", "cd", "The Democratic Republic Of The Congo")),
            "+244" to listOf(RegionInfo("244", "ao", "Angola")),
            "+245" to listOf(RegionInfo("245", "gw", "Guinea-bissau")),
            "+246" to listOf(RegionInfo("246", "io", "British Indian Ocean Territory")),
            "+248" to listOf(RegionInfo("248", "sc", "Seychelles")),
            "+249" to listOf(RegionInfo("249", "sd", "Sudan")),
            "+251" to listOf(RegionInfo("251", "et", "Ethiopia")),
            "+253" to listOf(RegionInfo("253", "dj", "Djibouti")),
            "+254" to listOf(RegionInfo("254", "ke", "Kenya")),
            "+255" to listOf(RegionInfo("255", "tz", "Tanzania, United Republic Of")),
            "+256" to listOf(RegionInfo("256", "ug", "Uganda")),
            "+257" to listOf(RegionInfo("257", "bi", "Burundi")),
            "+260" to listOf(RegionInfo("260", "zm", "Zambia")),
            "+261" to listOf(RegionInfo("261", "mg", "Madagascar")),
            "+262" to listOf(
                    RegionInfo("262", "yt", "Mayotte"),
                    RegionInfo("262", "re", "Réunion")
            ),
            "+263" to listOf(RegionInfo("263", "zw", "Zimbabwe")),
            "+264" to listOf(RegionInfo("264", "na", "Namibia")),
            "+265" to listOf(RegionInfo("265", "mw", "Malawi")),
            "+266" to listOf(RegionInfo("266", "ls", "Lesotho")),
            "+267" to listOf(RegionInfo("267", "bw", "Botswana")),
            "+268" to listOf(RegionInfo("268", "sz", "Swaziland")),
            "+269" to listOf(RegionInfo("269", "km", "Comoros")),
            "+290" to listOf(RegionInfo("290", "sh", "Saint Helena, Ascension And Tristan Da Cunha")),
            "+291" to listOf(RegionInfo("291", "er", "Eritrea")),
            "+297" to listOf(RegionInfo("297", "aw", "Aruba")),
            "+298" to listOf(RegionInfo("298", "fo", "Faroe Islands")),
            "+299" to listOf(RegionInfo("299", "gl", "Greenland")),
            "+350" to listOf(RegionInfo("350", "gi", "Gibraltar")),
            "+351" to listOf(RegionInfo("351", "pt", "Portugal")),
            "+352" to listOf(RegionInfo("352", "lu", "Luxembourg")),
            "+353" to listOf(RegionInfo("353", "ie", "Ireland")),
            "+354" to listOf(RegionInfo("354", "is", "Iceland")),
            "+355" to listOf(RegionInfo("355", "al", "Albania")),
            "+356" to listOf(RegionInfo("356", "mt", "Malta")),
            "+357" to listOf(RegionInfo("357", "cy", "Cyprus")),
            "+358" to listOf(
                    RegionInfo("358", "ax", "Aland Islands"),
                    RegionInfo("358", "fi", "Finland")
            ),
            "+359" to listOf(RegionInfo("359", "bg", "Bulgaria")),
            "+370" to listOf(RegionInfo("370", "lv", "Lithuania")),
            "+371" to listOf(RegionInfo("371", "lv", "Latvia")),
            "+373" to listOf(RegionInfo("373", "md", "Moldova")),
            "+377" to listOf(RegionInfo("377", "mc", "Monaco")),
            "+372" to listOf(RegionInfo("372", "ee", "Estonia")),
            "+374" to listOf(RegionInfo("374", "am", "Armenia")),
            "+375" to listOf(RegionInfo("375", "by", "Belarus")),
            "+376" to listOf(RegionInfo("376", "ad", "Andorra")),
            "+378" to listOf(RegionInfo("378", "sm", "San Marino")),
            "+379" to listOf(RegionInfo("379", "va", "Holy See (vatican City State)")),
            "+380" to listOf(RegionInfo("380", "ua", "Ukraine")),
            "+381" to listOf(RegionInfo("381", "rs", "Serbia")),
            "+382" to listOf(RegionInfo("382", "me", "Montenegro")),
            "+383" to listOf(RegionInfo("383", "xk", "Kosovo")),
            "+385" to listOf(RegionInfo("385", "hr", "Croatia")),
            "+386" to listOf(RegionInfo("386", "si", "Slovenia")),
            "+387" to listOf(RegionInfo("387", "ba", "Bosnia And Herzegovina")),
            "+389" to listOf(RegionInfo("389", "mk", "Macedonia (FYROM)")),
            "+420" to listOf(RegionInfo("420", "cz", "Czech Republic")),
            "+421" to listOf(RegionInfo("421", "sk", "Slovakia")),
            "+423" to listOf(RegionInfo("423", "li", "Liechtenstein")),
            "+450" to listOf(RegionInfo("450", "gp", "Guadeloupe")),
            "+500" to listOf(RegionInfo("500", "fk", "Falkland Islands (malvinas)")),
            "+501" to listOf(RegionInfo("501", "bz", "Belize")),
            "+502" to listOf(RegionInfo("502", "gt", "Guatemala")),
            "+503" to listOf(RegionInfo("503", "sv", "El Salvador")),
            "+504" to listOf(RegionInfo("504", "hn", "Honduras")),
            "+505" to listOf(RegionInfo("505", "ni", "Nicaragua")),
            "+506" to listOf(RegionInfo("506", "cr", "Costa Rica")),
            "+507" to listOf(RegionInfo("507", "pa", "Panama")),
            "+508" to listOf(RegionInfo("508", "pm", "Saint Pierre And Miquelon")),
            "+509" to listOf(RegionInfo("509", "ht", "Haiti")),
            "+590" to listOf(
                    RegionInfo("590", "bl", "Saint Barthélemy"),
                    RegionInfo("590", "mf", "Saint Martin")
            ),
            "+250" to listOf(RegionInfo("250", "rw", "Rwanda")),
            "+252" to listOf(RegionInfo("252", "so", "Somalia")),
            "+258" to listOf(RegionInfo("258", "mz", "Mozambique")),
            "+591" to listOf(RegionInfo("591", "bo", "Bolivia, Plurinational State Of")),
            "+592" to listOf(RegionInfo("592", "gy", "Guyana")),
            "+593" to listOf(RegionInfo("593", "ec", "Ecuador")),
            "+594" to listOf(RegionInfo("594", "gf", "French Guiana")),
            "+595" to listOf(RegionInfo("595", "py", "Paraguay")),
            "+596" to listOf(RegionInfo("596", "mq", "Martinique")),
            "+597" to listOf(RegionInfo("597", "sr", "Suriname")),
            "+598" to listOf(RegionInfo("598", "uy", "Uruguay")),
            "+599" to listOf(RegionInfo("599", "cw", "Curaçao")),
            "+670" to listOf(RegionInfo("670", "aq", "Timor-leste")),
            "+672" to listOf(
                    RegionInfo("672", "aq", "Antarctica"),
                    RegionInfo("672", "nf", "Norfolk Islands")
            ),
            "+673" to listOf(RegionInfo("673", "bn", "Brunei Darussalam")),
            "+674" to listOf(RegionInfo("674", "nr", "Nauru")),
            "+675" to listOf(RegionInfo("675", "pg", "Papua New Guinea")),
            "+676" to listOf(RegionInfo("676", "to", "Tonga")),
            "+677" to listOf(RegionInfo("677", "sb", "Solomon Islands")),
            "+678" to listOf(RegionInfo("678", "vu", "Vanuatu")),
            "+679" to listOf(RegionInfo("679", "fj", "Fiji")),
            "+680" to listOf(RegionInfo("680", "pw", "Palau")),
            "+681" to listOf(RegionInfo("681", "wf", "Wallis And Futuna")),
            "+682" to listOf(RegionInfo("682", "ck", "Cook Islands")),
            "+683" to listOf(RegionInfo("683", "nu", "Niue")),
            "+685" to listOf(RegionInfo("685", "ws", "Samoa")),
            "+686" to listOf(RegionInfo("686", "ki", "Kiribati")),
            "+687" to listOf(RegionInfo("687", "nc", "New Caledonia")),
            "+688" to listOf(RegionInfo("688", "tv", "Tuvalu")),
            "+689" to listOf(RegionInfo("689", "pf", "French Polynesia")),
            "+690" to listOf(RegionInfo("690", "tk", "Tokelau")),
            "+691" to listOf(RegionInfo("691", "fm", "Federated States Of Micronesia")),
            "+692" to listOf(RegionInfo("692", "mh", "Marshall Islands")),
            "+850" to listOf(RegionInfo("850", "kp", "North Korea")),
            "+852" to listOf(RegionInfo("852", "hk", "Hong Kong")),
            "+853" to listOf(RegionInfo("853", "mo", "Macau")),
            "+855" to listOf(RegionInfo("855", "kh", "Cambodia")),
            "+856" to listOf(RegionInfo("856", "la", "Lao People's Democratic Republic")),
            "+870" to listOf(RegionInfo("870", "pn", "Pitcairn Islands")),
            "+880" to listOf(RegionInfo("880", "bd", "Bangladesh")),
            "+886" to listOf(RegionInfo("886", "tw", "Taiwan")),
            "+960" to listOf(RegionInfo("960", "mv", "Maldives")),
            "+961" to listOf(RegionInfo("961", "lb", "Lebanon")),
            "+962" to listOf(RegionInfo("962", "jo", "Jordan")),
            "+964" to listOf(RegionInfo("964", "iq", "Iraq")),
            "+963" to listOf(RegionInfo("963", "sy", "Syrian Arab Republic")),
            "+965" to listOf(RegionInfo("965", "kw", "Kuwait")),
            "+966" to listOf(RegionInfo("966", "sa", "Saudi Arabia")),
            "+967" to listOf(RegionInfo("967", "ye", "Yemen")),
            "+968" to listOf(RegionInfo("968", "om", "Oman")),
            "+970" to listOf(RegionInfo("970", "ps", "Palestine")),
            "+971" to listOf(RegionInfo("971", "ae", "United Arab Emirates (UAE)")),
            "+972" to listOf(RegionInfo("972", "il", "Israel")),
            "+973" to listOf(RegionInfo("973", "bh", "Bahrain")),
            "+974" to listOf(RegionInfo("974", "qa", "Qatar")),
            "+975" to listOf(RegionInfo("975", "bt", "Bhutan")),
            "+976" to listOf(RegionInfo("976", "mn", "Mongolia")),
            "+977" to listOf(RegionInfo("977", "np", "Nepal")),
            "+992" to listOf(RegionInfo("992", "tj", "Tajikistan")),
            "+993" to listOf(RegionInfo("993", "tm", "Turkmenistan")),
            "+994" to listOf(RegionInfo("994", "az", "Azerbaijan")),
            "+995" to listOf(RegionInfo("995", "ge", "Georgia")),
            "+996" to listOf(RegionInfo("996", "kg", "Kyrgyzstan")),
            "+998" to listOf(RegionInfo("998", "uz", "Uzbekistan"))
    )
}