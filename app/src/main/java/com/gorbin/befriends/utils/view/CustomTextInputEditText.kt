package com.gorbin.befriends.utils.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.gorbin.befriends.R


class CustomTextInputEditText : TextInputEditText {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getBackground(): Drawable? {
        return ContextCompat.getDrawable(context, R.drawable.bg_edit_text)
    }
}