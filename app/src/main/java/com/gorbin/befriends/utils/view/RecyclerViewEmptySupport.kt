package com.gorbin.befriends.utils.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewEmptySupport : RecyclerView {
    private var emptyView: View? = null

    private val emptyObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            handleRecyclerViewUpdates()
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
            handleRecyclerViewUpdates()
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
            super.onItemRangeChanged(positionStart, itemCount)
            handleRecyclerViewUpdates()
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            handleRecyclerViewUpdates()
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            handleRecyclerViewUpdates()
        }

        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            handleRecyclerViewUpdates()
        }
    }

    private var minCount = 0

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)

        adapter?.registerAdapterDataObserver(emptyObserver)

        emptyObserver.onChanged()
    }

    fun setMinCount(count: Int) {
        minCount = count
    }

    fun setEmptyView(emptyView: View) {
        this.emptyView = emptyView
        handleRecyclerViewUpdates()
    }

    private fun handleRecyclerViewUpdates() {
        val adapter = adapter
        val emptyView = emptyView
        if (adapter != null && emptyView != null) {
            if (adapter.itemCount == minCount) {
                emptyView.visibility = View.VISIBLE
            } else {
                emptyView.visibility = View.GONE
            }
        }
    }
}

@BindingAdapter(value = ["empty_view"], requireAll = false)
fun setEmptyView(list: RecyclerViewEmptySupport, emptyView: View) {
    list.setEmptyView(emptyView)
}

@BindingAdapter("min_count")
fun setMinCount(list: RecyclerViewEmptySupport, minCount: Int) {
    list.setMinCount(minCount)
}