<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />
        <import type="com.gorbin.befriends.R" />

        <variable
            name="viewModel"
            type="com.gorbin.befriends.presentation.chat.ChatViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/colorBackground">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/interlocutor"
            android:layout_width="match_parent"
            android:layout_height="56dp"
            android:background="@color/colorDeepBlue"
            app:layout_constraintTop_toTopOf="parent">

            <ImageView
                android:id="@+id/back_button"
                android:layout_width="40dp"
                android:layout_height="40dp"
                android:layout_marginStart="@dimen/spacing_8"
                android:layout_marginTop="@dimen/spacing_8"
                android:padding="8dp"
                android:src="@drawable/ic_arrow_back_24"
                android:contentDescription="@null"
                android:onClick="@{()->viewModel.back()}"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"/>

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="0dp"
                android:layout_height="match_parent"
                android:layout_marginEnd="@dimen/spacing_16"
                android:onClick="@{()->viewModel.openProfile()}"
                app:layout_goneMarginEnd="0dp"
                app:layout_constraintStart_toEndOf="@id/back_button"
                app:layout_constraintEnd_toStartOf="@id/command_button">

                <ImageView
                    android:id="@+id/image"
                    android:layout_width="32dp"
                    android:layout_height="32dp"
                    android:layout_marginStart="8dp"
                    android:contentDescription="@null"
                    android:scaleType="centerCrop"
                    android:background="@drawable/avatar_placeholder"
                    app:picasso_placeholder="@{R.drawable.avatar_placeholder}"
                    app:picasso_uri="@{viewModel.image}"
                    app:picasso_circle_shape="@{true}"
                    app:layout_constraintTop_toTopOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintBottom_toBottomOf="parent" />

                <TextView
                    android:id="@+id/name"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="6dp"
                    android:layout_marginStart="@dimen/spacing_12"
                    android:layout_marginEnd="@dimen/spacing_12"
                    android:text="@{viewModel.name}"
                    android:textAppearance="@style/TextStyle.Bold"
                    android:singleLine="true"
                    android:ellipsize="end"
                    app:layout_constraintTop_toTopOf="parent"
                    app:layout_constraintStart_toEndOf="@id/image"
                    app:layout_constraintEnd_toEndOf="parent"
                    tools:text="Елена Набокова Набокова" />

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/rating_container"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="8dp"
                    android:visibility="@{viewModel.typing ? View.GONE : View.VISIBLE}"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintStart_toStartOf="@id/name"
                    app:layout_constraintEnd_toEndOf="@id/name">

                    <TextView
                        android:id="@+id/newbie"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:paddingTop="2dp"
                        android:paddingBottom="2dp"
                        android:paddingStart="6dp"
                        android:paddingEnd="6dp"
                        android:text="@string/newbie"
                        android:textAppearance="@style/TextStyle.Medium.12"
                        android:textColor="@color/colorBackground"
                        android:background="@drawable/bg_newbie"
                        android:visibility="@{viewModel.isNewbie ? View.VISIBLE : View.GONE}"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />

                    <ImageView
                        android:id="@+id/rating_icon"
                        android:layout_width="16dp"
                        android:layout_height="16dp"
                        android:layout_marginTop="2dp"
                        android:layout_marginStart="@dimen/spacing_16"
                        android:src="@drawable/ic_star_5"
                        android:contentDescription="@null"
                        app:layout_goneMarginStart="0dp"
                        app:layout_constraintTop_toTopOf="parent"
                        app:layout_constraintStart_toEndOf="@id/newbie" />

                    <TextView
                        android:id="@+id/rating_value"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="6dp"
                        android:text="@{viewModel.rating}"
                        android:textAppearance="@style/TextStyle.Bold"
                        android:textColor="@color/colorRed"
                        android:visibility="@{viewModel.isNewbie ? View.GONE : View.VISIBLE}"
                        app:layout_constraintTop_toTopOf="@id/rating_icon"
                        app:layout_constraintBottom_toBottomOf="@id/rating_icon"
                        app:layout_constraintStart_toEndOf="@id/rating_icon"
                        tools:text="10.0" />
                </androidx.constraintlayout.widget.ConstraintLayout>

                <TextView
                    android:id="@+id/typing_status"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="8dp"
                    android:text="@string/typing"
                    android:textAppearance="@style/TextStyle.14"
                    android:textColor="@color/colorLightGray"
                    android:visibility="@{viewModel.typing ? View.VISIBLE : View.GONE}"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintStart_toStartOf="@id/name" />
            </androidx.constraintlayout.widget.ConstraintLayout>

            <androidx.constraintlayout.widget.Guideline
                android:id="@+id/vertical_guideline"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                app:layout_constraintGuide_percent="0.6" />

            <Button
                android:id="@+id/command_button"
                android:layout_width="wrap_content"
                android:layout_height="24dp"
                android:layout_marginEnd="@dimen/spacing_16"
                android:paddingStart="@dimen/spacing_8"
                android:paddingEnd="@dimen/spacing_8"
                android:paddingTop="0dp"
                android:paddingBottom="0dp"
                android:visibility="@{viewModel.commandOptionAvailable ? View.VISIBLE : View.GONE}"
                android:background="@{viewModel.commandButtonBackground}"
                android:stateListAnimator="@null"
                android:text="@{viewModel.commandAction}"
                android:textAppearance="@style/CommandButtonTextStyle"
                android:singleLine="true"
                android:ellipsize="end"
                android:onClick="@{()->viewModel.onCommandButtonClick()}"
                app:layout_constrainedWidth="true"
                app:layout_constraintHorizontal_bias="1"
                app:layout_constraintStart_toEndOf="@id/vertical_guideline"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                tools:text="Предложить заказ"
                tools:background="@drawable/bg_button" />
        </androidx.constraintlayout.widget.ConstraintLayout>

        <com.stfalcon.chatkit.messages.MessagesList
            android:id="@+id/messagesList"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:paddingTop="@dimen/spacing_8"
            android:paddingBottom="@dimen/spacing_8"
            android:clipToPadding="false"
            android:foregroundGravity="bottom"
            android:overScrollMode="never"
            app:layout_constraintTop_toBottomOf="@id/interlocutor"
            app:layout_constraintBottom_toTopOf="@id/input"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:dateHeaderTextColor="@color/colorLightGray"
            app:incomingTextColor="@color/colorLightGray"
            app:incomingTimeTextColor="@color/colorLightGray_80"
            app:incomingDefaultBubbleColor="@color/colorGray"
            app:incomingDefaultBubblePressedColor="@color/colorGray"
            app:outcomingTextColor="@color/colorLightGray"
            app:outcomingTimeTextColor="@color/colorLightGray_80"
            app:outcomingDefaultBubbleColor="@color/colorBlue"
            app:outcomingDefaultBubblePressedColor="@color/colorBlue"
            app:textAutoLink="all"
            app:incomingBubblePaddingLeft="@dimen/spacing_16"
            app:incomingBubblePaddingRight="@dimen/spacing_16"
            app:incomingBubblePaddingBottom="@dimen/spacing_8"
            app:incomingBubblePaddingTop="@dimen/spacing_8"
            app:outcomingBubblePaddingLeft="@dimen/spacing_16"
            app:outcomingBubblePaddingRight="@dimen/spacing_16"
            app:outcomingBubblePaddingTop="@dimen/spacing_8"
            app:outcomingBubblePaddingBottom="@dimen/spacing_8" />

        <com.stfalcon.chatkit.messages.MessageInput
            android:id="@+id/input"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:padding="16dp"
            android:background="@color/colorDeepBlue"
            android:visibility="@{viewModel.chatAvailable ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:attachmentButtonBackground="@drawable/bg_circle_white_10"
            app:attachmentButtonIcon="@drawable/ic_plus"
            app:attachmentButtonWidth="32dp"
            app:attachmentButtonHeight="32dp"
            app:translateButtonBackground="@drawable/bg_circle_white_10"
            app:translateButtonIcon="@drawable/ic_translate_24"
            app:translateButtonWidth="32dp"
            app:translateButtonHeight="32dp"
            app:inputButtonBackground="@color/transparent"
            app:inputButtonIcon="@drawable/ic_send_24"
            app:inputButtonWidth="32dp"
            app:inputButtonHeight="32dp"
            app:inputTextColor="@color/colorLightGray"
            app:inputHintColor="@color/colorLightGray_50"
            app:inputHint="@string/chat_hint"
            app:inputMaxLength="1000"
            app:inputMaxLines="10"
            app:inputBackground="@drawable/bg_gray_block_alt_8"
            app:messageInput_attachments_listener="@{()->viewModel.onAddAttachments()}"
            app:messageInput_input_listener="@{(input)->viewModel.onSubmit(input)}"
            app:messageInput_typeface="@{R.font.roboto_regular}"
            app:messageInput_typing_listener="@{viewModel.getTypingListener()}"
            app:messageInput_translate_listener="@{()->viewModel.onTranslateClick()}"
            app:showAttachmentButton="true" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>