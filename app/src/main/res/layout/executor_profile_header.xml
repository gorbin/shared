<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.gorbin.befriends.presentation.executorProfile.ExecutorProfileViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@color/colorPrimaryDark">

        <androidx.constraintlayout.widget.Guideline
            android:id="@+id/guideline_half_vertical"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            app:layout_constraintGuide_percent="0.5" />

        <androidx.viewpager.widget.ViewPager
            android:id="@+id/pager"
            android:layout_width="0dp"
            android:layout_height="200dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:pager_images="@{viewModel.gallery}"
            app:pager_indicator="@{indicator}"
            tools:background="@color/colorGray" />

        <ImageView
            android:id="@+id/pager_bg"
            android:layout_width="0dp"
            android:layout_height="100dp"
            android:src="@drawable/ic_camera"
            android:visibility="@{viewModel.showPlaceholder?View.VISIBLE:View.GONE}"
            app:layout_constraintBottom_toBottomOf="@+id/pager"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />


        <com.google.android.material.tabs.TabLayout
            android:id="@+id/indicator"
            android:layout_width="wrap_content"
            android:layout_height="16dp"
            android:layout_marginEnd="@dimen/spacing_16"
            android:layout_marginBottom="@dimen/spacing_16"
            app:layout_constraintBottom_toBottomOf="@id/pager"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:tabBackground="@drawable/tab_dot_selector"
            app:tabGravity="center"
            app:tabIndicatorHeight="0dp"
            app:tabPadding="@dimen/spacing_4"
            tools:background="@color/white"
            tools:layout_height="15dp"
            tools:layout_width="100dp" />

        <ImageView
            android:id="@+id/back"
            android:layout_width="40dp"
            android:layout_height="40dp"
            android:layout_marginStart="@dimen/spacing_8"
            android:layout_marginTop="@dimen/spacing_32"
            android:contentDescription="@null"
            android:onClick="@{()->viewModel.back()}"
            android:padding="@dimen/spacing_8"
            android:src="@drawable/ic_arrow_back"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/title"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/spacing_16"
            android:layout_marginTop="@dimen/spacing_16"
            android:layout_marginEnd="@dimen/spacing_16"
            android:lineSpacingExtra="-6dp"
            android:text="@{viewModel.name}"
            android:textAppearance="@style/TextStyle.Black.26"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/pager"
            tools:text="Ольга Воронец" />

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/rating_container"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_16"
            app:layout_constraintStart_toStartOf="@id/title"
            app:layout_constraintTop_toBottomOf="@id/title">

            <TextView
                android:id="@+id/rating_title"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:alpha="0.6"
                android:text="@string/rating"
                android:textAppearance="@style/TextStyle.14"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <ImageView
                android:id="@+id/rating_image"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="4dp"
                android:contentDescription="@null"
                android:src="@drawable/ic_star_5"
                android:visibility="@{viewModel.isNewbie ? View.GONE : View.VISIBLE}"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/rating_title" />

            <TextView
                android:id="@+id/rating"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="6dp"
                android:text="@{viewModel.rating}"
                android:textAppearance="@style/TextStyle.Bold.16"
                android:textColor="@android:color/white"
                android:visibility="@{viewModel.isNewbie ? View.GONE : View.VISIBLE}"
                app:layout_constraintStart_toEndOf="@id/rating_image"
                app:layout_constraintTop_toBottomOf="@id/rating_title"
                tools:text="0.0" />

            <TextView
                android:id="@+id/newbie"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:background="@drawable/bg_newbie"
                android:paddingStart="6dp"
                android:paddingTop="2dp"
                android:paddingEnd="6dp"
                android:paddingBottom="2dp"
                android:text="@string/newbie"
                android:textAppearance="@style/TextStyle.Medium.12"
                android:textColor="@color/colorBackground"
                android:visibility="@{viewModel.isNewbie ? View.VISIBLE : View.GONE}"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/rating_title" />
        </androidx.constraintlayout.widget.ConstraintLayout>

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/spacing_24"
            android:layout_marginTop="@dimen/spacing_16"
            app:layout_constraintStart_toEndOf="@id/rating_container"
            app:layout_constraintTop_toBottomOf="@id/title">

            <TextView
                android:id="@+id/consult_title"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:alpha="0.6"
                android:text="@string/consultation_short"
                android:textAppearance="@style/TextStyle.14"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <TextView
                android:id="@+id/consult"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@{viewModel.cost}"
                android:textAppearance="@style/TextStyle.Bold.16"
                android:textColor="@color/colorGreen"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@id/consult_title"
                tools:text="$ 1000" />
        </androidx.constraintlayout.widget.ConstraintLayout>

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginEnd="@dimen/spacing_8"
            android:onClick="@{()->viewModel.complain()}"
            android:visibility="@{viewModel.isAuthorized() ? View.VISIBLE : View.GONE}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent">

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/spacing_8"
                android:text="@string/complaint"
                android:textAppearance="@style/TextStyle.14"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toStartOf="@id/complaint"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <ImageView
                android:id="@+id/complaint"
                android:layout_width="40dp"
                android:layout_height="40dp"
                android:contentDescription="@null"
                android:padding="@dimen/spacing_8"
                android:src="@drawable/ic_list"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toTopOf="parent" />
        </androidx.constraintlayout.widget.ConstraintLayout>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>