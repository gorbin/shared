package com.stfalcon.chatkit.commons.models;


import androidx.annotation.Nullable;

/**
 * Default media type for image message.
 */
public interface Image extends IMessage {
    @Nullable
    String getImageUrl();

    Boolean needScroll();
}
