package com.stfalcon.chatkit.commons.models;

public interface Location extends IMessage {
    Double getLatitude();
    Double getLongitude();
}
