package com.stfalcon.chatkit.commons.models;


public interface Progress extends IMessage {
    int getProgress();
}
